// @flow
import React from 'react';
import layoutStyles from '../app/app.scss';
import SeriesFormContainer from '../../containers/SeriesForm/SeriesFormContainer';

const CreateSeriesPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SeriesFormContainer />
    </div>
  </div>
);

export default CreateSeriesPage;
