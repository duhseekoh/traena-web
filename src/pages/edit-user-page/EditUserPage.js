// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import EditUserForm from '../../containers/UserForm/EditUserForm';

const EditUserPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <EditUserForm userId={props.match.params.userId} />
    </div>
  </div>
);

export default EditUserPage;
