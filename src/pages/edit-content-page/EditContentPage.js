// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import ContentFormContainer from '../../containers/ContentForm/ContentFormContainer';

const EditContentPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <ContentFormContainer
        contentId={parseInt(props.match.params.contentId, 10)}
      />
    </div>
  </div>
);

export default EditContentPage;
