// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import ContentContainer from '../../containers/Content/ContentContainer';

const ViewContentPage = (props: ContextRouter) => (
  <div>
    <div>
      <ContentContainer
        contentId={parseInt(props.match.params.contentId, 10)}
      />
    </div>
  </div>
);

export default ViewContentPage;
