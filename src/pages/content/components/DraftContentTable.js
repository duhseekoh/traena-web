// @flow
import React from 'react';
import { Icon, Table } from 'semantic-ui-react';
import TableRow from '../../../components/Table/TableRow';
import styles from './ContentTable.scss';
import ContentTableRow from './DraftContentTableRow';
import Pagination from '../../../components/Pagination/Pagination';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';

import type { Pagination as PaginationType } from '../../../types';
import type { ContentItem } from '../../../types/content';
import type { SortDirections } from '../../../utils/redux-sort/types';

type Props = {|
  contents: ContentItem[],
  pagination: PaginationType,
  goToContentsPage: (page: number) => any,
  onPressDeleteItem: (contentId: number) => any,
  onPressTableRow: (contentId: number) => any,
  getSortDirection: (property: string) => SortDirections,
  sortTable: (property: string) => any,
|};

/**
 * Management table to display posts that are filtered to exclusively 'Drafts'
 */
const DraftContentTable = (props: Props) => {
  if (!props.contents.length) {
    return (
      <h2 className={styles.contentTableNoContentMessage}>
        No posts have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.contentTable}>
        <Table.Header>
          <TableRow>
            <TableHeaderCell {...props} property="body.type">
              Type
            </TableHeaderCell>
            <TableHeaderCell {...props} property="author.firstName">
              Author
            </TableHeaderCell>
            <TableHeaderCell {...props} property="channel.name">
              Channel
            </TableHeaderCell>
            <TableHeaderCell {...props} property="body.title">
              Title
            </TableHeaderCell>
            <TableHeaderCell title="tags">
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell
              className={styles.alignRight}
              {...props}
              property="modifiedTimestamp"
            >
              Last Edited
            </TableHeaderCell>
            <th />
          </TableRow>
        </Table.Header>
        <Table.Body>
          {props.contents.map(content => (
            <ContentTableRow
              key={content.id}
              content={content}
              onPressDeleteItem={props.onPressDeleteItem}
              onPressTableRow={props.onPressTableRow}
            />
          ))}
        </Table.Body>
      </Table>
      <Pagination
        pagination={props.pagination}
        goToPage={props.goToContentsPage}
      />
    </div>
  );
};

DraftContentTable.defaultProps = {
  contents: [],
};

export default DraftContentTable;
