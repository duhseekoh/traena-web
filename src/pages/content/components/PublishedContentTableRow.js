/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import moment from 'moment';
import classnames from 'classnames';
import copy from 'copy-to-clipboard';
import { Table, Dropdown, Popup } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import TableMenuTriggerIcon from '../../../components/TableMenuTriggerIcon/TableMenuTriggerIcon';
import { getRouteLink, routes } from '../../../constants/routes';
import type { ContentItem } from '../../../types/content';
import DateShort from '../../../components/Date/DateShort';
import { generateContentURL } from '../../../utils/linkUtils';
import styles from './ContentTable.scss';
import TableRow from '../../../components/Table/TableRow';
import AccessControl from '../../../components/AccessControl/AccessControl';
import * as permissionUtils from '../../../utils/permissionUtils';

type Props = {|
  content: ContentItem,
  onPressDeleteItem: (contentId: number) => any,
  userOrganizationId: number,
  onPressTableRow: (contentId: number) => any,
|};

const contentTypesToDisplayNames = {
  DailyAction: 'Text',
  TrainingVideo: 'Video',
  Image: 'Image',
};

const PublishedContentTableRow = (props: Props) => {
  const { content, userOrganizationId, onPressTableRow } = props;
  // TODO: wrap delete with component that verifies access rights
  // to decide if it should be displayed
  // https://panderaappdev.atlassian.net/browse/TRN-456
  const isOwnOrg = content.author.organization.id === userOrganizationId;
  const allowDelete = isOwnOrg;
  const displayOrgName = !isOwnOrg; // org name is implied for org own org content in table

  return (
    <TableRow
      key={content.id}
      onClick={() => onPressTableRow(content.id)}
    >
      <Table.Cell>{contentTypesToDisplayNames[content.body.type]}</Table.Cell>
      <Table.Cell>{`${content.author.firstName} ${content.author.lastName}`}</Table.Cell>
      <Table.Cell>
        <div>{content.channel.name}</div>
        {displayOrgName && (
          <div className={styles.subText}>
            {content.author.organization.name}
          </div>
        )}
      </Table.Cell>
      <Table.Cell>{content.body.title}</Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.tags.length}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.likeCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.commentCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.completedCount}
      </Table.Cell>
      <Table.Cell
        className={
          !content.publishedTimestamp
            ? classnames(styles.fadedCell, styles.alignRight)
            : styles.alignRight
        }
      >
        {content.publishedTimestamp ? (
          <Popup
            trigger={<div><DateShort date={content.publishedTimestamp} /></div>}
            content={`${moment().diff(moment(content.publishedTimestamp), 'days')} days live`}
            basic
            on="hover"
            position="bottom center"
          />
        ) : (
          'None'
        )}
      </Table.Cell>
      <Table.Cell>
        <Dropdown
          fluid
          icon={null}
          trigger={<TableMenuTriggerIcon />}
        >
          <Dropdown.Menu className={classnames(styles.dropdown, 'left')}>
            {allowDelete && (
              <Dropdown.Item
                text="Delete"
                onClick={() => props.onPressDeleteItem(content.id)}
              />
            )}
            <AccessControl
              customAccessData={{
                authorId: content.author.id,
              }}
              accessCheck={permissionUtils.canModifyContent}
              noAccessView={null}
            >
              <Dropdown.Item
                as={Link}
                to={
                  getRouteLink(routes.CONTENT_EDIT, { contentId: content.id })
                }
              >
                Edit
              </Dropdown.Item>
            </AccessControl>
            <Dropdown.Item
              as={Link}
              target="_blank"
              to={getRouteLink(
                routes.CONTENT_VIEW,
                { contentId: content.id })
              }
            >
              View
            </Dropdown.Item>
            <Dropdown.Item
              as={Link}
              to={getRouteLink(routes.CONTENT_STATS, {
                contentId: content.id,
              })}
            >
              Stats
            </Dropdown.Item>
            <Dropdown.Item onClick={() => copy(generateContentURL(content.id))}>
              <a
                href={generateContentURL(content.id)}
                onClick={e => e.preventDefault()}
              >
                Copy Shareable Link
              </a>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Table.Cell>
    </TableRow>
  );
};

export default PublishedContentTableRow;
