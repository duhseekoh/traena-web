// @flow
import React from 'react';
import { Table, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import TableMenuTriggerIcon from '../../../components/TableMenuTriggerIcon/TableMenuTriggerIcon';
import { getRouteLink, routes } from '../../../constants/routes';
import type { ContentItem } from '../../../types/content';
import DateShort from '../../../components/Date/DateShort';
import TimeShort from '../../../components/Date/TimeShort';
import styles from './ContentTable.scss';
import TableRow from '../../../components/Table/TableRow';

type Props = {|
  content: ContentItem,
  onPressDeleteItem: (contentId: number) => any,
  onPressTableRow: (contentId: number) => any,
|};

const contentTypesToDisplayNames = {
  DailyAction: 'Text',
  TrainingVideo: 'Video',
  Image: 'Image',
};

const DraftContentTableRow = (props: Props) => {
  // eslint-disable-line
  const { content, onPressTableRow } = props;
  return (
    <TableRow
      key={content.id}
      onClick={() => onPressTableRow(content.id)}
    >
      <Table.Cell>{contentTypesToDisplayNames[content.body.type]}</Table.Cell>
      <Table.Cell>{`${content.author.firstName} ${content.author.lastName}`}</Table.Cell>
      <Table.Cell>
        <div>{content.channel.name}</div>
      </Table.Cell>
      <Table.Cell>{content.body.title}</Table.Cell>
      <Table.Cell>{content.tags.length}</Table.Cell>
      <Table.Cell className={styles.alignRight}>
        <DateShort date={content.modifiedTimestamp} />
        <div className={styles.subText}>
          <TimeShort date={content.modifiedTimestamp} />
        </div>
      </Table.Cell>
      <Table.Cell>
        <Dropdown
          fluid
          icon={null}
          trigger={<TableMenuTriggerIcon />}
        >
          <Dropdown.Menu className="left">
            <Dropdown.Item
              text="Delete"
              onClick={() => props.onPressDeleteItem(content.id)}
            />
            <Dropdown.Item
              as={Link}
              to={getRouteLink(routes.CONTENT_EDIT, { contentId: content.id })}
            >
              Edit
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Table.Cell>
    </TableRow>
  );
};

export default DraftContentTableRow;
