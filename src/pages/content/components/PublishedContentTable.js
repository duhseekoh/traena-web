// @flow
import React from 'react';
import { Icon, Table } from 'semantic-ui-react';
import styles from './ContentTable.scss';
import ContentTableRow from './PublishedContentTableRow';
import Pagination from '../../../components/Pagination/Pagination';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';

import type { Pagination as PaginationType } from '../../../types';
import type { ContentItem } from '../../../types/content';
import type { SortDirections } from '../../../utils/redux-sort/types';

type Props = {|
  contents: ContentItem[],
  pagination: PaginationType,
  goToContentsPage: (page: number) => any,
  userOrganizationId: number,
  onPressDeleteItem: (contentId: number) => any,
  onPressTableRow: (contentId: number) => any,
  sortTable: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
|};

/**
 * Management table to display posts that are filtered to exclusively 'Published'
 */
const PublishedContentTable = (props: Props) => {
  const { sortTable, getSortDirection } = props;
  const headerCellProps = { sortTable, getSortDirection };

  if (!props.contents.length) {
    return (
      <h2 className={styles.contentTableNoContentMessage}>
        No posts have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.contentTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell {...headerCellProps} property="body.type">
              Type
            </TableHeaderCell>
            <TableHeaderCell {...headerCellProps} property="author.firstName">
              Author
            </TableHeaderCell>
            <TableHeaderCell {...headerCellProps} property="channel.name">
              Channel
            </TableHeaderCell>
            <TableHeaderCell {...headerCellProps} property="body.title">
              Title
            </TableHeaderCell>
            {/* TODO make sure this does not appear sortable */}
            <TableHeaderCell
              title="tags"
              className={styles.alignRight}
              {...headerCellProps}
            >
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell
              title="likes"
              className={styles.alignRight}
              {...headerCellProps}
              property="likeCount"
            >
              <Icon name="empty heart" />
            </TableHeaderCell>
            <TableHeaderCell
              title="comments"
              className={styles.alignRight}
              {...headerCellProps}
              property="commentCount"
            >
              <Icon name="comment outline" />
            </TableHeaderCell>
            <TableHeaderCell
              title="completed"
              className={styles.alignRight}
              {...headerCellProps}
              property="completedCount"
            >
              <Icon name="check circle outline" />
            </TableHeaderCell>
            <TableHeaderCell
              className={styles.alignRight}
              {...headerCellProps}
              property="publishedTimestamp"
            >
              Published
            </TableHeaderCell>
            <TableHeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.contents.map(content => (
            <ContentTableRow
              key={content.id}
              content={content}
              onPressDeleteItem={props.onPressDeleteItem}
              userOrganizationId={props.userOrganizationId}
              onPressTableRow={props.onPressTableRow}
            />
          ))}
        </Table.Body>
      </Table>
      {props.pagination && (
        <Pagination
          pagination={props.pagination}
          goToPage={props.goToContentsPage}
        />
      )}
    </div>
  );
};

PublishedContentTable.defaultProps = {
  contents: [],
};

export default PublishedContentTable;
