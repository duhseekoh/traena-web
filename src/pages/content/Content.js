/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { Grid, Loader, Dropdown, Button, Input } from 'semantic-ui-react';
import classnames from 'classnames';
import { deleteContentItem } from '../../redux/modules/content/actions';
import {
  fetchContentsTable,
  setContentsPage,
  setContentsIncludeFilter,
  setContentsStatusFilter,
  setContentTableSort,
  setSearchText,
} from '../../redux/modules/ui-content-table/actions';
import * as fromAuth from '../../redux/modules/auth/selectors';
import * as fromUiContentTable from '../../redux/modules/ui-content-table/selectors';
import styles from './Content.scss';
import layoutStyles from '../app/app.scss';
import DraftContentTable from './components/DraftContentTable';
import PublishedContentTable from './components/PublishedContentTable';
import tabStyles from '../../components/TabButtonGroup/TabButtonGroup.scss';
import { routes, getRouteLink } from '../../constants/routes';
import type { ContentItem } from '../../types/content';
import type { SortDirections } from '../../utils/redux-sort/types';

import {
  CONTENT_STATUSES,
  CONTENT_INCLUDE_SOURCES,
} from '../../constants/appConstants';

import { PERMISSIONS } from '../../constants/permissions';
import AccessControl from '../../components/AccessControl/AccessControl';

type Props = {
  fetchContentsTable: () => Promise<any>,
  setContentsPage: (page: number) => any,
  setContentsIncludeFilter: (include: string) => any,
  contents: ContentItem[],
  pagination: *,
  contentFilters: *,
  isContentTableLoading: boolean,
  deleteItem: (contentId: number) => any,
  userOrganizationId: number,
  setContentsStatusFilter: (status: string) => any,
  handleTableRowClick: (url: string) => any,
  getSortDirection: (property: string) => SortDirections,
  setContentTableSort: (property: string) => any,
  setSearchText: (searchText: string) => any,
};

class Posts extends Component<Props> {
  props: Props;

  static defaultProps;

  componentDidMount() {
    this.props.fetchContentsTable();
  }

  goToContentsPage = (page: number) => {
    this.props.setContentsPage(page);
    this.props.fetchContentsTable();
  }

  includeContentFromSource = include => {
    this.props.setContentsIncludeFilter(include);
    this.props.setContentsPage(0);
    this.props.fetchContentsTable();
  }

  handleToggleChange = (e, data) => {
    this.props.setContentsStatusFilter(data.value);
    this.props.setContentsPage(0);
    this.props.fetchContentsTable();
  }

  sortTable = (property: string) => {
    this.props.setContentTableSort(property);
    this.props.fetchContentsTable();
  }

  handleTableRowClick = (contentId: number) => {
    this.props.handleTableRowClick(
      getRouteLink(routes.CONTENT_DETAIL, { contentId }),
    );
  };

  search = _.throttle((searchText: string) => {
    this.props.setSearchText(searchText);
    this.props.fetchContentsTable();
  }, 800);

  /**
   * Display loading, or drafts table, published table.
   */
  renderTable = () => {
    const {
      isContentTableLoading,
      contentFilters,
      getSortDirection,
    } = this.props;

    const {
      status: contentStatusFilter,
      include: contentIncludeFilter,
    } = contentFilters;

    if (isContentTableLoading) {
      return <Loader active />;
    }

    // Only show the Drafts view when looking at 'My Posts' and 'Drafts' is
    // selected.
    if (
      contentIncludeFilter === CONTENT_INCLUDE_SOURCES.SELF &&
      contentStatusFilter === CONTENT_STATUSES.DRAFT
    ) {
      return (
        <DraftContentTable
          goToContentsPage={this.goToContentsPage}
          contents={this.props.contents}
          pagination={this.props.pagination}
          onPressDeleteItem={this.props.deleteItem}
          onPressTableRow={this.handleTableRowClick}
          sortTable={this.sortTable}
          getSortDirection={getSortDirection}
        />
      );
    }

    // Otherwise, the published view can be displayed for 'My Posts' published
    // and 'Internal' and 'External'
    return (
      <PublishedContentTable
        goToContentsPage={this.goToContentsPage}
        contents={this.props.contents}
        pagination={this.props.pagination}
        onPressDeleteItem={this.props.deleteItem}
        userOrganizationId={this.props.userOrganizationId}
        onPressTableRow={this.handleTableRowClick}
        sortTable={this.sortTable}
        getSortDirection={getSortDirection}
      />
    );
  };

  render() {
    const postStatusOptions = [
      { text: 'Drafts', value: CONTENT_STATUSES.DRAFT },
      { text: 'Published', value: CONTENT_STATUSES.PUBLISHED },
    ];
    const activeTabStyle = classnames(tabStyles.tabButton, tabStyles.active);
    return (
      <div
        className={classnames(
          layoutStyles.primaryPageContainer,
          styles.postsPageContainer,
        )}
      >
        <Grid stackable>
          {/* Title and Create button */}
          <Grid.Row verticalAlign="bottom">
            <Grid.Column width={8} left>
              <h1>Posts</h1>
            </Grid.Column>
            <Grid.Column width={8} textAlign="right">
              <AccessControl
                allowedPermissions={[PERMISSIONS.CONTENT.MODIFY.SELF]}
                noAccessView={<span />}
              >
                <Link to={getRouteLink(routes.CONTENT_CREATE)}>
                  <Button size="big" primary>Create New</Button>
                </Link>
              </AccessControl>
            </Grid.Column>
          </Grid.Row>
          {/* Filters */}
          <Grid.Row verticalAlign="middle">
            <Grid.Column width={11}>
              <button
                className={
                  this.props.contentFilters.include ===
                  CONTENT_INCLUDE_SOURCES.SELF
                    ? activeTabStyle
                    : tabStyles.tabButton
                }
                onClick={() => {
                  this.includeContentFromSource(CONTENT_INCLUDE_SOURCES.SELF);
                }}
              >
                My Posts
              </button>
              <AccessControl
                allowedPermissions={[PERMISSIONS.CONTENT.MODIFY.ORG]}
                noAccessView={<span />}
              >
                <button
                  className={
                    this.props.contentFilters.include ===
                    CONTENT_INCLUDE_SOURCES.INTERNAL
                      ? activeTabStyle
                      : tabStyles.tabButton
                  }
                  onClick={() => {
                    this.includeContentFromSource(
                      CONTENT_INCLUDE_SOURCES.INTERNAL,
                    );
                  }}
                >
                  Internal
                </button>
                <button
                  className={
                    this.props.contentFilters.include ===
                    CONTENT_INCLUDE_SOURCES.EXTERNAL
                      ? activeTabStyle
                      : tabStyles.tabButton
                  }
                  onClick={() => {
                    this.includeContentFromSource(
                      CONTENT_INCLUDE_SOURCES.EXTERNAL,
                    );
                  }}
                >
                  External
                </button>
              </AccessControl>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>
              <Input placeholder="Search..." onChange={(e, data) => this.search(data.value)} />
            </Grid.Column>
            <Grid.Column width={5}>
              {this.props.contentFilters.include === 'self' && (
                <Dropdown
                  defaultValue={this.props.contentFilters.status}
                  selection
                  options={postStatusOptions}
                  onChange={this.handleToggleChange}
                />
              )}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row stretched>
            <Grid.Column>{this.renderTable()}</Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

Posts.defaultProps = {
  pagination: {},
  contents: [],
};

const mapStateToProps = state => ({
  pagination: state.uiContentTable.pagination,
  contents: fromUiContentTable.selectContents(state),
  contentFilters: fromUiContentTable.selectCurrentContentFilters(state),
  isContentTableLoading: fromUiContentTable.selectIsContentTableLoading(state),
  userOrganizationId: fromAuth.selectOrganizationId(state),
  getSortDirection: property =>
    fromUiContentTable.getSortDirectionLongName(state, property),
});

const mapDispatchToProps = dispatch => ({
  fetchContentsTable: () => dispatch(fetchContentsTable()),
  setContentsPage: page => dispatch(setContentsPage(page)),
  setSearchText: searchText => dispatch(setSearchText(searchText)),
  setContentsIncludeFilter: include =>
    dispatch(setContentsIncludeFilter(include)),
  deleteItem: item => dispatch(deleteContentItem(item)),
  setContentsStatusFilter: statuses =>
    dispatch(setContentsStatusFilter(statuses)),
  handleTableRowClick: url => dispatch(push(url)),
  setContentTableSort: property =>
    dispatch(setContentTableSort(property)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
