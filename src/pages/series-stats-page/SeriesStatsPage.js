// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import SeriesStatsContainer from '../../containers/SeriesStats/SeriesStatsContainer';

const SeriesStatsPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SeriesStatsContainer
        seriesId={Number(props.match.params.seriesId)}
      />
    </div>
  </div>
);

export default SeriesStatsPage;
