import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import ActivityFormContainer from '../../containers/ActivityForm/ActivityFormContainer';

const AddActivityPage = (props: ContextRouter) => (
  <div className={layoutStyles.primaryPageContainer}>
    <ActivityFormContainer
      activityType={props.match.params.activityType}
      contentId={Number(props.match.params.contentId)}
    />
  </div>
);

export default AddActivityPage;
