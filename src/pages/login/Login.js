// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showLogin } from '../../redux/modules/auth/actions';

type Props = {
  showLogin: () => Promise<*>,
};

class Login extends Component<Props> {
  componentWillMount() {
    this.props.showLogin();
  }

  render() {
    return <div />;
  }
}

const mapDispatchToProps = dispatch => ({
  showLogin: () => dispatch(showLogin()),
});

export default connect(null, mapDispatchToProps)(Login);
