// @flow
import React from 'react';
import { withRouter } from 'react-router-dom';
import Notifications from '../../components/Notifications/Notifications';
import FixedFooterViewport from '../../components/ui-fixed-footer/FixedFooterViewport';

import styles from './app.scss';

type Props = {
  children: React.DOM.Node,
};

const App = (props: Props) => (
  <FixedFooterViewport>
    <div className={styles.main}>
      <div>{props.children}</div>
      <Notifications />
    </div>
  </FixedFooterViewport>
);

export default withRouter(App);
