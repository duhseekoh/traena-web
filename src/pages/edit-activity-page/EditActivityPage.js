import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import ActivityFormContainer from '../../containers/ActivityForm/ActivityFormContainer';

const EditActivityPage = (props: ContextRouter) => (
  <div className={layoutStyles.primaryPageContainer}>
    <ActivityFormContainer
      activityId={Number(props.match.params.activityId)}
      activityType={props.match.params.activityType}
      contentId={Number(props.match.params.contentId)}
    />
  </div>
);

export default EditActivityPage;
