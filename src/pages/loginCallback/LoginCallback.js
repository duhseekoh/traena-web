/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import {
  handleAuthentication,
  clearCallbackRedirect,
} from '../../redux/modules/auth/actions';
import * as fromAuth from '../../redux/modules/auth/selectors';
import * as localstore from '../../utils/localstore';

import { routes, getRouteLink } from '../../constants/routes';

type Props = {
  handleAuthentication: () => Promise<*>,
  isAuthenticated: boolean,
  clearCallbackRedirect: () => Promise<any>,
};

type State = {
  hasErrorLoggingIn: boolean,
};

class LoginCallback extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      hasErrorLoggingIn: false,
    };
  }

  componentWillMount() {
    this.props.handleAuthentication().catch(() => {
      this.setState({
        hasErrorLoggingIn: true,
      });
    });
  }

  render() {
    if (this.state.hasErrorLoggingIn) {
      // Went over to Auth0, came back to us, but doesn't have correct info
      // TODO: Determine if this needs to change (show error details).
      return (
        <Link
          as={Button}
          primary
          size="massive"
          to={getRouteLink(routes.LOGIN)}
        >
          Log In
        </Link>
      );
    } else if (this.props.isAuthenticated) {
      // We parsed the response from auth0 and its all good.
      const savedRedirect = localstore.get('callbackRedirect');
      const redirect = savedRedirect || getRouteLink(routes.CONTENT);

      this.props.clearCallbackRedirect();
      return <Redirect to={redirect} />;
    }
    // Still waiting for an error or a successful auth
    return <div>Authenticating...</div>;
  }
}

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.selectIsUserAuthenticated(state),
});

function mapDispatchToProps(dispatch) {
  return {
    handleAuthentication: () => dispatch(handleAuthentication()),
    clearCallbackRedirect: () => dispatch(clearCallbackRedirect()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginCallback);
