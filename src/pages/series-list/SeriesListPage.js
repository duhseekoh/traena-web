// @flow
import React from 'react';
import layoutStyles from '../app/app.scss';
import SeriesTableContainer from '../../containers/SeriesTable/SeriesTableContainer';

const SeriesListPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SeriesTableContainer />
    </div>
  </div>
);

export default SeriesListPage;
