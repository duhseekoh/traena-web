import React from 'react';
import type { ContextRouter } from 'react-router';

import layoutStyles from '../app/app.scss';
import SeriesDetailContainer from '../../containers/SeriesDetail/SeriesDetailContainer';

const SeriesDetailPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SeriesDetailContainer
        seriesId={Number(props.match.params.seriesId)}
      />
    </div>
  </div>
);

export default SeriesDetailPage;
