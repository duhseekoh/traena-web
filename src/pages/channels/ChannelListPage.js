// @flow
import React from 'react';
import layoutStyles from '../app/app.scss';
import SubscriptionTableContainer from '../../containers/SubscriptionTable/SubscriptionTableContainer';

const ChannelListPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SubscriptionTableContainer />
    </div>
  </div>
);

export default ChannelListPage;
