// @flow
import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../redux/modules/auth/actions';
import * as fromAuth from '../../redux/modules/auth/selectors';

import { routes, getRouteLink } from '../../constants/routes';

type Props = {
  logout: () => Promise<any>,
  isAuthenticated: boolean,
};
function Logout(props: Props) {
  if (props.isAuthenticated) {
    props.logout();
  }
  return <div />;
}

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.selectIsUserAuthenticated(state),
});

function mapDispatchToProps(dispatch, props) {
  return {
    logout: () => {
      dispatch(logout());
      props.history.push(getRouteLink(routes.LOGIN));
    },
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
