// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import SeriesFormContainer from '../../containers/SeriesForm/SeriesFormContainer';

const EditSeriesPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <SeriesFormContainer
        seriesId={parseInt(props.match.params.seriesId, 10)}
      />
    </div>
  </div>
);

export default EditSeriesPage;
