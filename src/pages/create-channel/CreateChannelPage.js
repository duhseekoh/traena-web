import React from 'react';
import layoutStyles from '../app/app.scss';
import ChannelFormContainer from '../../containers/ChannelForm/ChannelFormContainer';

const CreateChannelPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <ChannelFormContainer />
    </div>
  </div>
);

export default CreateChannelPage;
