// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import ContentItemAnalyticsContainer from '../../containers/ContentItemAnalytics/ContentItemAnalyticsContainer';

const ContentItemAnalyticsPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <ContentItemAnalyticsContainer
        contentId={Number(props.match.params.contentId)}
      />
    </div>
  </div>
);

export default ContentItemAnalyticsPage;
