import React from 'react';
import layoutStyles from '../app/app.scss';
import ContentFormContainer from '../../containers/ContentForm/ContentFormContainer';

const CreateContentPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <ContentFormContainer />
    </div>
  </div>
);

export default CreateContentPage;
