import React from 'react';
import type { ContextRouter } from 'react-router';

import layoutStyles from '../app/app.scss';
import ContentDetailContainer from '../../containers/ContentDetail/ContentDetailContainer';

const ContentDetailPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <ContentDetailContainer
        contentId={Number(props.match.params.contentId)}
      />
    </div>
  </div>
);

export default ContentDetailPage;
