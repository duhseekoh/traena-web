// @flow
import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import EditUserRolesForm from '../../containers/UserRolesForm/UserRolesForm';

const UserRolesPage = (props: ContextRouter) => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <EditUserRolesForm userId={parseInt(props.match.params.userId, 10)} />
    </div>
  </div>
);

export default UserRolesPage;
