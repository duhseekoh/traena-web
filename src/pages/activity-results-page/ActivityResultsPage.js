import React from 'react';
import type { ContextRouter } from 'react-router';
import layoutStyles from '../app/app.scss';
import Modal from '../../components/Modal/Modal';
import ActivityResultsContainer from '../../containers/ActivityResults/ActivityResultsContainer';
import { TRANSPARENT_MODAL_OPACITY } from '../../constants/appConstants';

const ActivityResultsPage = (props: ContextRouter) => (
  <Modal opacity={TRANSPARENT_MODAL_OPACITY}>
    <div className={layoutStyles.primaryPageContainer}>
      <ActivityResultsContainer
        activityId={Number(props.match.params.activityId)}
        contentId={Number(props.match.params.contentId)}
        activityType={props.match.params.activityType}
      />
    </div>
  </Modal>
);

export default ActivityResultsPage;
