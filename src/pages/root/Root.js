// @flow
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as fromAuth from '../../redux/modules/auth/selectors';
import { routes, getRouteLink } from '../../constants/routes';

type Props = {
  isAuthenticated: boolean,
};

const Root = (props: Props) =>
  props.isAuthenticated ? (
    <Redirect to={getRouteLink(routes.CONTENT)} />
  ) : (
    <Redirect to={getRouteLink(routes.LOGIN)} />
  );

const mapStateToProps = (state): Props => ({
  isAuthenticated: fromAuth.selectIsUserAuthenticated(state),
});

export default connect(mapStateToProps)(Root);
