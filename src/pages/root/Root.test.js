import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';
import Root from './Root';

it('renders without crashing', () => {
  const div = document.createElement('div');

  const store = {
    getState: () => ({
      auth: {
        accessToken: 'abc',
        idToken: null,
      },
    }),
    dispatch: jest.fn(),
    subscribe: () => {},
  };

  ReactDOM.render(
    <Provider store={store}>
      <StaticRouter context={{}}>
        <Root />
      </StaticRouter>
    </Provider>,
    div,
  );
});
