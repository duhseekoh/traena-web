import React from 'react';
import layoutStyles from '../app/app.scss';
import CreateUserForm from '../../containers/UserForm/CreateUserForm';

const CreateUserPage = () => (
  <div>
    <div className={layoutStyles.primaryPageContainer}>
      <CreateUserForm />
    </div>
  </div>
);

export default CreateUserPage;
