import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import type { ContextRouter } from 'react-router';
import styles from './SubscriptionDetailsPage.scss';
import SubscriptionDetailsContainer
  from '../../containers/SubscriptionDetails/SubscriptionDetailsContainer';
import * as fromChannels from '../../redux/modules/channels/selectors';

import ChannelFormContainer from '../../containers/ChannelForm/ChannelFormContainer';
import SubscribedOrgsTable from '../../containers/SubscribedOrgsTable/SubscribedOrgsTable';
import SubscriptionFormContainer from '../../containers/SubscriptionForm/SubscriptionFormContainer';
import * as permissionUtils from '../../utils/permissionUtils';
import AccessControl from '../../components/AccessControl/AccessControl';
import type { Channel } from '../../types/channels';
import { routes } from '../../constants/routes';

type Props = {
  channel: Channel,
}

const SubscriptionDetailsPage = (props: Props & ContextRouter) => (
  <div>
    <SubscriptionDetailsContainer
      channelId={Number(props.match.params.channelId)}
    />

    <Route
      path={routes.CHANNEL_EDIT.path}
      render={() => (
        <ChannelFormContainer
          channelId={Number(props.match.params.channelId)}
        />
      )}
    />

    <div className={styles.subscriptionsChannelPageContainer}>
      <SubscriptionFormContainer
        channelId={Number(props.match.params.channelId)}
      />
      {props.channel &&
        <AccessControl
          customAccessData={{ organizationId: props.channel.organizationId }}
          accessCheck={permissionUtils.isOwnOrgChannel}
          noAccessView={<span />}
        >
          <Grid>
            <Grid.Row>
              <Grid.Column width={16}>
                <SubscribedOrgsTable
                  {...props}
                  channelId={Number(props.match.params.channelId)}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </AccessControl>
      }
    </div>
  </div>
);

/**
* Note: typically Page components are not connected!
* This is necessary so that we can wrap our SubscribedOrgsTable in the AccessControl
* component, in order to restrict its view to users with the correct orgId
*/
const mapStateToProps = (state, props) => ({
  channel: fromChannels.selectChannelById(state, props.match.params.channelId),
});

export default connect(mapStateToProps, null)(SubscriptionDetailsPage);
