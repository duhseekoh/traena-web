/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import classnames from 'classnames';
import type { ContextRouter } from 'react-router';
import UsersTable from '../../containers/UsersTable/components/UsersTable';
import UsersTableContainer from '../../containers/UsersTable/UsersTableContainer';
import layoutStyles from '../app/app.scss';
import styles from './UserManagement.scss';

const UserManagement = (props: ContextRouter) => (
  <div
    className={classnames(
      layoutStyles.primaryPageContainer,
      styles.usersPageContainer,
    )}
  >
    <UsersTableContainer
      {...props}
      renderTable={ownProps => (
        <UsersTable
          {...ownProps}
        />
      )}
    />
  </div>
);


export default UserManagement;
