import React from 'react';
import { Button } from 'semantic-ui-react';

import styles from './DownloadAppPage.scss';

const DownloadAppPage = () => (
  <div className={styles.pageHeader}>
    <h1>Download Traena</h1>
    <p>This content is only viewable through our app.</p>
    <p>Get it now!</p>
    <a href="https://appstore.com/traena/">
      <Button primary size="big" type="submit">
        iOS
      </Button>
    </a>
    <a href="http://play.google.com/store/apps/details?id=com.traena">
      <Button primary size="big" type="submit">
        ANDROID
      </Button>
    </a>
  </div>
);

export default DownloadAppPage;
