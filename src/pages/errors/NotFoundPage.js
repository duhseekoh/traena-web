/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { routes, getRouteLink } from '../../constants/routes';

import styles from './NotFoundPage.scss';

const NotFoundPage = () => (
  <div>
    <h1 className={styles.pageHeader}>Whoops!</h1>
    <div className={styles.pageSubtitle}>
      We couldnt find the page you were looking for.
    </div>
    <div className={styles.backButtonWrapper}>
      <Link to={getRouteLink(routes.CONTENT)}>
        <span className={styles.backButton}>
          <Icon circular name="chevron left" size="big" />Go back
        </span>
      </Link>
    </div>
    <div className={styles.errorCodeText}>404</div>
  </div>
);

export default NotFoundPage;
