// @flow
type Choice = {
  id: string,
  order: number,
  choiceText: string,
  correct: boolean,
  reasonText?: string,
};

type MultipleChoiceQuestion = {
  id: string,
  questionText: string,
  order: number,
  choices: Choice[],
};

export type QuestionSet = {
  questions: MultipleChoiceQuestion[],
};

type PollChoice = {
  id: string,
  choiceText: string,
};

export type Poll = {
  title: string,
  choices: PollChoice[],
};

// Properties that all types of Activities have
type BaseActivity = {
  id?: number,
  contentId: number,
  version?: number,
  order: number,
  deleted: boolean,
};

export type QuestionSetActivity = BaseActivity & {
  type: 'QuestionSet',
  body: QuestionSet,
};

export type PollActivity = BaseActivity & {
  type: 'Poll',
  body: Poll,
};

// Disjoint union representing all possible shapes of an Activity
export type Activity = QuestionSetActivity | PollActivity;

type PollChoiceResult = {
  count: number,
  percent: number,
};

export type QuestionSetResults = {
  /**
  * The final structure for QuestionSetResults has not been defined
  */
};

export type PollResults = {
  activity: PollActivity,
  count: number,
  choiceResults: {
    [choiceId: string]: PollChoiceResult,
  },
};

export type ActivityResults = PollResults;
