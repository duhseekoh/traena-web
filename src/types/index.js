// @flow
import type { UiProgressStatus } from '../constants/appConstants';

export type Pagination = {
  totalElements: number,
  size: number,
  number: number,
  totalPages: number,
  numberOfElements: number,
  first: boolean,
  last: boolean,
};

export type IndexedPage<T> = {
  number: number,
  size: number,
  totalElements: number,
  totalPages: number,
  first: number,
  last: number,
  ids: number[],
  index: {
    [id: number]: T,
  },
  numberOfElements: number,
}

export type ArrayPage<T> = {
  number: number,
  size: number,
  totalElements: number,
  totalPages: number,
  first: number,
  last: number,
  numberOfElements: number,
  content: T[]
}

export type ProgressUpdate = {
  message: string,
  percent: number,
  status: ?UiProgressStatus,
};

export type Notification = {
  message: string,
  title: string,
  onConfirm: () => void,
  onDismiss: () => void,
};

export type Option = {
  text: string,
  value: number | string
}
