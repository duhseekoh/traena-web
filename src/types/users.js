// @flow

export type UserFormVals = {
  tags: string[],
  passwordReset: boolean,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  position: string,
};

export type AuthUser = {
  email: string,
  exp: number,
  iat: number,
  iss: string,
  sub: string,
  aud: string,
  permissions: string[],
  roles: string[],
  details: {
    traenaId: number,
    organizationId: number,
    organizationName: string,
    firstName: string,
    lastName: string,
    profileImageURI: ?string,
    subscribedChannelIds: number[],
  },
  groups: string[],
};

export type User = {
  id: number,
  firstName: string,
  lastName: string,
  email: string,
  position: string,
  isDisabled: boolean,
  createdTimestamp: string,
  tags: string[],
};

export type Role = {
  id: number,
  name: string,
  description: string,
};
