// @flow
import type { SubscriptionVisibility, SubscriptionSources } from '../constants/appConstants';
import type { Channel } from './channels';

export type Subscription = {
  channel: Channel,
  channelId: number,
  organizationId: number,
  visibility: SubscriptionVisibility,
};

export type SubscriptionFilters = {
  source: SubscriptionSources,
  visibility: SubscriptionVisibility,
}
