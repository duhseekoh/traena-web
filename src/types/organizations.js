// @flow

export type Organization = {
  id: number,
  name: string,
  organizationType: string,
};
