import type { ContentItem } from './content';
import type { User } from './users';
import type { Channel } from './channels';
import type { Organization } from './organizations';
import type { SubscriptionSources } from '../constants/appConstants';


export type Series = {
  author?: User,
  authorId: number,
  channelId: number,
  contentCount: number,
  contentPreview?: ContentItem[],
  description: ?string,
  channel?: Channel,
  id: number,
  title: string,
  contentIds: number[],
  organization?: Organization,
}

export type SeriesFilters = {
  source: SubscriptionSources,
}
