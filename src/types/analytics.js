// @flow
import type { EventType } from '../constants/events';
import type { User } from './users';

export type Event = {|
  id: ?number,
  userId: ?number,
  type: EventType,
  data: any,
  modifiedTimestamp: string,
  createdTimestamp: string,
  user: ?User,
|};

export type ContentItemUserStats = {|
  liked: boolean,
  comments: number,
  views: number,
  completed: boolean,
|};

export type UserWithContentStats = {|
  user: User,
  contentItemStats: ContentItemUserStats,
|};

export type ContentItemAggregateStats = {|
  likes: number,
  comments: number,
  completedTasks: number,
  views: number,
|};

export type SeriesUserStats = {|
  completedContentCount: number,
  latestContentCompletionTimestamp: ?string,
  latestViewedContentTimestamp: ?string,
|};

export type UserWithSeriesStats = {|
  user: User,
  seriesStats: SeriesUserStats,
|};

export type SeriesAggregateStats = {|
  userCompletionCount: number,
  userNotStartedCount: number,
  userInProgressCount: number,
  contentCount: number,
|};
