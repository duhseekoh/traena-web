// @flow
import type { ChannelVisibility } from '../constants/appConstants';
import type { Organization } from './organizations';

export type Channel = {
  id: number,
  organizationId: number,
  name: string,
  description?: string,
  default?: boolean,
  createdTimestamp: string,
  modifiedTimestamp?: string,
  visibility: ChannelVisibility,
  organization: Organization,
  type: string,
};
