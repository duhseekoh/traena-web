// @flow
import type { TranscodingStatus } from '../constants/appConstants';

export type TranscodeJob = {
  id: number,
  userId: number,
  jobId: string, // AWS jobId
  videoUri: string,
  thumbnailUri: string,
  status: TranscodingStatus,
};
