// @flow
import type { ContentStatus } from '../constants/appConstants';
import type { Channel } from './channels';
import type { Series } from './series';

export type ImageVariation = {
  width: number,
  height: number,
  filename: string,
};

export type ImageEntry = {
  id: string,
  baseCDNPath: string,
  variations: {
    tiny: ImageVariation,
    small: ImageVariation,
    medium: ImageVariation,
    large: ImageVariation,
    original: ImageVariation,
  },
};

type ContentItemGeneric<B> = {
  author: *,
  channel: Channel,
  createdTimestamp: string,
  modifiedTimestamp: string,
  id: number,
  publishedTimestamp: string,
  status: ContentStatus,
  body: B,
  commentCount: number,
  likeCount: number,
  completedCount: number,
  series: ?Series,
  tags: string[], // TODO Do we always have this?
};

type ImageContentItemBody = {|
  type: 'Image',
  images: ImageEntry[],
  text: string,
  title: string,
|};

type TextContentItemBody = {|
  type: 'DailyAction',
  title: string,
  text: string,
|};

export type Video = {
  videoURI: string,
  previewImageURI: string,
  transcodeId?: ?number,
};

type VideoContentItemBody = {|
  type: 'TrainingVideo',
  title: string,
  text: string,
  video: Video,
|};

export type ImageContentItem = ContentItemGeneric<ImageContentItemBody>;
export type VideoContentItem = ContentItemGeneric<VideoContentItemBody>;
export type TextContentItem = ContentItemGeneric<TextContentItemBody>;

export type ContentItem = ImageContentItem | VideoContentItem | TextContentItem;

export type ContentFormVals = {
  ...ContentItem,
  // TODO - not sure if we need these or if they are correctly defined on ContentItem
  // tags: string[],
  // status: string,
  desiredStatus: ContentStatus,
};
