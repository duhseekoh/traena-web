// @flow
export const API_BASE_URL: string = process.env.REACT_APP_API_BASE_URL || '';
export type AuthConfig = {
  domain: string,
  clientId: string,
  audience: string,
  userClaimNamespace: string,
  redirectUri: string,
  responseType: string,
  scope: string,
  connection: string,
};
export const AUTH_CONFIG: AuthConfig = {
  domain: process.env.REACT_APP_AUTH0_DOMAIN || '', // default for flow
  clientId: process.env.REACT_APP_AUTH0_CLIENT_ID || '',
  audience: process.env.REACT_APP_AUTH0_AUDIENCE || '',
  userClaimNamespace: process.env.REACT_APP_AUTH0_USER_CLAIM_NAMESPACE || '',
  redirectUri: process.env.REACT_APP_AUTH0_REDIRECT_URI || '',
  responseType: 'token id_token',
  scope: 'openid email groups details roles permissions',
  connection: 'traena-user-database',
};

export const FEEDBACK_URI = process.env.REACT_APP_FEEDBACK_URI || '';

export const CONTENT_TYPES = {
  DailyAction: 'DailyAction',
  TrainingVideo: 'TrainingVideo',
  Image: 'Image',
};

export const CONTENT_STATUSES = {
  DRAFT: 'DRAFT',
  PUBLISHED: 'PUBLISHED',
  ARCHIVED: 'ARCHIVED',
};

export const CONTENT_INCLUDE_SOURCES = {
  SELF: 'self',
  INTERNAL: 'internal',
  EXTERNAL: 'external',
};

export const TRANSCODING_STATUSES = {
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETE: 'COMPLETE',
  ERROR: 'ERROR',
};

export const SERVER_ERROR_TYPES = {
  ConflictError: 'ConflictError',
};

export const UI_PROGRESS_STATUSES = {
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETE: 'COMPLETE',
  ERROR: 'ERROR',
};

// These are the possible visibility values for a Channel
export const CHANNEL_VISIBILITIES = {
  MARKET: 'MARKET', // publicly available for any subscriber
  INTERNAL: 'INTERNAL', // not subscribable other than to owning org
  LIMITED: 'LIMITED', // content created by an org for a specific set of subscriber orgs
};

// These are used for filter values to send to the API
// i.e. ?visibility=INTERNAL,MARKET,LIMITED
export const CHANNEL_VISIBILITY_FILTERS = {
  ALL: `${CHANNEL_VISIBILITIES.INTERNAL}, ${CHANNEL_VISIBILITIES.LIMITED}, ${CHANNEL_VISIBILITIES.MARKET}`,
  SHARED: `${CHANNEL_VISIBILITIES.LIMITED}, ${CHANNEL_VISIBILITIES.MARKET}`,
  INTERNAL: `${CHANNEL_VISIBILITIES.INTERNAL}`, // not subscribable other than to owning org
};

// These are the values rendered for the user.  We do not currently
// inform the end user of all the possible visibilities a channel may have
export const CHANNEL_VISIBILITY_DISPLAY = {
  [CHANNEL_VISIBILITIES.INTERNAL]: 'Internal',
  [CHANNEL_VISIBILITIES.MARKET]: 'Shared',
  [CHANNEL_VISIBILITIES.LIMITED]: 'Shared',
};

export const SUBSCRIPTION_SOURCES = {
  INTERNAL: 'internal',
  EXTERNAL: 'external',
  ALL: 'all',
};

export const SUBSCRIPTION_VISIBILITIES = {
  ADMINS: 'ADMINS',
  ORGANIZATION: 'ORGANIZATION',
  USERS: 'USERS',
};

/*
* A list of all the redux modules that support sorting.  The values here are
* used as arguments to `redux-sort` factory methods as the `reducerKey` argument.
* These values are the keys to the sortable reducer at the root level of the redux state tree.
*/
export const SORTABLE_REDUX_MODULE_NAMES = {
  UI_CONTENT_TABLE: 'uiContentTable',
  UI_SERIES_TABLE: 'uiSeriesTable',
  SERIES_STATS_USERS: 'seriesStatsUsers',
  UI_USERS_TABLE: 'uiUsersTable',
  UI_SUBSCRIPTION_TABLE: 'uiSubscriptionTable',
  UI_CHANNEL_CONTENT_TABLE: 'uiChannelContentTable',
};

/**
* These redux modules control ui states in our application
* these values are used as args to our `redux-ui-loading` factory methods
*/
export const UI_REDUX_MODULE_NAMES = {
  UI_SERIES_TABLE: 'uiSeriesTable',
  UI_SERIES_FORM: 'uiSeriesForm',
  UI_USERS_TABLE: 'uiUsersTable',
  UI_SUBSCRIPTION_TABLE: 'uiSubscriptionTable',
  UI_SERIES_DETAIL: 'uiSeriesDetail',
  UI_CHANNEL_FORM: 'uiChannelForm',
  UI_CONTENT_FORM: 'uiContentForm',
  UI_ACTIVITIES_FORM: 'uiActivitiesForm',
  UI_SUBSCRIPTION_FORM: 'uiSubscriptionForm',
};

export const TRANSPARENT_MODAL_OPACITY = 0.9;

export const IMAGE_PROCESSING_LAMBDA_TYPICAL_TIME: number = 2000;
export const VIDEO_TRANSCODER_TYPICAL_TIME: number = 15000;
export const CONTENT_IMAGES_MAX_ALLOWED: number = 25;


export type ContentIncludeSource = $Values<typeof CONTENT_INCLUDE_SOURCES>;
export type ContentType = $Values<typeof CONTENT_TYPES>;
export type ContentStatus = $Values<typeof CONTENT_STATUSES>;
export type TranscodingStatus = $Values<typeof TRANSCODING_STATUSES>;
export type ServerErrorType = $Values<typeof SERVER_ERROR_TYPES>;
export type UiProgressStatus = $Values<typeof UI_PROGRESS_STATUSES>;
export type ChannelVisibility = $Values<typeof CHANNEL_VISIBILITIES>;
export type SubscriptionVisibility = $Values<typeof SUBSCRIPTION_VISIBILITIES>;
export type SubscriptionSources = $Values<typeof SUBSCRIPTION_SOURCES>;
