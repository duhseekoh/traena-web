// @flow
import { COLORS } from './colors';
import PollActivityIcon from '../images/icon-poll.svg';
import PollActivityIconFadedGreen from '../images/icon-poll-faded-green.svg';
import QuizActivityIcon from '../images/icon-quiz.svg';
import QuizActivityIconDarkerGreen from '../images/icon-quiz-darker-green.svg';

export const ACTIVITY_TYPES = {
  QUESTION_SET: 'QuestionSet',
  POLL: 'Poll',
};

export const ACTIVITY_UI_NAMES = {
  [ACTIVITY_TYPES.QUESTION_SET]: 'Quiz',
  [ACTIVITY_TYPES.POLL]: 'Poll',
};

export const ACTIVITY_ICONS = {
  [ACTIVITY_TYPES.QUESTION_SET]: {
    [COLORS.DARKER_GREEN]: QuizActivityIconDarkerGreen,
    [COLORS.PRIMARY_GREEN]: QuizActivityIcon,
  },
  [ACTIVITY_TYPES.POLL]: {
    [COLORS.PRIMARY_GREEN]: PollActivityIcon,
    [COLORS.FADED_GREEN]: PollActivityIconFadedGreen,
  },
};

export type ActivityType = $Values<typeof ACTIVITY_TYPES>;
