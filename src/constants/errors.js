// @flow
// eslint-disable-next-line
export class Auth0UnexpectedAuthResultError extends Error {
  name: string;
  message: string;
  details: ?Object;
  stack: string;

  constructor(message: string, details: Object) {
    super(message);
    this.name = 'Auth0UnexpectedAuthResultError';
    this.message = message;
    this.details = details;
    this.stack = new Error().stack;
  }
}
