export const COLORS = { // eslint-disable-line
  PRIMARY_GREEN: '#00B49C',
  DARKER_GREEN: '#076470',
  DARK_GREEN: '#018a97',
  FADED_GREEN: '#d1f0eb',
  LIGHT_GREY: '#ebebeb',
  MEDIUM_GREY: '#aab2bd',
};
