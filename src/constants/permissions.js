/* eslint-disable import/prefer-default-export */
/**
 * Permissions constants are used in our AccessControl components.
 * Depending on which permissions a user has, different content can be viewed in the UI.
 */

const INFERRED_PERMISSION = 'inferred';

export const PERMISSIONS = Object.freeze({
  ADMIN: 'admin',
  COMMENTS: {
    READ: {
      ALL: 'read:all:comments',
      ORG: 'read:org:comments',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:comments',
      ORG: 'modify:org:comments',
      SELF: 'modify:self:comments',
    },
  },
  CONTENT: {
    READ: {
      ALL: 'read:all:content',
      ORG: 'read:org:content',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:content',
      ORG: 'modify:org:content',
      SELF: 'modify:self:content',
    },
  },
  DEVICES: {
    READ: {
      ALL: 'read:all:devices',
      ORG: 'read:org:devices',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:devices',
      ORG: 'modify:org:devices',
      SELF: 'modify:self:devices',
    },
  },
  ORGANIZATIONS: {
    READ: {
      ALL: 'read:all:organizations',
      ORG: 'read:org:organizations',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:organizations',
      ORG: 'modify:org:organizations',
      SELF: INFERRED_PERMISSION,
    },
  },
  TASKS: {
    READ: {
      ALL: 'read:all:tasks',
      ORG: 'read:org:tasks',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:tasks',
      ORG: 'modify:org:tasks',
      SELF: 'modify:self:tasks',
    },
  },
  USERS: {
    READ: {
      ALL: 'read:all:users',
      ORG: 'read:org:users',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:users',
      ORG: 'modify:org:users',
      SELF: 'modify:self:users',
    },
  },
});
