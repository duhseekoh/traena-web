// @flow
import _ from 'lodash';
import { FEEDBACK_URI } from './appConstants';

/**
 * Given a key-value object turn it into an encoded qs.
 * e.g. { name: 'dom dicicco', food: 'pizza' } => 'name=dom%20dicicco&food=pizza'
 */
const encodeQueryParams = (params: Object): string => {
  const entries: string[] = Object.keys(params).map(
    key => `${key}=${encodeURIComponent(params[key])}`,
  );
  return entries.join('&');
};

const routes = {
  ROOT: {
    path: '/',
    link: _.template('/'),
  },
  LOGIN: {
    path: '/login',
    link: _.template('/login'),
  },
  LOGIN_CALLBACK: {
    path: '/callback',
    link: _.template('/callback'),
  },
  LOGOUT: {
    path: '/logout',
    link: _.template('/logout'),
  },
  CONTENT: {
    path: '/content',
    link: _.template('/content'),
  },
  CONTENT_CREATE: {
    path: '/content/create',
    link: _.template('/content/create'),
  },
  CONTENT_EDIT: {
    path: '/content/:contentId/edit',
    link: _.template('/content/<%= contentId %>/edit'),
  },
  CONTENT_VIEW: {
    path: '/content/:contentId/view',
    link: _.template('/content/<%= contentId %>/view'),
  },
  CONTENT_DETAIL: {
    path: '/content/:contentId/detail',
    link: _.template('/content/<%= contentId %>/detail'),
  },
  CONTENT_STATS: {
    path: '/content/:contentId/stats',
    link: _.template('/content/<%= contentId %>/stats'),
  },
  CONTENT_ACTIVITY_ADD: {
    path: '/content/:contentId/activity/add/:activityType',
    link: _.template(
      '/content/<%= contentId %>/activity/add/<%= activityType %>',
    ),
  },
  CONTENT_ACTIVITY_EDIT: {
    path: '/content/:contentId/activity/:activityId/edit/:activityType',
    link: _.template(
      '/content/<%= contentId %>/activity/<%= activityId %>/edit/<%= activityType %>',
    ),
  },
  ACTIVITY_RESULTS: {
    path: '/content/:contentId/activity/:activityId/results/:activityType',
    link: _.template(
      '/content/<%= contentId %>/activity/<%= activityId %>/results/<%= activityType %>',
    ),
  },
  SERIES: {
    path: '/series',
    link: _.template('/series'),
  },
  SERIES_DETAIL: {
    path: '/series/:seriesId/detail',
    link: _.template('/series/<%= seriesId %>/detail'),
  },
  SERIES_STATS: {
    path: '/series/:seriesId/stats',
    link: _.template('/series/<%= seriesId %>/stats'),
  },
  SERIES_CREATE: {
    path: '/series/create',
    link: _.template('/series/create'),
  },
  SERIES_EDIT: {
    path: '/series/:seriesId/edit',
    link: _.template('/series/<%= seriesId %>/edit'),
  },
  USERS: {
    path: '/users',
    link: _.template('/users'),
  },
  USERS_CREATE: {
    path: '/users/create',
    link: _.template('/users/create'),
  },
  USERS_EDIT: {
    path: '/users/:userId/edit',
    link: _.template('/users/<%= userId %>/edit'),
  },
  USERS_ROLES: {
    path: '/users/:userId/roles',
    link: _.template('/users/<%= userId %>/roles'),
  },
  CHANNELS: {
    path: '/channels',
    link: _.template('/channels'),
  },
  CHANNEL_CREATE: {
    path: '/channels/create',
    link: _.template('/channels/create'),
  },
  CHANNEL_EDIT: {
    path: '/channels/:channelId/edit',
    link: _.template('/channels/<%= channelId %>/edit'),
  },
  SUBSCRIPTION_DETAILS: {
    path: '/channels/:channelId',
    link: _.template('/channels/<%= channelId %>/'),
  },
  EXTERNAL_FEEDBACK_URI: {
    path: '',
    link: (params: {
      userEmail: string,
      firstName: string,
      lastName: string,
      organizationName: string,
    }) => {
      const qs = encodeQueryParams({
        prefill_Email: params.userEmail,
        prefill_FirstName: params.firstName,
        prefill_LastName: params.lastName,
        prefill_OrganizationName: params.organizationName,
      });
      return `${FEEDBACK_URI}?${qs}`;
    },
  },
};

/*
* 'link' is used for <Link>'s to={} property
* 'path' is supplied to react-router's <Route>'s path={} property
*/

export type RoutesConfigEntry = {
  path: string,
  link: any => string,
};

export type RoutesConfig = {
  [$Keys<typeof routes>]: RoutesConfigEntry,
};

const getRouteLink = (route: RoutesConfigEntry, params: ?Object) =>
  route.link(params);

export { routes, getRouteLink };
