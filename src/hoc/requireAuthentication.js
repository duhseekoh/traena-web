// @flow
import React, { Component, type ComponentType } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as fromAuth from '../redux/modules/auth/selectors';
import { routes, getRouteLink } from '../constants/routes';
import { setCallbackRedirect } from '../redux/modules/auth/actions';
import type { Dispatch } from '../redux/rootReducer';

type HOCProps = {
  isUserAuthenticated: boolean,
  setCallbackRedirect: (pathname: string) => Promise<any>,
};

type OriginalProps = *;

export default function requireAuthentication(
  OriginalComponent: ComponentType<OriginalProps>,
) {
  class AuthenticatedComponent extends Component<HOCProps> {
    props: HOCProps;
    componentWillMount() {
      if (!this.props.isUserAuthenticated) {
        this.props.setCallbackRedirect(window.location.pathname);
      }
    }

    render() {
      return (
        <div>
          {this.props.isUserAuthenticated ? (
            <OriginalComponent {...this.props} />
          ) : (
            <Redirect to={getRouteLink(routes.LOGIN)} />
          )}
        </div>
      );
    }
  }

  const mapStateToProps = state => ({
    isUserAuthenticated: fromAuth.selectIsUserAuthenticated(state),
  });

  const mapDispatchToProps = (dispatch: Dispatch) => ({
    setCallbackRedirect: redirect => dispatch(setCallbackRedirect(redirect)),
  });

  return connect(mapStateToProps, mapDispatchToProps)(AuthenticatedComponent);
}
