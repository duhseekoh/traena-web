import React from 'react';
import AccessControl from '../components/AccessControl/AccessControl';

/**
 * By wrapping a component in this HOC, the wrapped component will only be rendered if the
 * logged in user has the permissions supplied as an argument.
 *
 * Usage example:
 * import withAccessControl from '../../hoc/withAccessControl';
 * const ContentCreator = withAccessControl(['content.modify.self'])
 * const RestrictedComponent = ContentCreator(ContentCreatorPage)
 *
 * // Only renders ContentCreatorPage if current user has permission 'content.modify.self'
 * render() => <RestrictedComponent />
 */

const withAccessControl = (
  allowedPermissions,
  opts = {},
) => WrappedComponent => {
  const WithAccessControl = props => {
    const { noAccessView, accessCheck, customAccessData } = opts;
    return (
      <AccessControl
        allowedPermissions={allowedPermissions}
        accessCheck={accessCheck}
        noAccessView={noAccessView}
        customAccessData={customAccessData}
      >
        <WrappedComponent {...props} />
      </AccessControl>
    );
  };

  return WithAccessControl;
};

export default withAccessControl;
