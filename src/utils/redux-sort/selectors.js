// @flow
import type { ReduxState } from '../../redux/rootReducer';
import {
  SORT_DIRECTIONS,
} from './constants';
/**
* Builds a set of selectors with which you can retrieve the sort state for a given reducer property
* @param {string} reducerKey the key within `state` where we have sort information.  (This should be the
* same as the `reducerKey` passed to `createSortActions`)
*/
export const createSortSelectors = (reducerKey: string = '') => { // eslint-disable-line
  const _selectSortOrder = (state: ReduxState) => {
    if (!state[`${reducerKey}`].sort) {
      return null;
    }
    return state[`${reducerKey}`].sort.orders;
  };

  const _getSortDirection = (state: ReduxState, property: string) => {
    const sortOrder = _selectSortOrder(state);
    if (!sortOrder) {
      return null;
    }
    const sortObj = sortOrder.filter((sort) =>
      sort.property === property)[0] || null;
    return sortObj && sortObj.direction;
  };

  return {
    selectSortOrder: (state: ReduxState) => _selectSortOrder(state),

    /*
    * Returns sort direction for the given property, or null
    */
    getSortDirection: (state: ReduxState, property: string) =>
      _getSortDirection(state, property),

    getSortDirectionLongName: (
      state: ReduxState,
      property: string,
    ): ?string => {
      const currentSortDirection = _getSortDirection(state, property);
      if (currentSortDirection === SORT_DIRECTIONS.ASCENDING) {
        return 'ascending';
      } else if (currentSortDirection === SORT_DIRECTIONS.DESCENDING) {
        return 'descending';
      }
      return null;
    },

    selectSortQueryString: (state: ReduxState) => {
      const sortOrder = _selectSortOrder(state);
      if (!sortOrder) {
        return '';
      }
      return sortOrder.reduce((queryString, sortObj, i) => {
        queryString += `${sortObj.property}:${sortObj.direction}${i === sortOrder.length - 1 ? '' : ','}`; // eslint-disable-line
        return queryString;
      }, '');
    },
  };
};
