// @flow
import {
  SORT_DIRECTIONS,
} from './constants';

import type { Sort } from './types';

type ReduxStateSort = Sort;

type Options = {
  // only allow a set number of sort orders to be active at a particular time
  maxActiveOrders: number,
};

const initialSortState: ReduxStateSort = {
  orders: [],
};

/**
* Creates a namespaced reducer to support a sorted table of data
* @param reducerKey namespace for the sort reducer.  Should be the same as the `reducerKeys` given to `createSortActions(reducerKey)`.
* @param initialSortState an array of objects that define an initial sorting for columns, i.e. { sort: [{ direction: 'asc', property: 'firstName' }] }
*/
export function createSortReducer( // eslint-disable-line
  reducerKey: string = '',
  initialState: ReduxStateSort = initialSortState,
  options?: Options = {
    maxActiveOrders: 2,
  },
) {
  return function sort(
    sortState: ReduxStateSort = initialState,
    action: { type: string, payload: any },
  ) {
    switch (action.type) {
      case `${reducerKey}/SET_SORT`:
        const { property } = action.payload;
        const clonedOrder = sortState.orders.slice(0);

        const currentOrder = clonedOrder.filter((order) =>
          order.property === property)[0];
        const currentOrderDirection = currentOrder ?
          currentOrder.direction :
          null;

        let direction;
        if (currentOrderDirection === SORT_DIRECTIONS.ASCENDING) {
          direction = SORT_DIRECTIONS.DESCENDING;
        } else if (currentOrderDirection === SORT_DIRECTIONS.DESCENDING) {
          direction = undefined;
        } else if (!currentOrderDirection) {
          direction = SORT_DIRECTIONS.ASCENDING;
        }

        // If we're already sorted by the column,
        // we start by removing that old sort definition.
        const cleanedOrder = clonedOrder.filter((order) =>
          order.property !== property);
        let resultOrder = cleanedOrder;
        if (direction) {
          resultOrder = [
            {
              property,
              direction,
            },
            ...cleanedOrder,
          ];
        }
        return {
          ...sortState,
          orders: resultOrder.slice(0, options.maxActiveOrders),
        };

      case `${reducerKey}/CLEAR_SORT`:
        return initialSortState;

      default:
        return sortState;
    }
  };
}
