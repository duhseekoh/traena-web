// @flow
export const SORT_DIRECTIONS = {
  ASCENDING: 'asc',
  DESCENDING: 'desc',
};

export const SORT_DIRECTIONS_LONG = {
  ASCENDING: 'ascending',
  DESCENDING: 'descending',
};
