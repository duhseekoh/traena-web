import { createSortReducer } from './reducer';

describe('redux-sort reducer', () => {
  it('factory function should create a reducer', () => {
    const reducer = createSortReducer('testKey');
    expect(typeof reducer).toEqual('function');
  });

  describe('when creating a sort reducer with no initial state or options defined', () => {
    let reducer;
    beforeEach(() => {
      reducer = createSortReducer('testKey');
    });

    it('should return the default initial state', () => {
      const state = reducer(undefined, { type: 'DUMMY', payload: 'DUMMY' });
      expect(state).toEqual({
        orders: [],
      });
    });

    it('should accept a sortable property when called with SET_SORT action', () => {
      const state = reducer(undefined, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      expect(state).toEqual({
        orders: [
          {
            property: 'firstName',
            direction: 'asc',
          },
        ],
      });
    });

    it('should reverse the direction when a property is sorted twice', () => {
      const state1 = reducer(undefined, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      const state2 = reducer(state1, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      expect(state2).toEqual({
        orders: [
          {
            property: 'firstName',
            direction: 'desc',
          },
        ],
      });
    });

    it('should set the most recently added order to the beginning of the orders', () => {
      // add first order
      const state1 = reducer(undefined, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      // add another order
      const state2 = reducer(state1, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'lastName',
        },
      });

      expect(state2.orders[0]).toEqual({
        property: 'lastName',
        direction: 'asc',
      });

      expect(state2.orders[1]).toEqual({
        property: 'firstName',
        direction: 'asc',
      });
    });

    it('should sort with a max of the default maxActiveOrders', () => {
      // add first order
      const state1 = reducer(undefined, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      // add another order
      const state2 = reducer(state1, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'lastName',
        },
      });
      // add a third order
      const state3 = reducer(state2, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'gender',
        },
      });

      expect(state3.orders).toHaveLength(2);
      expect(state3).toEqual({
        orders: [
          {
            property: 'gender',
            direction: 'asc',
          },
          {
            property: 'lastName',
            direction: 'asc',
          },
        ],
      });
    });
  });

  describe('when creating a sort reducer with a maxActiveOrders option set', () => {
    let reducer;
    beforeEach(() => {
      reducer = createSortReducer('testKey', undefined, { maxActiveOrders: 3 });
    });

    it('should sort with a max of the passed in maxActiveOrders', () => {
      // add first order
      const state1 = reducer(undefined, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'firstName',
        },
      });
      // add another order
      const state2 = reducer(state1, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'lastName',
        },
      });
      // add a third order
      const state3 = reducer(state2, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'gender',
        },
      });
      // add a fourth order
      const state4 = reducer(state3, {
        type: 'testKey/SET_SORT',
        payload: {
          property: 'address',
        },
      });

      expect(state4.orders).toHaveLength(3);
      expect(state4).toEqual({
        orders: [
          {
            property: 'address',
            direction: 'asc',
          },
          {
            property: 'gender',
            direction: 'asc',
          },
          {
            property: 'lastName',
            direction: 'asc',
          },
        ],
      });
    });
  });
});
