import type { createSortActionsOptions } from './types';

/**
* Creates a namespaced set of actions related to sorting a table of data
* @param {string} reducerKey namespace for the sort actions.  Should be the same as the `reducerKey` given to `createSortReducer(reducerKey)`.
*/
export const createSortActions = ( // eslint-disable-line
  reducerKey: string = '',
  options: createSortActionsOptions = {},
) => ({
  /**
  * Sets the sort Order of the given property in redux state.  If the data is already sorted already as descending by this property, it will become ascending,
  * otherwise it will be set to un-sorted.
  * @param property The name of the property for which the data should be sorted
  */
  setSort: (property: string) => async (dispatch: Dispatch) => {
    if (
      options.sortableProperties &&
      !options.sortableProperties.includes(property)
    ) {
      return false;
    }

    return dispatch({
      type: `${reducerKey}/SET_SORT`,
      payload: {
        property,
      },
    });
  },

  clearSort: () => async (dispatch: Dispatch) => dispatch({
    type: `${reducerKey}/CLEAR_SORT`,
  }),

  sortActionTypes: {
    setSort: `${reducerKey}/SET_SORT`,
    clearSort: `${reducerKey}/CLEAR_SORT`,
  },
});
