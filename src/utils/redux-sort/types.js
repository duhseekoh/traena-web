// @flow
import { SORT_DIRECTIONS, SORT_DIRECTIONS_LONG } from './constants';

export type createSortActionsOptions = {
  sortableProperties?: string[],
};

export type Order = {
  property: string,
  direction: string,
};

export type Sort = {
  orders: Order[],
};

export type SortDirections = $Values<typeof SORT_DIRECTIONS>;
export type SortDirectionsLong = $Values<typeof SORT_DIRECTIONS_LONG>;
