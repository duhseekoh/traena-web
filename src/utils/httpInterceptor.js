import fetchIntercept from 'fetch-intercept';
import { API_BASE_URL } from '../constants/appConstants';
import { get } from './localstore';

function getJsonConfig(config) {
  config.headers = Object.assign(
    {},
    {
      'Content-Type': 'application/json',
    },
    config.headers,
  );
  if (config.body) config.body = JSON.stringify(config.body);
  return config;
}

export default () =>
  fetchIntercept.register({
    request(url, config = {}) {
      let endpoint = url;
      const token = config.token || get('accessToken');
      const baseHeaders = {};
      if (!/https?:\/\//.test(url)) {
        endpoint = API_BASE_URL + endpoint;
        baseHeaders.Authorization = token && `Bearer ${token}`;
        baseHeaders.Accept = 'application/json';
      }
      const formData =
        config.body && config.body.constructor.name === 'FormData';

      config.headers = Object.assign({}, baseHeaders, config.headers);
      if (!formData && !/https?:\/\//.test(url)) {
        return [endpoint, getJsonConfig(config)];
      }
      return [endpoint, config];
    },
    async response(response) {
      if (!response.ok) {
        const errorBody = await response.json().then(res => res);
        const err = new Error(response.statusText);
        err.code = response.status;
        err.body = errorBody;

        throw err;
      }
      const type = response.headers.get('content-type');
      if (/application\/json/.test(type)) {
        return response.json();
      }
      return response.text();
    },
  });
