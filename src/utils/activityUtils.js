// @flow
import type { QuestionSet, Poll } from '../types/activities';
import { ACTIVITY_TYPES } from '../constants/activities';

function _buildEmptyQuestionSetActivityBody(): QuestionSet {
  return {
    questions: [],
  };
}

function _buildEmptyPollActivityBody(): Poll {
  return {
    title: '',
    choices: [],
  };
}

export function buildEmptyActivityBody(type: string) { // eslint-disable-line
  if (type === ACTIVITY_TYPES.QUESTION_SET) {
    return _buildEmptyQuestionSetActivityBody();
  } else if (type === ACTIVITY_TYPES.POLL) {
    return _buildEmptyPollActivityBody();
  }
  return null;
}
