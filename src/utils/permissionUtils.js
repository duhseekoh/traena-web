// @flow
import { PERMISSIONS } from '../constants/permissions';
import type { AuthUser } from '../types/users';

export function canModifyContent(
  customAccessData: any,
  user: AuthUser,
): boolean {
  if (
    customAccessData.authorId === user.details.traenaId &&
    user.permissions.includes(PERMISSIONS.CONTENT.MODIFY.SELF)
  ) {
    return true;
  }
  return false;
}

export function canDisableUser(customAccessData: any, user: AuthUser): boolean {
  if (
    customAccessData.userId !== user.details.traenaId &&
    user.permissions.includes(PERMISSIONS.USERS.MODIFY.ORG)
  ) {
    return true;
  }
  return false;
}

export function isOwnOrgChannel(
  customAccessData: any,
  user: AuthUser,
): boolean {
  if (customAccessData.organizationId === user.details.organizationId) {
    return true;
  }
  return false;
}

export function canModifySeries(
  customAccessData: any,
  user: AuthUser,
): boolean {
  if (
    customAccessData.organizationId === user.details.organizationId &&
    user.permissions.includes(PERMISSIONS.CONTENT.MODIFY.ORG)
  ) {
    return true;
  }
  return false;
}
