// @flow
import _ from 'lodash';
import { CONTENT_STATUSES } from '../constants/appConstants';
import { ACTIVITY_TYPES } from '../constants/activities';
import type { Activity, QuestionSetActivity, PollActivity } from '../types/activities';
import type { Series } from '../types/series';
import type { Channel } from '../types/channels';
import type { Subscription } from '../types/subscriptions';
// --- Field Level Validators
// --------------------------

export const required = (value: any) =>
  value && /([^\s])/.test(value) ? undefined : 'This field is required';

export const atLeastOneTag = (tags: string[]) => {
  if (!Array.isArray(tags) || tags.length === 0) {
    return 'At least one tag is required';
  }
  return undefined;
};

export const atLeastOneMedia = (body: Object) => {
  const hasImages = body.images && body.images.length > 0;
  const hasVideo = !!body.video;
  const hasText = !!body.text;
  const hasMedia = hasImages || hasVideo || hasText;
  return hasMedia ? undefined : 'Text is required if an image or video are not supplied';
};

// --- Form Level Syncronous Validators
// ------------------------------------
// Use as redux-form syncronous validation methods: http://redux-form.com/7.0.3/examples/syncValidation/

export const validateUserForm = (values: Object /* , props */) => {
  const errors = {};
  const requiredFirstNameError = required(values.firstName);
  if (requiredFirstNameError) {
    errors.firstName = requiredFirstNameError;
  }
  const requiredLastNameError = required(values.lastName);
  if (requiredLastNameError) {
    errors.lastName = requiredLastNameError;
  }
  const requiredEmailError = required(values.email);
  if (requiredEmailError) {
    errors.email = requiredEmailError;
  }
  const requiredPasswordError = required(values.password);
  if (requiredPasswordError) {
    errors.password = requiredPasswordError;
  }
  const requiredPositionError = required(values.position);
  if (requiredPositionError) {
    errors.position = requiredPositionError;
  }
  return errors;
};

/**
 * Validates a content form. When we are attempting to publish all the checks
 * run, otherwise we let the user save no matter what they've filled out.
 */
export const validateContentForm = (values: Object /* , props */) => {
  if (values.desiredStatus === CONTENT_STATUSES.DRAFT) {
    return {};
  }

  const errors: Object = { body: {} };

  const requiredChannelIdError = required(values.channelId);
  if (requiredChannelIdError) {
    errors.channelId = requiredChannelIdError;
  }

  const requiredTitleError = required(values.body.title);
  if (requiredTitleError) {
    errors.body.title = requiredTitleError;
  }

  const requiredMediaError = atLeastOneMedia(values.body);
  if (requiredMediaError) {
    errors.body.text = requiredMediaError;
  }

  const atLeastOneTagError = atLeastOneTag(values.tags);
  if (atLeastOneTagError) {
    errors.tags = atLeastOneTagError;
  }

  return errors;
};

const _validateQuestionSetActivityForm = (values: QuestionSetActivity) => {
  const errors = {};

  values.body.questions.forEach((question, i) => {
    const questionTextRequiredError = required(question.questionText);
    if (questionTextRequiredError) {
      _.set(
        errors,
        `body.questions[${i}].questionText`,
        questionTextRequiredError,
      );
    }
    question.choices.forEach((choice, j) => {
      const choiceTextRequiredError = required(choice.choiceText);
      if (choiceTextRequiredError) {
        _.set(
          errors,
          `body.questions[${i}].choices[${j}].choiceText`,
          choiceTextRequiredError,
        );
      }
    });
  });
  return errors;
};

const _validatePollActivityForm = (values: PollActivity) => {
  const errors = {};

  const pollTitleRequiredError = required(values.body.title);
  if (pollTitleRequiredError) {
    _.set(
      errors,
      'body.title',
      pollTitleRequiredError,
    );
  }
  values.body.choices.forEach((choice, j) => {
    const choiceTextRequiredError = required(choice.choiceText);
    if (choiceTextRequiredError) {
      _.set(
        errors,
        `body.choices[${j}].choiceText`,
        choiceTextRequiredError,
      );
    }
  });
  return errors;
};

export const validateActivityForm = (values: Activity) => {
  let errors;
  if (values.type === ACTIVITY_TYPES.QUESTION_SET) {
    errors = _validateQuestionSetActivityForm(values);
  } else if (values.type === ACTIVITY_TYPES.POLL) {
    errors = _validatePollActivityForm(values);
  }
  return errors;
};

export const validateSeriesForm = (values: Series) => {
  const errors = {};
  const requiredTitleError = required(values.title);
  if (requiredTitleError) {
    errors.title = requiredTitleError;
  }
  return errors;
};

export const validateChannelForm = (values: Channel) => {
  const errors = {};
  const requiredNameError = required(values.name);
  if (requiredNameError) {
    errors.name = requiredNameError;
  }
  return errors;
};

export const validateSubscriptionForm = (values: Subscription) => {
  const errors = {};
  const requiredVisibilityError = required(values.visibility);
  if (requiredVisibilityError) {
    errors.visibility = requiredVisibilityError;
  }
  return errors;
};
