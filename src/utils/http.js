/* eslint-disable guard-for-in,no-restricted-syntax,import/prefer-default-export */

// Sourced from https://github.com/github/fetch/issues/89#issuecomment-256610849
export function fetchWithProgress(url, opts = {}, onProgress) {
  return new Promise((res, rej) => {
    const xhr = new XMLHttpRequest();
    xhr.open(opts.method || 'get', url);
    for (const k in opts.headers || {}) {
      xhr.setRequestHeader(k, opts.headers[k]);
    }
    xhr.onload = e => {
      if (e.target.status >= 200 && e.target.status <= 299) {
        res(e.target.responseText);
      } else {
        rej(e.target);
      }
    };
    xhr.onerror = rej;
    if (xhr.upload && onProgress) {
      xhr.upload.onprogress = onProgress;
    }
    xhr.send(opts.body);
  });
}
