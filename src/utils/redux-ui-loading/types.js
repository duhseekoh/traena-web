// @flow
export type createUiLoadingActionsOptions = {};
export type ReduxStateUiLoading = {
  loading: boolean,
  saving: boolean,
  errorLoading: boolean,
  errorSaving: boolean,
};
