// @flow
import type { ReduxStateUiLoading } from './types';

export const initialUiLoadingState: ReduxStateUiLoading = {
  loading: false,
  saving: false,
  errorLoading: false,
  errorSaving: false,
};

/**
* Creates a namespaced reducer to support loading/saving/error states for a ui
* @param reducerKey namespace for the uiLoading reducer.  Should be the same as the `reducerKeys` given to `createUiLoadingActions(reducerKey)`.
* @param initialSortState states for { loading, saving, error }
*/
export function createUiLoadingReducer( // eslint-disable-line
  reducerKey: string = '',
  initialState: ReduxStateUiLoading = initialUiLoadingState,
) {
  return function uiLoading(
    uiLoadingState: ReduxStateUiLoading = initialState,
    action: { type: string, payload: any },
  ) {
    const { payload } = action;
    switch (action.type) {
      case `${reducerKey}/SET_LOADING`:
        const { loading } = payload;
        return {
          ...uiLoadingState,
          loading,
        };

      case `${reducerKey}/SET_SAVING`:
        const { saving } = payload;
        return {
          ...uiLoadingState,
          saving,
        };

      case `${reducerKey}/SET_ERROR_LOADING`:
        const { errorLoading } = payload;
        return {
          ...uiLoadingState,
          errorLoading,
        };

      case `${reducerKey}/SET_ERROR_SAVING`:
        const { errorSaving } = payload;
        return {
          ...uiLoadingState,
          errorSaving,
        };

      default:
        return uiLoadingState;
    }
  };
}
