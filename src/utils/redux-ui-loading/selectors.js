// @flow
import type { ReduxStateUiLoading } from './types';
import { initialUiLoadingState } from './reducer';

type ReduxState = {
  [reducerKey: string]: {
    uiLoading?: ReduxStateUiLoading,
  },
};

/**
* Builds a set of selectors with which you can retrieve the ui-loading state (i.e. { loading, saving, error }) for a given reducer property
* @param {string} reducerKey the key within `state` where we have ui-loading information.  (This should be the
* same as the `reducerKey` passed to `createUiLoadingActions`)
*/
export const createUiLoadingSelectors = (reducerKey: string = '') => { // eslint-disable-line
  const _selectUiLoading = (state: ReduxState) => {
    const stateObj = state[`${reducerKey}`];
    if (!stateObj || !stateObj.uiLoading) {
      return initialUiLoadingState;
    }
    return stateObj.uiLoading;
  };

  return {
    selectUiLoading: (state: ReduxState) => _selectUiLoading(state),
    selectIsLoading: (state: ReduxState) => _selectUiLoading(state).loading,
    selectIsSaving: (state: ReduxState) => _selectUiLoading(state).saving,
    selectErrorLoading: (state: ReduxState) =>
      _selectUiLoading(state).errorLoading,
    selectErrorSaving: (state: ReduxState) =>
      _selectUiLoading(state).errorSaving,
  };
};
