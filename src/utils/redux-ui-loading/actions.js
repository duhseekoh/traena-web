
/**
* Creates a namespaced set of actions related to ui state surrounding loading/saving/initializing
* @param {string} reducerKey namespace for the sort actions.  Should be the same as the `reducerKey` given to `createLoadingReducer(reducerKey)`.
*/
export const createUiLoadingActions = ( // eslint-disable-line
  reducerKey: string = '',
) => ({
  /**
  * Sets the loading state of a redux module
  * @param loading should the ui be in a loading state or not
  */
  setLoading: (loading: boolean) => async (dispatch: Dispatch) =>
    dispatch({
      type: `${reducerKey}/SET_LOADING`,
      payload: {
        loading,
      },
    }),

  /**
  * Sets the saving state of a redux module
  * @param saving should the ui be in a saving state or not
  */
  setSaving: (saving: boolean) => async (dispatch: Dispatch) =>
    dispatch({
      type: `${reducerKey}/SET_SAVING`,
      payload: {
        saving,
      },
    }),

  /**
  * Sets the error state of a redux module
  * @param error did the data fail to load
  */
  setErrorLoading: (errorLoading: boolean) => async (dispatch: Dispatch) =>
    dispatch({
      type: `${reducerKey}/SET_ERROR_LOADING`,
      payload: {
        errorLoading,
      },
    }),


  /**
  * Sets the error saving of a redux module.
  * @param error did the data fail to save
  */
  setErrorSaving: (errorSaving: boolean) => async (dispatch: Dispatch) =>
    dispatch({
      type: `${reducerKey}/SET_ERROR_SAVING`,
      payload: {
        errorSaving,
      },
    }),


  uiLoadingActionTypes: {
    setLoading: `${reducerKey}/SET_LOADING`,
    setSaving: `${reducerKey}/SET_SAVING`,
    setErrorLoading: `${reducerKey}/SET_ERROR_LOADING`,
    setErrorSaving: `${reducerKey}/SET_ERROR_SAVING`,
  },
});
