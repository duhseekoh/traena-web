/* eslint-disable import/prefer-default-export */
// @flow
export const generateContentURL = (itemId: number | string) => {
  const { origin } = window.location;
  return `${origin}/content/${itemId}/view`;
};
