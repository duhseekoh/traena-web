import 'whatwg-fetch';
import 'babel-polyfill'; // eslint-disable-line
import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import configureStore from './store/configureStore';
import Routes from './Routes';
import initInterceptor from './utils/httpInterceptor';
// Disabling because of all the issues in Creact React App around service worker caching
import { unregister } from './registerServiceWorker';
import history from './history';
import './index.scss';

initInterceptor();

const store = configureStore(history);

ReactDOM.render(<Routes store={store} />, document.getElementById('root'));

unregister();
