// @flow
import React from 'react';
import type { Store } from 'redux';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import AppBarAdmin from './components/AppBar/AppBarAdmin';
import AppBar from './components/AppBar/AppBar';
import App from './pages/app/App';
import Root from './pages/root/Root';
import Login from './pages/login/Login';
import LoginCallback from './pages/loginCallback/LoginCallback';
import Logout from './pages/logout/Logout';
import ChannelListPage from './pages/channels/ChannelListPage';
import Content from './pages/content/Content';
import ContentItemAnalyticsPage from './pages/content-item-analytics-page/ContentItemAnalyticsPage';
import ContentDetailPage from './pages/content-detail-page/ContentDetailPage';
import SeriesListPage from './pages/series-list/SeriesListPage';
import SeriesDetailPage from './pages/series-detail-page/SeriesDetailPage';
import CreateUserPage from './pages/create-user-page/CreateUserPage';
import EditUserPage from './pages/edit-user-page/EditUserPage';
import UserManagement from './pages/user-management/UserManagement';
import CreateContentPage from './pages/create-content-page/CreateContentPage';
import AddActivityPage from './pages/add-activity-page/AddActivityPage';
import EditActivityPage from './pages/edit-activity-page/EditActivityPage';
import EditContentPage from './pages/edit-content-page/EditContentPage';
import ViewContentPage from './pages/view-content-page/ViewContentPage';
import UserRolesPage from './pages/user-roles-page/UserRolesPage';
import NotFoundPage from './pages/errors/NotFoundPage';
import DownloadAppPage from './pages/errors/DownloadAppPage';
import CreateSeriesPage from './pages/create-series-page/CreateSeriesPage';
import EditSeriesPage from './pages/edit-series-page/EditSeriesPage';
import SeriesStatsPage from './pages/series-stats-page/SeriesStatsPage';
import ActivityResultsPage from './pages/activity-results-page/ActivityResultsPage';
import SubscriptionDetailsPage from './pages/subscription-details/SubscriptionDetailsPage';
import CreateChannelPage from './pages/create-channel/CreateChannelPage';

import { routes } from './constants/routes';
import history from './history';
import requireAuthentication from './hoc/requireAuthentication';
import { PERMISSIONS } from './constants/permissions';
import withAccessControl from './hoc/withAccessControl';

type Props = {
  store: Store<*, *>,
};

export default function Routes(props: Props) {
  return (
    <Provider store={props.store}>
      <ConnectedRouter history={history}>
        <App>
          <Switch>
            <Route exact path={routes.LOGIN.path} component={Login} />
            <Route exact path={routes.CONTENT_VIEW.path}>
              <div>
                <AppBar />
                <Route
                  exact
                  path={routes.CONTENT_VIEW.path}
                  component={requireAuthentication(
                    withAccessControl([PERMISSIONS.CONTENT.READ.ORG])(
                      ViewContentPage,
                    ),
                  )}
                />
              </div>
            </Route>
            <Route path={routes.ROOT.path}>
              {/*
                List of routes that need our main app navbar.
                The route specific content must still be listed in another <Route> below.
              */}
              <div>
                {[
                  routes.CONTENT_EDIT.path,
                  routes.CONTENT_CREATE.path,
                  routes.CONTENT_DETAIL.path,
                  routes.CONTENT_STATS.path,
                  routes.CONTENT.path,
                  routes.SERIES.path,
                  routes.SERIES_DETAIL.path,
                  routes.SERIES_STATS.path,
                  routes.SERIES_CREATE.path,
                  routes.SERIES_EDIT.path,
                  routes.CHANNELS.path,
                  routes.USERS.path,
                  routes.USERS_CREATE.path,
                  routes.USERS_EDIT.path,
                  routes.USERS_ROLES.path,
                  routes.CHANNEL_EDIT.path,
                  routes.SUBSCRIPTION_DETAILS.path,
                ].map(path => (
                  <Route
                    key={path}
                    exact
                    path={path}
                    component={AppBarAdmin}
                  />
                ))}

                <Switch>
                  <Route exact path={routes.ROOT.path} component={Root} />
                  <Route
                    exact
                    path={routes.LOGIN_CALLBACK.path}
                    component={LoginCallback}
                  />
                  <Route exact path={routes.LOGOUT.path} component={Logout} />
                  <Route
                    exact
                    path={routes.CONTENT_EDIT.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        EditContentPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CONTENT_DETAIL.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        ContentDetailPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CONTENT_CREATE.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        CreateContentPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CONTENT_STATS.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.CONTENT.MODIFY.SELF,
                    ])(ContentItemAnalyticsPage))}
                  />
                  <Route
                    exact
                    path={routes.CONTENT.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        Content,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CONTENT_ACTIVITY_ADD.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        AddActivityPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CONTENT_ACTIVITY_EDIT.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        EditActivityPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.ACTIVITY_RESULTS.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.CONTENT.MODIFY.SELF])(
                        ActivityResultsPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.SERIES.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(SeriesListPage))}
                  />
                  <Route
                    exact
                    path={routes.SERIES_DETAIL.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(SeriesDetailPage))}
                  />
                  <Route
                    exact
                    path={routes.SERIES_STATS.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(SeriesStatsPage))}
                  />
                  <Route
                    exact
                    path={routes.SERIES_CREATE.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(CreateSeriesPage))}
                  />
                  <Route
                    exact
                    path={routes.SERIES_EDIT.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(EditSeriesPage))}
                  />
                  <Route
                    exact
                    path={routes.USERS.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        UserManagement,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.USERS_CREATE.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        CreateUserPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.USERS_EDIT.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        EditUserPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.USERS_ROLES.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        UserRolesPage,
                      ),
                    )}
                  />
                  <Route
                    exact
                    path={routes.CHANNELS.path}
                    component={requireAuthentication(withAccessControl([
                      PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                    ])(ChannelListPage))}
                  />
                  <Route
                    exact
                    path={routes.CHANNEL_CREATE.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        CreateChannelPage,
                      ),
                    )}
                  />
                  <Route
                    path={routes.SUBSCRIPTION_DETAILS.path}
                    component={requireAuthentication(
                      withAccessControl([PERMISSIONS.USERS.MODIFY.ORG])(
                        SubscriptionDetailsPage,
                      ),
                    )}
                  />
                  <Route
                    path={routes.CONTENT.path}
                    component={DownloadAppPage}
                  />
                  <Route component={NotFoundPage} />
                </Switch>
              </div>
            </Route>
          </Switch>
        </App>
      </ConnectedRouter>
    </Provider>
  );
}
