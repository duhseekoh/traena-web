// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiChannelContentTableActions from '../../redux/modules/ui-channel-content-table/actions';
import * as fromUiChannelContentTable from '../../redux/modules/ui-channel-content-table/selectors';
import type { SortDirections } from '../../utils/redux-sort/types';

import styles from './ChannelContentTableContainer.scss';

type OwnProps = {
  channelId: number,
  renderTable: (props: any) => React.DOM.Node,
};

type ConnectedProps = {
  loading: boolean,
  initializeChannelContentTable: (channelId: number) => any,
  error: ?string,
  setPage: (page: number) => any,
  setChannelContentTableSort: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
};

type Props = OwnProps & ConnectedProps;

class ChannelContentTableContainer extends Component<Props> {
  props: Props;
  componentDidMount = () => {
    this.props.initializeChannelContentTable(this.props.channelId);
  }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.channelId !== this.props.channelId) {
      this.props.initializeChannelContentTable(nextProps.channelId);
    }
  }
  goToPage = (page: number) => {
    this.props.setPage(page);
    this.props.initializeChannelContentTable(this.props.channelId);
  }
  sortTable = (property: string) => {
    this.props.setChannelContentTableSort(property);
    this.props.initializeChannelContentTable(this.props.channelId);
  }
  render() {
    const { props } = this;
    return (
      <div className={styles.contentContainer}>
        {this.props.error &&
          'There was a problem loading content for this channel.'
        }
        {this.props.loading ?
          <Loader active />
          :
          this.props.renderTable({
            ...props,
            goToPage: this.goToPage,
            sortTable: this.sortTable,
            getSortDirection: this.props.getSortDirection,
          })
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  content: fromUiChannelContentTable.selectContentItems(state),
  pagination: fromUiChannelContentTable.selectPagination(state),
  loading: fromUiChannelContentTable.selectLoading(state),
  error: fromUiChannelContentTable.selectError(state),
  getSortDirection: property =>
    fromUiChannelContentTable.getSortDirectionLongName(state, property),
});

const mapDispatchToProps = dispatch => ({
  initializeChannelContentTable: channelId =>
    dispatch(uiChannelContentTableActions
      .initializeChannelContentTable(channelId)),
  setPage: (page: number) =>
    dispatch(uiChannelContentTableActions.setChannelContentPage(page)),
  setChannelContentTableSort: property =>
    dispatch(uiChannelContentTableActions.setChannelContentTableSort(property)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChannelContentTableContainer);
