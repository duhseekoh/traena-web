// @flow
import React from 'react';
import { Grid } from 'semantic-ui-react';
import Linkify from 'linkifyjs/react';
import VideoPlayer from '../../../components/VideoPlayer/VideoPlayer';
import contentContainerStyles from '../ContentContainer.scss';
import styles from './ContentTypeVideoView.scss';
import type { VideoContentItem } from '../../../types/content';

type Props = {
  contentItem: VideoContentItem,
};

const ContentTypeVideoView = (props: Props) => (
  <Grid>
    <Grid.Row centered>
      <Grid.Column computer={12} tablet={14} mobile={16}>
        <div className={styles.videoPlayerContainer}>
          <VideoPlayer
            video={props.contentItem.body.video}
            autoplay
          />
        </div>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row centered>
      <Grid.Column computer={9} tablet={13} mobile={16}>
        <h2 className={contentContainerStyles.contentTitle}>
          {props.contentItem.body.title}
        </h2>
        <Linkify className={contentContainerStyles.contentDescription}>
          {props.contentItem.body.text}
        </Linkify>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default ContentTypeVideoView;
