// @flow
import React from 'react';
import { Grid } from 'semantic-ui-react';
import Linkify from 'linkifyjs/react';
import continerStyles from '../ContentContainer.scss';
import '../../../../node_modules/slick-carousel/slick/slick.css';
import '../../../../node_modules/slick-carousel/slick/slick-theme.css';
import type { ImageContentItem } from '../../../types/content';
import ImageSlider from '../../../components/ImageSlider/ImageSlider';

type Props = {
  contentItem: ImageContentItem,
};


const ContentTypeImageView = (props: Props) => {
  const { images, title, text } = props.contentItem.body;
  return (
    <Grid>
      <Grid.Row centered>
        <Grid.Column computer={12} tablet={14} mobile={16}>
          <ImageSlider images={images} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row centered>
        <Grid.Column computer={9} tablet={13} mobile={16}>
          <h2 className={continerStyles.contentTitle}>{title}</h2>
          <Linkify className={continerStyles.contentDescription}>
            {text}
          </Linkify>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default ContentTypeImageView;
