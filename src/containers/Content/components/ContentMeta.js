// @flow
import React from 'react';
import moment from 'moment';
import { Grid } from 'semantic-ui-react';
import DateLong from '../../../components/Date/DateLong';
import styles from '../ContentContainer.scss';
import type { Channel } from '../../../types/channels';

type Props = {
  tags: string[],
  author: any,
  timestamp: string,
  channel: Channel,
};

const ContentMeta = (props: Props) => (
  <Grid>
    <Grid.Row className={styles.metaRow} centered>
      <Grid.Column computer={3} tablet={3} mobile={4}>
        <h4 className={styles.metaTitle}>AUTHOR</h4>
        <div className={styles.metaText}>
          {props.author.firstName} {props.author.lastName}{' '}
          <span className={styles.metaSubtext}>
            {props.author.organization.name}
          </span>
        </div>
        <div className={styles.metaSubtext}>{props.channel.name}</div>
      </Grid.Column>
      <Grid.Column computer={6} tablet={6} mobile={7}>
        <h4 className={styles.metaTitle}>TAGS</h4>
        <div className={styles.tagContainer}>
          {props.tags.map(tag => (
            <div key={tag} className={styles.tag}>
              #{tag}
            </div>
          ))}
        </div>
      </Grid.Column>
      <Grid.Column computer={3} tablet={3} mobile={4}>
        <h4 className={styles.metaTitle}>PUBLISHED</h4>
        <div className={styles.metaText}>
          <DateLong date={props.timestamp} />
        </div>
        <div className={styles.metaSubtext}>
          {moment().diff(moment(props.timestamp), 'days')} days ago
        </div>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default ContentMeta;
