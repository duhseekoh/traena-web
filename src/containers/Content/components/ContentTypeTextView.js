// @flow
import React from 'react';
import { Grid } from 'semantic-ui-react';
import Linkify from 'linkifyjs/react';
import styles from '../ContentContainer.scss';
import type { TextContentItem } from '../../../types/content';

type Props = {
  contentItem: TextContentItem,
};

const ContentTypeTextView = (props: Props) => (
  <Grid>
    <Grid.Row centered>
      <Grid.Column computer={9} mobile={16}>
        <h2 className={styles.contentTitle}>{props.contentItem.body.title}</h2>
        <Linkify className={styles.contentDescription}>
          {props.contentItem.body.text}
        </Linkify>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default ContentTypeTextView;
