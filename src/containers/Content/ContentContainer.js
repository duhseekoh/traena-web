// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as uiContentViewActions from '../../redux/modules/ui-content-view/actions';
import * as fromContent from '../../redux/modules/content/selectors';
import * as fromContentView from '../../redux/modules/ui-content-view/selectors';
import ContentTypeVideoView from './components/ContentTypeVideoView';
import ContentTypeTextView from './components/ContentTypeTextView';
import ContentTypeImageView from './components/ContentTypeImageView';
import ContentMeta from './components/ContentMeta';
import styles from './ContentContainer.scss';
import type {
  ContentItem,
  VideoContentItem,
  TextContentItem,
  ImageContentItem,
} from '../../types/content';

type OwnProps = {
  contentId: number,
};

type ConnectedProps = {
  contentItem?: ContentItem,
  loadContentItem: (contentId: number) => any,
  isLoading: boolean,
  errorInitializing: boolean,
};

type Props = OwnProps & ConnectedProps;

class ContentContainer extends Component<Props> {
  props: Props;

  componentWillMount() {
    if (!this.props.contentId) {
      return;
    }
    this.props.loadContentItem(this.props.contentId);
  }

  render() {
    const { contentItem } = this.props;
    let content;
    if (this.props.isLoading) {
      content = <div className={styles.pageMessage}>Loading...</div>;
    }
    if (this.props.errorInitializing) {
      content = (
        <div className={styles.pageMessage}>
          Whoops! This content is not available...
        </div>
      );
    }
    if (contentItem) {
      if (contentItem.body.type === 'TrainingVideo') {
        // TODO, there's a better way than having to cast these to a more specific type.
        // But it requires having a distinguishable property on the root of the ContentItemGeneric.
        // In retrospect, the 'type' field we currently have on 'body' should be at the root level.
        const videoContentItem: VideoContentItem = (contentItem: any);
        content = <ContentTypeVideoView contentItem={videoContentItem} />;
      } else if (contentItem.body.type === 'DailyAction') {
        const textContentItem: TextContentItem = (contentItem: any);
        content = <ContentTypeTextView contentItem={textContentItem} />;
      } else if (contentItem.body.type === 'Image') {
        const imageContentItem: ImageContentItem = (contentItem: any);
        content = <ContentTypeImageView contentItem={imageContentItem} />;
      }
    }
    return (
      <div className={styles.contentContainer}>
        {this.props.contentItem && (
          <ContentMeta
            desc={this.props.contentItem.body.text}
            tags={this.props.contentItem.tags}
            author={this.props.contentItem.author}
            channel={this.props.contentItem.channel}
            timestamp={this.props.contentItem.publishedTimestamp}
          />
        )}
        <div className={styles.pageBreak} />
        {content}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  contentItem: fromContent.selectContentItemById(state, props),
  isLoading: fromContentView.selectIsContentViewLoading(state),
  errorInitializing: fromContentView.selectContentViewHasError(state),
});

const mapDispatchToProps = dispatch => ({
  loadContentItem: contentId =>
    dispatch(uiContentViewActions.loadContentItem(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContentContainer);
