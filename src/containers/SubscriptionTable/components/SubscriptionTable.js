// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import styles from './SubscriptionTable.scss';
import SubscriptionTableRow from './SubscriptionTableRow';
import Pagination from '../../../components/Pagination/Pagination';

import type { Pagination as PaginationType } from '../../../types';
import type { Subscription } from '../../../types/subscriptions';

type Props = {
  subscriptions: Subscription[],
  pagination: PaginationType,
  goToSubscriptionPage: (page: number) => any,
  userOrganizationId: number,
  onPressTableRow: (channelId: number) => any,
};

const SubscriptionTable = (props: Props) => {
  if (!props.subscriptions.length) {
    return (
      <h2 className={styles.channelTableNoContentMessage}>
        No channels for these filters...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.channelTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell {...props} property="channel.name">
              Name & Description
            </TableHeaderCell>
            <TableHeaderCell {...props} property="visibility">
              Audience
            </TableHeaderCell>
            <TableHeaderCell
              {...props}
              property="channel.createdTimestamp"
              className={styles.alignRight}
            >
              Created
            </TableHeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.subscriptions.map(subscription => (
            <SubscriptionTableRow
              key={subscription.channelId}
              subscription={subscription}
              userOrganizationId={props.userOrganizationId}
              onPressTableRow={props.onPressTableRow}
            />
          ))}
        </Table.Body>
      </Table>
      <Pagination
        pagination={props.pagination}
        goToPage={props.goToSubscriptionPage}
      />
    </div>
  );
};

SubscriptionTable.defaultProps = {
  subscriptions: [],
};

export default SubscriptionTable;
