// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import classnames from 'classnames';
import type { Subscription } from '../../../types/subscriptions';
import DateShort from '../../../components/Date/DateShort';
import TableRow from '../../../components/Table/TableRow';

import styles from './SubscriptionTable.scss';

type Props = {
  subscription: Subscription,
  onPressTableRow: (channelId: number) => any,
};

const SubscriptionTableRow = (props: Props) => {
  const { subscription, onPressTableRow } = props;
  const { channel } = subscription;
  const visibilityEnum = {
    ADMINS: 'None',
    ORGANIZATION: 'All Org',
    USERS: 'Private',
  };
  return (
    <TableRow
      key={channel.id}
      onClick={() => onPressTableRow(channel.id)}
    >
      <Table.Cell>
        <div className={styles.boldText}>{channel.name}</div>
        <div className={styles.subText}>{channel.description}</div>
      </Table.Cell>
      <Table.Cell>{visibilityEnum[subscription.visibility]}</Table.Cell>
      <Table.Cell
        className={
          !channel.createdTimestamp
            ? classnames(styles.fadedCell, styles.alignRight)
            : styles.alignRight
        }
      >
        {channel.createdTimestamp ? (
          <DateShort date={channel.createdTimestamp} />
        ) : (
          'None'
        )}
      </Table.Cell>
    </TableRow>
  );
};

export default SubscriptionTableRow;
