/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { Grid, Loader, Button, Dropdown } from 'semantic-ui-react';
import classnames from 'classnames';
import * as uiSubscriptionActions from '../../redux/modules/ui-subscriptions-table/actions';
import * as fromAuth from '../../redux/modules/auth/selectors';
import * as fromUiSubscriptionTable from '../../redux/modules/ui-subscriptions-table/selectors';
import SubscriptionTable from './components/SubscriptionTable';
import ExternalSubscriptionTable from './components/ExternalSubscriptionTable';
import tabStyles from '../../components/TabButtonGroup/TabButtonGroup.scss';
import { routes, getRouteLink } from '../../constants/routes';
import type { Subscription, SubscriptionFilters } from '../../types/subscriptions';
import type { Pagination as PaginationType } from '../../types';
import type { SortDirections } from '../../utils/redux-sort/types';
import {
  CHANNEL_VISIBILITY_FILTERS,
  SUBSCRIPTION_SOURCES,
} from '../../constants/appConstants';

type Props = {
  fetchSubscriptionTable: () => Promise<any>,
  setSubscriptionTablePage: (page: number) => any,
  setSubscriptionTableSourceFilter: (source: string) => any,
  subscriptions: Subscription[],
  pagination: PaginationType,
  subscriptionFilters: SubscriptionFilters,
  isSubscriptionTableLoading: boolean,
  userOrganizationId: number,
  setSubscriptionTableVisibilityFilter: (visibility: string) => any,
  handleTableRowClick: (url: string) => any,
  getSortDirection: (property: string) => SortDirections,
  setSubscriptionTableSort: (property: string) => any,
};

class SubscriptionTableContainer extends Component<Props> {
  props: Props;

  static defaultProps;

  componentDidMount() {
    this.props.fetchSubscriptionTable();
  }

  goToSubscriptionPage = (page: number) => {
    this.props.setSubscriptionTablePage(page);
    this.props.fetchSubscriptionTable();
  };

  includeSubsctiptionsFromSource = source => {
    this.props.setSubscriptionTableSourceFilter(source);
    this.props.setSubscriptionTablePage(0);
    this.props.fetchSubscriptionTable();
  };

  handleToggleChange = (e, data) => {
    this.props.setSubscriptionTableVisibilityFilter(data.value);
    this.props.setSubscriptionTablePage(0);
    this.props.fetchSubscriptionTable();
  };

  handleTableRowClick = (channelId: number) => {
    this.props.handleTableRowClick(
      getRouteLink(routes.SUBSCRIPTION_DETAILS, { channelId }),
    );
  };

  sortTable = (property: string) => {
    this.props.setSubscriptionTableSort(property);
    this.props.fetchSubscriptionTable();
  };

  renderTable() {
    const { subscriptionFilters } = this.props;
    if (subscriptionFilters.source === SUBSCRIPTION_SOURCES.EXTERNAL) {
      return (
        <ExternalSubscriptionTable
          goToSubscriptionPage={this.goToSubscriptionPage}
          subscriptions={this.props.subscriptions}
          pagination={this.props.pagination}
          userOrganizationId={this.props.userOrganizationId}
          onPressTableRow={this.handleTableRowClick}
          sortTable={this.sortTable}
          getSortDirection={this.props.getSortDirection}
        />
      );
    }
    return (
      <SubscriptionTable
        goToSubscriptionPage={this.goToSubscriptionPage}
        subscriptions={this.props.subscriptions}
        pagination={this.props.pagination}
        userOrganizationId={this.props.userOrganizationId}
        onPressTableRow={this.handleTableRowClick}
        sortTable={this.sortTable}
        getSortDirection={this.props.getSortDirection}
      />
    );
  }

  render() {
    const channelVisibility = [
      { text: 'All Internal Channels', value: CHANNEL_VISIBILITY_FILTERS.ALL },
      { text: 'Internal Only', value: CHANNEL_VISIBILITY_FILTERS.INTERNAL },
      { text: 'Shared with Others', value: CHANNEL_VISIBILITY_FILTERS.SHARED },
    ];
    const activeTabStyle = classnames(tabStyles.tabButton, tabStyles.active);
    const { subscriptionFilters, isSubscriptionTableLoading } = this.props;

    return (
      <div>
        <Grid stackable>
          {/* Title and Create button */}
          <Grid.Row verticalAlign="bottom">
            <Grid.Column width={11}>
              <h1>Channels</h1>
            </Grid.Column>
            <Grid.Column width={5} textAlign="right">
              <Link to={getRouteLink(routes.CHANNEL_CREATE)}>
                <Button size="big" primary>Create New</Button>
              </Link>
            </Grid.Column>
          </Grid.Row>
          {/* Filters */}
          <Grid.Row verticalAlign="middle">
            <Grid.Column width={11}>
              <button
                className={
                  subscriptionFilters.source ===
                  SUBSCRIPTION_SOURCES.INTERNAL
                    ? activeTabStyle
                    : tabStyles.tabButton
                }
                onClick={() => {
                  this.includeSubsctiptionsFromSource(
                    SUBSCRIPTION_SOURCES.INTERNAL,
                  );
                }}
              >
                Internal
              </button>
              <button
                className={
                  subscriptionFilters.source ===
                  SUBSCRIPTION_SOURCES.EXTERNAL
                    ? activeTabStyle
                    : tabStyles.tabButton
                }
                onClick={() => {
                  this.includeSubsctiptionsFromSource(
                    SUBSCRIPTION_SOURCES.EXTERNAL,
                  );
                }}
              >
                External
              </button>
            </Grid.Column>
            <Grid.Column width="5" textAlign="right">
              {subscriptionFilters.source === SUBSCRIPTION_SOURCES.INTERNAL &&
                <Dropdown
                  defaultValue={subscriptionFilters.visibility}
                  selection
                  options={channelVisibility}
                  onChange={this.handleToggleChange}
                />
              }
            </Grid.Column>
          </Grid.Row>
          {/* Table */}
          <Grid.Row stretched>
            <Grid.Column>
              {isSubscriptionTableLoading ? (
                <Loader active />
              ) : this.renderTable()}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

SubscriptionTableContainer.defaultProps = {
  pagination: {},
  subscriptions: [],
};

const mapStateToProps = state => ({
  pagination: state.uiSubscriptionTable.pagination,
  subscriptions: fromUiSubscriptionTable.selectSubscriptions(state),
  subscriptionFilters:
    fromUiSubscriptionTable.selectCurrentSubscriptionFilters(state),
  isSubscriptionTableLoading:
    fromUiSubscriptionTable.selectIsLoading(state),
  userOrganizationId: fromAuth.selectOrganizationId(state),
  getSortDirection: property =>
    fromUiSubscriptionTable.getSortDirectionLongName(state, property),
});

const mapDispatchToProps = dispatch => ({
  fetchSubscriptionTable: () =>
    dispatch(uiSubscriptionActions.fetchSubscriptionTable()),
  setSubscriptionTablePage: page =>
    dispatch(uiSubscriptionActions.setSubscriptionTablePage(page)),
  setSubscriptionTableSourceFilter: source =>
    dispatch(uiSubscriptionActions.setSubscriptionTableSourceFilter(source)),
  setSubscriptionTableVisibilityFilter: visibility =>
    dispatch(
      uiSubscriptionActions.setSubscriptionTableVisibilityFilter(visibility),
    ),
  handleTableRowClick: url => dispatch(push(url)),
  setSubscriptionTableSort: property =>
    dispatch(uiSubscriptionActions.setSubscriptionTableSort(property)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubscriptionTableContainer);
