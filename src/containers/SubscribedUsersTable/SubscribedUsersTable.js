// @flow

import React, { Component } from 'react';
import type { ContextRouter } from 'react-router';
import { connect } from 'react-redux';
import { Loader, Table } from 'semantic-ui-react';
import styles from './SubscribedUsersTable.scss';
import Pagination from '../../components/Pagination/Pagination';
import TableHeaderCell from '../../components/Table/TableHeaderCell';
import type { Pagination as PaginationType } from '../../types';
import type { User } from '../../types/users';

import {
  fetchSubscribedUsersTable,
  setSubscribedUsersPage,
} from '../../redux/modules/ui-subscribed-users-table/actions';
import * as fromUiSubscribedUsersTable from '../../redux/modules/ui-subscribed-users-table/selectors';

type OwnProps = {
  channelId: number,
};

type ConnectedProps = {
  users: User[],
  pagination: PaginationType,
  loading: boolean,
  error: boolean,
  fetchSubscribedUsersTable: (channelId: number) => any,
  setSubscribedUsersPage: (page: number) => any,
};

type Props = OwnProps & ConnectedProps & ContextRouter;

class SubscribedUsersTable extends Component<Props> {
  props: Props;
  componentDidMount() {
    this.props.fetchSubscribedUsersTable(
      Number(this.props.channelId),
    );
  }

  goToSubscribedUsersPage = (page: number) => {
    this.props.setSubscribedUsersPage(page);
    this.props.fetchSubscribedUsersTable(
      Number(this.props.channelId),
    );
  };

  render() {
    const { loading, users, error, pagination } = this.props;

    const noSubscribers = !loading && !error && !users.length;
    const subscribersLoaded = !error && !noSubscribers;
    return (
      <div className={styles.subscribersTableWrapper}>
        {loading ?
          <Loader active /> :
          <div>
            {error &&
              <h3 className={styles.subscribersTableNoContentMessage}>
                Error loading subscribed users...
              </h3>
            }

            {noSubscribers &&
              <h3 className={styles.subscribersTableNoContentMessage}>
                No users have been subscribed to this channel yet...
              </h3>
            }

            {subscribersLoaded &&
              <div>
                <Table>
                  <Table.Header>
                    <Table.Row>
                      <TableHeaderCell>Name</TableHeaderCell>
                      <TableHeaderCell>Title</TableHeaderCell>
                      <TableHeaderCell>Progress</TableHeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {users.map(user => (
                      <tr key={user.id}>
                        <td>{user.firstName} {user.lastName}</td>
                        <td>{user.position}</td>
                        <td>
                          {(user.tags && user.tags.length > 0) &&
                            `#${user.tags.join(' #')}`}
                        </td>
                      </tr>
                    ))}
                  </Table.Body>
                </Table>

                <Pagination
                  pagination={pagination}
                  goToPage={this.goToSubscribedUsersPage}
                />
              </div>
            }
          </div>}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  users: fromUiSubscribedUsersTable.selectUsers(state),
  loading: fromUiSubscribedUsersTable
    .selectIsSubscribedUsersTableLoading(state),
  error: fromUiSubscribedUsersTable.selectSubscribedUsersTableError(state),
  pagination: fromUiSubscribedUsersTable.selectPagination(state),
});

const mapDispatchToProps = dispatch => ({
  fetchSubscribedUsersTable: (channelId: number) =>
    dispatch(fetchSubscribedUsersTable(channelId)),
  setSubscribedUsersPage: page =>
    dispatch(setSubscribedUsersPage(page)),
});

// eslint-disable-next-line
export default connect(mapStateToProps, mapDispatchToProps)(SubscribedUsersTable);
