// @flow
import React, { Component } from 'react';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiSeriesContentTableActions from '../../redux/modules/ui-series-content-table/actions';
import * as fromUiSeriesContentTable from '../../redux/modules/ui-series-content-table/selectors';
import { routes, getRouteLink } from '../../constants/routes';
import styles from './SeriesContentTableContainer.scss';

type OwnProps = {
  seriesId: number,
  renderTable: (props: any) => React.DOM.Node,
};

type ConnectedProps = {
  loading: boolean,
  initializeSeriesContentTable: (seriesId: number) => any,
  error: ?string,
  handleClickRow: (contentId: number) => any,
};

type Props = OwnProps & ConnectedProps;

class SeriesContentTableContainer extends Component<Props> {
  props: Props;

  componentDidMount = () => {
    this.props.initializeSeriesContentTable(this.props.seriesId);
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.seriesId !== this.props.seriesId) {
      this.props.initializeSeriesContentTable(nextProps.seriesId);
    }
  }

  render() {
    const { props } = this;
    return (
      <div className={styles.contentContainer}>
        {this.props.error &&
          'There was a problem loading content for this channel.'
        }
        {this.props.loading ?
          <Loader active />
          :
          this.props.renderTable({
            ...props,
            onPressTableRow: this.props.handleClickRow,
          })
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  contents: fromUiSeriesContentTable.selectContentItems(state),
  loading: fromUiSeriesContentTable.selectLoading(state),
  error: fromUiSeriesContentTable.selectError(state),
});

const mapDispatchToProps = dispatch => ({
  initializeSeriesContentTable: seriesId =>
    dispatch(uiSeriesContentTableActions
      .initializeSeriesContentTable(seriesId)),
  handleClickRow: contentId =>
    dispatch(push(getRouteLink(routes.CONTENT_DETAIL, { contentId }))),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SeriesContentTableContainer);
