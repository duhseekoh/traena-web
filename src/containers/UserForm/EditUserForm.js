// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { reduxForm, Field, type FormProps } from 'redux-form';
import { Grid, Card, Message, Button, Loader } from 'semantic-ui-react';
import { TextField } from '@panderalabs/redux-form-semantic';
import TagsInput from '../../components/TagsInput/TagsInput';
import ResetPasswordModal from './components/ResetPasswordModal';
import DisableUserModal from './components/DisableUserModal';
import styles from './UserForm.scss';
import * as formValidators from '../../utils/formValidators';
import * as uiUserFormActions from '../../redux/modules/ui-user-form/actions';
import * as fromUiUserForm from '../../redux/modules/ui-user-form/selectors';
import { getRouteLink, routes } from '../../constants/routes';
import type { UserFormVals, User } from '../../types/users';

type OwnProps = {
  handleSubmit: ((userFormVals: UserFormVals) => any) => Promise<any>,
  userId: number,
};

type ConnectedProps = {
  isSaving: boolean,
  isLoading: boolean,
  initialValues: {
    tags: string[],
    firstName: string,
    lastName: string,
    email: string,
    position: number,
    id: number,
  },
  user: User,
  editUserFromForm: (userFormVals: UserFormVals) => Promise<any>,
  fetchUser: (userId: number) => Promise<any>,
};

type Props = OwnProps & ConnectedProps & FormProps;

class EditUserForm extends Component<Props> {
  props: Props;

  componentWillMount() {
    this.props.fetchUser(this.props.userId);
  }

  onSubmit = userFormVals => this.props.editUserFromForm(userFormVals);

  render() {
    const { props } = this;
    const { isLoading, isSaving } = props;

    if (isLoading) {
      return <Loader active />;
    }

    if (!props.user) {
      return null;
    }

    return (
      <form className={styles.userForm} autoComplete="off">
        <Grid>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              <h1>Edit User</h1>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              <Button
                primary
                size="big"
                type="submit"
                className="right"
                loading={isSaving}
                onClick={props.handleSubmit(this.onSubmit)}
              >
                Save Changes
              </Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              <Card fluid>
                <Card.Content>
                  <h5>First Name *</h5>
                  <TextField
                    name="firstName"
                    placeholder="First name of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Last Name *</h5>
                  <TextField
                    name="lastName"
                    placeholder="Last name of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Email *</h5>
                  <TextField
                    name="email"
                    placeholder="Email of user..."
                    className={styles.userInput}
                    normalize={v => v.toLowerCase()}
                  />
                </Card.Content>
              </Card>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              <Card fluid>
                <Card.Content>
                  <h5>Position / Title *</h5>
                  <TextField
                    name="position"
                    placeholder="Position of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Tags</h5>
                  <Field
                    name="tags"
                    component={TagsInput}
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>

              <DisableUserModal user={props.user} />
              <ResetPasswordModal email={props.initialValues.email} />

              <Button
                as={Link}
                to={getRouteLink(routes.USERS_ROLES, {
                  userId: props.initialValues.id,
                })}
              >
                Edit User Roles
              </Button>
              {props.error && (
                <Message error header="Error" content={props.error} />
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </form>
    );
  }
}

const userForm = reduxForm({
  form: 'user',
  validate: formValidators.validateUserForm,
})(EditUserForm);

const mapStateToProps = (state, props) => {
  const user = fromUiUserForm.selectUserById(props.userId)(state) || {};
  return {
    isSaving: fromUiUserForm.selectIsSaving(state),
    isLoading: fromUiUserForm.selectIsLoading(state),
    initialValues: {
      tags: user.tags || [],
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      position: user.position,
      id: user.id,
      isDisabled: user.isDisabled,
    },
    user,
  };
};

const mapDispatchToProps = dispatch => ({
  editUserFromForm: userFormVals =>
    dispatch(uiUserFormActions.editUserFromForm(userFormVals)),
  fetchUser: userId => dispatch(uiUserFormActions.fetchUser(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(userForm);
