// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  reduxForm,
  formValueSelector,
  Field,
  type FormProps,
} from 'redux-form';
import { Grid, Card, Message, Button } from 'semantic-ui-react';
import { TextField } from '@panderalabs/redux-form-semantic';
import TagsInput from '../../components/TagsInput/TagsInput';
import Checkbox from '../../components/forms/Checkbox';
import styles from './UserForm.scss';
import * as formValidators from '../../utils/formValidators';
import * as uiUserFormActions from '../../redux/modules/ui-user-form/actions';
import * as fromAuth from '../../redux/modules/auth/selectors';
import * as fromUiUserForm from '../../redux/modules/ui-user-form/selectors';
import type { UserFormVals } from '../../types/users';

type OwnProps = {
  handleSubmit: ((userFormVals: UserFormVals) => any) => Promise<any>,
};

type ConnectedProps = {
  isSaving: boolean,
  showPasswordInput: boolean,
  organizationId: number,
  initialValues: {
    tags: string[],
    passwordReset: boolean,
  },
  createUserFromForm: (
    userFormVals: UserFormVals,
    organizationId: number,
  ) => Promise<any>,
};

type Props = OwnProps & ConnectedProps & FormProps;

class CreateUserForm extends Component<Props> {
  props: Props;

  onSubmit = userFormVals =>
    this.props.createUserFromForm(userFormVals, this.props.organizationId);

  render() {
    const { props } = this;

    return (
      <form className={styles.userForm} autoComplete="off">
        <Grid>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              <h1>Create User</h1>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              <Button
                primary
                size="big"
                type="submit"
                className="right"
                loading={props.isSaving}
                onClick={props.handleSubmit(this.onSubmit)}
              >
                Create
              </Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              <Card fluid>
                <Card.Content>
                  <h5>First Name *</h5>
                  <TextField
                    name="firstName"
                    placeholder="First name of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Last Name *</h5>
                  <TextField
                    name="lastName"
                    placeholder="Last name of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Email *</h5>
                  <TextField
                    name="email"
                    placeholder="Email of user..."
                    className={styles.userInput}
                    normalize={v => v.toLowerCase()}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Send Password Reset</h5>
                  <Checkbox name="passwordReset" toggle />
                  {!props.showPasswordInput && (
                    <TextField
                      name="password"
                      placeholder="Default password for account..."
                      className={styles.userInput}
                    />
                  )}
                </Card.Content>
              </Card>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              <Card fluid>
                <Card.Content>
                  <h5>Position / Title *</h5>
                  <TextField
                    name="position"
                    placeholder="Position of user..."
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              <Card fluid>
                <Card.Content>
                  <h5>Tags</h5>
                  <Field
                    name="tags"
                    component={TagsInput}
                    className={styles.userInput}
                  />
                </Card.Content>
              </Card>
              {props.error && (
                <Message error header="Error" content={props.error} />
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </form>
    );
  }
}

const userForm = reduxForm({
  form: 'user',
  validate: formValidators.validateUserForm,
})(CreateUserForm);

const mapStateToProps = state => ({
  isSaving: fromUiUserForm.selectIsSaving(state),
  showPasswordInput: formValueSelector('user')(state, 'passwordReset'),
  organizationId: fromAuth.selectOrganizationId(state),
  initialValues: {
    tags: [],
    passwordReset: false,
  },
});

const mapDispatchToProps = dispatch => ({
  createUserFromForm: (userFormVals, organizationId) =>
    dispatch(
      uiUserFormActions.createUserFromForm(userFormVals, organizationId),
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(userForm);
