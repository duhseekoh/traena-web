// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Modal } from 'semantic-ui-react';
import * as uiUserFormActions from '../../../redux/modules/ui-user-form/actions';
import * as permissionUtils from '../../../utils/permissionUtils';
import AccessControl from '../../../components/AccessControl/AccessControl';
import type { User } from '../../../types/users';

type OwnProps = {
  user: User,
};

type ConnectedProps = {
  enableUser: (userId: number) => any,
  disableUser: (userId: number) => any,
};

type Props = OwnProps & ConnectedProps;

type State = {
  open: boolean,
};

class DisableUserModal extends Component<Props, State> {
  props: Props;
  state = { open: false };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  render() {
    const { enableUser, disableUser, user } = this.props;

    return (
      <AccessControl
        customAccessData={{ userId: user.id }}
        accessCheck={permissionUtils.canDisableUser}
        noAccessView={<span />}
      >
        <Modal
          open={this.state.open}
          onOpen={this.open}
          onClose={this.close}
          trigger={
            <Button
              onClick={e => {
                e.preventDefault();
              }}
            >
              {user.isDisabled ? 'Enable User' : 'Disable User'}
            </Button>
          }
        >
          <Modal.Header>Enable/Disable this user</Modal.Header>
          <Modal.Content>
            <p>
              You are changing whether or not this user will be able to sign
              into the Traena system.
            </p>
            {user.isDisabled ? (
              <p>Is it okay to enable this user?</p>
            ) : (
              <p>Is it okay to disable this user?</p>
            )}
          </Modal.Content>
          <Modal.Actions>
            <div>
              <Button
                default
                onClick={() => {
                  this.close();
                }}
              >
                Dismiss
              </Button>
              <Button
                primary
                type="submit"
                onClick={() => {
                  if (user.isDisabled) {
                    enableUser(user.id);
                  } else {
                    disableUser(user.id);
                  }
                  this.close();
                }}
              >
                {user.isDisabled ? 'Enable User' : 'Disable User'}
              </Button>
            </div>
          </Modal.Actions>
        </Modal>
      </AccessControl>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  enableUser: userId => dispatch(uiUserFormActions.enableUser(userId)),
  disableUser: userId => dispatch(uiUserFormActions.disableUser(userId)),
});

export default connect(null, mapDispatchToProps)(DisableUserModal);
