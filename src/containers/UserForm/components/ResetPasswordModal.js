// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Modal } from 'semantic-ui-react';
import {
  UI_PROGRESS_STATUSES,
  type UiProgressStatus,
} from '../../../constants/appConstants';
import * as resetPasswordActions from '../../../redux/modules/ui-reset-password/actions';
import * as fromResetPassword from '../../../redux/modules/ui-reset-password/selectors';

type OwnProps = {
  email: string,
};

type ConnectedProps = {
  sendResetPasswordEmail: (email: string) => Promise<any>,
  resetModal: () => Promise<any>,
  status: UiProgressStatus,
};

type Props = OwnProps & ConnectedProps;

type State = {
  open: boolean,
};

class ResetPasswordModal extends Component<Props, State> {
  state = { open: false };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  render() {
    const { props } = this;
    const emailNotYetSent = !this.props.status;
    const emailSentLoading = props.status === UI_PROGRESS_STATUSES.IN_PROGRESS;
    const emailSentSuccess = props.status === UI_PROGRESS_STATUSES.COMPLETE;
    const emailSentError = props.status === UI_PROGRESS_STATUSES.ERROR;

    return (
      <Modal
        open={this.state.open}
        onOpen={this.open}
        onClose={this.close}
        trigger={
          <Button
            onClick={e => {
              e.preventDefault();
            }}
          >
            Send Reset Password Email
          </Button>
        }
      >
        <Modal.Header>
          Send a reset password email to {this.props.email}
        </Modal.Header>
        <Modal.Content>
          {(emailNotYetSent || emailSentLoading || emailSentError) && (
            <div>
              <p>
                If you continue, this user will recieve an emailed link to reset
                their password.
              </p>
              <p>Is it okay to send this email?</p>
            </div>
          )}

          {emailSentSuccess && (
            <p>A reset password email was successfully sent to {props.email}</p>
          )}
        </Modal.Content>
        <Modal.Actions>
          {(emailNotYetSent || emailSentLoading) && (
            <Button
              primary
              type="submit"
              loading={props.status === UI_PROGRESS_STATUSES.IN_PROGRESS}
              onClick={() => {
                props.sendResetPasswordEmail(props.email);
              }}
            >
              Send Email
            </Button>
          )}

          {emailSentSuccess && (
            <Button
              primary
              onClick={() => {
                this.props.resetModal();
                this.close();
              }}
            >
              Finish
            </Button>
          )}

          {emailSentError && (
            <div>
              <Button
                default
                onClick={() => {
                  props.resetModal();
                  this.close();
                }}
              >
                Dismiss
              </Button>
              <Button
                primary
                type="submit"
                loading={props.status === UI_PROGRESS_STATUSES.IN_PROGRESS}
                onClick={() => {
                  this.props.sendResetPasswordEmail(props.email);
                }}
              >
                Retry
              </Button>
            </div>
          )}
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  sendResetPasswordEmail: email =>
    dispatch(resetPasswordActions.sendResetPasswordEmail(email)),
  resetModal: () => dispatch(resetPasswordActions.resetPasswordModalReset()),
});

const mapStateToProps = state => ({
  status: fromResetPassword.selectModalStatus(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordModal);
