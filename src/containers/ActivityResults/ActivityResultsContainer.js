/* eslint-disable react/prop-types */
// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as fromActivityResults from '../../redux/modules/activities-results/selectors';
import * as uiActivityResultsActions from '../../redux/modules/ui-activities-results/actions';
import PollResults from './components/PollResults';
import type { ActivityResults } from '../../types/activities';
import { ACTIVITY_TYPES } from '../../constants/activities';
import type { ActivityType } from '../../constants/activities';

type OwnProps = {
  contentId: number,
  activityId: number,
  activityType: ActivityType,
}

type ConnectedProps = {
  errorInitializing: boolean,
  loading: boolean,
  activityResults: ActivityResults,
  initializeActivityResults: (activityId: number, contentId: number) => any
}

type Props = OwnProps & ConnectedProps;

class ActivityResultsContainer extends Component<Props> {
  componentDidMount = () => {
    this.props.initializeActivityResults(
      this.props.activityId,
      this.props.contentId,
    );
  };

  render() {
    const {
      activityResults,
      activityType,
      errorInitializing,
      loading,
      contentId,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading these activity results.
        </div>
      );
    }

    if (activityResults) {
      const activityTypeToResultsComponent = {
        [ACTIVITY_TYPES.POLL]: PollResults,
        [ACTIVITY_TYPES.QUESTION_SET]: () => (<div>Not yet implemented</div>),
      };

      const Results = activityTypeToResultsComponent[activityType];
      return (
        <Results
          contentId={contentId}
          activityType={activityType}
          activityResults={activityResults}
        />
      );
    }

    /**
     * If no activity has been loaded yet,
     * but we also have not yet started loading in the data
     */
    return null;
  }
}

const mapDispatchToProps = dispatch => ({
  initializeActivityResults: (activityId, contentId) =>
    dispatch(
      uiActivityResultsActions.initializeActivityResults(
        activityId,
        contentId,
      ),
    ),
});

const mapStateToProps = (state) => ({
  activityResults: fromActivityResults.selectActivityResults(
    state,
  ),
  loading: fromActivityResults.selectLoading(state),
  errorInitializing: fromActivityResults.selectErrorInitializing(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ActivityResultsContainer,
);
