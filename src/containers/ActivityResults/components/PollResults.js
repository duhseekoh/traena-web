/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { Divider, Icon } from 'semantic-ui-react';
import { getRouteLink, routes } from '../../../constants/routes';
import type { PollResults as PollResultsType } from '../../../types/activities';
import styles from '../ActivityResults.scss';

type Props = {
  activityResults: PollResultsType,
  contentId: number,
}

/**
* Checks if the given percent is higher than all other results
* @param percent 0-100
* @param activityResults array of PollResults objects
* @return {boolean}
*/
function _isHighestResultPercent(
  percent: number,
  activityResults: PollResultsType,
): boolean {
  return activityResults.activity.body.choices.reduce((result, choice) => {
    const comparePercent = activityResults.choiceResults[choice.id] ?
      activityResults.choiceResults[choice.id].percent : 0;

    if (percent < comparePercent) {
      result = false; // eslint-disable-line
    }
    return result;
  }, true);
}

const PollResults = (props: Props) => {
  const { activityResults, contentId } = props;
  return (
    <div className={styles.pollResults}>
      <div className={styles.pollResultsTopBar}>
        <Link
          className={styles.backButton}
          to={getRouteLink(routes.CONTENT_EDIT, { contentId })}
        >
          <Icon name="chevron left" size="large" />
        </Link>
        <div className={styles.pollResultsTopBarTitle}>
          <h3>Poll Results</h3>
        </div>
      </div>

      <Divider hidden section />
      <h1>{activityResults.activity.body.title}</h1>
      {activityResults.count} responses
      <Divider hidden section />

      {activityResults.activity.body.choices.map(choice => {
        const percent = activityResults.choiceResults[choice.id] ?
          activityResults.choiceResults[choice.id].percent : 0;

        const topResult = _isHighestResultPercent(
          percent,
          activityResults,
        );

        return (
          <div
            className={
              topResult ?
              classnames(styles.pollResultTotalBar, styles.topResult) :
              styles.pollResultTotalBar
            }
          >
            <div
              className={styles.fill}
              style={{
                width: `${percent}%`,
              }}
            />
            <span className={styles.response}>
              {choice.choiceText}
            </span>
            <span className={styles.percent}>
              {Math.round(percent)}%
            </span>
          </div>
        );
      })}

    </div>
  );
};

export default PollResults;
