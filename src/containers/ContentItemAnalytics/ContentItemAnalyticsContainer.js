/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Grid, Loader, Header } from 'semantic-ui-react';
import AggregateStatsContainer from '../ContentItemAggregateStats/AggregateStatsContainer';
import StatsByUserContainer from '../ContentItemStatsByUser/StatsByUserContainer';
import { getRouteLink, routes } from '../../constants/routes';
import * as uiContentItemAnalyticsActions from '../../redux/modules/ui-content-item-analytics/actions';
import * as fromUiContentItemAnalytics from '../../redux/modules/ui-content-item-analytics/selectors';
import * as fromContent from '../../redux/modules/content/selectors';
import type { ContentItem } from '../../types/content';

type OwnProps = {|
  contentId: number,
|};

type ConnectedProps = {|
  isLoading: boolean,
  error: ?string,
  contentItem: ?ContentItem,
  initialize: () => any,
|};

type Props = OwnProps & ConnectedProps;

/**
 * A common parent for content item analytics. It handles loading of a content item
 * so data about the content itself can be displayed.
 * To handle the retrieval and rendering of the stats themselves, other containers
 * are nested.
 */
class ContentItemAnalytics extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const { contentId, contentItem, isLoading, error } = this.props;

    if (error) {
      return <div>Error loading post analytics.</div>;
    }

    // block UI with loading since there is no reasonable UI to display
    // without having a content item retrieved.
    if (isLoading) {
      return <Loader active />;
    }

    // only attempt to load the rest of the analytics if we can actually retrieve
    // the content item itself.
    if (contentItem) {
      return (
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16}>
              <h1>Post Stats</h1>
              <Header.Subheader size="small" as="h2">
                <Link
                  to={getRouteLink(
                    routes.CONTENT_DETAIL,
                    { contentId: contentItem.id })
                  }
                >
                  {contentItem.body.title}
                </Link>
              </Header.Subheader>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column mobile={16}>
              <AggregateStatsContainer contentId={contentId} />
              <StatsByUserContainer contentId={contentId} />
              {/* Currently not displaying the event feed, but we could! */}
              {/* <EventFeedContainer contentId={contentId} /> */}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    }

    return null;
  }
}

const mapStateToProps = (state, props: OwnProps) => ({
  isLoading: fromUiContentItemAnalytics.selectInitializing(state),
  error: fromUiContentItemAnalytics.selectInitializeError(state),
  contentItem: fromContent.selectContentItemById(state, props),
});

const mapDispatchToProps = (dispatch, { contentId }: OwnProps) => ({
  initialize: () =>
    dispatch(uiContentItemAnalyticsActions.initialize(contentId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContentItemAnalytics);
