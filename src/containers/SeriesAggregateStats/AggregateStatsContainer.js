// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Loader, Grid, Statistic } from 'semantic-ui-react';
import * as seriesStatsActions from '../../redux/modules/series-stats/actions';
import * as fromSeriesStats from '../../redux/modules/series-stats/selectors';
import type { SeriesAggregateStats } from '../../types/analytics';
import styles from './AggregateStatsContainer.scss';

type OwnProps = {|
  // used in mapDispatchToProps
  seriesId: number, // eslint-disable-line react/no-unused-prop-types
|};

type ConnectedProps = {|
  error: ?string,
  aggregateStats: ?SeriesAggregateStats,
  isLoading: boolean,
  getAggregateStats: () => any,
|};

type Props = OwnProps & ConnectedProps;

/**
 * For a single content item, retrieves and renders basic aggregate stats.
 */
class AggregateStats extends Component<Props> {
  props: Props;

  componentWillMount() {
    this.props.getAggregateStats();
  }

  renderStats = () => {
    const { aggregateStats } = this.props;
    if (!aggregateStats) {
      return null;
    }

    return (
      <Grid columns={4} stackable centered>
        <Grid.Row divided>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>{aggregateStats.contentCount}</Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Posts
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>
                {aggregateStats.userCompletionCount}
              </Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Users Completed
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>
                {aggregateStats.userInProgressCount}
              </Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Users In Progress
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>
                {aggregateStats.userNotStartedCount}
              </Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Users Not Started
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  };

  render() {
    const { error, isLoading } = this.props;
    if (error) {
      return <div>Error: Could not display stats.</div>;
    }

    return (
      <Card fluid>
        <Card.Content>
          <Loader active={isLoading} />
          {this.renderStats()}
        </Card.Content>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: fromSeriesStats.selectAggregateStatsIsLoading(state),
  error: fromSeriesStats.selectAggregateStatsError(state),
  aggregateStats: fromSeriesStats.selectAggregateStats(state),
});

const mapDispatchToProps = (dispatch, { seriesId }: OwnProps) => ({
  getAggregateStats: () =>
    dispatch(seriesStatsActions.getAggregateStats(seriesId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AggregateStats);
