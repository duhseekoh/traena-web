// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { ToggleField } from '@panderalabs/redux-form-semantic';
import { Grid, Card, Button, Loader, Header, Segment } from 'semantic-ui-react';
import styles from './UserRolesForm.scss';
import * as uiUserRolesFormActions from '../../redux/modules/ui-user-roles-form/actions';
import * as fromUiUserRolesForm from '../../redux/modules/ui-user-roles-form/selectors';
import * as fromUiUserForm from '../../redux/modules/ui-user-form/selectors';
import { getRouteLink, routes } from '../../constants/routes';
import type { Role } from '../../types/users';

type OwnProps = {
  userId: number,
};

type ConnectedProps = {
  isSaving: boolean,
  isLoading: boolean,
  availableRoles: Role[],
  userFullName: string,
  userRolesFormInitialize: (userId: number) => Promise<any>,
  addUserRole: (role: Role) => Promise<any>,
  removeUserRole: (role: Role) => Promise<any>,
};

type Props = OwnProps & ConnectedProps;

class UserRolesForm extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.userRolesFormInitialize(this.props.userId);
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active />;
    }
    return (
      <form className={styles.userRolesForm} autoComplete="off">
        <Grid>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              <h1>
                Edit User Roles
                <span className={styles.formTitleUserName}>
                  {this.props.userFullName}
                </span>
              </h1>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              <Button
                default
                size="big"
                type="submit"
                className="right"
                as={Link}
                to={getRouteLink(routes.USERS_EDIT, {
                  userId: this.props.userId,
                })}
              >
                BACK TO EDIT USER
              </Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              {this.props.availableRoles.map(availableRole => (
                <Card fluid>
                  <Card.Content>
                    <Header as="h2" disabled={this.props.isSaving}>
                      <ToggleField
                        key={availableRole.id}
                        onChange={(e, val) => {
                          if (val === true) {
                            this.props.addUserRole(availableRole);
                          } else {
                            this.props.removeUserRole(availableRole);
                          }
                        }}
                        disabled={this.props.isSaving}
                        name={availableRole.name}
                        className="right"
                        toggle
                      />
                      {availableRole.name}
                    </Header>
                    <Segment>{availableRole.description}</Segment>
                  </Card.Content>
                </Card>
              ))}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </form>
    );
  }
}

const userRolesForm = reduxForm({
  form: 'userRoles',
})(UserRolesForm);

const mapStateToProps = (state, props: OwnProps) => ({
  isSaving: fromUiUserRolesForm.selectIsSaving(state),
  isLoading: fromUiUserRolesForm.selectIsLoading(state),
  availableRoles: fromUiUserRolesForm.selectAvailableRoles(state),
  userFullName: fromUiUserForm.selectUserFullNameById(props.userId)(state),
  initialValues: {},
});

const mapDispatchToProps = (dispatch, props) => ({
  userRolesFormInitialize: userId =>
    dispatch(uiUserRolesFormActions.userRolesFormInitialize(userId)),
  addUserRole: role =>
    dispatch(uiUserRolesFormActions.addUserRole(props.userId, role)),
  removeUserRole: role =>
    dispatch(uiUserRolesFormActions.removeUserRole(props.userId, role)),
});

export default connect(mapStateToProps, mapDispatchToProps)(userRolesForm);
