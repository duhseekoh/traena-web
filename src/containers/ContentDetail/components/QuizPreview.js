import React from 'react';
import classnames from 'classnames';
import { Icon } from 'semantic-ui-react';
import styles from './QuizPreview.scss';

/**
* Renders a preview of a quiz, with all of its questions + answers, and the
* correct answer indicated.
*/
const QuizPreview = (props: { activity: Activity }) => {
  const { activity } = props;
  return (
    <div className={styles.activityWrapper} key={activity.id}>
      {activity.body.questions.map(question =>
        (
          <div className={styles.activityQuestion}>
            <h5>{question.questionText}</h5>
            {question.choices.map(choice =>
              (
                <div
                  key={choice.id}
                  className={choice.correct ?
                    classnames(styles.activityChoice, styles.correct) :
                    styles.activityChoice
                  }
                >
                  {choice.correct &&
                    <Icon name="check" className={styles.correctCheck} />
                  } {choice.choiceText}
                </div>
              ),
            )}
          </div>
        ),
      )}
    </div>
  );
};

export default QuizPreview;
