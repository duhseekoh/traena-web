import React from 'react';
import styles from './PollPreview.scss';

/**
* Renders a preview of a poll, with all of its questions + answers
*/
const PollPreview = (props: { activity: Activity }) => {
  const { activity } = props;
  return (
    <div className={styles.activityWrapper} key={activity.id}>
      <div className={styles.activityQuestion}>
        <h5>{activity.body.title}</h5>
        {activity.body.choices.map(choice =>
          (
            <div
              key={choice.id}
              className={styles.activityChoice}
            >
              {choice.choiceText}
            </div>
          ),
        )}
      </div>
    </div>
  );
};

export default PollPreview;
