// @flow
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import copy from 'copy-to-clipboard';
import { Link } from 'react-router-dom';
import Linkify from 'linkifyjs/react';
import { Grid, Button, Icon, Loader } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import DateShort from '../../components/Date/DateShort';
import * as uiContentDetailActions from '../../redux/modules/ui-content-detail/actions';
import * as fromContent from '../../redux/modules/content/selectors';
import * as fromUiContentDetail from '../../redux/modules/ui-content-detail/selectors';
import * as fromActivities from '../../redux/modules/activities/selectors';
import VideoPlayer from '../../components/VideoPlayer/VideoPlayer';
import ImageSlider from '../../components/ImageSlider/ImageSlider';
import TagsList from '../../components/TagsList/TagsList';
import styles from './ContentDetailContainer.scss';
import type {
  ContentItem,
} from '../../types/content';
import type {
  Organization,
} from '../../types/organizations';
import type {
  Activity,
} from '../../types/activities';
import AccessControl from '../../components/AccessControl/AccessControl';
import * as permissionUtils from '../../utils/permissionUtils';
import { PERMISSIONS } from '../../constants/permissions';
import { generateContentURL } from '../../utils/linkUtils';
import QuizPreview from './components/QuizPreview';
import PollPreview from './components/PollPreview';
import { ACTIVITY_TYPES } from '../../constants/activities';
import { CONTENT_TYPES, CONTENT_STATUSES } from '../../constants/appConstants';

type OwnProps = {
  contentId: number,
};

type ConnectedProps = {
  contentItem?: ContentItem,
  organization: Organization,
  activities: Activity[],
  initializeContentDetail: (contentId: number) => any,
  isLoading: boolean,
  errorInitializing: boolean,
};

type Props = OwnProps & ConnectedProps;

class ContentDetailContainer extends Component<Props> {
  props: Props;

  componentWillMount() {
    if (!this.props.contentId) {
      return;
    }
    this.props.initializeContentDetail(this.props.contentId);
  }

  renderContent() {
    const { contentItem } = this.props;
    if (!contentItem) {
      return null;
    }
    let content;
    if (contentItem.body.type === CONTENT_TYPES.TrainingVideo) {
      content = (
        <div>
          {contentItem.body.video &&
            <VideoPlayer
              video={contentItem.body.video}
            />
          }
          <Linkify className={styles.contentDescription}>
            {contentItem.body.text}
          </Linkify>
        </div>

      );
    } else if (contentItem.body.type === CONTENT_TYPES.DailyAction) {
      content = (
        <Linkify className={styles.contentDescription}>
          {contentItem.body.text}
        </Linkify>
      );
    } else if (contentItem.body.type === CONTENT_TYPES.Image) {
      content = (
        <div>
          <ImageSlider images={contentItem.body.images} />
          <Linkify className={styles.contentDescription}>
            {contentItem.body.text}
          </Linkify>
        </div>
      );
    }
    return (
      <div className={styles.contentWrapper}>
        {content}
      </div>
    );
  }

  renderActivity() {
    const { activities } = this.props;
    if (!activities.length) {
      return null;
    }
    return (
      <div className={styles.activitiesWrapper}>
        <Icon
          name="lightning"
          className={styles.activityIcon}
          size="big"
        />
        {activities.map(activity => {
          if (activity.type === ACTIVITY_TYPES.QUESTION_SET) {
            return <QuizPreview activity={activity} />;
          } else if (activity.type === ACTIVITY_TYPES.POLL) {
            return <PollPreview activity={activity} />;
          }
          return null;
        })}
      </div>
    );
  }

  render() {
    const {
      contentItem,
      contentId,
      organization,
      errorInitializing,
      isLoading,
    } = this.props;

    if (isLoading) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading this content to edit.
        </div>
      );
    }

    if (!contentItem) {
      return null;
    }

    return (
      <div className={styles.contentContainer}>
        <Grid stackable>
          <Grid.Row className={styles.headerRow}>
            <Grid.Column width={8}>
              <h1>
                Post Details
              </h1>
            </Grid.Column>
            <Grid.Column width={8}>
              <AccessControl
                customAccessData={{
                  authorId: contentItem.author.id,
                }}
                accessCheck={permissionUtils.canModifyContent}
                noAccessView={null}
              >
                <Button
                  primary
                  size="big"
                  as={Link}
                  to={getRouteLink(routes.CONTENT_EDIT, { contentId })}
                  className="right"
                >Edit Post
                </Button>
              </AccessControl>
              {contentItem.status === CONTENT_STATUSES.PUBLISHED &&
                <Button
                  default
                  size="big"
                  className="right"
                  as={Link}
                  to={getRouteLink(routes.CONTENT_STATS, { contentId })}
                >View Stats
                </Button>
              }
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={4}>
              {this.props.contentItem && (
                <div className={styles.sidebarWrapper}>
                  <div className={styles.sidebarAuthorInfo}>
                    <div>
                      by{' '}
                      <strong>
                        {this.props.contentItem.author.firstName}{' '}
                        {this.props.contentItem.author.lastName}
                      </strong>
                    </div>
                    <div>
                      Org <strong>{organization && organization.name}</strong>
                    </div>
                    {contentItem.status === CONTENT_STATUSES.PUBLISHED &&
                      <div>
                        Published{' '}
                        <strong>
                          <DateShort
                            date={this.props.contentItem.publishedTimestamp}
                          />
                        </strong>
                      </div>
                    }
                  </div>

                  {contentItem.status === CONTENT_STATUSES.PUBLISHED &&
                    <div className={styles.sidebarLinks}>
                      <div>
                        <Link
                          to={getRouteLink(routes.CONTENT_VIEW, { contentId })}
                        >
                          Consumer Desktop View
                        </Link>
                      </div>
                      <div>
                        <button
                          onClick={() => copy(generateContentURL(contentId))}
                        >
                          Copy Shareable Link
                        </button>
                      </div>
                    </div>
                  }

                  {contentItem.status === CONTENT_STATUSES.PUBLISHED &&
                    <div className={styles.sidebarStats}>
                      <div className={styles.sidebarStat}>
                        <Icon name="heart outline" size="large" /> <strong>{this.props.contentItem.likeCount}</strong>
                      </div>
                      <div className={styles.sidebarStat}>
                        <Icon name="comment outline" size="large" /> <strong>{this.props.contentItem.commentCount}</strong>
                      </div>
                      <div className={styles.sidebarStat}>
                        <Icon name="check circle outline" size="large" /> <strong>{this.props.contentItem.completedCount}</strong>
                      </div>
                    </div>
                  }

                  {contentItem.series &&
                    <div className={styles.sidebarSeriesInfo}>
                      <div className={styles.seriesTitle}>
                        <Icon name="clone outline" size="large" />
                        from <strong>{contentItem.series.title}</strong>
                      </div>
                      <AccessControl
                        allowedPermissions={[
                          PERMISSIONS.ORGANIZATIONS.MODIFY.ORG,
                        ]}
                        noAccessView={null}
                      >
                        <Link
                          to={
                            getRouteLink(
                              routes.SERIES_DETAIL,
                              { seriesId: contentItem.series.id },
                            )
                          }
                        >
                          Go to Series Details
                        </Link>
                      </AccessControl>
                    </div>
                  }

                </div>
              )}
            </Grid.Column>

            <Grid.Column width={7}>
              <h2>{contentItem.body.title}</h2>
              <h5>{contentItem.channel.name}</h5>
              {this.renderContent()}
              <TagsList tags={contentItem.tags} />
              {this.renderActivity()}
            </Grid.Column>

          </Grid.Row>
        </Grid>

        <div className={styles.pageBreak} />

      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  contentItem: fromContent.selectContentItemById(state, props),
  isLoading: fromUiContentDetail.selectLoading(state),
  errorInitializing: fromUiContentDetail.selectErrorInitializing(state),
  organization: fromContent.selectContentOrganizationByContentId(state, props),
  activities: fromActivities.selectActivitiesByContentId(
    state,
    props.contentId,
  ),
});

const mapDispatchToProps = dispatch => ({
  initializeContentDetail: contentId =>
    dispatch(uiContentDetailActions.initializeContentDetail(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ContentDetailContainer,
);
