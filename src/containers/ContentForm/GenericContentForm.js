// cannot enable flow in this file yet, because of issue with redux-form `change` return
// type in the redux-form flow types
import React, { Component } from 'react';
import { Link, Prompt } from 'react-router-dom';
import copy from 'copy-to-clipboard';
import { connect } from 'react-redux';
import {
  Field,
  reduxForm,
  formValueSelector,
  change,
  isDirty as isDirtySelector,
} from 'redux-form';
import type { FormProps } from 'redux-form/lib/types.js.flow';
import { Button, Grid, Card, Message, Icon, Label } from 'semantic-ui-react';
import { TextField, TextAreaField } from '@panderalabs/redux-form-semantic';
import * as uiContentFormActions from '../../redux/modules/ui-content-form/actions';
import * as fromUiContentForm from '../../redux/modules/ui-content-form/selectors';
import ContentStatusText from '../../components/ContentStatusText/ContentStatusText';
import TimeAgo from '../../components/Date/TimeAgo';
import { getRouteLink, routes } from '../../constants/routes';
import ChannelSelector from './components/ChannelSelector';
import TagsInput from '../../components/TagsInput/TagsInput';
import ActivitiesInput from './components/ActivitiesInput';
import * as formValidators from '../../utils/formValidators';
import ImageUploadFields from './components/ImageUploadFields';

import type { Activity } from '../../types/activities';
import {
  type ContentStatus,
  type ContentType,
  CONTENT_STATUSES,
  CONTENT_TYPES,
  UI_PROGRESS_STATUSES,
} from '../../constants/appConstants';
import { generateContentURL } from '../../utils/linkUtils';
import type { ContentFormVals } from '../../types/content';
import type { Series } from '../../types/series';

import VideoUploadFields from './components/VideoUploadFields';
import styles from './GenericContentForm.scss';

type OwnProps = {
  title: string,
  activities: Activity[],
  series: ?Series,
  disabled: boolean,
  progress: ProgressUpdate,
};

type ConnectedProps = {
  changeContentFormValue: (field: string, value: string) => Promise<any>,
  publishContentForm: (contentFormVals: ContentFormVals) => Promise<any>,
  saveContentForm: (contentFormVals: ContentFormVals) => Promise<any>,
  navigateToActivityFormForContentItem: (
    contentId: number,
    activityType: ActivityType,
    activityId?: number,
  ) => void,
  saving: boolean,
  contentId?: number,
  contentItemStatus: ContentStatus,
  contentType: ContentType,
};

type Props = OwnProps & ConnectedProps & FormProps;

class GenericContentForm extends Component<Props> {
  props: Props;

  componentDidMount() {
    window.addEventListener('beforeunload', this.handleBeforeUnload);
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.handleBeforeUnload);
  }

  /**
   * If the user attempts to close the browser/tab and the form is dirty, let em know.
   */
  handleBeforeUnload = (e: BeforeUnloadEvent) => {
    if (this.props.isDirty) {
      const confirmationMessage = 'You have unsaved changes, are you sure you want to leave the page?';
      e.returnValue = confirmationMessage;
      return confirmationMessage;
    }
    return null;
  }

  /**
   * Passed to redux-form handleSubmit
   */
  submit = (contentFormVals: ContentFormVals) => {
    if (contentFormVals.desiredStatus === CONTENT_STATUSES.PUBLISHED) {
      return this.props.publishContentForm(contentFormVals);
    }
    return this.props.saveContentForm(contentFormVals);
  };

  renderMediaFields = () => {
    // Images can be added on a Text or Image post (if a text post is saved with images, it flips over to an image post).
    const displayImageFields =
      this.props.contentType === CONTENT_TYPES.DailyAction ||
      this.props.contentType === CONTENT_TYPES.Image;
    // Videos can only be added ona Video type of post.
    const displayVideoFields =
      this.props.contentType === CONTENT_TYPES.TrainingVideo;

    if (displayImageFields) {
      return (
        <ImageUploadFields
          onPressChangeMedia={() => {
            this.props.changeMedia(CONTENT_TYPES.TrainingVideo);
          }}
        />
      );
    }

    if (displayVideoFields) {
      return (
        <VideoUploadFields
          onPressChangeMedia={() => {
            this.props.changeMedia(CONTENT_TYPES.Image);
          }}
        />
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    const { error, progress, contentId } = props;
    const isEditingExisting = !!contentId;
    const isUploading = progress.status === UI_PROGRESS_STATUSES.IN_PROGRESS;
    return (
      <div>
        {/*
          Prompt's when any route changes are attempted within the app and
          the form is dirty. Does not have window.onbeforeunload functionality.
        */}
        <Prompt
          when={props.isDirty}
          message={
            "You have unsaved changes. Click 'OK' to discard. or 'Cancel' to continue editing."
          }
        />
        <form className={styles.contentForm} autoComplete="off">
          <Grid>
            <Grid.Row>
              <Grid.Column computer={8} mobile={16}>
                <h1>
                  {isEditingExisting ? 'Edit Post' : 'Create Post'}
                  {isEditingExisting && (
                    <a
                      title="Copy this link to share this specific piece of content."
                      href={generateContentURL(props.contentId)}
                      className={styles.copyLink}
                      onClick={e => {
                        e.preventDefault();
                        if (props.contentId) {
                          copy(generateContentURL(props.contentId));
                        }
                      }}
                    >
                      <Icon name="linkify" />copy shareable link
                    </a>
                  )}
                </h1>
              </Grid.Column>
              <Grid.Column computer={8} mobile={16} stackable>
                {/* TODO convert this whole button section into its own
                  component */}
                {this.props.contentItemStatus === CONTENT_STATUSES.PUBLISHED ? (
                  <div className={styles.toolbar}>
                    <Button
                      default
                      size="big"
                      type="submit"
                      as={Link}
                      disabled={isUploading}
                      className={styles.toolbarButton}
                      to={getRouteLink(routes.CONTENT)}
                    >
                      Discard Changes
                    </Button>
                    <Button
                      primary
                      size="big"
                      type="submit"
                      className={styles.toolbarButton}
                      loading={props.saving}
                      disabled={isUploading}
                      onClick={e => {
                        e.preventDefault();
                        props.changeContentFormValue(
                          'desiredStatus',
                          CONTENT_STATUSES.PUBLISHED,
                        );
                        setTimeout(this.props.handleSubmit(this.submit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
                      }}
                    >
                      Save and Publish
                    </Button>
                  </div>
                ) : (
                  <div className={styles.toolbar}>
                    <Button
                      default
                      size="big"
                      className={styles.toolbarButton}
                      loading={props.saving}
                      disabled={isUploading}
                      onClick={e => {
                        e.preventDefault();
                        props.changeContentFormValue(
                          'desiredStatus',
                          props.contentItemStatus,
                        );
                        setTimeout(props.handleSubmit(this.submit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
                      }}
                    >
                      Save
                    </Button>
                    <Button
                      primary
                      size="big"
                      type="submit"
                      className={styles.toolbarButton}
                      loading={props.saving}
                      disabled={isUploading}
                      onClick={e => {
                        e.preventDefault();
                        props.changeContentFormValue(
                          'desiredStatus',
                          CONTENT_STATUSES.PUBLISHED,
                        );
                        setTimeout(props.handleSubmit(this.submit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
                      }}
                    >
                      Publish
                    </Button>
                  </div>
                )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column computer={8} mobile={16}>
                <Card fluid>
                  <Card.Content>
                    <h5>Status</h5>
                    <ContentStatusText status={props.contentItemStatus} />{' '}
                    {props.contentPublishedTimestamp && (
                      <TimeAgo date={props.contentPublishedTimestamp} />
                    )}
                    {/* We could put an unsaved changes icon by the save buttons.
                      Here's an example of how that would be done.
                    */}
                    {props.isDirty && (
                      <span className={styles.saveMessage}>
                        {' '}
                        (Unsaved changes)
                      </span>
                    )}
                  </Card.Content>
                </Card>
                <Card fluid>
                  <Card.Content>
                    <h5>Title *</h5>
                    <TextField
                      name="body.title"
                      placeholder="Add a title to your post..."
                      className={styles.postInput}
                    />
                  </Card.Content>
                </Card>
                <Card fluid>
                  <Card.Content>
                    <h5>Channel *</h5>
                    <ChannelSelector
                      className=""
                      disabled={props.disabled || !!props.series}
                      onChange={props.selectChannel}
                      options={props.channels}
                    />
                    {!!props.series && (
                      <Label size="mini">
                        You must remove this post from its series before
                        reassigning its channel.
                      </Label>
                    )}
                  </Card.Content>
                </Card>
                <Card fluid>
                  <Card.Content>
                    <h5>Tags *</h5>
                    <Field
                      name="tags"
                      component={TagsInput}
                      className={styles.postInput}
                    />
                  </Card.Content>
                </Card>
                <Card fluid>
                  <Card.Content>
                    <Field
                      name="activitiesOrder"
                      component={ActivitiesInput}
                      disabled={props.disabled}
                      contentId={props.contentId}
                      activities={props.activities}
                      navigateToActivityFormForContentItem={
                        props.navigateToActivityFormForContentItem
                      }
                    />
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column computer={8} mobile={16}>
                {/* Main Body Text */}
                <Card fluid>
                  <Card.Content>
                    <h5>Text</h5>
                    <TextAreaField
                      name="body.text"
                      rows={6}
                      placeholder="Enter the text for your post..."
                      className={styles.postInput}
                    />
                  </Card.Content>
                </Card>
                {this.renderMediaFields()}
                {error && <Message error header="Error" content={error} />}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </form>
      </div>
    );
  }
}

const contentForm = reduxForm({
  form: 'content',
  validate: (values, props) =>
    formValidators.validateContentForm(values, props),
  // After any save of the content we reinitialize the content form with the
  // most recent values from the server. This starts the form over with a clean
  // slate.
  enableReinitialize: true,
})(GenericContentForm);

const mapDispatchToProps = dispatch => ({
  // $FlowFixMe - optional fields incorrectly required. fixed in newer version of redux-form https://github.com/erikras/redux-form/pull/3624
  changeContentFormValue: (field, value) =>
    dispatch(change('content', field, value)),
  publishContentForm: (contentFormVals: ContentFormVals) =>
    dispatch(uiContentFormActions.publishContentForm(contentFormVals)),
  saveContentForm: (contentFormVals: ContentFormVals) =>
    dispatch(uiContentFormActions.saveContentForm(contentFormVals)),
  navigateToActivityFormForContentItem: (contentId, activityType, activityId) =>
    dispatch(
      uiContentFormActions.navigateToActivityFormForContentItem(
        contentId,
        activityType,
        activityId,
      ),
    ),
  changeMedia: (toContentType: ContentType) =>
    dispatch(uiContentFormActions.changeMedia(toContentType)),
});

const mapStateToProps = state => ({
  saving: fromUiContentForm.selectIsSaving(state),
  contentId: formValueSelector('content')(state, 'id'),
  contentItemStatus: formValueSelector('content')(state, 'status'),
  contentType: formValueSelector('content')(state, 'body.type'),
  contentPublishedTimestamp: formValueSelector('content')(
    state,
    'publishedTimestamp',
  ),
  // props.dirty prop provided by redux form does not correctly calculate
  // property after our form save, so using redux-form isDirty selector instead.
  // https://github.com/erikras/redux-form/issues/3299
  isDirty: isDirtySelector('content')(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(contentForm);
