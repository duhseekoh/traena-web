// @flow
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { Button, Icon } from 'semantic-ui-react';
import styles from './ImageFileDropper.scss';

type Props = {
  disabled: boolean,
  onDropImage: (files: File[]) => any,
};

const allowedImageTypes = 'image/jpeg, image/png';

/**
 * Simply an area to that accepts browser drops of images files or triggers
 * a file browser dialog to select images from.
 * It doesn't stored the selected file reference, just passes it along to the
 * onDropImage handler.
 */
class ImageFileDropper extends Component<Props> {
  props: Props;

  handleAddFile = (filesToUpload: File[]) => {
    const { onDropImage, disabled } = this.props;
    // Don't allow uploading another image if this one is in progress
    if (disabled) {
      return;
    }

    onDropImage(filesToUpload);
  };

  render() {
    return (
      <Dropzone
        onDropAccepted={this.handleAddFile}
        multiple
        accept={allowedImageTypes}
        style={{}}
      >
        <div className={styles.fileDropper}>
          <div>
            <h5>Drop an image here</h5>
            <Icon name="image" className={styles.uploadIcon} />
            <div className={styles.blurb}>
              Upload up to 25 images (png, jpeg) you would like to post.
            </div>
            <Button
              disabled={this.props.disabled}
              primary
              onClick={e => e.preventDefault()}
            >
              Browse
            </Button>
          </div>
        </div>
      </Dropzone>
    );
  }
}

export default ImageFileDropper;
