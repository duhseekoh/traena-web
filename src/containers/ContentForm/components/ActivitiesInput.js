// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import classnames from 'classnames';
import type { FieldProps } from 'redux-form';
import ActivityItem from './ActivityItem/ActivityItem';
import {
  ACTIVITY_TYPES,
  ACTIVITY_ICONS,
  ACTIVITY_UI_NAMES,
} from '../../../constants/activities';
import { COLORS } from '../../../constants/colors';
import type { ActivityType } from '../../../constants/activities';
import type { Activity } from '../../../types/activities';
import styles from '../GenericContentForm.scss';

type Props = {
  contentId: number,
  activities: Activity[],
  navigateToActivityFormForContentItem: (
    contentId: number,
    activityType: ActivityType,
    activityId?: number,
  ) => void,
  disabled: boolean,
} & FieldProps;

/**
 * This component is used to control the value of `activitiesOrder: number[]`. `activitiesOrder` is
 * a property on a `contentItem` which holds the display order of activities attached to the content.
 */
class ActivitiesInput extends Component<Props> {
  /**
   * Sorts an array of Activity objects to fit a given sort order
   * @param activities An array of activity objects we want to sort
   * @param activitiesOrder An array of activity IDs in sorted order
   * @return Array of activities sorted by activitiesOrder
   */
  getSortedActivities = (
    activities: Activity[],
    activitiesOrder: number[],
  ): Activity[] => {
    if (!activitiesOrder) {
      return activities;
    }

    // follows order of activitiesOrder and appends missing activities
    // (not in the order array) to the end of the list
    const sortedActivities = _.sortBy(activities, activity => {
      // sortBy is going to sort the passed in collection based on whatever
      // number is returned at the end here
      // so if the activity already exists in activitiesOrder, perfect
      const indexOfActivity = activitiesOrder.indexOf(activity.id);
      // when it doesn't exist (the -1 case), we jut return a really big number,
      // so we know it'll get bubbled to the end
      return indexOfActivity === -1 ? Number.MAX_SAFE_INTEGER : indexOfActivity;
    });

    return sortedActivities;
  };

  moveActivity = (from: number, to: number) => {
    const sorted = this.getSortedActivities(
      this.props.activities,
      this.props.input.value,
    ).map(activity => activity.id);
    sorted.splice(to, 0, sorted.splice(from, 1)[0]);
    this.props.input.onChange(sorted);
    this.forceUpdate(); // @TODO: Why does input.onChange in the line above not trigger a rerender?
  };

  render() {
    const {
      input,
      activities,
      contentId,
      navigateToActivityFormForContentItem,
    } = this.props;
    const sortedActivities = this.getSortedActivities(activities, input.value);
    return (
      <div>
        <h5 className="with-subheader">Activities</h5>
        Add up to 1 activity to your post.
        {sortedActivities.length > 0 &&
          sortedActivities.map((activity) => (
            <ActivityItem
              key={activity.id}
              activity={activity}
              contentId={contentId}
              editActivity={e => {
                e.preventDefault();
                if (this.props.disabled) {
                  return;
                }
                navigateToActivityFormForContentItem(
                  contentId,
                  activity.type,
                  activity.id,
                );
              }}
            />
          ))}

        {sortedActivities.length === 0 && (
          <div className={styles.activityButtonsWrapper}>
            <button
              className={classnames(
                styles.addActivityButton,
                styles.addQuestionSetActivityButton,
              )}
              onClick={e => {
                e.preventDefault();
                if (this.props.disabled) {
                  return;
                }
                navigateToActivityFormForContentItem(
                  contentId,
                  ACTIVITY_TYPES.QUESTION_SET,
                );
              }}
            >
              <div className={styles.addActivityButtonImage}>
                <img
                  src={ACTIVITY_ICONS[ACTIVITY_TYPES.QUESTION_SET][COLORS.DARKER_GREEN]} // eslint-disable-line
                  alt={`${ACTIVITY_TYPES.QUESTION_SET} icon`}
                />
              </div>
              <div className={styles.addActivityButtonText}>
                {ACTIVITY_UI_NAMES[ACTIVITY_TYPES.QUESTION_SET]}
              </div>
            </button>
            <button
              className={classnames(
                styles.addActivityButton,
                styles.addPollActivityButton,
              )}
              onClick={e => {
                e.preventDefault();
                if (this.props.disabled) {
                  return;
                }
                navigateToActivityFormForContentItem(
                  contentId,
                  ACTIVITY_UI_NAMES[ACTIVITY_TYPES.POLL],
                );
              }}
            >
              <div className={styles.addActivityButtonImage}>
                <img
                  src={ACTIVITY_ICONS[ACTIVITY_TYPES.POLL][COLORS.FADED_GREEN]} // eslint-disable-line
                  alt={`${ACTIVITY_TYPES.POLL} icon`}
                />
              </div>
              <div className={styles.addActivityButtonText}>Poll</div>
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default ActivitiesInput;
