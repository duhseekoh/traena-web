// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Message } from 'semantic-ui-react';
import { FieldArray } from 'redux-form';

import type { ProgressUpdate } from '../../../types';
import type { ImageEntry } from '../../../types/content';
import * as uiContentFormActions from '../../../redux/modules/ui-content-form/actions';
import UploadSuccessIndicator from '../../../components/UploadSuccessIndicator/UploadSuccessIndicator';
import ImageFileDropper from '../components/ImageFileDropper/ImageFileDropper';
import ImagesFieldArray from '../components/ImagesFieldArray/ImagesFieldArray';
import ProgressBar from '../../../components/ProgressBar/ProgressBar';
import LinkButton from '../../../components/LinkButton/LinkButton';
import styles from '../GenericContentForm.scss';
import { UI_PROGRESS_STATUSES, CONTENT_IMAGES_MAX_ALLOWED } from '../../../constants/appConstants';

import * as fromUiContentForm from '../../../redux/modules/ui-content-form/selectors';

type OwnProps = {
  onPressChangeMedia: (e: *) => any,
};

type ConnectedProps = {
  uploadAndProcessImages: (files: File[]) => Promise<any>,
  progress: ProgressUpdate,
  images: ImageEntry[],
};

type Props = OwnProps & ConnectedProps;

/**
 * Fields that enable Image upload to the content form. Conditionally displays
 * the image dropper section and/or the current list of images already set on
 * the content item.
 */
class ImageUploadFields extends Component<Props> {
  props: Props;
  static defaultProps;

  handleDropImage = (files: File[]) => {
    this.props.uploadAndProcessImages(files);
  };

  render() {
    const { images, progress } = this.props;
    const isComplete = progress.status === UI_PROGRESS_STATUSES.COMPLETE;
    const isUploading = progress.status === UI_PROGRESS_STATUSES.IN_PROGRESS;
    const displayUploadSuccess = isComplete;

    return (
      <div>
        <Card fluid>
          <Card.Content>
            <h5>
              Upload Images{' '}
              <LinkButton
                disabled={isUploading}
                onClick={this.props.onPressChangeMedia}
              >
                or switch to Video
              </LinkButton>
            </h5>

            {images.length < CONTENT_IMAGES_MAX_ALLOWED ? (
              <ImageFileDropper
                className={styles.postInput}
                disabled={isUploading}
                onDropImage={this.handleDropImage}
              />
            ) : (
              <Message negative>
                <p>A maximum of {CONTENT_IMAGES_MAX_ALLOWED} images
                  may be included.
                </p>
              </Message>
            )}
            {(images.length > 0 || isUploading) && (
              <FieldArray
                name="body.images"
                component={ImagesFieldArray}
                progress={progress}
              />
            )}
            {displayUploadSuccess && (
              <UploadSuccessIndicator message="Image upload successful" />
            )}
          </Card.Content>
        </Card>
        <ProgressBar percent={progress.percent} />
      </div>
    );
  }
}

ImageUploadFields.defaultProps = {
  images: [],
  progress: {
    message: '',
    percent: 0,
    status: '',
  },
};

const mapStateToProps = state => ({
  progress: fromUiContentForm.selectProgress(state),
  images: fromUiContentForm.selectImages(state),
});

const mapDispatchToProps = dispatch => ({
  uploadAndProcessImages: files => {
    dispatch(uiContentFormActions.uploadAndProcessImages(files));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploadFields);
