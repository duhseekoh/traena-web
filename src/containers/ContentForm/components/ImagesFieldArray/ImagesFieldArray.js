// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import type { FieldArrayProps } from 'redux-form';
import { Loader, Icon } from 'semantic-ui-react';
import styles from './ImagesFieldArray.scss';
import { resetUploadProgress } from '../../../../redux/modules/ui-content-form/actions';
import { UI_PROGRESS_STATUSES } from '../../../../constants/appConstants';
import type { ProgressUpdate } from '../../../../types';

type ConnectedProps = {
  progress: ProgressUpdate,
  resetUploadProgress: () => Promise<any>,
};

class ImagesFieldArray extends Component<ConnectedProps & FieldArrayProps> {
  props: ConnectedProps & FieldArrayProps;

  handleClickRemoveImage = (e, idx, fields) => {
    e.preventDefault();
    this.props.resetUploadProgress();
    fields.remove(idx);
  };

  handleClickImageMoveUp = (e, idx, fields) => {
    e.preventDefault();
    if (idx - 1 >= 0) {
      fields.swap(idx, idx - 1);
    }
  };
  handleClickImageMoveDown = (e, idx, fields) => {
    e.preventDefault();
    if (idx + 1 < fields.length) {
      fields.swap(idx, idx + 1);
    }
  };
  render() {
    const { fields } = this.props;
    const isUploading =
      this.props.progress.status === UI_PROGRESS_STATUSES.IN_PROGRESS;

    const n = isUploading ? fields.length + 1 : fields.length;

    // adding fillerItems to keep items left aligned while also
    // allowing them to fill the space horizontally and wrap
    const fillerItems = [];
    for (let i = 0; i < n; i += 1) {
      fillerItems.push(
        <div
          key={i}
          className={classnames(styles.imageListItem, styles.fillerItem)}
        />,
      );
    }
    return (
      <div className={styles.imageList}>
        {fields.map((imageField, idx) => {
          const { variations, baseCDNPath, id } = fields.get(idx);
          return (
            <a
              href={`${baseCDNPath}${variations.large.filename}`}
              target="_blank"
              className={styles.imageListItem}
              style={{
                'background-image': `url(${baseCDNPath}${
                  variations.medium.filename
                })`,
              }}
              key={id}
            >
              <Icon
                link
                onClick={e => this.handleClickRemoveImage(e, idx, fields)}
                className={styles.toolbarButton}
                name="window close"
              />
              <div className={styles.toolbar}>
                {idx !== 0 && (
                  <Icon
                    link
                    onClick={e => this.handleClickImageMoveUp(e, idx, fields)}
                    className={styles.toolbarButton}
                    name="arrow left"
                  />
                )}
                {idx !== fields.length - 1 && (
                  <Icon
                    link
                    onClick={e => this.handleClickImageMoveDown(e, idx, fields)}
                    className={styles.toolbarButton}
                    name="arrow right"
                  />
                )}
              </div>
            </a>
          );
        })}
        {isUploading && (
          <div className={classnames(styles.imageListItem, styles.loadingItem)}>
            <Loader active inline="centered" />
          </div>
        )}
        {fillerItems}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  resetUploadProgress: () => dispatch(resetUploadProgress()),
});

export default connect(null, mapDispatchToProps)(ImagesFieldArray);
