// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Icon, Loader } from 'semantic-ui-react';
import { change } from 'redux-form';

import * as uiContentFormActions from '../../../redux/modules/ui-content-form/actions';
import UploadSuccessIndicator from '../../../components/UploadSuccessIndicator/UploadSuccessIndicator';
import VideoPlayer from '../../../components/VideoPlayer/VideoPlayer';
import VideoFileDropper from '../components/VideoFileDropper/VideoFileDropper';
import ProgressBar from '../../../components/ProgressBar/ProgressBar';
import LinkButton from '../../../components/LinkButton/LinkButton';
import formStyles from '../GenericContentForm.scss';
import styles from './VideoUploadFields.scss';
import type { ProgressUpdate } from '../../../types';
import type { Video } from '../../../types/content';
import { UI_PROGRESS_STATUSES } from '../../../constants/appConstants';

import * as fromUiContentForm from '../../../redux/modules/ui-content-form/selectors';

type OwnProps = {
  onPressChangeMedia: () => any,
};

type ConnectedProps = {
  uploadAndTranscodeTrainingVideo: (file: File) => any,
  progress: ProgressUpdate,
  video: ?Video,
  removeVideo: () => any,
};

type Props = OwnProps & ConnectedProps;

/**
 * Fields that enable Video upload to the content form. Conditionally displays
 * the video dropper section and/or the current playable video.
 */
class VideoUploadFields extends Component<Props> {
  props: Props;

  renderVideo = () => {
    const { video, removeVideo } = this.props;
    if (!video) {
      // only calling this render method when we have a video, but extra protection
      // and flow insists.
      return null;
    }

    return (
      <div className={styles.videoWrapper}>
        <VideoPlayer video={video} autoplay={false} />
        <div className={styles.toolbar}>
          <Icon
            link
            fitted
            bordered
            onClick={removeVideo}
            className={styles.toolbarButton}
            name="window close"
            alt="Remove video"
            title="Remove video"
          />
        </div>
      </div>
    );
  };

  render() {
    const { video, progress, uploadAndTranscodeTrainingVideo } = this.props;
    const isUploading = progress.status === UI_PROGRESS_STATUSES.IN_PROGRESS;
    const isComplete = progress.status === UI_PROGRESS_STATUSES.COMPLETE;

    const displayDropper = !video && !isUploading;
    const displayVideo = video && !isUploading;
    const displayUploadSuccess = isComplete;
    const displayLoader = isUploading;
    return (
      <div>
        <Card fluid>
          <Card.Content>
            <h5>
              Upload a Video{' '}
              <LinkButton
                disabled={isUploading}
                onClick={this.props.onPressChangeMedia}
              >
                or switch to Images
              </LinkButton>
            </h5>

            {displayLoader && (
              <div>
                <Loader active />
                <div className={styles.progressMessage}>
                  {progress.message} {progress.percent}%
                </div>
              </div>
            )}

            {displayDropper && (
              <VideoFileDropper
                className={formStyles.postInput}
                onDropVideo={uploadAndTranscodeTrainingVideo}
              />
            )}

            {displayVideo && this.renderVideo()}

            {displayUploadSuccess && (
              <UploadSuccessIndicator message="Video upload successful" />
            )}
          </Card.Content>
        </Card>
        <ProgressBar percent={progress.percent} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  progress: fromUiContentForm.selectProgress(state),
  video: fromUiContentForm.selectVideo(state),
});

const mapDispatchToProps = dispatch => ({
  uploadAndTranscodeTrainingVideo: file => {
    dispatch(uiContentFormActions.uploadAndTranscodeTrainingVideo(file));
  },
  removeVideo: () => dispatch(change('content', 'body.video', null)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VideoUploadFields);
