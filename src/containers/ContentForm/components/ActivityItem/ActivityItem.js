/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import type { Activity } from '../../../../types/activities';
import { ACTIVITY_TYPES, ACTIVITY_ICONS } from '../../../../constants/activities';
import { COLORS } from '../../../../constants/colors';
import { routes, getRouteLink } from '../../../../constants/routes';
import styles from './ActivityItem.scss';

type Props = {
  activity: Activity,
  editActivity: () => void,
  contentId: number,
};

/**
 * @TODO: This component should be able to handle different activity types as they are
 * added to the system.
 */

function _getQuestionSetPreviewText(activity: Activity): string {
  return _.get(activity, 'body.questions[0].questionText') || null;
}

function _getPollPreviewText(activity: Activity): string {
  return _.get(activity, 'body.title') || null;
}

function _getPreviewText(activity: Activity): string {
  if (!activity) {
    return null;
  }
  let previewText = null;
  if (activity.type === ACTIVITY_TYPES.QUESTION_SET) {
    previewText = _getQuestionSetPreviewText(activity);
  } else if (activity.type === ACTIVITY_TYPES.POLL) {
    previewText = _getPollPreviewText(activity);
  }
  return previewText;
}

const ActivityItem = ({
  activity,
  editActivity,
  contentId,
}: Props) => {
  const canViewResults = activity.type === ACTIVITY_TYPES.POLL;
  return (
    <button width={15} className={styles.activityItem} onClick={editActivity}>
      <div className={styles.activityItemTypeLabel}>
        <img
          src={ACTIVITY_ICONS[activity.type][COLORS.PRIMARY_GREEN]}
          className={styles.activityItemIcon}
          alt={`${activity.type} icon`}
        />
      </div>
      <div className={styles.activityItemPreview}>
        {_getPreviewText(activity)}
      </div>
      {canViewResults &&
        <Link
          to={getRouteLink(
            routes.ACTIVITY_RESULTS,
            {
              activityId: activity.id,
              contentId,
              activityType: activity.type,
            },
          )}
          onClick={e => e.stopPropagation()}
          className={styles.viewResultsButton}
        >View Results
        </Link>
      }
    </button>
  );
};

export default ActivityItem;
