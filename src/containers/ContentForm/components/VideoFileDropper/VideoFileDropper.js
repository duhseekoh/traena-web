// @flow
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { Button, Icon } from 'semantic-ui-react';
import styles from './VideoFileDropper.scss';

type Props = {
  onDropVideo: (file: File) => Promise<any>,
};

const allowedVideoTypes = 'video/mp4, video/quicktime';

/**
 * Simply an area to that accepts browser drops of video files or triggers
 * a file browser dialog to select videos from.
 * It doesn't stored the selected file reference, just passes it along to the
 * onDropVideo handler.
 */
class VideoFileDropper extends Component<Props> {
  props: Props;

  handleAddFile = (fileToUpload: File) => {
    const { onDropVideo } = this.props;
    onDropVideo(fileToUpload);
  };

  render() {
    return (
      <Dropzone
        onDropAccepted={acceptedFiles => this.handleAddFile(acceptedFiles[0])}
        multiple={false}
        accept={allowedVideoTypes}
        style={{}}
      >
        <div className={styles.fileDropper}>
          <div>
            <h5>Drop a video here</h5>
            <Icon name="video" className={styles.uploadIcon} />
            <div className={styles.blurb}>
              Upload any one video (mp4, mov) you would like to post.
            </div>
            <Button primary onClick={e => e.preventDefault()}>
              Browse
            </Button>
          </div>
        </div>
      </Dropzone>
    );
  }
}

export default VideoFileDropper;
