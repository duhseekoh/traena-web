// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiContentFormActions from '../../redux/modules/ui-content-form/actions';
import * as fromUiContentForm from '../../redux/modules/ui-content-form/selectors';
import * as fromContent from '../../redux/modules/content/selectors';
import * as fromChannels from '../../redux/modules/channels/selectors';
import * as fromActivities from '../../redux/modules/activities/selectors';
import {
  CONTENT_STATUSES,
  CONTENT_TYPES,
} from '../../constants/appConstants';
import type { ContentItem } from '../../types/content';
import type { Activity } from '../../types/activities';
import type { ProgressUpdate } from '../../types';
import GenericContentForm from './GenericContentForm';

type OwnProps = {
  contentId?: number,
};

type ConnectedProps = {
  contentItem?: ContentItem,
  initializeContentFormData: (contentId?: number) => any,
  errorInitializing?: boolean,
  readOnly: boolean,
  activities: Activity[],
  channels: { value: number, text: string },
  defaultChannelId: number,
  loading: boolean,
  progress: ProgressUpdate,
};

type Props = OwnProps & ConnectedProps;

class ContentFormContainer extends Component<Props> {
  props: Props;

  componentDidMount() {
    this.props.initializeContentFormData(this.props.contentId);
  }

  /*
  * Render a different form (Video, Image, or Text), based on the contentType selected by the user,
  */
  renderContentForm = () => {
    /**
     * If this container has been passed a contentId, we know we are editing an existing item.
     * We wait until the contentItem has been fetched before rendering the form,
     * so we can use the fetched values as the form's initialValues
     */
    const {
      contentItem,
      contentId,
      errorInitializing,
      readOnly,
      channels,
      activities,
      loading,
      progress,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading this content to edit. You can refresh to
          try again.
        </div>
      );
    }

    /**
     * If no contentItem, activities, or channels have been loaded yet,
     * but we also have not yet started loading in the data
     */
    if ((contentId && !contentItem) || !channels || !activities) {
      return null;
    }

    /*
    * If we have fetched a contentItem to edit, we set the desiredStatus equal to the status
    * of the item, so the UI runs the appropriate form validation on the edit form
    */
    /* eslint-disable indent */
    const initialValues =
      contentId && contentItem
        ? {
            ...contentItem,
            desiredStatus: contentItem.status,
          }
        : {
            tags: [],
            activitiesOrder: [],
            status: CONTENT_STATUSES.DRAFT,
            body: {
              // default to text type, user switches to image or video by
              // selecting those options in the form
              type: CONTENT_TYPES.DailyAction,
            },
            channelId: this.props.defaultChannelId,
            desiredStatus: CONTENT_STATUSES.DRAFT,
          };
    /* eslint-enable indent */
    return (
      <fieldset disabled={readOnly}>
        <GenericContentForm
          initialValues={initialValues}
          disabled={readOnly}
          series={contentItem ? contentItem.series : null}
          channels={channels}
          activities={activities}
          progress={progress}
        />
      </fieldset>
    );
  };

  render() {
    return <div>{this.renderContentForm()}</div>;
  }
}

const mapStateToProps = (state, props: OwnProps) => ({
  contentItem: fromContent.selectContentItemById(state, props),
  channels: fromChannels.selectSelfInternalSubscriptionsAsInputOptions(state),
  activities: fromActivities.selectActivitiesByContentId(
    state,
    props.contentId,
  ),
  defaultChannelId: fromChannels.selectSelfInternalDefaultSubscriptionId(state),
  errorInitializing: fromUiContentForm.selectErrorLoading(state),
  loading: fromUiContentForm.selectIsLoading(state),
  readOnly: fromUiContentForm.selectIsReadOnly(state),
  progress: fromUiContentForm.selectProgress(state),
});

const mapDispatchToProps = dispatch => ({
  initializeContentFormData: contentId =>
    dispatch(uiContentFormActions.initializeContentFormData(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ContentFormContainer,
);
