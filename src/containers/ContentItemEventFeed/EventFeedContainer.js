// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  Loader,
  Header,
  Table,
} from 'semantic-ui-react';
import * as contentItemAnalyticsFeedActions from '../../redux/modules/content-item-analytics-feed/actions';
import * as fromContentItemAnalyticsFeed from '../../redux/modules/content-item-analytics-feed/selectors';
import EventFeedRow from './components/EventFeedRow';
import type { Event } from '../../types/analytics';

type OwnProps = {|
  // used in mapDispatchToProps
  contentId: number, // eslint-disable-line react/no-unused-prop-types
|};

type ConnectedProps = {|
  error: ?string,
  events: Event[],
  hasMore: boolean,
  isLoading: boolean,
  initialize: () => any,
  getMoreEvents: () => any,
|};

type Props = OwnProps & ConnectedProps;

/**
 * For a single content item, retrieves and renders a select set of analytics events.
 * Allows the user to see any interaction that has ever occured with the content item.
 */
class EventFeed extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const { hasMore, isLoading, events, error, getMoreEvents } = this.props;
    const hasEvents = events.length > 0;

    if (error) {
      return <div>Error: Could not display events.</div>;
    }

    return (
      <Card fluid>
        <Card.Content>
          <Header as="h2">
            Recent Events
          </Header>
          <Loader active={isLoading} />
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Date</Table.HeaderCell>
                <Table.HeaderCell>Event</Table.HeaderCell>
                <Table.HeaderCell>User</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {hasEvents && events.map(event =>
                (<EventFeedRow
                  key={event.id}
                  event={event}
                />))
              }
              {
                !hasEvents && isLoading &&
                <Table.Row>
                  <Table.Cell colSpan="3">No events have been tracked for this post yet.</Table.Cell>
                </Table.Row>
              }
            </Table.Body>
            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan="3" textAlign="center">
                  { hasEvents && hasMore &&
                    <Button
                      loading={isLoading}
                      disabled={isLoading}
                      onClick={getMoreEvents}
                      primary
                    >Load More
                    </Button>
                  }
                  { hasEvents && !hasMore &&
                    <div>end of results</div>
                  }
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
        </Card.Content>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: fromContentItemAnalyticsFeed.selectIsLoading(state),
  error: fromContentItemAnalyticsFeed.selectError(state),
  events: fromContentItemAnalyticsFeed.selectEvents(state),
  hasMore: fromContentItemAnalyticsFeed.selectHasMore(state),
});

const mapDispatchToProps = (dispatch, { contentId }: OwnProps) => ({
  initialize: () =>
    dispatch(contentItemAnalyticsFeedActions.initialize(contentId)),
  getMoreEvents: () =>
    dispatch(contentItemAnalyticsFeedActions.getMoreEvents()),
});

export default connect(mapStateToProps, mapDispatchToProps)(EventFeed);
