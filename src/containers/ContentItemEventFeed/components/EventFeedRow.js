// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import TimeAgo from '../../../components/Date/TimeAgo';
import type { Event } from '../../../types/events';

type Props = {
  event: Event,
}

function EventFeedRow(props: Props) {
  return (
    <Table.Row>
      <Table.Cell>
        <TimeAgo date={props.event.createdTimestamp} />
      </Table.Cell>
      <Table.Cell>{props.event.type}</Table.Cell>
      <Table.Cell>
        {props.event.user.firstName} {props.event.user.lastName}
      </Table.Cell>
    </Table.Row>
  );
}

export default EventFeedRow;
