// @flow
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Grid, Button, Loader } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import DateShort from '../../components/Date/DateShort';
import * as uiSeriesDetailActions from '../../redux/modules/ui-series-detail/actions';
import * as fromUiSeriesDetail from '../../redux/modules/ui-series-detail/selectors';
import * as fromSeries from '../../redux/modules/series/selectors';
import SeriesContentTableContainer from '../SeriesContentTable/SeriesContentTableContainer';
import SeriesContentTable from './components/SeriesContentTable';
import AccessControl from '../../components/AccessControl/AccessControl';
import * as permissionUtils from '../../utils/permissionUtils';
import type { Series } from '../../types/series';

import styles from './SeriesDetailContainer.scss';

type OwnProps = {
  seriesId: number,
};

type ConnectedProps = {
  initialize: (seriesId: number) => any,
  errorInitializing: boolean,
  loading: boolean,
  series: Series,
};

type Props = OwnProps & ConnectedProps;

class SeriesDetailContainer extends Component<Props> {
  props: Props;

  componentWillMount() {
    if (!this.props.seriesId) {
      return;
    }
    this.props.initialize(this.props.seriesId);
  }

  render() {
    const {
      seriesId,
      series,
      loading,
      errorInitializing,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading details for this series.
        </div>
      );
    }

    if (!series) {
      return null;
    }

    const { organization } = series;
    return (
      <Grid stackable>
        <Grid.Row className={styles.headerRow}>
          <Grid.Column width={8}>
            <h1>
              {series.title}
            </h1>
          </Grid.Column>
          <Grid.Column width={8}>
            <AccessControl
              customAccessData={{
                organizationId: organization.id,
              }}
              accessCheck={permissionUtils.canModifySeries}
              noAccessView={<span />}
            >
              <Button
                primary
                size="big"
                as={Link}
                to={getRouteLink(routes.SERIES_EDIT, { seriesId })}
                className="right"
              >Edit Series
              </Button>
            </AccessControl>
            <Button
              default
              size="big"
              className="right"
              as={Link}
              to={getRouteLink(routes.SERIES_STATS, { seriesId })}
            >View Stats
            </Button>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={8}>
            <div className={styles.authorInfo}>
              <div>
                by{' '}
                <strong>
                  {series.author.firstName}{' '}
                  {series.author.lastName}
                </strong>
              </div>
              <div>
                Org <strong>{organization && organization.name}</strong>
              </div>
              <div>
                Published{' '}
                <strong>
                  <DateShort
                    date={series.createdTimestamp}
                  />
                </strong>
              </div>
              <div className={styles.description}>
                {series.description}
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <SeriesContentTableContainer
              {...this.props}
              seriesId={seriesId}
              renderTable={(props) => ( // eslint-disable-line
                <SeriesContentTable
                  {...props}
                />
              )}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = (state, props) => ({
  errorInitializing: fromUiSeriesDetail.selectErrorLoading(state),
  loading: fromUiSeriesDetail.selectIsLoading(state),
  series: fromSeries.selectSeries(state, props.seriesId),
});

const mapDispatchToProps = dispatch => ({
  initialize: (seriesId) =>
    dispatch(uiSeriesDetailActions.initialize(seriesId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  SeriesDetailContainer,
);
