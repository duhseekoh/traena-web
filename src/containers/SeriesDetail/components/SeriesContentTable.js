// @flow
import React from 'react';
import { Icon, Table } from 'semantic-ui-react';
import styles from './SeriesContentTable.scss';
import ContentTableRow from './SeriesContentTableRow';
import Pagination from '../../../components/Pagination/Pagination';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';

import type { Pagination as PaginationType } from '../../../types';
import type { ContentItem } from '../../../types/content';

type Props = {|
  contents: ContentItem[],
  pagination: PaginationType,
  goToContentsPage: (page: number) => any,
  userOrganizationId: number,
  onPressTableRow: (contentId: number) => any,
|};

/**
 * Management table to display posts that are filtered to exclusively 'Published'
 */
const SeriesContentTable = (props: Props) => {
  if (!props.contents.length) {
    return (
      <h2 className={styles.contentTableNoContentMessage}>
        No posts have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table className={styles.contentTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell>#</TableHeaderCell>
            <TableHeaderCell>Type</TableHeaderCell>
            <TableHeaderCell>Author</TableHeaderCell>
            <TableHeaderCell>Channel</TableHeaderCell>
            <TableHeaderCell>Title</TableHeaderCell>
            <TableHeaderCell title="tags" className={styles.alignRight}>
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell title="likes" className={styles.alignRight}>
              <Icon name="empty heart" />
            </TableHeaderCell>
            <TableHeaderCell title="comments" className={styles.alignRight}>
              <Icon name="comment outline" />
            </TableHeaderCell>
            <TableHeaderCell title="completed" className={styles.alignRight}>
              <Icon name="check circle outline" />
            </TableHeaderCell>
            <TableHeaderCell className={styles.alignRight}>
              Published
            </TableHeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.contents.map((content, i) => (
            <ContentTableRow
              key={content.id}
              content={content}
              order={i}
              userOrganizationId={props.userOrganizationId}
              onPressTableRow={props.onPressTableRow}
            />
          ))}
        </Table.Body>
      </Table>
      {props.pagination &&
        <Pagination
          pagination={props.pagination}
          goToPage={props.goToContentsPage}
        />
      }
    </div>
  );
};

SeriesContentTable.defaultProps = {
  contents: [],
};

export default SeriesContentTable;
