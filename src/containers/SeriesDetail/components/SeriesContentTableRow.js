// @flow
import React from 'react';
import moment from 'moment';
import classnames from 'classnames';
import { Table, Popup } from 'semantic-ui-react';
import type { ContentItem } from '../../../types/content';
import DateShort from '../../../components/Date/DateShort';
import styles from './SeriesContentTable.scss';
import TableRow from '../../../components/Table/TableRow';

type Props = {|
  content: ContentItem,
  userOrganizationId: number,
  order: number,
  onPressTableRow: (contentId: number) => any,
|};

const contentTypesToDisplayNames = {
  DailyAction: 'Text',
  TrainingVideo: 'Video',
  Image: 'Image',
};

const SeriesContentTableRow = (props: Props) => {
  const { content, userOrganizationId, onPressTableRow, order } = props;
  // TODO: wrap delete with component that verifies access rights
  // to decide if it should be displayed
  // https://panderaappdev.atlassian.net/browse/TRN-456
  const isOwnOrg = content.author.organization.id === userOrganizationId;
  const displayOrgName = !isOwnOrg; // org name is implied for org own org content in table

  return (
    <TableRow
      key={content.id}
      onClick={() => onPressTableRow(content.id)}
    >
      <Table.Cell>{order + 1}</Table.Cell>
      <Table.Cell>{contentTypesToDisplayNames[content.body.type]}</Table.Cell>
      <Table.Cell>{`${content.author.firstName} ${content.author.lastName}`}</Table.Cell>
      <Table.Cell>
        <div>{content.channel.name}</div>
        {displayOrgName && (
          <div className={styles.subText}>
            {content.author.organization.name}
          </div>
        )}
      </Table.Cell>
      <Table.Cell>{content.body.title}</Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.tags.length}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.likeCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.commentCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.completedCount}
      </Table.Cell>
      <Table.Cell
        className={
          !content.publishedTimestamp
            ? classnames(styles.fadedCell, styles.alignRight)
            : styles.alignRight
        }
      >
        {content.publishedTimestamp ? (
          <Popup
            trigger={<div><DateShort date={content.publishedTimestamp} /></div>}
            content={`${moment().diff(moment(content.publishedTimestamp), 'days')} days live`}
            basic
            on="hover"
            position="bottom center"
          />
        ) : (
          'None'
        )}
      </Table.Cell>
    </TableRow>
  );
};

export default SeriesContentTableRow;
