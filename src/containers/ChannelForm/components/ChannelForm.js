// @flow
import React from 'react';
import { reduxForm } from 'redux-form';
import { withRouter, type ContextRouter } from 'react-router-dom';
import { TextField } from '@panderalabs/redux-form-semantic';
import { Button, Grid, Message, Icon, Loader } from 'semantic-ui-react';
import styles from '../ChannelForm.scss';
import type { Channel } from '../../../types/channels';
import Modal from '../../../components/Modal/Modal';
import TextFieldCharCount from '../../../components/forms/TextFieldCharCount/TextFieldCharCount';
import TextInputCharCount from '../../../components/forms/TextInputCharCount/TextInputCharCount';
import { COLORS } from '../../../constants/colors';
import { CHANNEL_VISIBILITY_DISPLAY } from '../../../constants/appConstants';
import DateShort from '../../../components/Date/DateShort';

type OwnProps = {
  channel: Channel,
  channelId: number,
  saving: boolean,
  deleting: boolean,
  onSubmit: (channel: Channel) => any,
  onDelete: (channelId: number) => any,
  channelNameLength: number,
  organizationName: ?string,
};
type ConnectedProps = {};
type ReduxFormProps = {
  error: string,
  handleSubmit: (*) => any,
};

type Props = OwnProps & ConnectedProps & ReduxFormProps & ContextRouter;

const ChannelForm = (props: Props) => {
  const {
    channel,
    channelId,
    error,
    saving,
    deleting,
    handleSubmit,
    onSubmit,
    onDelete,
    channelNameLength,
    organizationName,
  } = props;
  return (
    <Modal opacity={0.8} backgroundColor={COLORS.LIGHT_GREY}>
      <form
        className={styles.channelForm}
        autoComplete="off"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <h3>
                {channelId ? 'Edit Channel' : 'Create Channel'}
              </h3>
              <h5>Name *</h5>
              <TextInputCharCount
                name="name"
                maxLength={50}
                placeholder="Add a name to your channel..."
                initialValue={channel ? channel.name : ''}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <h5>Description</h5>
              <TextFieldCharCount
                name="description"
                maxLength={150}
                placeholder="Describe the purpose of your channel..."
                className={styles.cantResize}
                initialValue={channel ? channel.description : ''}
              />
              <TextField name="visibility" className={styles.hiddenInput} />
            </Grid.Column>
          </Grid.Row>

          {channel &&
            <Grid.Row columns={3}>
              <Grid.Column>
                <h5>Channel Type</h5>
                {CHANNEL_VISIBILITY_DISPLAY[channel.visibility]}
              </Grid.Column>
              <Grid.Column>
                <h5>Created By</h5>
                {organizationName}
              </Grid.Column>
              <Grid.Column>
                <h5>Date Created</h5>
                <DateShort date={channel.createdTimestamp} />
              </Grid.Column>
            </Grid.Row>
          }

          {error &&
            <Grid.Row>
              <Grid.Column width={16}>
                <Message
                  error
                  header="Error"
                  channel={error}
                />
              </Grid.Column>
            </Grid.Row>
          }

          <Grid.Row>
            <Grid.Column width={4}>
              {deleting ?
                <Loader active /> :
                <button
                  onClick={e => {
                    e.preventDefault();
                    onDelete(channelId);
                  }}
                  className={styles.deleteButton}
                >
                  <Icon name="trash" size="large" className={styles.deleteIcon} />
                </button>}
            </Grid.Column>
            <Grid.Column width={12}>
              <Button
                primary
                size="big"
                loading={saving}
                disabled={channelNameLength < 1}
                onClick={(e) => {
                  e.preventDefault();
                  setTimeout(handleSubmit(onSubmit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
                }}
                className="right"
              >Save
              </Button>
              <Button
                default
                size="big"
                type="submit"
                className="right"
                onClick={e => {
                  e.preventDefault();
                  props.history.goBack();
                }}
              >Cancel
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </form>
    </Modal>
  );
};


const channelForm = reduxForm({
  form: 'channel',
  touchOnBlur: false,
})(withRouter(ChannelForm));

export default channelForm;
