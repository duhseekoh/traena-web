// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as formValidators from '../../utils/formValidators';
import * as uiChannelFormActions from '../../redux/modules/ui-channel-form/actions';
import * as fromChannels from '../../redux/modules/channels/selectors';
import * as fromSubscriptions from '../../redux/modules/subscriptions/selectors';
import * as fromUiChannelForm from '../../redux/modules/ui-channel-form/selectors';
import { CHANNEL_VISIBILITIES } from '../../constants/appConstants';
import type { Channel } from '../../types/channels';

import ChannelForm from './components/ChannelForm';

type OwnProps = {
  channelId: number,
}

type ConnectedProps = {
  initializeChannelFormData: (channelId: number) => any,
  saveChannel: (channel: Channel) => any,
  deleteChannel: (channelId: number) => any,
  channel: Channel,
  loading: boolean,
  deleting: boolean,
  errorInitializing: boolean,
  saving: boolean,
  channelNameLength: number,
  organizationName: ?string,
}

type Props = OwnProps & ConnectedProps;

class ChannelFormContainer extends Component<Props> {
  componentDidMount = () => {
    this.props.initializeChannelFormData(
      this.props.channelId,
    );
  }

  submit = (channel: Channel) => {
    this.props.saveChannel(channel);
  }

  delete = (channelId: number) => {
    this.props.deleteChannel(channelId);
  }

  render() {
    const {
      channel,
      channelId,
      errorInitializing,
      loading,
      saving,
      deleting,
      channelNameLength,
      organizationName,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading this channel. You can refresh to
          try again.
        </div>
      );
    }

    if (channelId && !channel) {
      return null;
    }
    /* eslint-disable indent */
    const initialValues = channelId
      ? {
          ...channel,
        }
      : {
          name: '',
          description: '',
          visibility: CHANNEL_VISIBILITIES.INTERNAL,
        };
    /* eslint-enable indent */
    return (
      <ChannelForm
        channel={channel}
        initialValues={initialValues}
        onSubmit={this.submit}
        onDelete={this.delete}
        saving={saving}
        deleting={deleting}
        editing={!!channelId}
        channelId={channelId}
        validate={formValidators.validateChannelForm}
        channelNameLength={channelNameLength}
        organizationName={organizationName}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  initializeChannelFormData: (channelId) =>
    dispatch(
      uiChannelFormActions.initializeChannelFormData(channelId),
    ),
  saveChannel: channel =>
    dispatch(
      uiChannelFormActions.saveChannel(channel),
    ),
  deleteChannel: channelId =>
    dispatch(
      uiChannelFormActions.deleteChannel(channelId),
    ),
});

const mapStateToProps = (state, { channelId }) => ({
  channel: fromChannels.selectChannelById(state, channelId),
  loading: fromUiChannelForm.selectIsLoading(state),
  errorInitializing: fromUiChannelForm.selectErrorLoading(state),
  saving: fromUiChannelForm.selectIsSaving(state),
  deleting: fromUiChannelForm.selectDeleting(state),
  channelNameLength: fromUiChannelForm.selectChannelNameLength(state),
  organizationName: fromSubscriptions
    .selectSubscriptionOrgName(state, channelId),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ChannelFormContainer,
);
