// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Loader, Grid, Statistic } from 'semantic-ui-react';
import * as contentItemAnalyticsStatsActions from '../../redux/modules/content-item-analytics-stats/actions';
import * as fromContentItemAnalyticsStats from '../../redux/modules/content-item-analytics-stats/selectors';
import type { ContentItemAggregateStats } from '../../types/analytics';
import styles from './AggregateStatsContainer.scss';

type OwnProps = {|
  // used in mapDispatchToProps
  contentId: number, // eslint-disable-line react/no-unused-prop-types
|};

type ConnectedProps = {|
  error: ?string,
  aggregateStats: ?ContentItemAggregateStats,
  isLoading: boolean,
  getAggregateStats: () => Promise<*>,
|};

type Props = OwnProps & ConnectedProps;

/**
 * For a single content item, retrieves and renders basic aggregate stats.
 */
class AggregateStats extends Component<Props> {
  props: Props;

  componentWillMount() {
    this.props.getAggregateStats();
  }

  renderStats = () => {
    const { aggregateStats } = this.props;
    if (!aggregateStats) {
      return null;
    }

    return (
      <Grid columns={4} stackable centered>
        <Grid.Row divided>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>{aggregateStats.likes}</Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Likes
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>{aggregateStats.comments}</Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Comments
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>{aggregateStats.completedTasks}</Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Completions
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
          <Grid.Column>
            <Statistic className={styles.statistic}>
              <Statistic.Value>{aggregateStats.views}</Statistic.Value>
              <Statistic.Label className={styles.statisticLabel}>
                Views
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  };

  render() {
    const { error, isLoading } = this.props;
    if (error) {
      return <div>Error: Could not display stats.</div>;
    }

    return (
      <Card fluid>
        <Card.Content>
          <Loader active={isLoading} />
          {this.renderStats()}
        </Card.Content>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: fromContentItemAnalyticsStats.selectAggregateStatsIsLoading(state),
  error: fromContentItemAnalyticsStats.selectAggregateStatsError(state),
  aggregateStats: fromContentItemAnalyticsStats.selectAggregateStats(state),
});

const mapDispatchToProps = (dispatch, { contentId }: OwnProps) => ({
  getAggregateStats: () =>
    dispatch(contentItemAnalyticsStatsActions.getAggregateStats(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AggregateStats);
