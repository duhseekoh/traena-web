// @flow

import React from 'react';
import { Loader } from 'semantic-ui-react';
import styles from '../../SubscribedUsersTable/SubscribedUsersTable.scss';
import Pagination from '../../../components/Pagination/Pagination';
import type { Pagination as PaginationType } from '../../../types';
import type { User } from '../../../types/users';

type ConnectedProps = {
  users: User[],
  pagination: PaginationType,
  loading: boolean,
  error: boolean,
  goToUsersPage: (page: number) => any,
};

type Props = ConnectedProps;

/**
* This table version is rendered with no clickable actions, and few details for each user
* To be passed into the `renderTable` method of a users table container.
* For example, <SubscribedUsersTable renderTable={() => <BasicUsersTable {...props}} />
*/
const BasicUsersTable = (props: Props) => {
  const { loading, users, error, pagination } = props;

  const noSubscribers = !loading && !error && !users.length;
  const subscribersLoaded = !error && !noSubscribers;
  return (
    <div className={styles.subscribersTableWrapper}>
      {loading ?
        <Loader active /> :
        <div>
          {error &&
            <h3 className={styles.subscribersTableNoContentMessage}>
              Error loading users...
            </h3>
          }

          {noSubscribers &&
            <h3 className={styles.subscribersTableNoContentMessage}>
              No users...
            </h3>
          }

          {subscribersLoaded &&
            <div>
              <table className={styles.subscribersTable}>
                <thead>
                  <tr>
                    <th>NAME</th>
                    <th>TITLE</th>
                    <th>TAGS</th>
                  </tr>
                </thead>
                <tbody>
                  {users.map(user => (
                    <tr key={user.id}>
                      <td>{user.firstName} {user.lastName}</td>
                      <td>{user.position}</td>
                      <td>
                        {user.tags && user.tags.length &&
                          `#${user.tags.join(' #')}`}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>

              <Pagination
                pagination={pagination}
                goToPage={props.goToUsersPage}
              />
            </div>
          }
        </div>}
    </div>
  );
};


export default BasicUsersTable;
