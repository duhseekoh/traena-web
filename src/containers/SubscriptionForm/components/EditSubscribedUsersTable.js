// @flow
import React from 'react';
import { Table, Icon } from 'semantic-ui-react';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import styles from './EditSubscribedUsersTable.scss';
import EditSubscribedUsersTableRow from './EditSubscribedUsersTableRow';
import Pagination from '../../../components/Pagination/Pagination';
import type { Pagination as PaginationType } from '../../../types';
import type { User } from '../../../types/users';

type Props = {
  users: User[],
  pagination: PaginationType,
  subscribedUserIds: number[],
  goToUsersPage: (page: number) => any,
  addUser: (userId: number) => any,
  removeUser: (userId: number) => any,
};

const EditSubscribedUsersTable = (props: Props) => {
  // eslint-disable-line
  if (!props.users.length) {
    return (
      <h2 className={styles.usersTableNoContentMessage}>
        No users have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.subscribersTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell />
            <TableHeaderCell {...props} property="firstName">
              Name
            </TableHeaderCell>
            <TableHeaderCell {...props} property="position">
              Position
            </TableHeaderCell>
            <TableHeaderCell title="tags" className={styles.alignRight}>
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell />
          </Table.Row>
        </Table.Header>
        <tbody>
          {props.users.map(user => (
            <EditSubscribedUsersTableRow
              key={user.id}
              user={user}
              addUser={props.addUser}
              removeUser={props.removeUser}
              selected={props.subscribedUserIds.includes(user.id)}
            />
          ))}
        </tbody>
      </Table>
      <Pagination
        pagination={props.pagination}
        goToPage={props.goToUsersPage}
      />
    </div>
  );
};

export default EditSubscribedUsersTable;
