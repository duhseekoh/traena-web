// @flow
import React from 'react';
import { Table, Icon } from 'semantic-ui-react';
import type { User } from '../../../types/users';
import styles from './EditSubscribedUsersTable.scss';
import TableRow from '../../../components/Table/TableRow';

type Props = {
  user: User,
  selected: boolean,
  addUser: (userId: number) => any,
  removeUser: (userId: number) => any,
};

const EditSubscribedUsersTableRow = (props: Props) => {
  const {
    user,
    selected,
    addUser,
    removeUser,
  } = props;
  return (
    <TableRow
      key={user.id}
      onClick={() => {
        if (selected) {
          removeUser(user.id);
        } else if (!selected) {
          addUser(user.id);
        }
      }}
    >
      <Table.Cell><Icon name={selected ? 'check' : ''} /></Table.Cell>
      <Table.Cell>
        {`${user.firstName} ${user.lastName}`}
        {user.isDisabled && (
          <Icon
            className={styles.disabledUserIcon}
            title="User is disabled"
            name="ban"
          />
        )}
      </Table.Cell>
      <Table.Cell>{user.position}</Table.Cell>
      <Table.Cell>
        {(user.tags && user.tags.length > 0) &&
          `#${user.tags.join(' #')}`}
      </Table.Cell>
    </TableRow>
  );
};

export default EditSubscribedUsersTableRow;
