// @flow
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Prompt } from 'react-router-dom';
import classnames from 'classnames';
import { Button, Grid, Message, Icon } from 'semantic-ui-react';
import InfoPopup from '../../../components/InfoPopup/InfoPopup';
import FixedFooter from '../../../components/ui-fixed-footer/FixedFooter';
import UsersTableContainer from '../../../containers/UsersTable/UsersTableContainer';
import EditSubscribedUsersTable from './EditSubscribedUsersTable';
import styles from '../SubscriptionForm.scss';
import type { Subscription } from '../../../types/subscriptions';
import SubscribedUsersTable from '../../SubscribedUsersTable/SubscribedUsersTable';
import { SUBSCRIPTION_VISIBILITIES } from '../../../constants/appConstants';
import type { SubscriptionVisibility } from '../../../constants/appConstants';

type OwnProps = {
  editing: boolean,
  subscription: Subscription,
  errorInitializing: boolean,
  loading: boolean,
  saving: boolean,
  addUser: (userId: number) => any,
  removeUser: (userId: number) => any,
  onSubmit: (subscription: Subscription) => any,
  subscribedUserIds: number[],
  setEditing: (editing: boolean) => any,
  subscriptionVisibility: SubscriptionVisibility,
  subscribedUsersCount: number,
};
type ConnectedProps = {};
type ReduxFormProps = {
  error: string,
  handleSubmit: (*) => any,
  dirty: boolean,
  reset: (*) => any,
};
type Props = OwnProps & ConnectedProps & ReduxFormProps;

class SubscriptionForm extends Component<Props> {
  componentWillUnmount() {
    this.props.setEditing(false);
  }

  renderNoUsersDetails = () => null;

  renderSubscribedUsersDetails = props => (
    <div>
      <h5>
        Internal Audience {this.renderInfoButtonUsers()}
        <button
          className={styles.editUserSubscriptionsButton}
          onClick={e => {
            e.preventDefault();
            props.setEditing(true);
          }}
        >
          Edit
        </button>
      </h5>
      <SubscribedUsersTable {...props} />
    </div>
  );

  renderSubscribedUsersDetailsEditing = props => (
    <div>
      <h5>
        Internal Audience {this.renderInfoButtonUsers()}
        {!props.editing && (
          <button
            className={styles.editUserSubscriptionsButton}
            onClick={e => {
              e.preventDefault();
              props.setEditing(true);
            }}
          >
            Edit
          </button>
        )}
      </h5>
      <UsersTableContainer
        {...props}
        renderTable={(
          tableProps,
        ) => <EditSubscribedUsersTable {...tableProps} />}
      />
    </div>
  );

  renderAllUsersSubscribedDetails = () => (
    <div>
      <h5>Selected Users {this.renderInfoButtonUsers()}</h5>
      Everyone in your organization can see this channel.
    </div>
  );

  renderInfoButtonUsers = () => (
    <InfoPopup
      position="bottom left"
      content={
        <div>
          <p>
            {`By default, no one is subscribed to a new channel, which gives you
            the opportunity to confidently set up and launch your channel.`}
          </p>
          <p>
            {`When you're ready, you can change the channel's audience to your
            entire organization or subscribe individual team members.`}
          </p>
          <p>
            {`When the entire organization is subscribed, new team members will be
            added to the channel automatically. Otherwise, you'll have to
            manually add new team members to the audience.`}
          </p>
        </div>
      }
    />
  );

  renderNoSubscribedUsersDetails = () => (
    <div>
      <h5>Selected Users {this.renderInfoButtonUsers()}</h5>
      No one in your organization can see this channel.
    </div>
  );

  renderFooter = props => (
    <FixedFooter>
      <div className={styles.fixedFooterFormButtons}>
        <Grid>
          <Grid.Row centered className="compact">
            <Grid.Column width={12}>
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={8} tablet={16}>
                    <h2>Subscribe Users</h2>
                  </Grid.Column>
                  <Grid.Column computer={8} tablet={16}>
                    <Button
                      primary
                      size="big"
                      type="submit"
                      className="right"
                      loading={props.saving}
                      onClick={e => {
                        e.preventDefault();
                        setTimeout(props.handleSubmit(props.onSubmit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
                      }}
                    >
                      Save
                    </Button>
                    <Button
                      default
                      className={classnames(styles.iconButton, 'right')}
                      onClick={() => {
                        props.reset();
                        props.setEditing(false);
                      }}
                      icon="x"
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </FixedFooter>
  );

  render() {
    const { props } = this;
    const {
      error,
      editing,
      setEditing,
      subscriptionVisibility,
      subscribedUsersCount,
      dirty,
    } = props;

    const displayEditSubscribedUsersTable =
      editing && subscriptionVisibility === SUBSCRIPTION_VISIBILITIES.USERS;
    const displaySubscribedUsersTable =
      !editing && subscriptionVisibility === SUBSCRIPTION_VISIBILITIES.USERS;
    const displayAllUsersTable =
      subscriptionVisibility === SUBSCRIPTION_VISIBILITIES.ORGANIZATION;
    const noSubscribedUsers =
      subscriptionVisibility === SUBSCRIPTION_VISIBILITIES.ADMINS;

    return (
      <form className={styles.seriesForm} autoComplete="off">
        <Prompt
          when={dirty}
          message={
            "You have unsaved changes. Click 'OK' to discard. or 'Cancel' to continue editing."
          }
        />
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={styles.tableTitle}>
                <Icon name="user" className={styles.tableIcon} />
                USERS SUBSCRIBED
                <p className={styles.tableSubtitle}>{subscribedUsersCount}</p>
              </div>
            </Grid.Column>

            <Grid.Column width={16}>
              <label
                className={styles.visibilityRadioInput}
                htmlFor={SUBSCRIPTION_VISIBILITIES.ADMINS}
              >
                <Field
                  onClick={() => setEditing(true)}
                  name="visibility"
                  component="input"
                  type="radio"
                  id={SUBSCRIPTION_VISIBILITIES.ADMINS}
                  value={SUBSCRIPTION_VISIBILITIES.ADMINS}
                  label="No one"
                />{' '}
                No One
              </label>
              <label
                className={styles.visibilityRadioInput}
                htmlFor={SUBSCRIPTION_VISIBILITIES.ORGANIZATION}
              >
                <Field
                  onClick={() => setEditing(true)}
                  name="visibility"
                  component="input"
                  type="radio"
                  id={SUBSCRIPTION_VISIBILITIES.ORGANIZATION}
                  value={SUBSCRIPTION_VISIBILITIES.ORGANIZATION}
                  label="Entire Organization"
                />{' '}
                Entire Organization
              </label>
              <label
                className={styles.visibilityRadioInput}
                htmlFor={SUBSCRIPTION_VISIBILITIES.USERS}
              >
                <Field
                  onClick={() => setEditing(true)}
                  name="visibility"
                  component="input"
                  type="radio"
                  id={SUBSCRIPTION_VISIBILITIES.USERS}
                  value={SUBSCRIPTION_VISIBILITIES.USERS}
                  label="Select Users"
                />{' '}
                Private
              </label>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              {displayEditSubscribedUsersTable &&
                this.renderSubscribedUsersDetailsEditing(props)}

              {displaySubscribedUsersTable &&
                this.renderSubscribedUsersDetails(props)}

              {displayAllUsersTable && this.renderAllUsersSubscribedDetails()}

              {noSubscribedUsers && this.renderNoSubscribedUsersDetails()}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column computer={8} mobile={16}>
              {error && <Message error header="Error" series={error} />}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        {editing && this.renderFooter(props)}
      </form>
    );
  }
}

const subscriptionForm = reduxForm({
  form: 'subscription',
  enableReinitialize: true,
})(SubscriptionForm);

export default subscriptionForm;
