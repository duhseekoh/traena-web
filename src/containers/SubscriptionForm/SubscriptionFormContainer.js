import React, { Component } from 'react';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import { Loader } from 'semantic-ui-react';
import * as uiSubscriptionFormActions from '../../redux/modules/ui-subscription-form/actions';
import * as fromUiSubscriptionForm from '../../redux/modules/ui-subscription-form/selectors';
import * as fromUiSubscribedUsersTable from '../../redux/modules/ui-subscribed-users-table/selectors';
import * as fromSubscriptions from '../../redux/modules/subscriptions/selectors';
import type { Subscription } from '../../types/subscriptions';
import * as formValidators from '../../utils/formValidators';
import { SubscriptionVisibility } from '../../constants/appConstants';
import SubscriptionForm from './components/SubscriptionForm';

type OwnProps = {
  subscriptionId?: number,
};

type ConnectedProps = {
  editing: boolean,
  subscription?: Subscription,
  errorInitializing?: boolean,
  channelId: number,
  loading: boolean,
  initializeSubscriptionForm: (channelId?: number) => any,
  saveSubscription: (subscription: Subscription) => any,
  addUser: (id: number) => any,
  removeUser: (id: number) => any,
  setEditing: (editing: boolean) => any,
  subscribedUserIds: number[],
  subscriptionVisibility: SubscriptionVisibility,
  subscribedUsersCount: number,
};

type Props = OwnProps & ConnectedProps;

class SubscriptionFormContainer extends Component<Props> {
  props: Props;

  componentDidMount() {
    this.props.initializeSubscriptionForm(this.props.channelId);
  }

  submit = (subscription: Subscription) => {
    this.props.saveSubscription(subscription);
  }

  addUser = (userId: number) => {
    this.props.addUser(userId);
  }

  removeUser = (userId: number) => {
    this.props.removeUser(userId);
  }

  render = () => {
    const { props } = this;
    const {
      channelId,
      subscription,
      errorInitializing,
      loading,
      editing,
      setEditing,
      subscriptionVisibility,
      subscribedUsersCount,
    } = props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading subscription data for this channel.
        </div>
      );
    }

    if (channelId && !subscription) {
      return null;
    }

    return (
      <fieldset>
        <div>
          <SubscriptionForm
            {...props}
            subscribedUsersCount={subscribedUsersCount}
            initialValues={subscription}
            validate={formValidators.validateSubscriptionForm}
            addUser={this.addUser}
            removeUser={this.removeUser}
            onSubmit={this.submit}
            editing={editing}
            setEditing={setEditing}
            subscriptionVisibility={subscriptionVisibility}
          />
        </div>
      </fieldset>
    );
  }
}

const mapStateToProps = (state, props) => ({
  errorInitializing: fromUiSubscriptionForm.selectErrorLoading(state),
  loading: fromUiSubscriptionForm.selectIsLoading(state),
  saving: fromUiSubscriptionForm.selectIsSaving(state),
  subscription: fromSubscriptions.selectSubscription(state, props.channelId),
  subscribedUserIds: fromUiSubscriptionForm.selectSubscribedUserIds(state),
  editing: fromUiSubscriptionForm.selectEditing(state),
  subscriptionVisibility: formValueSelector('subscription')(state, 'visibility'),
  subscribedUsersCount: fromUiSubscribedUsersTable.selectTotalElements(state),
});

const mapDispatchToProps = dispatch => ({
  initializeSubscriptionForm: channelId =>
    dispatch(uiSubscriptionFormActions.initializeSubscriptionForm(channelId)),
  saveSubscription: (subscription: Subscription) =>
    dispatch(uiSubscriptionFormActions.saveSubscription(subscription)),
  addUser: userId =>
    dispatch(uiSubscriptionFormActions.addUser(userId)),
  removeUser: userId =>
    dispatch(uiSubscriptionFormActions.removeUser(userId)),
  setEditing: editing =>
    dispatch(uiSubscriptionFormActions.setEditing(editing)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubscriptionFormContainer);
