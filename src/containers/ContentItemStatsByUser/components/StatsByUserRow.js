// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import type { UserWithContentStats } from '../../../types/analytics';

type Props = {|
  userWithStats: UserWithContentStats,
|};

function StatsByUserRow(props: Props) {
  const { user, contentItemStats } = props.userWithStats;
  return (
    <Table.Row>
      <Table.Cell>{user.firstName} {user.lastName}</Table.Cell>
      <Table.Cell>{contentItemStats.liked ? '1' : '0'}</Table.Cell>
      <Table.Cell>{contentItemStats.comments}</Table.Cell>
      <Table.Cell>{contentItemStats.completed ? 'Yes' : 'No'}</Table.Cell>
      <Table.Cell>{contentItemStats.views}</Table.Cell>
    </Table.Row>
  );
}

export default StatsByUserRow;
