// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader, Header, Table } from 'semantic-ui-react';
import * as contentItemAnalyticsUserActions from '../../redux/modules/content-item-analytics-users/actions';
import * as fromContentItemAnalyticsUserActions from '../../redux/modules/content-item-analytics-users/selectors';
import StatsByUserRow from './components/StatsByUserRow';
import Pagination from '../../components/Pagination/Pagination';
import TableHeaderCell from '../../components/Table/TableHeaderCell';
import TableItemCount from '../../components/Table/TableItemCount';
import type { Pagination as PaginationType } from '../../types';
import type { UserWithContentStats } from '../../types/analytics';

type OwnProps = {
  // used in mapDispatchToProps
  contentId: number, // eslint-disable-line react/no-unused-prop-types
};

type ConnectedProps = {
  error: ?string,
  isLoading: boolean,
  pagination: PaginationType,
  usersWithStats: UserWithContentStats[],
  initialize: () => any,
  getPage: (page: number) => any,
};

type Props = OwnProps & ConnectedProps;

/**
 * For a single content item, retrieves and renders stats for any user that has
 * interacted with this content.
 */
class StatsByUser extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const {
      usersWithStats,
      error,
      isLoading,
      pagination,
      getPage,
    } = this.props;
    const hasUsers = usersWithStats.length > 0;

    if (error) {
      return <div>Error: Could not display stats.</div>;
    }

    return (
      <div>
        <Header as="h4" title="Includes users that have at least one interaction with this post.">
          Stats by User{' '}
          {pagination && (
            <TableItemCount>{pagination.totalElements}</TableItemCount>
          )}
        </Header>
        <Loader active={isLoading} />
        <Table>
          <Table.Header>
            <Table.Row>
              <TableHeaderCell>Name</TableHeaderCell>
              <TableHeaderCell>Likes</TableHeaderCell>
              <TableHeaderCell>Comments</TableHeaderCell>
              <TableHeaderCell>Completed</TableHeaderCell>
              <TableHeaderCell>Views</TableHeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {hasUsers &&
              usersWithStats.map(userWithStats => (
                <StatsByUserRow
                  key={userWithStats.user.id}
                  userWithStats={userWithStats}
                />
              ))}
            {!hasUsers &&
              !isLoading && (
                <Table.Row>
                  <Table.Cell colSpan="5">
                    No users have interacted with this post yet.
                  </Table.Cell>
                </Table.Row>
              )}
          </Table.Body>
          <Table.Footer>
            <Table.Row>
              <TableHeaderCell colSpan="5" textAlign="center">
                {hasUsers &&
                  pagination && (
                    <Pagination pagination={pagination} goToPage={getPage} />
                  )}
              </TableHeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: fromContentItemAnalyticsUserActions.selectIsLoading(state),
  error: fromContentItemAnalyticsUserActions.selectError(state),
  usersWithStats: fromContentItemAnalyticsUserActions.selectUsersWithStats(
    state,
  ),
  pagination: fromContentItemAnalyticsUserActions.selectPagination(state),
});

const mapDispatchToProps = (dispatch, { contentId }: OwnProps) => ({
  initialize: () =>
    dispatch(contentItemAnalyticsUserActions.initialize(contentId)),
  getPage: (page: number) =>
    dispatch(contentItemAnalyticsUserActions.getPage(page)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StatsByUser);
