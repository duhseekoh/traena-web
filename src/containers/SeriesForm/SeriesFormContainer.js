import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiSeriesFormActions from '../../redux/modules/ui-series-form/actions';
import * as fromUiSeriesForm from '../../redux/modules/ui-series-form/selectors';
import * as fromSeries from '../../redux/modules/series/selectors';
import * as fromChannels from '../../redux/modules/channels/selectors';
import type { Series } from '../../types/series';
import type { Option } from '../../types';
import * as formValidators from '../../utils/formValidators';

import SeriesForm from './components/SeriesForm';

type OwnProps = {
  seriesId?: number,
};

type ConnectedProps = {
  series?: Series,
  errorInitializing?: boolean,
  channels: Option[],
  channelId: number,
  defaultChannelId: number,
  loading: boolean,
  initializeSeriesFormData: (seriesId?: number) => any,
  saveSeriesForm: (seriesFormVals: Series) => any,
  addContentToSeries: (id: number) => any,
  removeContentFromSeries: (id: number) => any,
  reorderContentForSeries: (from: number, to: number) => any,
  contentIds: number[],
  navigateToSeriesTable: () => any,
};

type Props = OwnProps & ConnectedProps;

class SeriesFormContainer extends Component<Props> {
  props: Props;

  componentDidMount() {
    this.props.initializeSeriesFormData(this.props.seriesId);
  }

  submit = (seriesFormVals: Series) => {
    this.props.saveSeriesForm(seriesFormVals);
  }

  addContentToSeries = (contentId) => {
    this.props.addContentToSeries(contentId);
  }

  removeContentFromSeries = (contentId) => {
    this.props.removeContentFromSeries(contentId);
  }

  reorderContentForSeries = (from: number, to: number) => {
    this.props.reorderContentForSeries(from, to);
  }

  /*
  * Render a different form (Video, Image, or Text), based on the contentType selected by the user,
  */
  renderSeriesForm = () => {
    /**
    * If this container has been passed a seriesId, we know we are editing an existing item.
    * We wait until the content has been fetched before rendering the form,
    * so we can use the fetched values as the form's initialValues
    */
    const {
      seriesId,
      series,
      defaultChannelId,
      errorInitializing,
      channels,
      loading,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading this content to edit.
        </div>
      );
    }

    /**
    * If no content, activities, or channels have been loaded yet,
    * but we also have not yet started loading in the data
    */
    if ((seriesId && !series) || !channels || !defaultChannelId) {
      return null;
    }
    /*
    * If we have fetched a content to edit, we set the desiredStatus equal to the status
    * of the item, so the UI runs the appropriate form validation on the edit form
    */
    const initialValues = seriesId && series ? {
      ...series,
    } : {
      channelId: defaultChannelId,
      contentIds: [],
    };
    return (
      <fieldset>
        <div>
          <SeriesForm
            initialValues={initialValues}
            {...this.props}
            validate={formValidators.validateSeriesForm}
            reorderContentForSeries={this.reorderContentForSeries}
            addContentToSeries={this.addContentToSeries}
            removeContentFromSeries={this.removeContentFromSeries}
            onSubmit={this.submit}
          />
        </div>
      </fieldset>
    );
  }

  render() {
    return (
      <div>
        {this.renderSeriesForm()}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  channels: fromChannels.selectOrganizationOwnedChannelsAsInputOptions(state),
  defaultChannelId: fromChannels.selectOrganizationDefaultChannelId(state),
  errorInitializing: fromUiSeriesForm.selectErrorLoading(state),
  loading: fromUiSeriesForm.selectIsLoading(state),
  saving: fromUiSeriesForm.selectIsSaving(state),
  seriesSelectedContent:
    fromSeries.selectSeriesSelectedContentFromReduxForm(state),
  series: fromSeries.selectSeries(state, props.seriesId),
  channelId: fromUiSeriesForm.selectChannelId(state),
  contentIds: fromUiSeriesForm.selectContentIds(state),
});

const mapDispatchToProps = dispatch => ({
  initializeSeriesFormData: seriesId =>
    dispatch(uiSeriesFormActions.initializeSeriesFormData(seriesId)),
  saveSeriesForm: (seriesFormVals: Series) =>
    dispatch(uiSeriesFormActions.saveSeriesForm(seriesFormVals)),
  addContentToSeries: contentId =>
    dispatch(uiSeriesFormActions.addContentToSeries(contentId)),
  removeContentFromSeries: contentId =>
    dispatch(uiSeriesFormActions.removeContentFromSeries(contentId)),
  reorderContentForSeries: (from, to) =>
    dispatch(uiSeriesFormActions.reorderContentForSeries(from, to)),
  navigateToSeriesTable: () =>
    dispatch(uiSeriesFormActions.navigateToSeriesTable()),
  deleteSeries: (seriesId: number) =>
    dispatch(uiSeriesFormActions.deleteSeries(seriesId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SeriesFormContainer);
