// @flow
import React from 'react';
import moment from 'moment';
import classnames from 'classnames';
import { Table, Button } from 'semantic-ui-react';
import type { ContentItem } from '../../../types/content';
import DateShort from '../../../components/Date/DateShort';
import UpDownButtons from '../../../components/UpDownButtons/UpDownButtons';
import TableRow from '../../../components/Table/TableRow';

import styles from '../../../pages/content/components/ContentTable.scss';

type Props = {
  content: ContentItem,
  handleClickRow: (contentId: number) => any,
  move: (from: number, to: number) => any,
  index: number,
  totalItems: number,
};

const contentTypesToDisplayNames = {
  DailyAction: 'Text',
  TrainingVideo: 'Video',
  Image: 'Image',
};

const SeriesSelectedContentTableRow = (props: Props) => {
  // eslint-disable-line
  const { content, handleClickRow, move, index, totalItems } = props;

  return (
    <TableRow
      key={content.id}
    >
      <Table.Cell>
        <UpDownButtons
          move={(from, to) => { move(from, to); }}
          index={index}
          totalItems={totalItems}
        />
      </Table.Cell>
      <Table.Cell>
        {contentTypesToDisplayNames[content.body.type]}
      </Table.Cell>
      <Table.Cell>
        {`${content.author.firstName} ${content.author.lastName}`}
      </Table.Cell>
      <Table.Cell>
        <div>{content.channel.name}</div>
        <div className={styles.subText}>
          {content.author.organization.name}
        </div>
      </Table.Cell>
      <Table.Cell>{content.body.title}</Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.tags.length}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.likeCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.commentCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.completedCount}
      </Table.Cell>
      <Table.Cell
        className={
          !content.publishedTimestamp
            ? classnames(styles.fadedCell, styles.alignRight)
            : styles.alignRight
        }
      >
        {content.publishedTimestamp ? (
          <DateShort date={content.publishedTimestamp} />
        ) : (
          'None'
        )}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.publishedTimestamp &&
          moment().diff(moment(content.publishedTimestamp), 'days')}
      </Table.Cell>
      <Table.Cell>
        <Button small onClick={() => handleClickRow(content.id)}>
          Remove
        </Button>
      </Table.Cell>
    </TableRow>
  );
};

export default SeriesSelectedContentTableRow;
