/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import moment from 'moment';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { Table, Icon } from 'semantic-ui-react';
import { getRouteLink, routes } from '../../../constants/routes';
import type { ContentItem } from '../../../types/content';
import DateShort from '../../../components/Date/DateShort';
import TableRow from '../../../components/Table/TableRow';

import styles from '../../../pages/content/components/ContentTable.scss';

type Props = {
  content: ContentItem,
  addContentToSeries: (contentId: number) => any,
  removeContentFromSeries: (contentId: number) => any,
  selected: boolean,
  disabled: boolean,
};

const contentTypesToDisplayNames = {
  DailyAction: 'Text',
  TrainingVideo: 'Video',
  Image: 'Image',
};

const SeriesChannelContentTableRow = (props: Props) => {
  // eslint-disable-line
  const {
    content,
    addContentToSeries,
    removeContentFromSeries,
    selected,
    disabled,
  } = props;

  return (
    <TableRow
      className={disabled ? styles.disabled : ''}
      key={content.id}
      onClick={() => {
        if (selected) {
          removeContentFromSeries(content.id);
        } else if (!disabled) {
          addContentToSeries(content.id);
        }
      }}
    >
      <Table.Cell>{selected && <Icon name="check" />}</Table.Cell>
      <Table.Cell>{contentTypesToDisplayNames[content.body.type]}</Table.Cell>
      <Table.Cell>{`${content.author.firstName} ${content.author.lastName}`}</Table.Cell>
      <Table.Cell>
        <div>{content.channel.name}</div>
        <div className={styles.subText}>
          {content.author.organization.name}
        </div>
      </Table.Cell>
      <Table.Cell>{content.body.title}</Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.tags.length}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.likeCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.commentCount}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.completedCount}
      </Table.Cell>
      <Table.Cell
        className={
          !content.publishedTimestamp
            ? classnames(styles.fadedCell, styles.alignRight)
            : styles.alignRight
        }
      >
        {content.publishedTimestamp ? (
          <DateShort date={content.publishedTimestamp} />
        ) : (
          'None'
        )}
      </Table.Cell>
      <Table.Cell className={styles.alignRight}>
        {content.publishedTimestamp &&
          moment().diff(moment(content.publishedTimestamp), 'days')}
      </Table.Cell>
      <Table.Cell>
        <Link
          className={styles.viewContentLink}
          to={getRouteLink(routes.CONTENT_VIEW, { contentId: content.id })}
          target="_blank"
        >View
        </Link>
      </Table.Cell>
    </TableRow>
  );
};

export default SeriesChannelContentTableRow;
