// @flow
import React from 'react';
import { Table, Icon } from 'semantic-ui-react';
import Pagination from '../../../components/Pagination/Pagination';
import type { Pagination as PaginationType } from '../../../types';
import type { ContentItem } from '../../../types/content';
import styles from '../../../pages/content/components/ContentTable.scss';
import SeriesChannelContentTableRow from './SeriesChannelContentTableRow';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import type { SortDirections } from '../../../utils/redux-sort/types';

type Props = {
  content: ContentItem[],
  pagination: PaginationType,
  addContentToSeries: (contentId: number) => any,
  removeContentFromSeries: (contentId: number) => any,
  contentIds: number[],
  goToPage: (page: number) => any,
  seriesId: number,
  sortTable: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
};

const SeriesChannelContentTable = (props: Props) => {
  if (!props.content.length) {
    return (
      <h2 className={styles.contentTableNoContentMessage}>
        No posts have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.contentTable}>
        <Table.Header>
          <Table.Row>
            <th />
            <TableHeaderCell {...props} property="body.type">
              Type
            </TableHeaderCell>
            <TableHeaderCell {...props} property="author.firstName">
              Author
            </TableHeaderCell>
            <TableHeaderCell>Channel</TableHeaderCell>
            <TableHeaderCell {...props} property="body.title">
              Title
            </TableHeaderCell>
            <TableHeaderCell title="tags" className={styles.alignRight}>
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell
              {...props}
              title="likes"
              className={styles.alignRight}
              property="likeCount"
            >
              <Icon name="empty heart" />
            </TableHeaderCell>
            <TableHeaderCell
              {...props}
              title="comments"
              className={styles.alignRight}
              property="commentCount"
            >
              <Icon name="comment outline" />
            </TableHeaderCell>
            <TableHeaderCell
              {...props}
              title="completed"
              className={styles.alignRight}
              property="completedCount"
            >
              <Icon name="check circle outline" />
            </TableHeaderCell>
            <TableHeaderCell
              {...props}
              className={styles.alignRight}
              property="publishedTimestamp"
            >
              Published
            </TableHeaderCell>
            <TableHeaderCell className={styles.alignRight}>
              Days Live
            </TableHeaderCell>
            <th />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.content.map(content => (
            <SeriesChannelContentTableRow
              content={content}
              addContentToSeries={props.addContentToSeries}
              removeContentFromSeries={props.removeContentFromSeries}
              selected={props.contentIds.includes(content.id)}
              disabled={
                !!content.series && content.series.id !== props.seriesId
              }
            />
          ))}
        </Table.Body>
      </Table>
      <Pagination pagination={props.pagination} goToPage={props.goToPage} />
    </div>
  );
};

export default SeriesChannelContentTable;
