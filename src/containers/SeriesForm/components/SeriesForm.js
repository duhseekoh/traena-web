// @flow
import React from 'react';
import { reduxForm } from 'redux-form';
import { TextField, TextAreaField } from '@panderalabs/redux-form-semantic';
import { Button, Grid, Card, Message } from 'semantic-ui-react';
import ContentTablePanes from './ContentTablePanes';
import ChannelSelector from '../../../components/ChannelSelector/ChannelSelector';
import styles from '../SeriesForm.scss';
import type { Series } from '../../../types/series';
import type { ContentItem } from '../../../types/content';
import type { Option } from '../../../types';

type OwnProps = {
  seriesId: number,
  series: Series,
  defaultChannelId: number,
  channelId: number,
  errorInitializing: boolean,
  channels: Option[],
  loading: boolean,
  saving: boolean,
  seriesSelectedContent: ContentItem[],
  addContentToSeries: (contentId: number) => any,
  removeContentFromSeries: (contentId: number) => any,
  onSubmit: (series: Series) => any,
  reorderContentForSeries: (from: number, to: number) => any,
  contentIds: number[],
  navigateToSeriesTable: () => any,
  deleteSeries: (seriesId: number) => any,
};
type ConnectedProps = {};
type ReduxFormProps = {
  error: string,
  handleSubmit: (series: Series) => any,
};
type Props = OwnProps & ConnectedProps & ReduxFormProps;

const SeriesForm = (props: Props) => {
  const {
    seriesId,
    error,
    channels,
    saving,
    handleSubmit,
    onSubmit,
    reorderContentForSeries,
    contentIds,
    navigateToSeriesTable,
    deleteSeries,
  } = props;

  return (
    <form
      className={styles.seriesForm}
      autoComplete="off"
    >
      <Grid>
        <Grid.Row>
          <Grid.Column computer={8} mobile={16}>
            <h1>
              {seriesId ? 'Edit Series' : 'Create Series'}
            </h1>
          </Grid.Column>
          <Grid.Column computer={8} mobile={16}>
            <Button
              default
              size="big"
              loading={saving}
              onClick={(e) => {
                e.preventDefault();
                setTimeout(handleSubmit(onSubmit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
              }}
              className="right"
            >Save and Publish
            </Button>
            <Button
              default
              size="big"
              type="submit"
              className="right"
              onClick={(e) => {
                e.preventDefault();
                navigateToSeriesTable();
              }}
            >Discard Changes
            </Button>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column computer={8} mobile={16}>
            <Card fluid>
              <Card.Content>
                <h5>Title *</h5>
                <TextField
                  name="title"
                  maxLength={40}
                  placeholder="Add a title to your series..."
                  className={styles.postInput}
                />
              </Card.Content>
            </Card>
            <Card fluid>
              <Card.Content>
                <h5>Channel *</h5>
                <ChannelSelector
                  className=""
                  options={channels}
                  disabled={contentIds.length > 0}
                />
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column computer={8} mobile={16}>
            <Card fluid>
              <Card.Content>
                <h5>Description</h5>
                <TextAreaField
                  name="description"
                  rows={6}
                  maxLength={255}
                  placeholder="Enter a description for your series..."
                  className={styles.postInput}
                />
              </Card.Content>
            </Card>
            {error &&
              <Message
                error
                header="Error"
                series={error}
              />
            }
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <ContentTablePanes
              move={reorderContentForSeries}
              {...props}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Button
              danger
              size="big"
              className="right"
              onClick={(e) => {
                e.preventDefault();
                deleteSeries(seriesId);
              }}
            >DELETE SERIES
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </form>
  );
};


const seriesForm = reduxForm({
  form: 'series',
  enableReinitialize: true,
})(SeriesForm);

export default seriesForm;
