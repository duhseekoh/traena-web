import React from 'react';
import { Tab, Menu, Label } from 'semantic-ui-react';
import { Field } from 'redux-form';
import SeriesSelectedContentTable from './SeriesSelectedContentTable';
import SeriesChannelContentTable from './SeriesChannelContentTable';

import ChannelContentTableContainer from '../../../containers/ChannelContentTableContainer/ChannelContentTableContainer';

type Props = {
  channelId: number,
  removeContentFromSeries: (id: number) => any,
  seriesSelectedContent: Object[],
  reorderContentForSeries: (from: number, to: number) => any,
}
const ContentTablePanes = (props: Props) => {
  const {
    channelId,
    removeContentFromSeries,
    seriesSelectedContent,
    reorderContentForSeries,
  } = props;
  const panes = [
    {
      menuItem: 'Available Content',
      render: () => (
        <Tab.Pane>
          {channelId &&
            <ChannelContentTableContainer
              {...props}
              channelId={channelId}
              renderTable={(props) => ( // eslint-disable-line
                <SeriesChannelContentTable
                  {...props}
                />
              )}
            />
          }
        </Tab.Pane>
      ),
    },
    {
      menuItem:
  <Menu.Item>
    Selected Content <Label>{seriesSelectedContent.length}</Label>
  </Menu.Item>,
      render: () => (
        <Tab.Pane>
          <Field
            name="contentIds"
            component={SeriesSelectedContentTable}
            content={seriesSelectedContent}
            handleClickRow={removeContentFromSeries}
            move={reorderContentForSeries}
          />
        </Tab.Pane>
      ),
    },
  ];

  return (
    <Tab panes={panes} />
  );
};

export default ContentTablePanes;
