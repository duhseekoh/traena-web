// @flow
import React from 'react';
import { Table, Icon } from 'semantic-ui-react';
import type { ContentItem } from '../../../types/content';
import styles from '../../../pages/content/components/ContentTable.scss';
import SeriesSelectedContentTableRow from './SeriesSelectedContentTableRow';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';

type Props = {
  content: ContentItem[],
  handleClickRow: (contentId: number) => any,
  move: (from: number, to: number) => any,
};

const SeriesSelectedContentTable = (props: Props) => {
  if (!props.content.length) {
    return (
      <h2 className={styles.contentTableNoContentMessage}>
        No posts have been selected yet...
      </h2>
    );
  }
  return (
    <div>
      <Table className={styles.contentTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell />
            <TableHeaderCell>Type</TableHeaderCell>
            <TableHeaderCell>Author</TableHeaderCell>
            <TableHeaderCell>Channel</TableHeaderCell>
            <TableHeaderCell>Title</TableHeaderCell>
            <TableHeaderCell title="tags" className={styles.alignRight}>
              <Icon name="hashtag" />
            </TableHeaderCell>
            <TableHeaderCell title="likes" className={styles.alignRight}>
              <Icon name="empty heart" />
            </TableHeaderCell>
            <TableHeaderCell title="comments" className={styles.alignRight}>
              <Icon name="comment outline" />
            </TableHeaderCell>
            <TableHeaderCell className={styles.alignRight}>
              Completed
            </TableHeaderCell>
            <TableHeaderCell className={styles.alignRight}>
              Published
            </TableHeaderCell>
            <TableHeaderCell className={styles.alignRight}>
              Days Live
            </TableHeaderCell>
            <TableHeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.content.map((content, i) =>
            (<SeriesSelectedContentTableRow
              content={content}
              key={content.id}
              move={props.move}
              index={i}
              totalItems={props.content.length}
              handleClickRow={props.handleClickRow}
            />))}
        </Table.Body>
      </Table>
    </div>
  );
};

export default SeriesSelectedContentTable;
