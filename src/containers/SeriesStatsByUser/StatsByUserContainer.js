// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader, Header, Table } from 'semantic-ui-react';
import TableItemCount from '../../components/Table/TableItemCount';
import TableHeaderCell from '../../components/Table/TableHeaderCell';
import * as seriesStatsUserActions from '../../redux/modules/series-stats-users/actions';
import * as fromSeriesStatsUser from '../../redux/modules/series-stats-users/selectors';
import StatsByUserRow from './components/StatsByUserRow';
import Pagination from '../../components/Pagination/Pagination';
import type { Pagination as PaginationType } from '../../types';
import type { UserWithSeriesStats } from '../../types/analytics';
import type { Series } from '../../types/series';
import type { SortDirectionsLong } from '../../utils/redux-sort/types';

type OwnProps = {|
  series: Series,
|};

type ConnectedProps = {|
  error: ?string,
  isLoading: boolean,
  pagination: ?PaginationType,
  usersWithStats: UserWithSeriesStats[],
  initialize: () => any,
  getPage: (page: number) => any,
  sortTable: (property: string) => any,
  getSortDirection: (property: string) => ?SortDirectionsLong,
|};

type Props = OwnProps & ConnectedProps;

/**
 * For a single content item, retrieves and renders stats for any user that has
 * interacted with this content.
 */
class StatsByUser extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const {
      usersWithStats,
      series,
      error,
      isLoading,
      pagination,
      getPage,
    } = this.props;
    const hasUsers = usersWithStats.length > 0;

    if (error) {
      return <div>Error: Could not display stats.</div>;
    }

    return (
      <div>
        <Header as="h4">
          Stats by User{' '}
          {pagination && (
            <TableItemCount>{pagination.totalElements}</TableItemCount>
          )}
        </Header>
        <Loader active={isLoading} />
        <Table sortable>
          <Table.Header>
            <Table.Row>
              <TableHeaderCell
                {...this.props}
                property={seriesStatsUserActions.SORTABLE_PROPERTIES.FIRST_NAME}
              >
                Name
              </TableHeaderCell>
              <TableHeaderCell
                {...this.props}
                property={seriesStatsUserActions.SORTABLE_PROPERTIES.POSITION}
              >
                Title
              </TableHeaderCell>
              <TableHeaderCell>Status</TableHeaderCell>
              <TableHeaderCell
                {...this.props}
                property={
                  seriesStatsUserActions.SORTABLE_PROPERTIES
                    .COMPLETED_CONTENT_COUNT
                }
              >
                Progress
              </TableHeaderCell>
              <TableHeaderCell
                {...this.props}
                property={
                  seriesStatsUserActions.SORTABLE_PROPERTIES
                    .LAST_VIEWED_CONTENT_TIMESTAMP
                }
              >
                Date Last Viewed
              </TableHeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {hasUsers &&
              usersWithStats.map(userWithStats => (
                <StatsByUserRow
                  key={userWithStats.user.id}
                  userWithStats={userWithStats}
                  seriesContentCount={series.contentCount}
                />
              ))}
            {!hasUsers &&
              !isLoading && (
                <Table.Row>
                  <Table.Cell colSpan="5">
                    No users have access to this series.
                  </Table.Cell>
                </Table.Row>
              )}
          </Table.Body>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan="5" textAlign="center">
                {hasUsers &&
                  pagination && (
                    <Pagination pagination={pagination} goToPage={getPage} />
                  )}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: fromSeriesStatsUser.selectIsLoading(state),
  error: fromSeriesStatsUser.selectError(state),
  usersWithStats: fromSeriesStatsUser.selectUsersWithStats(state),
  pagination: fromSeriesStatsUser.selectPagination(state),
  getSortDirection: property =>
    fromSeriesStatsUser.getSortDirectionLongName(state, property),
});

const mapDispatchToProps = (dispatch, { series }: OwnProps) => ({
  initialize: () => dispatch(seriesStatsUserActions.initialize(series.id)),
  getPage: (page: number) => dispatch(seriesStatsUserActions.getPage(page)),
  sortTable: property =>
    dispatch(seriesStatsUserActions.setStatsSort(property)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StatsByUser);
