// @flow
import React from 'react';
import { Label } from 'semantic-ui-react';
import styles from './StatusStat.scss';

type Props = {|
  completedCount: number,
  totalCount: number,
|};

const STATUSES = {
  COMPLETED: 'COMPLETED',
  NOT_STARTED: 'NOT_STARTED',
  IN_PROGRESS: 'IN_PROGRESS',
};

function getStatus(
  completedCount: number,
  totalCount: number,
): $Values<typeof STATUSES> {
  if (completedCount === totalCount) {
    return STATUSES.COMPLETED;
  } else if (completedCount === 0) {
    return STATUSES.NOT_STARTED;
  }

  return STATUSES.IN_PROGRESS;
}

function StatusStat(props: Props) {
  const { completedCount, totalCount } = props;
  const status = getStatus(completedCount, totalCount);

  if (status === STATUSES.COMPLETED) {
    return (
      <Label circular className={styles.completed}>
        Completed
      </Label>
    );
  }

  if (status === STATUSES.NOT_STARTED) {
    return (
      <Label circular className={styles.notStarted}>
        Not Started
      </Label>
    );
  }

  if (status === STATUSES.IN_PROGRESS) {
    return (
      <Label circular className={styles.inProgress}>
        In Progress
      </Label>
    );
  }

  return null;
}

export default StatusStat;
