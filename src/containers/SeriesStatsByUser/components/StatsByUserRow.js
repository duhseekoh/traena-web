// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import type { UserWithSeriesStats } from '../../../types/analytics';
import DateShort from '../../../components/Date/DateShort';
import StatusStat from './StatusStat';

type Props = {|
  userWithStats: UserWithSeriesStats,
  seriesContentCount: number,
|};

function StatsByUserRow(props: Props) {
  const { userWithStats, seriesContentCount } = props;
  const { user, seriesStats } = userWithStats;
  const { completedContentCount, latestViewedContentTimestamp } = seriesStats;

  return (
    <Table.Row>
      <Table.Cell>
        {user.firstName} {user.lastName}
      </Table.Cell>
      <Table.Cell>{user.position}</Table.Cell>
      <Table.Cell>
        <StatusStat
          completedCount={completedContentCount}
          totalCount={seriesContentCount}
        />
      </Table.Cell>
      <Table.Cell>
        {completedContentCount} / {seriesContentCount}
      </Table.Cell>
      <Table.Cell>
        {latestViewedContentTimestamp ? (
          <DateShort date={latestViewedContentTimestamp} />
        ) : (
          <span>-</span>
        )}
      </Table.Cell>
    </Table.Row>
  );
}

export default StatsByUserRow;
