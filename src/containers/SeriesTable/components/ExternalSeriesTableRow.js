// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import type { Series } from '../../../types/series';
import TableRow from '../../../components/Table/TableRow';

type Props = {
  series: Series,
  handleClickRow: (seriesId: number) => any,
};

const ExternalSeriesTableRow = (props: Props) => {
  const { series, handleClickRow } = props;
  return (
    <TableRow key={series.id} onClick={() => handleClickRow(series.id)}>
      <Table.Cell>{series.title}</Table.Cell>
      <Table.Cell>{series.organization && series.organization.name}</Table.Cell>
      <Table.Cell>{series.channel && series.channel.name}</Table.Cell>
      <Table.Cell>{series.contentCount}</Table.Cell>
    </TableRow>
  );
};

export default ExternalSeriesTableRow;
