// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import type { Series } from '../../../types/series';
import DateShort from '../../../components/Date/DateShort';
import TableRow from '../../../components/Table/TableRow';

type Props = {
  series: Series,
  handleClickRow: (seriesId: number) => any,
};

const SeriesTableRow = (props: Props) => {
  const { series, handleClickRow } = props;
  return (
    <TableRow key={series.id} onClick={() => handleClickRow(series.id)}>
      <Table.Cell>{series.title}</Table.Cell>
      <Table.Cell>{series.channel && series.channel.name}</Table.Cell>
      <Table.Cell>{series.author && `${series.author.firstName} ${series.author.lastName}`}</Table.Cell>
      <Table.Cell>{series.contentCount}</Table.Cell>
      <Table.Cell>
        <DateShort date={series.createdTimestamp} />
      </Table.Cell>
    </TableRow>
  );
};

export default SeriesTableRow;
