// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import styles from './SeriesTable.scss';
import ExternalSeriesTableRow from './ExternalSeriesTableRow';
import Pagination from '../../../components/Pagination/Pagination';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import type { Pagination as PaginationType } from '../../../types';
import type { Series } from '../../../types/series';

type Props = {
  series: Series[],
  pagination: PaginationType,
  goToSeriesPage: (page: number) => any,
  handleClickRow: (seriesId: number) => any,
};

const ExternalSeriesTable = (props: Props) => {
  if (!props.series.length) {
    return (
      <h2 className={styles.seriesTableNoContentMessage}>
        No series have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Table sortable className={styles.seriesTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell {...props} property="title">
              Name
            </TableHeaderCell>
            <TableHeaderCell>
              Organization
            </TableHeaderCell>
            <TableHeaderCell {...props} property="channel.name">
              Channel
            </TableHeaderCell>
            <TableHeaderCell {...props} property="contentCount">
              Posts
            </TableHeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.series.map(series => (
            <ExternalSeriesTableRow
              key={series.id}
              series={series}
              handleClickRow={props.handleClickRow}
            />
          ))}
        </Table.Body>
      </Table>
      <Pagination
        pagination={props.pagination}
        goToPage={props.goToSeriesPage}
      />
    </div>
  );
};

export default ExternalSeriesTable;
