// @flow
import React from 'react';
import { Table } from 'semantic-ui-react';
import styles from './SeriesTable.scss';
import SeriesTableRow from './SeriesTableRow';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import Pagination from '../../../components/Pagination/Pagination';

import type { Pagination as PaginationType } from '../../../types';
import type { Series } from '../../../types/series';
import type { SortDirections } from '../../../utils/redux-sort/types';

type Props = {
  series: Series[],
  pagination: PaginationType,
  goToSeriesPage: (page: number) => any,
  handleClickRow: (seriesId: number) => any,
  sortTable: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
};

const SeriesTable = (props: Props) => {
  if (!props.series.length) {
    return (
      <h2 className={styles.seriesTableNoContentMessage}>
        No series have been created yet...
      </h2>
    );
  }

  return (
    <div>
      <Table sortable className={styles.seriesTable}>
        <Table.Header>
          <Table.Row>
            <TableHeaderCell {...props} property="title">
              Name
            </TableHeaderCell>
            <TableHeaderCell {...props} property="channel.name">
              Channel
            </TableHeaderCell>
            <TableHeaderCell {...props} property="author.firstName">
              Creator
            </TableHeaderCell>
            <TableHeaderCell {...props} property="contentCount">
              Posts
            </TableHeaderCell>
            <TableHeaderCell {...props} property="createdTimestamp">
              Created
            </TableHeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.series.map(series => (
            <SeriesTableRow
              key={series.id}
              series={series}
              handleClickRow={props.handleClickRow}
            />
          ))}
        </Table.Body>
      </Table>
      <Pagination
        pagination={props.pagination}
        goToPage={props.goToSeriesPage}
      />
    </div>
  );
};

export default SeriesTable;
