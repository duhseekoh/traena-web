/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { push } from 'connected-react-router';
import { Link } from 'react-router-dom';
import { Loader, Grid, Button } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import * as uiSeriesTableActions from '../../redux/modules/ui-series-table/actions';
import * as fromUiSeriesTable from '../../redux/modules/ui-series-table/selectors';
import SeriesTable from './components/SeriesTable';
import ExternalSeriesTable from './components/ExternalSeriesTable';

import tabStyles from '../../components/TabButtonGroup/TabButtonGroup.scss';
import type { Series, SeriesFilters } from '../../types/series';
import type { Pagination } from '../../types';
import type { SortDirections } from '../../utils/redux-sort/types';

import {
  SUBSCRIPTION_SOURCES,
} from '../../constants/appConstants';

type Props = {
  fetchSeriesTable: () => any,
  handleClickRow: (seriesId: number) => any,
  setSeriesTablePage: (page: number) => any,
  setSeriesTableSort: (property: string) => any,
  series: Series[],
  pagination: Pagination,
  loading: boolean,
  setSeriesTableSourceFilter: (source: string) => any,
  seriesFilters: SeriesFilters,
  getSortDirection: (property: string) => SortDirections,
}
class SeriesTableContainer extends Component<Props> {
  componentDidMount() {
    this.props.fetchSeriesTable();
  }

  handleClickRow = (seriesId: number) => {
    this.props.handleClickRow(seriesId);
  }

  goToSeriesPage = (page: number) => {
    this.props.setSeriesTablePage(page);
    this.props.fetchSeriesTable();
  }

  includeSeriesFromSource = (source: string) => {
    this.props.setSeriesTableSourceFilter(source);
    this.props.setSeriesTablePage(0);
    this.props.fetchSeriesTable();
  }

  sortTable = (property: string) => {
    this.props.setSeriesTableSort(property);
    this.props.fetchSeriesTable();
  }

  renderTable() {
    const {
      seriesFilters,
      getSortDirection,
    } = this.props;

    if (seriesFilters.source === SUBSCRIPTION_SOURCES.EXTERNAL) {
      return (
        <ExternalSeriesTable
          series={this.props.series}
          handleClickRow={this.handleClickRow}
          pagination={this.props.pagination}
          goToSeriesPage={this.goToSeriesPage}
          sortTable={this.sortTable}
          getSortDirection={getSortDirection}
        />
      );
    }
    return (
      <SeriesTable
        series={this.props.series}
        handleClickRow={this.handleClickRow}
        pagination={this.props.pagination}
        goToSeriesPage={this.goToSeriesPage}
        sortTable={this.sortTable}
        getSortDirection={getSortDirection}
      />
    );
  }

  render() {
    const activeTabStyle = classnames(tabStyles.tabButton, tabStyles.active);
    const { seriesFilters } = this.props;

    return (
      <div>
        <Grid stackable>
          {/* Title and Create button */}
          <Grid.Row verticalAlign="bottom">
            <Grid.Column width={11}>
              <h1>Series</h1>
            </Grid.Column>
            <Grid.Column width={5} textAlign="right">
              <Link to={getRouteLink(routes.SERIES_CREATE)}>
                <Button size="big" primary>Create New</Button>
              </Link>
            </Grid.Column>
          </Grid.Row>
          {/* Filters */}
          <Grid.Row verticalAlign="middle">
            <Grid.Column width={11}>
              <button
                className={
                  seriesFilters.source ===
                  SUBSCRIPTION_SOURCES.INTERNAL
                    ? activeTabStyle
                    : tabStyles.tabButton
                }
                onClick={() => {
                  this.includeSeriesFromSource(
                    SUBSCRIPTION_SOURCES.INTERNAL,
                  );
                }}
              >
                Internal
              </button>
              <button
                className={
                  seriesFilters.source ===
                  SUBSCRIPTION_SOURCES.EXTERNAL
                    ? activeTabStyle
                    : tabStyles.tabButton
                }
                onClick={() => {
                  this.includeSeriesFromSource(
                    SUBSCRIPTION_SOURCES.EXTERNAL,
                  );
                }}
              >
                External
              </button>
            </Grid.Column>
          </Grid.Row>
          {/* Table */}
          <Grid.Row stretched>
            <Grid.Column>
              {this.props.loading ? <Loader active /> : this.renderTable()}
            </Grid.Column>
          </Grid.Row>

        </Grid>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchSeriesTable: () =>
    dispatch(uiSeriesTableActions.fetchSeriesTable()),
  setSeriesTablePage: (page) =>
    dispatch(uiSeriesTableActions.setSeriesTablePage(page)),
  handleClickRow: seriesId =>
    dispatch(push(getRouteLink(routes.SERIES_DETAIL, { seriesId }))),
  setSeriesTableSourceFilter: source =>
    dispatch(uiSeriesTableActions.setSeriesTableSourceFilter(source)),
  setSeriesTableSort: property =>
    dispatch(uiSeriesTableActions.setSeriesTableSort(property)),
});

const mapStateToProps = state => ({
  series: fromUiSeriesTable.selectSeries(state),
  loading: fromUiSeriesTable.selectIsLoading(state),
  pagination: fromUiSeriesTable.selectPagination(state),
  seriesFilters:
    fromUiSeriesTable.selectCurrentSeriesSourceFilters(state),
  getSortDirection: property =>
    fromUiSeriesTable.getSortDirectionLongName(state, property),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SeriesTableContainer);
