// @flow
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { Button, Icon } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import * as uiSubscriptionDetailsActions from '../../redux/modules/ui-subscription-details/actions';
import * as fromChannels from '../../redux/modules/channels/selectors';
import * as fromAuth from '../../redux/modules/auth/selectors';
import styles from './SubscriptionDetails.scss';
import type { Channel } from '../../types/channels';
import type { Subscription } from '../../types/subscriptions';
import * as fromSubscriptions from '../../redux/modules/subscriptions/selectors';
import AccessControl from '../../components/AccessControl/AccessControl';
import * as permissionUtils from '../../utils/permissionUtils';

import ChannelDetailsModal from './components/ChannelDetailsModal';

type ConnectedProps = {
  initializeSubscriptionDetails: (channelId: number) => any,
  handleBackButton: (url: string) => any,
  channel: Channel,
  subscription: Subscription,
  organizationName: ?string,
  ownOrgId: number,
}

type OwnProps = {
  channelId: number,
}

type Props = OwnProps & ConnectedProps;

type State = {
  displayDetailsModal: boolean,
}

class SubscriptionChannelDetailsContainer extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      displayDetailsModal: false,
    };
  }

  componentDidMount = () => {
    this.props.initializeSubscriptionDetails(this.props.channelId);
  };

  showDetailsModal = () => {
    this.setState({ displayDetailsModal: true });
  }

  hideDetailsModal = () => {
    this.setState({ displayDetailsModal: false });
  }

  render() {
    const {
      subscription,
      channel,
      channelId,
      handleBackButton,
      organizationName,
      ownOrgId,
    } = this.props;

    if (channelId && (!channel || !subscription)) {
      return null;
    }

    return (
      <div>
        <div className={styles.channelDetailBar}>
          <button
            className={styles.backButton}
            onClick={e => {
              e.preventDefault();
              handleBackButton(getRouteLink(routes.CHANNELS));
            }}
          >
            <Icon name="chevron left" size="large" />
          </button>
          <h3 className={styles.channelNameHeader}>
            {channel.name}
            {(organizationName &&
              ownOrgId !== channel.organizationId) &&
              <div className={styles.subheader}>{organizationName}</div>
            }
          </h3>

          <AccessControl
            customAccessData={{ organizationId: channel.organizationId }}
            accessCheck={permissionUtils.isOwnOrgChannel}
            noAccessView={
              <Button
                default
                className={styles.infoButton}
                onClick={this.showDetailsModal}
              >Info
              </Button>
            }
          >
            <Button
              default
              as={Link}
              className={styles.editButton}
              to={getRouteLink(routes.CHANNEL_EDIT, { channelId })}
            >Edit
            </Button>
          </AccessControl>

        </div>
        {this.state.displayDetailsModal &&
          <ChannelDetailsModal
            organizationName={organizationName}
            channel={channel}
            hide={this.hideDetailsModal}
          />
        }
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  initializeSubscriptionDetails: channelId =>
    dispatch(
      uiSubscriptionDetailsActions.initializeSubscriptionDetails(channelId),
    ),
  handleBackButton: url => dispatch(push(url)),
});

const mapStateToProps = (state, { channelId }) => ({
  channel: fromChannels.selectChannelById(state, channelId),
  subscription: fromSubscriptions.selectSubscription(state, channelId),
  organizationName: fromSubscriptions
    .selectSubscriptionOrgName(state, channelId),
  ownOrgId: fromAuth.selectOrganizationId(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  SubscriptionChannelDetailsContainer,
);
