import React from 'react';
import { Button, Grid } from 'semantic-ui-react';
import type { Channel } from '../../../types/channels';
import { CHANNEL_VISIBILITY_DISPLAY } from '../../../constants/appConstants';
import { COLORS } from '../../../constants/colors';
import DateShort from '../../../components/Date/DateShort';
import styles from '../SubscriptionDetails.scss';
import Modal from '../../../components/Modal/Modal';

type Props = {
  channel: Channel,
  organizationName: string,
  hide: () => any,
}

const ChannelDetailsModal = (props: Props) => {
  const { channel, organizationName, hide } = props;
  return (
    <Modal opacity={0.8} backgroundColor={COLORS.LIGHT_GREY}>
      <div className={styles.detailsModal}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <h3>
                Channel Details
              </h3>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <h5>Name</h5>
              {channel.name}
            </Grid.Column>
            <Grid.Column width={8}>
              <h5>Organization</h5>
              {organizationName}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <h5>Description</h5>
              {channel.description}
            </Grid.Column>
          </Grid.Row>

          {channel &&
            <Grid.Row columns={3}>
              <Grid.Column>
                <h5>Channel Type</h5>
                {CHANNEL_VISIBILITY_DISPLAY[channel.visibility]}
              </Grid.Column>
              <Grid.Column>
                <h5>Created By</h5>
                {organizationName}
              </Grid.Column>
              <Grid.Column>
                <h5>Date Created</h5>
                <DateShort date={channel.createdTimestamp} />
              </Grid.Column>
            </Grid.Row>
          }

          <Grid.Row>
            <Grid.Column width={16}>
              <Button
                default
                size="big"
                type="submit"
                className="right"
                onClick={e => {
                  e.preventDefault();
                  hide();
                }}
              >OK
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </Modal>
  );
};

export default ChannelDetailsModal;
