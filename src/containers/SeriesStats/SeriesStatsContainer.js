/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Grid, Loader, Header } from 'semantic-ui-react';
import AggregateStatsContainer from '../SeriesAggregateStats/AggregateStatsContainer';
import StatsByUserContainer from '../SeriesStatsByUser/StatsByUserContainer';
import { getRouteLink, routes } from '../../constants/routes';
import * as uiSeriesStatsActions from '../../redux/modules/ui-series-stats/actions';
import * as fromUiSeriesStats from '../../redux/modules/ui-series-stats/selectors';
import * as fromSeries from '../../redux/modules/series/selectors';
import type { Series } from '../../types/series';

type OwnProps = {|
  seriesId: number,
|};

type ConnectedProps = {|
  isLoading: boolean,
  error: ?string,
  series: ?Series,
  initialize: () => any,
|};

type Props = OwnProps & ConnectedProps;

/**
 * A common parent for content item analytics. It handles loading of a content item
 * so data about the content itself can be displayed.
 * To handle the retrieval and rendering of the stats themselves, other containers
 * are nested.
 */
class SeriesStats extends Component<Props> {
  props: OwnProps & ConnectedProps;

  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const { seriesId, series, isLoading, error } = this.props;

    if (error) {
      return <div>Error loading series stats.</div>;
    }

    // block UI with loading since there is no reasonable UI to display
    // without having a content item retrieved.
    if (isLoading) {
      return <Loader active />;
    }

    // only attempt to load the rest of the analytics if we can actually retrieve
    // the series itself.
    if (series) {
      return (
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16}>
              <h1>Series Stats</h1>
              <Header.Subheader size="small" as="h2">
                <Link
                  to={getRouteLink(
                    routes.SERIES_DETAIL,
                    { seriesId },
                  )}
                >
                  {series.title}
                </Link>
              </Header.Subheader>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column mobile={16}>
              <AggregateStatsContainer seriesId={seriesId} />
              <StatsByUserContainer series={series} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    }

    return null;
  }
}

const mapStateToProps = (state, props: OwnProps) => ({
  isLoading: fromUiSeriesStats.selectInitializing(state),
  error: fromUiSeriesStats.selectInitializeError(state),
  series: fromSeries.selectSeries(state, props.seriesId),
});

const mapDispatchToProps = (dispatch, { seriesId }: OwnProps) => ({
  initialize: () => dispatch(uiSeriesStatsActions.initialize(seriesId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SeriesStats);
