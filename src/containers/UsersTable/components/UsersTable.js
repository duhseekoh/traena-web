/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { Link } from 'react-router-dom';
import { Table, Grid, Button } from 'semantic-ui-react';
import styles from './UsersTable.scss';
import UsersTableRow from './UsersTableRow';
import TableHeaderCell from '../../../components/Table/TableHeaderCell';
import Pagination from '../../../components/Pagination/Pagination';
import { PERMISSIONS } from '../../../constants/permissions';
import AccessControl from '../../../components/AccessControl/AccessControl';
import type { Pagination as PaginationType } from '../../../types';
import type { User } from '../../../types/users';
import { routes, getRouteLink } from '../../../constants/routes';
import type { SortDirections } from '../../../utils/redux-sort/types';

type Props = {
  users: User[],
  pagination: PaginationType,
  goToUsersPage: (page: number) => any,
  sortTable: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
};

type ConnectedProps = {
  onPressTableRow: (userId: number) => any,
};

const UsersTable = (props: Props & ConnectedProps) => {
  // eslint-disable-line
  if (!props.users.length) {
    return (
      <h2 className={styles.usersTableNoContentMessage}>
        No users have been created yet...
      </h2>
    );
  }
  return (
    <div>
      <Grid stackable>
        {/* Title and Create button */}
        <Grid.Row verticalAlign="bottom">
          <Grid.Column width={8}>
            <h1>Users</h1>
          </Grid.Column>
          <Grid.Column width={8} textAlign="right">
            <AccessControl
              allowedPermissions={[PERMISSIONS.USERS.MODIFY.ORG]}
              noAccessView={<span />}
            >
              <Link to={getRouteLink(routes.USERS_CREATE)}>
                <Button size="big" primary>Create New</Button>
              </Link>
            </AccessControl>
          </Grid.Column>
        </Grid.Row>
        {/* Table */}
        <Grid.Row stretched>
          <Grid.Column>
            <Table sortable className={styles.usersTable}>
              <Table.Header>
                <Table.Row>
                  <TableHeaderCell {...props} property="firstName">
                    Name
                  </TableHeaderCell>
                  <TableHeaderCell {...props} property="email">
                    Email
                  </TableHeaderCell>
                  <TableHeaderCell {...props} property="position">
                    Position
                  </TableHeaderCell>
                  <TableHeaderCell {...props} property="createdTimestamp">
                    Created
                  </TableHeaderCell>
                  <TableHeaderCell />
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {props.users.map(user => (
                  <UsersTableRow
                    key={user.id}
                    user={user}
                    onPressTableRow={props.onPressTableRow}
                  />
                ))}
              </Table.Body>
            </Table>
            <Pagination
              pagination={props.pagination}
              goToPage={props.goToUsersPage}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onPressTableRow: userId =>
    dispatch(push(getRouteLink(routes.USERS_EDIT, { userId }))),
});

export default connect(null, mapDispatchToProps)(UsersTable);
