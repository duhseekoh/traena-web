// @flow
import React from 'react';
import { Link } from 'react-router-dom';
import { Table, Dropdown, Icon } from 'semantic-ui-react';
import { getRouteLink, routes } from '../../../constants/routes';
import type { User } from '../../../types/users';
import DateShort from '../../../components/Date/DateShort';
import TableMenuTriggerIcon from '../../../components/TableMenuTriggerIcon/TableMenuTriggerIcon';
import styles from './UsersTable.scss';
import TableRow from '../../../components/Table/TableRow';

type Props = {
  user: User,
  onPressTableRow: (userId: number) => any,
};

const UsersTableRow = (props: Props) => {
  const { user, onPressTableRow } = props;
  return (
    <TableRow key={user.id} onClick={() => onPressTableRow(user.id)}>
      <Table.Cell>
        {`${user.firstName} ${user.lastName}`}
        {user.isDisabled && (
          <Icon
            className={styles.disabledUserIcon}
            title="User is disabled"
            name="ban"
          />
        )}
      </Table.Cell>
      <Table.Cell>{user.email}</Table.Cell>
      <Table.Cell>{user.position}</Table.Cell>
      <Table.Cell>
        <DateShort date={user.createdTimestamp} />
      </Table.Cell>
      <Table.Cell>
        <Dropdown
          fluid
          icon={null}
          trigger={<TableMenuTriggerIcon />}
        >
          <Dropdown.Menu className="left">
            <Dropdown.Item
              as={Link}
              to={getRouteLink(routes.USERS_EDIT, { userId: user.id })}
            >
              Edit
            </Dropdown.Item>
            <Dropdown.Item
              as={Link}
              to={getRouteLink(routes.USERS_ROLES, { userId: user.id })}
            >
              Roles
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Table.Cell>
    </TableRow>
  );
};

export default UsersTableRow;
