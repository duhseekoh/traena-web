/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiUsersTableActions from '../../redux/modules/ui-users-table/actions';
import * as fromUiUserTable from '../../redux/modules/ui-users-table/selectors';
import type { Pagination } from '../../types';
import type { User } from '../../types/users';
import type { SortDirections } from '../../utils/redux-sort/types';

type ConnectedProps = {
  fetchUsersTable: () => any,
  setUsersPage: (page: number) => any,
  users: User[],
  isUsersTableLoading: boolean,
  pagination: Pagination,
  renderTable: (props: any) => React.DOM.Node,
  setUsersTableSort: (property: string) => any,
  getSortDirection: (property: string) => SortDirections,
};

class UsersTableContainer extends Component<ConnectedProps> {
  props: ConnectedProps;

  componentDidMount() {
    this.props.fetchUsersTable();
  }

  goToUsersPage = (page: number) => {
    this.props.setUsersPage(page);
    this.props.fetchUsersTable();
  }

  sortTable = (property: string) => {
    this.props.setUsersTableSort(property);
    this.props.fetchUsersTable();
  }

  render() {
    const { props } = this;

    if (this.props.isUsersTableLoading) {
      return (<Loader active />);
    }

    return this.props.renderTable({
      ...props,
      pagination: this.props.pagination,
      users: this.props.users,
      goToUsersPage: this.goToUsersPage,
      sortTable: this.sortTable,
      getSortDirection: this.props.getSortDirection,
    });
  }
}

const mapStateToProps = state => ({
  pagination: state.uiUsersTable.pagination,
  users: fromUiUserTable.selectUsersForCurrentPage(state),
  isUsersTableLoading: fromUiUserTable.selectIsUserTableLoading(state),
  getSortDirection: property =>
    fromUiUserTable.getSortDirectionLongName(state, property),
});

const mapDispatchToProps = dispatch => ({
  fetchUsersTable: () => dispatch(uiUsersTableActions.fetchUsersTable()),
  setUsersPage: page => dispatch(uiUsersTableActions.setUsersPage(page)),
  setUsersTableSort: property =>
    dispatch(uiUsersTableActions.setUsersTableSort(property)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersTableContainer);
