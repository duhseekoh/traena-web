import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Grid, Message, Icon, Divider } from 'semantic-ui-react';
import * as uiActivitiesFormActions from '../../redux/modules/ui-activities-form/actions';
import type { Activity } from '../../types/activities';
import {
  ACTIVITY_TYPES,
  ACTIVITY_UI_NAMES,
  ACTIVITY_ICONS,
} from '../../constants/activities';
import { COLORS } from '../../constants/colors';
import FixedFooter from '../../components/ui-fixed-footer/FixedFooter';
import ActivityBuilderBar from './components/ActivityBuilderBar';
import styles from './GenericActivityForm.scss';

type Props = {
  editing: boolean,
  activityId: number,
  contentId: number,
  saving: boolean,
  error: string,
  children: React.DOM.Node[],
  handleSubmit: (onSubmit: (activity: Activity) => void) => void,
  saveActivity: (activity: Activity) => void,
  navigateToContentForm: (contentId: number) => void,
  removeActivityFromContentItem: (
    contentId: number,
    activityId: number,
  ) => void,
  activityType: string,
};

export const SUBHEADERS = {
  [ACTIVITY_TYPES.QUESTION_SET]: '5 questions only',
  [ACTIVITY_TYPES.POLL]: '1 question only',
};


class GenericActivityForm extends Component<Props> {
  submit = (activityFormVals: Activity) =>
    this.props.saveActivity(activityFormVals);

  render() {
    const {
      error,
      children,
      contentId,
      editing,
      handleSubmit,
      navigateToContentForm,
      activityType,
    } = this.props;
    return (
      <div>
        <div className={styles.activityFormTopBar}>
          <button
            className={styles.backButton}
            onClick={e => {
              e.preventDefault();
              navigateToContentForm(contentId);
            }}
          >
            <Icon name="chevron left" size="large" />
          </button>
          <div className={styles.activityFormTopBarTitle}>
            <h3>
              <img
                src={ACTIVITY_ICONS[activityType][COLORS.PRIMARY_GREEN]}
                className={styles.activityFormTopBarTitleLogo}
                alt={`${activityType} icon`}
              />
              {editing ? 'Edit' : 'Create a'} {ACTIVITY_UI_NAMES[activityType]}
            </h3>
            {/* <div className={styles.subheader}>
              {SUBHEADERS[activityType]}
            </div> */}
          </div>
        </div>
        <form className={styles.activityForm} autoComplete="off">
          <Grid>
            <Divider hidden section />
            <Divider hidden section />
            {children}
            {error && <Message error header="Error" content={error} />}
          </Grid>
        </form>
        <FixedFooter>
          <ActivityBuilderBar
            {...this.props}
            submit={() => {
              setTimeout(handleSubmit(this.submit)); // setTimeout necessary due to this issue: https://github.com/erikras/redux-form/issues/1366
            }}
          />
        </FixedFooter>
      </div>
    );
  }
}

const activityForm = reduxForm({
  form: 'activity',
})(GenericActivityForm);

const mapDispatchToProps = dispatch => ({
  saveActivity: vals => dispatch(uiActivitiesFormActions.saveActivity(vals)),
  removeActivityFromContentItem: (contentId, activityId) =>
    dispatch(
      uiActivitiesFormActions.removeActivityFromContentItem(
        contentId,
        activityId,
      ),
    ),
  navigateToContentForm: contentId =>
    dispatch(uiActivitiesFormActions.navigateToContentForm(contentId)),
});

export default connect(null, mapDispatchToProps)(activityForm);
