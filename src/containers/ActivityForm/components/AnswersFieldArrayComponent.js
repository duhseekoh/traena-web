import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import AddButton from '../../../components/AddButton/AddButton';
import AnswerInput from './AnswerInput';

const MAX_ANSWERS = 4;
const MIN_ANSWERS = 2;

class AnswersFieldArrayComponent extends Component<FieldArrayProps> {
  props: FieldArrayProps;

  componentDidMount() {
    if (this.props.fields.getAll().length === 0) {
      this.addAnswer(true);
      this.addAnswer(false);
    }
  }

  addAnswer = (correct: boolean) => {
    if (this.props.fields.getAll().length >= MAX_ANSWERS) {
      return;
    }
    this.props.fields.push({
      correct,
      id: uuidv4(),
    });
  };

  moveAnswer = (from: number, to: number) => {
    this.props.fields.move(from, to);
  };

  removeAnswer = (index: number) => {
    if (this.props.fields.getAll().length <= MIN_ANSWERS) {
      return;
    }
    this.props.fields.remove(index);
  };

  render() {
    const fieldArrayValues = this.props.fields.getAll();
    return (
      <div>
        {this.props.fields.map((answer, index) => (
          <AnswerInput
            key={fieldArrayValues[index].id}
            correct={fieldArrayValues[index].correct}
            name={answer}
            moveAnswer={this.moveAnswer}
            removeAnswer={this.removeAnswer}
            index={index}
            isRemovable={(!fieldArrayValues[index].correct &&
              fieldArrayValues.length > 2)}
            totalAnswers={fieldArrayValues.length}
          />
        ))}
        {this.props.fields.getAll().length < MAX_ANSWERS && (
          <AddButton
            onClick={e => {
              e.preventDefault();
              this.addAnswer(false);
            }}
          >
            Add an Answer
          </AddButton>
        )}
      </div>
    );
  }
}

export default AnswersFieldArrayComponent;
