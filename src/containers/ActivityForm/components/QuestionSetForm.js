/* eslint-disable react/no-multi-comp,react/prefer-stateless-function */
// @flow
import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import { FieldArray } from 'redux-form';
import GenericActivityForm from '../GenericActivityForm';
import * as formValidators from '../../../utils/formValidators';
import QuestionsFieldArrayComponent from './QuestionsFieldArrayComponent';

type QuestionSetFormProps = {
  title: string,
  activityId?: number,
  activityType: string,
  contentId: number,
  initialValues: Object,
  disabled: boolean,
};

export default class QuestionSetForm extends Component<QuestionSetFormProps> {
  render() {
    return (
      <GenericActivityForm
        {...this.props}
        validate={formValidators.validateActivityForm}
      >
        <Grid.Row centered>
          <Grid.Column width={12}>
            <FieldArray
              name="body.questions"
              component={QuestionsFieldArrayComponent}
            />
          </Grid.Column>
        </Grid.Row>
      </GenericActivityForm>
    );
  }
}
