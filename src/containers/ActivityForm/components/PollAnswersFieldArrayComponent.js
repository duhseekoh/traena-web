import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import AddButton from '../../../components/AddButton/AddButton';
import PollAnswerInput from './PollAnswerInput';

const MAX_ANSWERS = 4;
const MIN_ANSWERS = 2;

class PollAnswersFieldArrayComponent extends Component<FieldArrayProps> {
  props: FieldArrayProps;

  componentDidMount() {
    if (this.props.fields.getAll().length === 0) {
      this.addAnswer();
      this.addAnswer();
    }
  }

  addAnswer = () => {
    if (this.props.fields.getAll().length >= MAX_ANSWERS) {
      return;
    }
    this.props.fields.push({
      id: uuidv4(),
    });
  };

  moveAnswer = (from: number, to: number) => {
    this.props.fields.move(from, to);
  };

  removeAnswer = (index: number) => {
    if (this.props.fields.getAll().length <= MIN_ANSWERS) {
      return;
    }
    this.props.fields.remove(index);
  };

  render() {
    const fieldArrayValues = this.props.fields.getAll();
    return (
      <div>
        {this.props.fields.map((answer, index) => (
          <PollAnswerInput
            key={fieldArrayValues[index].id}
            name={answer}
            moveAnswer={this.moveAnswer}
            removeAnswer={this.removeAnswer}
            index={index}
            totalAnswers={fieldArrayValues.length}
            isRemovable={fieldArrayValues.length > 2}
          />
        ))}
        {this.props.fields.getAll().length < MAX_ANSWERS && (
          <AddButton
            onClick={e => {
              e.preventDefault();
              this.addAnswer();
            }}
          >
            Add an option (Up to 4)
          </AddButton>
        )}
      </div>
    );
  }
}

export default PollAnswersFieldArrayComponent;
