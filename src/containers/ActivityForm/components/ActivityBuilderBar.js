// @flow
import React from 'react';
import classnames from 'classnames';
import { Button, Grid } from 'semantic-ui-react';
import styles from '../GenericActivityForm.scss';
import { ACTIVITY_UI_NAMES } from '../../../constants/activities';

type Props = {
  editing: boolean,
  activityId: number,
  contentId: number,
  saving: boolean,
  submit: () => any,
  removeActivityFromContentItem: (
    contentId: number,
    activityId: number,
  ) => void,
  activityType: string,
};

const ActivityBuilderBar = (props: Props) => {
  const {
    activityType,
    removeActivityFromContentItem,
    activityId,
    contentId,
    submit,
    saving,
    editing,
  } = props;
  return (
    <div className={styles.activityBuilderBar}>

      <Grid>
        <Grid.Row centered className="compact">
          <Grid.Column width={12}>
            <Grid>
              <Grid.Row>
                <Grid.Column computer={8} tablet={16}>
                  <h2>Activity Builder</h2>
                </Grid.Column>
                <Grid.Column computer={8} tablet={16}>
                  <Button
                    primary
                    size="big"
                    type="submit"
                    className="right"
                    loading={saving}
                    onClick={e => {
                      e.preventDefault();
                      submit();
                    }}
                  >
                    {editing ? 'Save' : 'Create'} {ACTIVITY_UI_NAMES[activityType]}
                  </Button>
                  <Button
                    default
                    type="submit"
                    className={classnames('right', styles.deleteButton)}
                    onClick={e => {
                      e.preventDefault();
                      removeActivityFromContentItem(contentId, activityId);
                    }}
                    icon="trash"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>

    </div>
  );
};

export default ActivityBuilderBar;
