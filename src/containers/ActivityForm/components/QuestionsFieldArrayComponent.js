import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Grid, Divider, Button } from 'semantic-ui-react';
import { FieldArray } from 'redux-form';
import type { FieldArrayProps } from 'redux-form';
import AddButton from '../../../components/AddButton/AddButton';
import TextFieldCharCount from '../../../components/forms/TextFieldCharCount/TextFieldCharCount';
import UpDownButtons from '../../../components/UpDownButtons/UpDownButtons';
import AnswersFieldArrayComponent from './AnswersFieldArrayComponent';
import styles from '../GenericActivityForm.scss';

const MAX_QUESTIONS = 5;
const MIN_QUESTIONS = 1;

class QuestionsFieldArrayComponent extends Component<FieldArrayProps> {
  componentDidMount() {
    if (this.props.fields.getAll().length === 0) {
      this.addQuestion();
    }
  }

  addQuestion = () => {
    this.props.fields.push({
      id: uuidv4(),
      choices: [],
    });
  }

  removeQuestion = (index: number) => {
    if (this.props.fields.getAll().length <= MIN_QUESTIONS) {
      return;
    }
    this.props.fields.remove(index);
  };

  moveQuestion = (from: number, to: number) => {
    this.props.fields.move(from, to);
  }

  render() {
    const { fields } = this.props;
    const fieldArrayValues = fields.getAll();

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column width={2} />
          <Grid.Column width={7}>
            <h5>Questions</h5>
          </Grid.Column>
          <Grid.Column width={7}>
            <h5>Answers</h5>
          </Grid.Column>
        </Grid.Row>
        {fields.map((question, index) => (
          <Grid.Row key={fieldArrayValues[index].id}>
            <Grid.Column width={2} className={styles.questionMoveRemoveButtons}>
              {fieldArrayValues.length > MIN_QUESTIONS &&
                <Button
                  onClick={e => {
                    e.preventDefault();
                    this.removeQuestion(index);
                  }}
                  icon="x"
                  className={styles.removeButton}
                />
              }
              <UpDownButtons
                move={this.moveQuestion}
                index={index}
                totalItems={fieldArrayValues.length}
              />
            </Grid.Column>
            <Grid.Column width={14} className="compact">
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={8} tablet={16}>
                    <TextFieldCharCount
                      maxLength={230}
                      name={`${question}.questionText`}
                      placeholder="Ask a Question..."
                      className={styles.postInput}
                    />
                  </Grid.Column>
                  <Grid.Column computer={8} tablet={16}>
                    <FieldArray
                      name={`${question}.choices`}
                      component={AnswersFieldArrayComponent}
                      rerenderOnEveryChange
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
            <Grid.Column width={16}>
              <Divider section />
            </Grid.Column>
          </Grid.Row>
        ))}
        <Grid.Row>
          <Grid.Column width={16}>
            {this.props.fields.getAll().length < MAX_QUESTIONS && (
              <AddButton
                onClick={e => {
                  e.preventDefault();
                  this.addQuestion();
                }}
              >
                Add a Question
              </AddButton>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default QuestionsFieldArrayComponent;
