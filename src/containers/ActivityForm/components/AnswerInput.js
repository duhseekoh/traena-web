/* eslint-disable jsx-a11y/interactive-supports-focus,jsx-a11y/click-events-have-key-events */
// @flow
import React from 'react';
import { Grid, Icon } from 'semantic-ui-react';
import classnames from 'classnames';
import { TextField } from '@panderalabs/redux-form-semantic';
import UpDownButtons from '../../../components/UpDownButtons/UpDownButtons';

import styles from '../GenericActivityForm.scss';

type Props = {
  correct: boolean,
  name: string,
  index: number,
  removeAnswer: (index: number) => any,
  moveAnswer: (from: number, to: number) => any,
  totalAnswers: number,
  isRemovable: boolean,
};

const AnswerInput = ({
  correct,
  name,
  index,
  removeAnswer,
  moveAnswer,
  totalAnswers,
  isRemovable,
}: Props) => (
  <Grid className={styles.answerInput}>
    <Grid.Row className={styles.answerInputRow}>
      <Grid.Column width={1} className={styles.answerInputColumn}>
        <span className={styles.answerNumberLabel}>{index + 1}</span>
      </Grid.Column>
      <Grid.Column width={14} className={styles.answerInputColumn}>
        <div className={styles.answerInputWrapper}>
          {correct &&
            <Icon name="check" className={styles.answerInputCorrectIcon} />
          }
          <TextField
            name={`${name}.choiceText`}
            className={
              correct
                ? classnames(styles.postInput, styles.correctInput)
                : classnames(styles.postInput, styles.incorrectInput)
            }
            placeholder={
              correct ? 'Correct Answer' : 'Wrong Answer'
            }
            maxLength={40}
          />
        </div>
        {isRemovable && (
          <span
            role="button"
            className={styles.removeAnswerButton}
            onClick={e => {
              e.preventDefault();
              removeAnswer(index);
            }}
          >
            Remove
          </span>
        )}
        <TextField name={`${name}.correct`} className={styles.hiddenInput} />
      </Grid.Column>
      <Grid.Column width={1} className={styles.answerInputColumn}>
        <UpDownButtons
          move={moveAnswer}
          index={index}
          totalItems={totalAnswers}
        />
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default AnswerInput;
