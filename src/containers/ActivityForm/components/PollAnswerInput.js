/* eslint-disable jsx-a11y/interactive-supports-focus,jsx-a11y/click-events-have-key-events */
// @flow
import React from 'react';
import { Grid } from 'semantic-ui-react';
import { TextField } from '@panderalabs/redux-form-semantic';
import UpDownButtons from '../../../components/UpDownButtons/UpDownButtons';

import styles from '../GenericActivityForm.scss';

type Props = {
  name: string,
  index: number,
  removeAnswer: (index: number) => any,
  moveAnswer: (from: number, to: number) => any,
  isRemovable: boolean,
  totalAnswers: number,
};

const PollAnswerInput = ({
  name,
  index,
  removeAnswer,
  moveAnswer,
  isRemovable,
  totalAnswers,
}: Props) => (
  <Grid className={styles.answerInput}>
    <Grid.Row className={styles.answerInputRow}>
      <Grid.Column width={1} className={styles.answerInputColumn}>
        <span className={styles.answerNumberLabel}>{index + 1}</span>
      </Grid.Column>
      <Grid.Column width={14} className={styles.answerInputColumn}>
        <TextField
          name={`${name}.choiceText`}
          className={styles.postInput}
          placeholder="Poll Response..."
          maxLength={40}
        />
        {isRemovable && (
          <span
            role="button"
            className={styles.removeAnswerButton}
            onClick={e => {
              e.preventDefault();
              removeAnswer(index);
            }}
          >
            Remove
          </span>
        )}
      </Grid.Column>
      <Grid.Column width={1} className={styles.answerInputColumn}>
        <UpDownButtons
          move={moveAnswer}
          index={index}
          totalItems={totalAnswers}
        />
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default PollAnswerInput;
