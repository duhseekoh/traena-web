/* eslint-disable react/no-multi-comp,react/prefer-stateless-function */
// @flow
import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import { FieldArray } from 'redux-form';
import GenericActivityForm from '../GenericActivityForm';
import * as formValidators from '../../../utils/formValidators';
import PollAnswersFieldArrayComponent from './PollAnswersFieldArrayComponent';
import TextFieldCharCount from '../../../components/forms/TextFieldCharCount/TextFieldCharCount';
import styles from '../GenericActivityForm.scss';
import type { PollActivity } from '../../../types/activities';

type PollFormProps = {
  title: string,
  activityId?: number,
  activityType: string,
  contentId: number,
  initialValues: $Shape<PollActivity>,
  disabled: boolean,
};

export default class PollForm extends Component<PollFormProps> {
  render() {
    return (
      <GenericActivityForm
        {...this.props}
        validate={formValidators.validateActivityForm}
      >
        <Grid.Row centered>
          <Grid.Column width={12}>
            <Grid>
              <Grid.Row centered>
                <Grid.Column width={8}>
                  <h5>Questions</h5>
                </Grid.Column>
                <Grid.Column width={8}>
                  <h5>Answers</h5>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={8}>
                  <TextFieldCharCount
                    maxLength={230}
                    name="body.title"
                    placeholder="Ask a Question..."
                    className={styles.postInput}
                  />
                </Grid.Column>
                <Grid.Column width={8}>
                  <FieldArray
                    name="body.choices"
                    component={PollAnswersFieldArrayComponent}
                    rerenderOnEveryChange
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </GenericActivityForm>
    );
  }
}
