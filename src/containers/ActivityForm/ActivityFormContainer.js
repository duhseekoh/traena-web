/* eslint-disable react/prop-types */
// TODO: Add types to this file.
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import * as uiActivitiesFormActions from '../../redux/modules/ui-activities-form/actions';
import * as fromActivities from '../../redux/modules/activities/selectors';
import * as fromUiActivitiesForm from '../../redux/modules/ui-activities-form/selectors';

import QuestionSetForm from './components/QuestionSetForm';
import PollForm from './components/PollForm';
import { ACTIVITY_TYPES } from '../../constants/activities';
import { buildEmptyActivityBody } from '../../utils/activityUtils';

class ActivityFormContainer extends Component {
  componentDidMount = () => {
    // We pass in contentId, so we can return to the edit content page in case of an error initializing
    this.props.initializeActivityFormData(
      this.props.contentId,
      this.props.activityId,
    );
  };

  render() {
    const {
      activity,
      activityId,
      activityType,
      errorInitializing,
      readOnly,
      loading,
      contentId,
      saving,
    } = this.props;

    if (loading && !errorInitializing) {
      return <Loader active />;
    }

    if (errorInitializing) {
      return (
        <div>
          There was an error loading this content to edit. You can refresh to
          try again.
        </div>
      );
    }

    /**
     * If no activity or has been loaded yet,
     * but we also have not yet started loading in the data
     */
    if (activityId && !activity) {
      return null;
    }

    const activityTypeToFormComponent = {
      [ACTIVITY_TYPES.QUESTION_SET]: QuestionSetForm,
      [ACTIVITY_TYPES.POLL]: PollForm,
    };
    /* eslint-disable indent */
    const initialValues = activityId
      ? {
          ...activity,
        }
      : {
          contentId,
          type: activityType,
          // @TODO:  Dynamically get initial values based on activityType
          body: buildEmptyActivityBody(activityType),
        };
    /* eslint-enable indent */
    const Form = activityTypeToFormComponent[activityType];
    return (
      <Form
        saving={saving}
        editing={!!activityId}
        activityId={activityId}
        activityType={activityType}
        contentId={contentId}
        initialValues={initialValues}
        disabled={readOnly}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  initializeActivityFormData: (contentId, activityId) =>
    dispatch(
      uiActivitiesFormActions.initializeActivityFormData(contentId, activityId),
    ),
});

const mapStateToProps = (state, { activityId }) => ({
  activity: fromActivities.selectActivityById(state, activityId),
  loading: fromUiActivitiesForm.selectIsLoading(state),
  errorInitializing: fromUiActivitiesForm.selectErrorLoading(state),
  saving: fromUiActivitiesForm.selectIsSaving(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ActivityFormContainer,
);
