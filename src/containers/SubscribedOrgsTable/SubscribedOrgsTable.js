// @flow

import React, { Component } from 'react';
import type { ContextRouter } from 'react-router';
import { connect } from 'react-redux';
import { Table, Loader, Icon } from 'semantic-ui-react';
import TableRow from '../../components/Table/TableRow';
import TableHeaderCell from '../../components/Table/TableHeaderCell';
import styles from './SubscribedOrgsTable.scss';
import Pagination from '../../components/Pagination/Pagination';
import type { Pagination as PaginationType } from '../../types';
import * as fromChannels from '../../redux/modules/channels/selectors';
import type { Organization } from '../../types/organizations';
import type { Channel } from '../../types/channels';

import {
  fetchSubscribedOrgsTable,
  setSubscribedOrgsPage,
} from '../../redux/modules/ui-subscribed-orgs-table/actions';
import * as fromUiSubscribedOrgsTable from '../../redux/modules/ui-subscribed-orgs-table/selectors';

type OwnProps = {
  channelId: number,
};

type ConnectedProps = {
  channel: Channel,
  organizations: Organization[],
  pagination: PaginationType,
  loading: boolean,
  error: boolean,
  fetchSubscribedOrgsTable: (channelId: number) => any,
  setSubscribedOrgsPage: (page: number) => any,
};

type Props = OwnProps & ConnectedProps & ContextRouter;

class SubscribedOrgsTable extends Component<Props> {
  props: Props;
  componentDidMount() {
    this.props.fetchSubscribedOrgsTable(
      Number(this.props.match.params.channelId),
    );
  }

  goToSubscribedOrgsPage = (page: number) => {
    this.props.setSubscribedOrgsPage(page);
    this.props.fetchSubscribedOrgsTable(
      Number(this.props.match.params.channelId),
    );
  };

  render() {
    const {
      loading,
      organizations,
      error,
      pagination,
      channel,
      channelId,
    } = this.props;

    if (channelId && (!organizations || !channel)) {
      return null;
    }

    if (!loading && !error && !organizations.length) {
      return (
        <h3 className={styles.subscribersTableNoContentMessage}>
          No organizations have subscribed yet...
        </h3>
      );
    }
    if (error) {
      return (
        <h3 className={styles.subscribersTableNoContentMessage}>
          Error loading subscribers...
        </h3>
      );
    }
    return (
      <div>
        {loading ?
          <Loader active /> :
          <div className={styles.table}>
            <div className={styles.tableTitle}>
              <Icon name="users" className={styles.tableIcon} />
              ORGANIZATIONS SUBSCRIBED
              <p className={styles.tableSubtitle}>{organizations.length}</p>
            </div>
            <Table>
              <Table.Header>
                <Table.Row>
                  <TableHeaderCell>Name</TableHeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {organizations.map(org => (
                  <TableRow key={org.id}><td>{org.name}</td></TableRow>
                ))}
              </Table.Body>
            </Table>
            <Pagination
              pagination={pagination}
              goToPage={this.goToSubscribedOrgsPage}
            />
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = (state, { channelId }) => ({
  organizations: fromUiSubscribedOrgsTable.selectOrganizations(state),
  loading: fromUiSubscribedOrgsTable.selectIsSubscribedOrgsTableLoading(state),
  error: fromUiSubscribedOrgsTable.selectSubscribedOrgsTableError(state),
  pagination: fromUiSubscribedOrgsTable.selectPagination(state),
  channel: fromChannels.selectChannelById(state, channelId),
});

const mapDispatchToProps = dispatch => ({
  fetchSubscribedOrgsTable: (channelId: number) =>
    dispatch(fetchSubscribedOrgsTable(channelId)),
  setSubscribedOrgsPage: page =>
    dispatch(setSubscribedOrgsPage(page)),
});

// eslint-disable-next-line
export default connect(mapStateToProps, mapDispatchToProps)(SubscribedOrgsTable);
