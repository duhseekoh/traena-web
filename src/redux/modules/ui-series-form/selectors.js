// @flow
import { formValueSelector } from 'redux-form';
import type { ReduxState } from '../../rootReducer';
import { UI_REDUX_MODULE_NAMES } from '../../../constants/appConstants';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

export const selectChannelId = (state: ReduxState) =>
  formValueSelector('series')(state, 'channelId');
export const selectContentIds = (state: ReduxState) =>
  formValueSelector('series')(state, 'contentIds') || [];

export const {
  selectIsLoading,
  selectIsSaving,
  selectErrorLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_SERIES_FORM);
