// @flow
import {
  SERIES_FORM_CLEAR,
  uiLoadingActionTypes,
} from './actions';
import type { Action } from '../../rootReducer';
import { UI_REDUX_MODULE_NAMES } from '../../../constants/appConstants';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_SERIES_FORM,
);

export type ReduxStateUiSeriesForm = {
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiSeriesForm = {
  uiLoading: initialUiLoadingState,
};

function uiSeriesForm(
  state: ReduxStateUiSeriesForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SERIES_FORM_CLEAR: {
      return {
        ...initialState,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiSeriesForm;
