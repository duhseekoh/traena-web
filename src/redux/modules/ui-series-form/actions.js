// cannot enable flow in this file yet, because of issue with redux-form `change` return
// type in the redux-form flow types
import _ from 'lodash';
import { push } from 'connected-react-router';
import { formValueSelector, change } from 'redux-form';
import { getRouteLink, routes } from '../../../constants/routes';
import * as applicationActions from '../application/actions';
import * as channelsActions from '../channels/actions';
import * as seriesActions from '../series/actions';
import type { Dispatch, GetState } from '../../rootReducer';
import type { Series } from '../../../types/series';
import { UI_REDUX_MODULE_NAMES } from '../../../constants/appConstants';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';

export const SERIES_FORM_CLEAR = 'SERIES_FORM/CLEAR';

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
  setErrorLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_SERIES_FORM);

export function initializeSeriesFormData(seriesId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      const initializationMethods = [
        dispatch(channelsActions.loadOrganizationChannels()),
      ];
      if (seriesId) {
        initializationMethods.push(
          dispatch(seriesActions.loadSeries(seriesId)),
        );
        initializationMethods.push(
          dispatch(seriesActions.loadContentForSeries(seriesId)),
        );
      }
      await Promise.all(initializationMethods);
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(applicationActions.createNotification({
        message: 'There was an issue with trying to create or edit this series',
        title: 'Oops!',
        onDismiss: () => { dispatch(push(getRouteLink(routes.SERIES))); },
      }));
    }
  };
}

function convertFormValsToSeries(seriesFormVals: Object): Series {
  const series = _.pick(seriesFormVals, [
    'authorId',
    'channelId',
    'description',
    'id',
    'title',
    'contentIds',
  ]);
  return series;
}

export function saveSeriesForm(seriesFormVals: Series) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSaving(true));
      const series = convertFormValsToSeries(seriesFormVals);
      await dispatch(seriesActions.saveSeries(series));
      dispatch(setSaving(false));
      dispatch(
        applicationActions.createNotification({
          message: `You just saved the series '${series.title}'`,
          title: 'Success!',
          onDismiss: () => {
            dispatch(push(getRouteLink(routes.SERIES)));
          },
        }),
      );
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(applicationActions.createNotification({
        message: 'There was an issue with trying to save this series',
        title: 'Oops!',
        onDismiss: () => { dispatch(push(getRouteLink(routes.SERIES))); },
      }));
    }
  };
}

export function addContentToSeries(contentId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const contentIds = formValueSelector('series')(getState(), 'contentIds') || [];
    if (contentIds.includes(contentId) || contentIds.length >= 100) {
      return;
    }
    dispatch(change('series', 'contentIds', [
      ...contentIds,
      contentId,
    ]));
  };
}

export function removeContentFromSeries(contentId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const contentIds = formValueSelector('series')(getState(), 'contentIds')
      .filter(id => id !== contentId) || [];
    dispatch(change('series', 'contentIds', contentIds));
  };
}

export function reorderContentForSeries(from: number, to: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const contentIds = formValueSelector('series')(getState(), 'contentIds');
    contentIds.splice(to, 0, contentIds.splice(from, 1)[0]);
    dispatch(change('series', 'contentIds', contentIds));
  };
}

export function navigateToSeriesTable() {
  return async (dispatch: Dispatch) => {
    dispatch(
      applicationActions.createNotification({
        message: "You may lose progress if you haven't saved your changes.",
        title: 'Remember to save!',
        onDismiss: () => {},
        onConfirm: async () => {
          dispatch(push(getRouteLink(routes.SERIES)));
        },
      }),
    );
  };
}

export function deleteSeriesConfirm(seriesId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      await dispatch(seriesActions.deleteSeries(seriesId));
      dispatch(setLoading(false));
      dispatch(applicationActions.createNotification({
        message: 'The series was deleted successfully.',
        title: 'Success!',
        onDismiss: () => dispatch(push(getRouteLink(routes.SERIES))),
      }));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(applicationActions.createNotification({
        message: 'There was an issue deleting this series.',
        title: 'Try again!',
        onDismiss: () => { },
      }));
    }
  };
}

export function deleteSeries(seriesId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(applicationActions.createNotification({
      message: 'Are you sure you would like to delete this series?',
      title: 'Confirm.',
      onDismiss: () => {},
      onConfirm: () => dispatch(deleteSeriesConfirm(seriesId)),
      dismissText: 'No',
      confirmText: 'Yes',
    }));
  };
}
