import { fetchContentItem } from '../content/actions';

export const CONTENT_ITEM_LOAD_SUCCESS = 'CONTENT_ITEM/LOAD_SUCCESS';
export const CONTENT_ITEM_LOAD_FAILURE = 'CONTENT_ITEM/LOAD_FAILURE';

function loadContentItemSuccess() {
  return {
    type: CONTENT_ITEM_LOAD_SUCCESS,
  };
}

function loadContentItemFailure() {
  return {
    type: CONTENT_ITEM_LOAD_FAILURE,
  };
}

export function loadContentItem(contentId) {
  return async dispatch => {
    try {
      await dispatch(fetchContentItem(contentId));
      dispatch(loadContentItemSuccess());
    } catch (err) {
      dispatch(loadContentItemFailure());
    }
  };
}
