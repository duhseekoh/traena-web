import type { ReduxState } from '../../rootReducer';

export const selectIsContentViewLoading = (state: ReduxState) =>
  state.uiContentView.loading;

export const selectContentViewHasError = (state: ReduxState) =>
  state.uiContentView.errorInitializing;
