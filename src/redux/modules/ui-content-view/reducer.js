// @flow
import {
  CONTENT_ITEM_LOAD_FAILURE,
  CONTENT_ITEM_LOAD_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiContentView = {
  loading: boolean,
  errorInitializing: boolean,
};

const initialState: ReduxStateUiContentView = {
  errorInitializing: false,
  loading: true,
};

function uiContentView(
  state: ReduxStateUiContentView = initialState,
  { type }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_LOAD_SUCCESS: {
      return {
        ...state,
        errorInitializing: false,
        loading: false,
      };
    }

    case CONTENT_ITEM_LOAD_FAILURE: {
      return {
        ...state,
        errorInitializing: true,
        loading: false,
      };
    }

    default:
      return state;
  }
}

export default uiContentView;
