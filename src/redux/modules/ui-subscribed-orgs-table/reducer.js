// @flow
import {
  SUBSCRIBED_ORGS_FETCH_SUCCESS,
  SUBSCRIBED_ORGS_SET_PAGE,
  SUBSCRIBED_ORGS_SET_LOADING,
  SUBSCRIBED_ORGS_FETCH_FAILURE,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiSubscribedOrgsTable = {
  currentItemIds: number[],
  pagination: {
    number: number,
  },
  loading: boolean,
  error: boolean,
};

const initialState: ReduxStateUiSubscribedOrgsTable = {
  currentItemIds: [],
  pagination: {
    number: 0,
  },
  loading: false,
  error: false,
};

function uiSubscribedOrgsTable(
  state: ReduxStateUiSubscribedOrgsTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case SUBSCRIBED_ORGS_FETCH_SUCCESS: {
      const { ids, ...pagination } = payload;
      return {
        ...state,
        pagination,
        currentItemIds: ids,
      };
    }
    case SUBSCRIBED_ORGS_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case SUBSCRIBED_ORGS_SET_LOADING:
      return {
        ...state,
        loading: payload,
        error: false,
      };

    case SUBSCRIBED_ORGS_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default uiSubscribedOrgsTable;
