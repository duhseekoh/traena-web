// @flow
import { createSelector } from 'reselect';
import * as fromOrganizations from '../organizations/selectors';
import type { ReduxState } from '../../rootReducer';

export const selectPagination = (state: ReduxState) =>
  state.uiSubscribedOrgsTable.pagination;

export const selectCurrentPage = (state: ReduxState) =>
  state.uiSubscribedOrgsTable.pagination.number;

const selectCurrentItemIds = (state: ReduxState) =>
  state.uiSubscribedOrgsTable.currentItemIds || [];

export const selectIsSubscribedOrgsTableLoading = (state: ReduxState) =>
  state.uiSubscribedOrgsTable.loading;

export const selectSubscribedOrgsTableError = (state: ReduxState) =>
  state.uiSubscribedOrgsTable.error;

export const selectOrganizations = createSelector(
  [fromOrganizations.selectOrganizationsById, selectCurrentItemIds],
  (organizationsById, currentItemIds) =>
    currentItemIds
      .map(id => organizationsById[id])
      .filter(o => o !== undefined),
);
