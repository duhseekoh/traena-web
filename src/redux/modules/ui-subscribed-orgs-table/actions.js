// @flow
import * as channelApi from '../../api/channels';
import * as fromAuth from '../auth/selectors';
import * as fromUiSubscribedOrgsTable from './selectors';
import { organizationsFetchSuccess } from '../organizations/actions';
import type { Dispatch, GetState } from '../../rootReducer';
import type { IndexedPage } from '../../../types';

export const SUBSCRIBED_ORGS_FETCH_SUCCESS = 'SUBSCRIBED_ORGS/FETCH_SUCCESS';
export const SUBSCRIBED_ORGS_FETCH_FAILURE = 'SUBSCRIBED_ORGS/FETCH_FAILURE';

export const SUBSCRIBED_ORGS_SET_PAGE = 'SUBSCRIBED_ORGS/SET_PAGE';

export const SUBSCRIBED_ORGS_SET_LOADING = 'SUBSCRIBED_ORGS/SET_LOADING';

export function subscribedOrgsFetchSuccess(subscribedOrgs: IndexedPage<any>) {
  return {
    type: SUBSCRIBED_ORGS_FETCH_SUCCESS,
    payload: subscribedOrgs,
  };
}

function subscribedOrgsFetchFailure(err: string) {
  return {
    type: SUBSCRIBED_ORGS_FETCH_FAILURE,
    payload: err,
  };
}

export function setSubscribedOrgsPage(page: number) {
  return {
    type: SUBSCRIBED_ORGS_SET_PAGE,
    payload: page,
  };
}

export function setSubscribedOrgsLoading(loading: boolean) {
  return {
    type: SUBSCRIBED_ORGS_SET_LOADING,
    payload: loading,
  };
}

export function fetchSubscribedOrgsTable(channelId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    const page = fromUiSubscribedOrgsTable.selectCurrentPage(getState());
    dispatch(setSubscribedOrgsLoading(true));
    try {
      const subscribedOrgs = await channelApi
        .getChannelSubscribedOrgs(organizationId, channelId, page);
      dispatch(subscribedOrgsFetchSuccess(subscribedOrgs));
      dispatch(organizationsFetchSuccess(subscribedOrgs.index));
      dispatch(setSubscribedOrgsLoading(false));
    } catch (err) {
      dispatch(subscribedOrgsFetchFailure(err));
    }
  };
}
