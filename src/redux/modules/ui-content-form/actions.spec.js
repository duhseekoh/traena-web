import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import {
  contentItemSaved,
  CONTENT_ITEM_SAVED,
  clearContentItemForm,
  CONTENT_ITEM_FORM_CLEAR,
  setSaving,
  resetUploadProgress,
  CONTENT_ITEM_RESET_UPLOAD_PROGRESS,
  uiLoadingActionTypes,
} from './actions';

jest.mock('../../api/content');

describe('content actions', () => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch CONTENT_ITEM_SAVED', () => {
    const payload = { body: { body: 'body' }, status: 'DRAFT', tags: [] };
    store.dispatch(contentItemSaved(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENT_ITEM_SAVED, payload }]);
  });

  it('should dispatch CONTENT_ITEM_FORM_CLEAR', () => {
    store.dispatch(clearContentItemForm());

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENT_ITEM_FORM_CLEAR }]);
  });

  it('should dispatch CONTENT_ITEM_SAVING', () => {
    const payload = true;
    store.dispatch(setSaving(payload));

    const actions = store.getActions();
    expect(actions).toEqual(
      [{ type: uiLoadingActionTypes.setSaving, payload: { saving: true } }],
    );
  });

  it('should dispatch CONTENT_ITEM_RESET_UPLOAD_PROGRESS', () => {
    store.dispatch(resetUploadProgress());

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENT_ITEM_RESET_UPLOAD_PROGRESS }]);
  });
});
