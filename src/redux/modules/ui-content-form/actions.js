// @flow
import _ from 'lodash';
import { push } from 'connected-react-router';
import { initialize, change, getFormValues, SubmissionError } from 'redux-form';
import { getRouteLink, routes } from '../../../constants/routes';
import { selectImages, selectVideo } from '../ui-content-form/selectors';
import * as applicationActions from '../application/actions';
import * as channelsActions from '../channels/actions';
import * as contentActions from '../content/actions';
import * as activitiesActions from '../activities/actions';
import * as contentApi from '../../api/content';
import type { TranscodeJob } from '../../../types/transcode';
import type { Video, ImageEntry } from '../../../types/content';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';

import {
  TRANSCODING_STATUSES,
  UI_PROGRESS_STATUSES,
  CONTENT_STATUSES,
  IMAGE_PROCESSING_LAMBDA_TYPICAL_TIME,
  VIDEO_TRANSCODER_TYPICAL_TIME,
  CONTENT_TYPES,
  CONTENT_IMAGES_MAX_ALLOWED,
  UI_REDUX_MODULE_NAMES,
  type ContentType,
} from '../../../constants/appConstants';
import type { Dispatch, GetState } from '../../rootReducer';

export const CONTENT_ITEM_SAVED = 'CONTENT_ITEM/SAVED';
export const CONTENT_ITEM_UPLOAD_STARTED = 'CONTENT_ITEM/UPLOAD_STARTED';
export const CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS =
  'CONTENT_ITEM/INCREASE_UPLOAD_PROGRESS';
export const CONTENT_ITEM_RESET_UPLOAD_PROGRESS =
  'CONTENT_ITEM/RESET_UPLOAD_PROGRESS';
export const CONTENT_ITEM_FORM_CLEAR = 'CONTENT_ITEM/FORM_CLEAR';

// TODO define a flow type for contentItem
export function contentItemSaved(contentItem: Object) {
  return {
    type: CONTENT_ITEM_SAVED,
    payload: contentItem,
  };
}

export function clearContentItemForm() {
  return {
    type: CONTENT_ITEM_FORM_CLEAR,
    payload: undefined, // to satisfy flow
  };
}

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
  setErrorLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_CONTENT_FORM);

/**
 * Given the content creation redux form, spit out a content item that can be sent
 * via api. Currently the form values directly map to the shape of a content item,
 * although we still need to be selective about what is picked, because any extra
 * fields that may not be part of the content itself should not be sent.
 * TODO - come up with something more robost. potentially use classes to do transformation.
 * TODO - use flow to add types for the form vals and content item
 * @param contentFormVals redux form object
 */
function convertFormValsToContentItem(contentFormVals: Object) {
  const contentItem = {
    body: contentFormVals.body,
    status: contentFormVals.status,
    tags: contentFormVals.tags,
    id: contentFormVals.id,
    channelId: contentFormVals.channelId,
    activitiesOrder: contentFormVals.activitiesOrder,
  };

  // In some cases, we set type back to the 'Text' type of content.
  // This will happen when a user toggled the media section to Image or Video
  // but didn't end up adding one of those. Or, when they removed all images or
  // the video from an existing post.

  const hasImages =
    contentItem.body.images && contentItem.body.images.length > 0;
  const hasVideo = !!contentItem.body.video;

  // No matter what, if there's no video or images, its a text post.
  // Currently only allow oneOf images or video, not both at the same time.
  if (hasImages) {
    contentItem.body.type = CONTENT_TYPES.Image;
    delete contentItem.body.video;
  } else if (hasVideo) {
    contentItem.body.type = CONTENT_TYPES.TrainingVideo;
    delete contentItem.body.images;
  } else {
    contentItem.body.type = CONTENT_TYPES.DailyAction;
    delete contentItem.body.video;
    delete contentItem.body.images;
  }

  return contentItem;
}

/**
 * Take a redux form submission and either create content or update existing content.
 * If the form includes a content id, it will perform an update.
 * @param contentFormVals redux form object
 */
export function saveContentForm(contentFormVals: Object) {
  return async (dispatch: Dispatch) => {
    const contentItem = convertFormValsToContentItem({
      ...contentFormVals,
      status: contentFormVals.desiredStatus,
    });

    const isNewContent = !contentItem.id;

    dispatch(setSaving(true));
    let savedContentItem;
    try {
      if (isNewContent) {
        // New content
        savedContentItem = await contentApi.createContentItem(contentItem);
      } else {
        // Existing content
        savedContentItem = await contentApi.editContentItem(contentItem);
      }
      dispatch(contentItemSaved(savedContentItem));
      dispatch(
        initialize(
          'content',
          {
            ...savedContentItem,
            desiredStatus: savedContentItem.status,
          },
          false,
          { updateUnregisteredField: true },
        ),
      );
    } catch (err) {
      dispatch(setSaving(false));
      throw new SubmissionError({
        _error: 'There was an issue saving. Please try again',
      });
    }
    dispatch(setSaving(false));

    // We take users from the 'Create' route to the 'Edit' route on their
    // first successful save.
    if (isNewContent && savedContentItem.status === CONTENT_STATUSES.DRAFT) {
      dispatch(
        push(
          getRouteLink(routes.CONTENT_EDIT, {
            contentId: savedContentItem.id,
          }),
        ),
      );
    }

    return savedContentItem;
  };
}

/**
 * Reset the form, notify the user its published, and go back to the routes page
 * @param  contentTitle title of item just published
 * @return {[type]}              [description]
 */
function contentItemPublishSuccess(contentTitle) {
  return dispatch => {
    dispatch(clearContentItemForm());
    dispatch(
      applicationActions.createNotification({
        message: `You just published '${contentTitle}'`,
        title: 'Success!',
        onDismiss: () => {
          dispatch(push(getRouteLink(routes.CONTENT)));
        },
      }),
    );
  };
}

/**
 * Essentially a passthrough to save, but has different ui feedback
 * @param contentFormVals redux form object
 * @return {Promise<undefined>}
 */

export function publishContentForm(contentFormVals: Object) {
  return async (dispatch: Dispatch) => {
    try {
      const savedContentItem = await dispatch(saveContentForm(contentFormVals));
      if (!savedContentItem) {
        throw new Error('Content Item did not save');
      }
      dispatch(contentItemPublishSuccess(savedContentItem.body.title));
    } catch (err) {
      dispatch(setSaving(false));
      throw new SubmissionError({
        _error: 'There was an issue publishing. Please try again',
      });
    }
  };
}

function increaseUploadProgress(progress) {
  return {
    type: CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS,
    payload: progress,
  };
}

export function resetUploadProgress() {
  return {
    type: CONTENT_ITEM_RESET_UPLOAD_PROGRESS,
    payload: undefined, // to satisfy flow
  };
}

/**
 * Calls actions necessary to initialize content form, and manages loading/error states for the form
 */
export function initializeContentFormData(contentId?: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      const initializationMethods = [
        dispatch(channelsActions.loadSelfSubscriptions()),
      ];
      if (contentId) {
        initializationMethods.push(
          dispatch(contentActions.fetchContentItem(contentId)),
        );
        initializationMethods.push(
          dispatch(activitiesActions.loadContentActivities(contentId)),
        );
      }
      await Promise.all(initializationMethods);
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(setErrorLoading());
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to create or edit content.',
          title: 'Oops!',
          onDismiss: () => {
            dispatch(push(getRouteLink(routes.CONTENT)));
          },
        }),
      );
    }
  };
}

/**
 * Update the form fields to have a video. Used when we kick off the transcoder
 * and we have the *eventual* video paths already defined.
 */
function updateContentFormVideo(video: Video) {
  return (dispatch: Dispatch) => {
    dispatch(change('content', 'body.video', video));
  };
}

/**
 * After a video file is uploaded, this simply pings the api repeatedly until we
 * get a success or failure on the trasncoding process. The UI gets updated
 * accordingly, along the way.
 * @param transcodingJob
 * @param timeElapsed
 */
function trackTranscodingProgress(transcodeId: number, timeElapsed = 0) {
  // eslint-disable-next-line
  return async (dispatch: Dispatch) => {
    const transcodingJobUpdate = await contentApi.checkTranscodingProgress(
      transcodeId,
    );
    if (transcodingJobUpdate.status === TRANSCODING_STATUSES.IN_PROGRESS) {
      return new Promise(resolve => {
        setTimeout(async () => {
          await dispatch(
            trackTranscodingProgress(transcodeId, timeElapsed + 2000),
          );
          resolve();
        }, 2000);
      });
    } else if (transcodingJobUpdate.status === TRANSCODING_STATUSES.ERROR) {
      dispatch(change('content', 'body.video', null));
      throw new SubmissionError({
        _error: 'There was an issue processing your video. Please try again',
      });
    }
    // We don't need to do anything when the transcoding completes. The video
    // properties are already set in the form when the transcoding was kicked off.
  };
}

/**
 * Get's a percentage that decelerates as numerator increases with a constant
 * denominator.
 */
function getPercent(numerator: number, denominator: number) {
  const ratio = numerator / denominator;
  // Gets a negative of eulers exponent, then substracts from 1 so we can turn it
  // into a percentage.
  const percent = 1 - Math.exp(-1 * ratio);
  return Math.ceil(percent * 100);
}

/**
 * Simulate's a 0 to 100 percent increase. Use logarithmic increase so percentage
 * increases decelerate at the end, accounting for edge cases where a process
 * might take the full targetTime. targetTime is the average case, not
 * the max case.
 */
function simulateProgressIncrease(
  message: string,
  targetTime: number, // milliseconds
) {
  return (dispatch: Dispatch) => {
    let timeElapsed = 0;
    const progressBar = setInterval(() => {
      const percent = getPercent(timeElapsed, targetTime);
      if (percent >= 100) {
        clearInterval(progressBar);
        return;
      }
      dispatch(
        increaseUploadProgress({
          message,
          percent,
          status: UI_PROGRESS_STATUSES.IN_PROGRESS,
        }),
      );
      timeElapsed += 1000;
    }, 1000);
    return progressBar;
  };
}

/**
 * Upload a video to the content currently being edited
 * @param file Video File object to pass to an upload request
 * TODO - just like images, move video upload to its own redux module
 */
export function uploadAndTranscodeTrainingVideo(file: File) {
  return async (dispatch: Dispatch) => {
    try {
      // Upload video to s3
      // We start our progress bar here, before the first async call involved in uploading
      dispatch(
        increaseUploadProgress({
          message: 'Uploading video',
          percent: 1,
          status: UI_PROGRESS_STATUSES.IN_PROGRESS,
        }),
      );
      // Get limited time signed url
      const {
        uploadUrl,
        contentType,
        fileName: generatedFileName,
      } = await contentApi.getSignedS3VideoUploadUrl(file.name);

      // Send the file to that signed url
      await contentApi.uploadFileToS3(
        uploadUrl,
        file,
        contentType,
        progress => {
          const percent = getPercent(progress.loaded, progress.total);
          dispatch(
            increaseUploadProgress({
              message: 'Uploading video',
              percent,
              status: UI_PROGRESS_STATUSES.IN_PROGRESS,
            }),
          );
        },
      );

      // Kick off the transcode, and wait until its done
      const transcodingJob: TranscodeJob =
        await contentApi.transcodeUploadedFile(generatedFileName);
      // Now that transcoding has started, update the form to
      // contain the new video properties.
      dispatch(
        updateContentFormVideo({
          videoURI: transcodingJob.videoUri,
          previewImageURI: transcodingJob.thumbnailUri,
          transcodeId: transcodingJob.id,
        }),
      );
      const intervalId = dispatch(
        simulateProgressIncrease(
          'Processing video, this may take up to 3 minutes...',
          VIDEO_TRANSCODER_TYPICAL_TIME,
        ),
      );
      await dispatch(trackTranscodingProgress(transcodingJob.id));
      clearInterval(intervalId);
      dispatch(
        increaseUploadProgress({
          message: 'Uploading video',
          percent: 0,
          status: UI_PROGRESS_STATUSES.COMPLETE,
        }),
      );
      // removes success message from UI after 3 seconds
      setTimeout(() => dispatch(resetUploadProgress()), 3000);
    } catch (err) {
      dispatch(resetUploadProgress());
      dispatch(
        applicationActions.createNotification({
          message: 'There was a problem uploading your video.',
          title: 'Oops.',
          onDismiss: () => {},
        }),
      );
    }
  };
}

/**
 * Upload a single image file, wait for it to process, then return the image object
 * @param file Image File object to pass to an upload request
 * TODO - All image upload functionality could be moved to its own redux module
 */
function uploadAndProcessImage(file: File) {
  return async (): Promise<ImageEntry> => {
    const {
      uploadUrl,
      fileName,
      contentType,
    } = await contentApi.getSignedS3ImageUploadUrl(file.name);

    await contentApi.uploadFileToS3(uploadUrl, file, contentType);

    // Process the image to generate multiple sizes
    const newImage = await contentApi.processImage(fileName);
    return newImage;
  };
}

/**
 * Given a requested upload count of images, determine if the user can upload them.
 * If they can upload some of them, ask them if they want to continue.
 * If they can upload all of them, let them proceed without a dialog.
 */
function validateAndConfirmImageCount(selectedCount: number) {
  /**
   * @return resolve to number of selected images to upload
   */
  return async (dispatch: Dispatch, getState: GetState): Promise<number> => {
    const confirmPromise = new Promise(resolve => {
      const currentImageCount = selectImages(getState()).length;
      // no confirmation necessary, within the limit
      if (currentImageCount + selectedCount <= CONTENT_IMAGES_MAX_ALLOWED) {
        return resolve(selectedCount);
      }

      // the number of remaining image slots available
      const allowedCount = _.clamp(
        selectedCount,
        0,
        Math.abs(CONTENT_IMAGES_MAX_ALLOWED - currentImageCount),
      );

      // ask if they want to fill those remaining slots, but leave the others behind
      dispatch(
        applicationActions.createNotification({
          message: `A maximum of ${CONTENT_IMAGES_MAX_ALLOWED} images may be included. Only the first ${allowedCount} of ${selectedCount} selected files will be added.`,
          title: 'Too many images',
          dismissText: 'Cancel',
          confirmText: 'Continue',
          onDismiss: () => {
            resolve(0);
          },
          onConfirm: () => {
            resolve(allowedCount);
          },
        }),
      );

      return null;
    });
    return confirmPromise;
  };
}

/**
 * Serially process each image the user wants to upload
 */
export function uploadAndProcessImages(selectedFiles: File[]) {
  return async (dispatch: Dispatch, getState: GetState) => {
    // determine if/how many images can be uploaded
    const countToUpload: number = await dispatch(
      validateAndConfirmImageCount(selectedFiles.length),
    );
    const filesToUpload: File[] = selectedFiles.slice(0, countToUpload);
    if (countToUpload === 0) {
      return null;
    }

    const intervalId = dispatch(
      simulateProgressIncrease(
        'Processing image',
        countToUpload * IMAGE_PROCESSING_LAMBDA_TYPICAL_TIME,
      ),
    );

    // upload and process each image
    const promises = [];
    filesToUpload.forEach(file => {
      promises.push(dispatch(uploadAndProcessImage(file)));
    });
    try {
      // wait for all uploads + processing to complete
      const newImages = await Promise.all(promises);
      const existingImages = selectImages(getState());
      const updatedImages = [...existingImages, ...newImages];
      dispatch(change('content', 'body.images', updatedImages));
      // all done, set completed status (ui shows some success text)
      dispatch(
        increaseUploadProgress({
          message: 'Processing image',
          percent: 0,
          status: UI_PROGRESS_STATUSES.COMPLETE,
        }),
      );
      // reset to not report an upload status anymore
      clearInterval(intervalId);
      setTimeout(() => dispatch(resetUploadProgress()), 3000);
    } catch (err) {
      clearInterval(intervalId);
      dispatch(resetUploadProgress());
      dispatch(
        applicationActions.createNotification({
          message:
            'There was a problem adding one or more of the supplied images.',
          title: 'Oops.',
          onDismiss: () => {},
        }),
      );
    }

    return promises;
  };
}

/**
 * A content item needs to be saved with an ID in order to attach an activity, using
 * the ActivityAdd form which exists at `/content/:contentId/activity/add`.
 * In order to visit this route, we must save the contentItem before navigating.
 */
export function navigateToActivityFormForContentItem(
  existingContentId: number,
  activityType: string,
  activityId: number,
) {
  return async (dispatch: Dispatch, getState: GetState) => {
    let contentId;
    /**
     * If this content item has already been persisted to database, no need to save before loading
     * activity form.
     */
    if (!existingContentId) {
      const contentItem = await dispatch(
        saveContentForm(getFormValues('content')(getState())),
      );
      contentId = contentItem.id;
    }
    contentId = contentId || existingContentId;
    if (activityId) {
      dispatch(
        push(
          getRouteLink(routes.CONTENT_ACTIVITY_EDIT, {
            contentId,
            activityType,
            activityId,
          }),
        ),
      );
    } else {
      dispatch(
        push(
          getRouteLink(routes.CONTENT_ACTIVITY_ADD, {
            contentId,
            activityType,
          }),
        ),
      );
    }
  };
}

/**
 * Change from an Image content type to a Video or vice versa.
 * This handles the confirmation dialogs and subsequent updates to the redux
 * form fields that reflect the users choice.
 */
export function changeMedia(toContentType: ContentType) {
  return async (dispatch: Dispatch, getState: GetState) => {
    if (toContentType === CONTENT_TYPES.TrainingVideo) {
      // Switching to Video, determine if we need to show the dialog
      const hasImages: boolean = selectImages(getState()).length > 0;

      const switchToVideo = () => {
        dispatch(change('content', 'body.images', null));
        dispatch(change('content', 'body.type', CONTENT_TYPES.TrainingVideo));
      };

      if (hasImages) {
        dispatch(
          applicationActions.createNotification({
            message:
              'A post can contain images or video, but not both. Would you like to remove the images?',
            title: 'Change Media?',
            onConfirm: switchToVideo,
          }),
        );
      } else {
        switchToVideo();
      }
    } else if (toContentType === CONTENT_TYPES.Image) {
      // Switching to Image, determine if we need to show the dialog
      const hasVideo: boolean = !!selectVideo(getState());

      const switchToImage = () => {
        dispatch(change('content', 'body.video', null));
        dispatch(change('content', 'body.type', CONTENT_TYPES.Image));
      };

      if (hasVideo) {
        dispatch(
          applicationActions.createNotification({
            message:
              'A post can contain images or video, but not both. Would you like to remove the video?',
            title: 'Change Media?',
            onConfirm: switchToImage,
          }),
        );
      } else {
        switchToImage();
      }
    }
  };
}
