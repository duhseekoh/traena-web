// @flow
import {
  CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS,
  CONTENT_ITEM_RESET_UPLOAD_PROGRESS,
  CONTENT_ITEM_FORM_CLEAR,
  uiLoadingActionTypes,
} from './actions';
import type { ProgressUpdate } from '../../../types/index';
import type { Action } from '../../rootReducer';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';

import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

export type ReduxStateUiContentForm = {
  progress: ProgressUpdate,
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiContentForm = {
  progress: {
    message: '',
    percent: 0,
    status: null,
  },
  uiLoading: initialUiLoadingState,
};

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_CONTENT_FORM,
);


function uiContentForm(
  state: ReduxStateUiContentForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS: {
      return {
        ...state,
        progress: payload,
      };
    }

    case CONTENT_ITEM_RESET_UPLOAD_PROGRESS: {
      return {
        ...state,
        progress: {
          percent: 0,
          message: '',
          status: null,
        },
      };
    }

    case CONTENT_ITEM_FORM_CLEAR: {
      return {
        ...initialState,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiContentForm;
