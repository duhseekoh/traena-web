// TODO ADD FLOW
import { createSelector } from 'reselect';
import { formValueSelector } from 'redux-form';
import type { ReduxState } from '../../rootReducer';
import * as fromAuth from '../auth/selectors';
import * as fromContent from '../content/selectors';
import * as permissionUtils from '../../../utils/permissionUtils';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  selectIsLoading,
  selectErrorLoading,
  selectIsSaving,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_CONTENT_FORM);
/**
 * currently editing content id is stored as part of redux-form state
 */
export const selectContentId = (state: ReduxState) =>
  formValueSelector('content')(state, 'id');
export const selectImages = (state: ReduxState) =>
  formValueSelector('content')(state, 'body.images') || [];
export const selectVideo = (state: ReduxState) =>
  formValueSelector('content')(state, 'body.video');
export const selectProgress = (state: ReduxState) =>
  state.uiContentForm.progress;

export const selectIsReadOnly = createSelector(
  [fromAuth.selectUser, fromContent.selectContentById, selectContentId],
  (user, contentById, contentId) => {
    const contentItem = contentById[contentId];
    if (!contentItem) {
      return false;
    }
    return !permissionUtils.canModifyContent(
      { authorId: contentItem.author.id },
      user,
    );
  },
);
