import reducer from './reducer';
import { initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import {
  CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS,
  CONTENT_ITEM_RESET_UPLOAD_PROGRESS,
  CONTENT_ITEM_FORM_CLEAR,
  uiLoadingActionTypes,
} from './actions';

describe('ui-content-form reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      progress: {
        message: '',
        percent: 0,
        status: null,
      },
      uiLoading: initialUiLoadingState,

    });
  });

  it('should handle CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENT_ITEM_INCREASE_UPLOAD_PROGRESS,
          payload: {
            percent: 10,
            message: 'Uploading Image',
            status: 'IN PROGRESS',
          },
        },
      ),
    ).toEqual({
      progress: {
        percent: 10,
        message: 'Uploading Image',
        status: 'IN PROGRESS',
      },
    });
  });

  it('should handle CONTENT_ITEM_RESET_UPLOAD_PROGRESS', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENT_ITEM_RESET_UPLOAD_PROGRESS,
        },
      ),
    ).toEqual({
      progress: { percent: 0, message: '', status: null },
    });
  });

  it('should handle CONTENT_ITEM_FORM_CLEAR', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENT_ITEM_FORM_CLEAR,
        },
      ),
    ).toEqual({
      progress: {
        message: '',
        percent: 0,
        status: null,
      },
      uiLoading: initialUiLoadingState,
    });
  });

  it('should handle CONTENT_ITEM_SAVING', () => {
    expect(
      reducer(
        {},
        {
          type: uiLoadingActionTypes.setSaving,
          payload: { saving: true },
        },
      ),
    ).toEqual({
      uiLoading: {
        loading: false,
        saving: true,
        errorLoading: false,
        errorSaving: false,
      },
    });
  });
});
