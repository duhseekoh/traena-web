// @flow
import * as channelsApi from '../../api/channels';
import * as uiContentTableActions from '../ui-content-table/actions';
import * as fromUiChannelContentTable from './selectors';
import type { IndexedPage } from '../../../types';
import type { Dispatch, GetState } from '../../rootReducer';
import { createSortActions } from '../../../utils/redux-sort/actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const CHANNEL_CONTENT_LOAD_SUCCESS = 'CHANNEL_CONTENT/LOAD_SUCCESS';
export function loadChannelContentTableSuccess(
  channelId: number,
  content: IndexedPage<any>,
) {
  return {
    type: CHANNEL_CONTENT_LOAD_SUCCESS,
    payload: { channelId, content },
  };
}

export const CHANNEL_CONTENT_SET_PAGE = 'CHANNEL_CONTENT_SET_PAGE';
export function setChannelContentPage(page: number) {
  return {
    type: CHANNEL_CONTENT_SET_PAGE,
    payload: page,
  };
}

export const CHANNEL_CONTENT_SET_LOADING = 'CHANNEL_CONTENT_SET_LOADING';
export function setChannelContentLoading(loading: boolean) {
  return {
    type: CHANNEL_CONTENT_SET_LOADING,
    payload: loading,
  };
}

export const CHANNEL_CONTENT_LOAD_FAILURE = 'CHANNEL_CONTENT/LOAD_FAILURE';
function loadChannelContentTableFailure(err: string) {
  return {
    type: CHANNEL_CONTENT_LOAD_FAILURE,
    payload: err,
  };
}

export const {
  setSort,
  sortActionTypes,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.UI_CHANNEL_CONTENT_TABLE, {
  sortableProperties: [
    'modifiedTimestamp',
    'publishedTimestamp',
    'channel.name',
    'body.type',
    'body.title',
    'likeCount',
    'completedCount',
    'commentCount',
    'author.firstName',
  ],
});


export function setChannelContentTableSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setChannelContentPage(0));
    dispatch(setSort(property));
  };
}

export function initializeChannelContentTable(channelId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    try {
      dispatch(setChannelContentLoading(true));
      const page = fromUiChannelContentTable.selectCurrentPage(getState());
      const sort = fromUiChannelContentTable.selectSortQueryString(getState());

      const channelContent =
        await channelsApi.getContentForChannel(channelId, { page, sort });
      dispatch(uiContentTableActions.contentsFetchSuccess(channelContent));
      dispatch(loadChannelContentTableSuccess(channelId, channelContent));
      dispatch(setChannelContentLoading(false));
    } catch (err) {
      dispatch(setChannelContentLoading(false));
      dispatch(loadChannelContentTableFailure('There was a problem loading content'));
    }
  };
}
