import reducer from './reducer';

describe('ui-channel-content-table reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      contentIds: [],
      channelId: null,
      pagination: {
        totalElements: 0,
        size: 0,
        number: 0,
        totalPages: 0,
        numberOfElements: 0,
        first: false,
        last: false,
      },
      loading: false,
      error: null,
      sort: {
        orders: [{
          direction: 'desc',
          property: 'publishedTimestamp',
        }],
      },
    });
  });
});
