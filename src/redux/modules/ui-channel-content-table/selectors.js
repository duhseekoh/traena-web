// @flow
import * as fromContent from '../content/selectors';
import type { ReduxState } from '../../rootReducer';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const selectPagination = (state: ReduxState) =>
  state.uiChannelContentTable.pagination;
export const selectCurrentPage = (state: ReduxState) =>
  selectPagination(state).number;
export const selectError = (state: ReduxState) =>
  state.uiChannelContentTable.error;
export const selectContentItems = (state: ReduxState) => {
  const { contentIds } = state.uiChannelContentTable;
  return contentIds.map(id =>
    fromContent.selectContentById(state)[id]);
};
export const selectLoading = (state: ReduxState) =>
  state.uiChannelContentTable.loading;

export const {
  getSortDirection,
  getSortDirectionLongName,
  selectSortQueryString,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.UI_CHANNEL_CONTENT_TABLE);
