// @flow
import {
  CHANNEL_CONTENT_LOAD_SUCCESS,
  CHANNEL_CONTENT_SET_PAGE,
  CHANNEL_CONTENT_SET_LOADING,
  CHANNEL_CONTENT_LOAD_FAILURE,
  sortActionTypes,
} from './actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import { createSortReducer } from '../../../utils/redux-sort/reducer';
import type { Action } from '../../rootReducer';
import type { Pagination } from '../../../types';
import type { Sort } from '../../../utils/redux-sort/types';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';

export type ReduxStateUiChannelContentTable = {
  contentIds: number[],
  pagination: Pagination,
  channelId: ?number,
  loading: boolean,
  error: ?string,
  sort: Sort,
};

const initialState: ReduxStateUiChannelContentTable = {
  contentIds: [],
  channelId: null,
  pagination: {
    totalElements: 0,
    size: 0,
    number: 0,
    totalPages: 0,
    numberOfElements: 0,
    first: false,
    last: false,
  },
  loading: false,
  error: null,
  sort: {
    orders: [{
      direction: SORT_DIRECTIONS.DESCENDING,
      property: 'publishedTimestamp',
    }],
  },
};

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.UI_CHANNEL_CONTENT_TABLE,
);

function uiChannelContentTable(
  state: ReduxStateUiChannelContentTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case CHANNEL_CONTENT_LOAD_SUCCESS: {
      const { content, channelId } = payload;
      const { ids, index, ...pagination } = content;
      return {
        ...state,
        pagination,
        contentIds: ids,
        channelId,
      };
    }

    case CHANNEL_CONTENT_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case CHANNEL_CONTENT_SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case CHANNEL_CONTENT_LOAD_FAILURE:
      return {
        ...state,
        error: payload,
      };

    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiChannelContentTable;
