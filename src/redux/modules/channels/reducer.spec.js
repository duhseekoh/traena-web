import reducer from './reducer';
import { USER_SUBSCRIPTIONS_LOAD_SUCCESS } from './actions';
/* eslint-disable */

describe('channels reducer', () => {
  const payload = {
    userId: 1,
    subscriptions: {
      1: {
        id: 1,
        name: 'Traena Internal Channel',
        description: 'Default internal channel for Traena',
        organizationId: 1,
        visibility: 'INTERNAL',
        default: true,
        createdTimestamp: '2017-12-20T17:10:40.759Z',
        modifiedTimestamp: '2017-12-20T17:10:40.759Z',
      },
      2: {
        id: 2,
        name: 'Pandera Systems Internal Channel',
        description: 'Default internal channel for Pandera Systems',
        organizationId: 2,
        visibility: 'INTERNAL',
        default: true,
        createdTimestamp: '2017-12-20T17:10:40.703Z',
        modifiedTimestamp: '2017-12-20T17:10:40.703Z',
      },
      10: {
        id: 10,
        name: 'Alice Heiman Public Channel',
        description: 'Default public channel for Alice Heiman',
        organizationId: 10,
        visibility: 'MARKET',
        default: true,
        createdTimestamp: '2017-12-20T17:10:40.678Z',
        modifiedTimestamp: '2017-12-20T17:10:40.678Z',
      },
      12: {
        id: 12,
        name: 'Strelmark Public Channel',
        description: 'Default public channel for Strelmark',
        organizationId: 12,
        visibility: 'MARKET',
        default: true,
        createdTimestamp: '2017-12-20T17:10:40.690Z',
        modifiedTimestamp: '2017-12-20T17:10:40.690Z',
      },
    },
  };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      channels: {},
      subscriptionsByUserId: {},
      channelsByOrganizationId: {},
    });
  });

  it('should handle USER_SUBSCRIPTIONS_LOAD_SUCCESS', () => {
    expect(
      reducer(
        {},
        {
          type: USER_SUBSCRIPTIONS_LOAD_SUCCESS,
          payload,
        },
      ),
    ).toEqual({
      channels: {
        1: {
          id: 1,
          name: 'Traena Internal Channel',
          description: 'Default internal channel for Traena',
          organizationId: 1,
          visibility: 'INTERNAL',
          default: true,
          createdTimestamp: '2017-12-20T17:10:40.759Z',
          modifiedTimestamp: '2017-12-20T17:10:40.759Z',
        },
        2: {
          id: 2,
          name: 'Pandera Systems Internal Channel',
          description: 'Default internal channel for Pandera Systems',
          organizationId: 2,
          visibility: 'INTERNAL',
          default: true,
          createdTimestamp: '2017-12-20T17:10:40.703Z',
          modifiedTimestamp: '2017-12-20T17:10:40.703Z',
        },
        10: {
          id: 10,
          name: 'Alice Heiman Public Channel',
          description: 'Default public channel for Alice Heiman',
          organizationId: 10,
          visibility: 'MARKET',
          default: true,
          createdTimestamp: '2017-12-20T17:10:40.678Z',
          modifiedTimestamp: '2017-12-20T17:10:40.678Z',
        },
        12: {
          id: 12,
          name: 'Strelmark Public Channel',
          description: 'Default public channel for Strelmark',
          organizationId: 12,
          visibility: 'MARKET',
          default: true,
          createdTimestamp: '2017-12-20T17:10:40.690Z',
          modifiedTimestamp: '2017-12-20T17:10:40.690Z',
        },
      },
      subscriptionsByUserId: {
        1: ['1', '2', '10', '12'],
      },
    });
  });
});
