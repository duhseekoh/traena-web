// @flow
import * as channelsApi from '../../api/channels';
import type { Dispatch, GetState } from '../../rootReducer';
import type { Channel } from '../../../types/channels';
import * as fromAuth from '../auth/selectors';

export const USER_SUBSCRIPTIONS_LOAD_SUCCESS =
  'USER_SUBSCRIPTIONS/LOAD_SUCCESS';
function loadUserSubscriptionsSuccess(
  userId: number,
  subscriptions: Channel[],
) {
  return {
    type: USER_SUBSCRIPTIONS_LOAD_SUCCESS,
    payload: { userId, subscriptions },
  };
}

export const ORG_CHANNELS_LOAD_SUCCESS = 'ORG_CHANNELS/LOAD_SUCCESS';
export function loadOrganizationChannelsSuccess(
  organizationId: number,
  channels: Channel[],
) {
  return {
    type: ORG_CHANNELS_LOAD_SUCCESS,
    payload: { organizationId, channels },
  };
}

export const CHANNEL_LOAD_SUCCESS = 'CHANNEL/LOAD_SUCCESS';
function loadChannelSuccess(channel: Channel) {
  return {
    type: CHANNEL_LOAD_SUCCESS,
    payload: channel,
  };
}

export const CHANNEL_DELETE_SUCCESS = 'CHANNEL/DELETE_SUCCESS';
function deleteChannelSuccess(channelId: number) {
  return {
    type: CHANNEL_DELETE_SUCCESS,
    payload: channelId,
  };
}

export function loadChannel(channelId: number) {
  return async (dispatch: Dispatch) => {
    const channel = await channelsApi.getChannel(channelId);
    dispatch(loadChannelSuccess(channel));
  };
}

export function createChannel(channel: Channel) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (!organizationId) {
      throw new Error('Couldn\'t find your organization. Try logging in again.');
    }
    channel.organizationId = organizationId;
    const createdChannel = await channelsApi.createChannel(
      channel,
      organizationId,
    );
    dispatch(loadChannelSuccess(createdChannel));
    return createdChannel;
  };
}

export function editChannel(channel: Channel) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (!organizationId) {
      throw new Error('Couldn\'t find your organization. Try logging in again.');
    }
    channel.organizationId = organizationId;
    const editedChannel = await channelsApi.editChannel(
      channel,
      organizationId,
    );
    dispatch(loadChannelSuccess(editedChannel));
    return editedChannel;
  };
}

/**
 * Fetches all channels to which a given user is subscribed.
 * @param userId ID of the user for which we want to load channel subscriptions
 */
export function loadUserSubscriptions(userId: number) {
  return async (dispatch: Dispatch) => {
    const subscriptions = await channelsApi.getUserSubscriptions(userId);
    dispatch(loadUserSubscriptionsSuccess(userId, subscriptions.index));
  };
}

export function loadOrganizationChannels() {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (organizationId) {
      const channels = await channelsApi
        .getOrganizationChannels(organizationId);
      dispatch(loadOrganizationChannelsSuccess(organizationId, channels.index));
    } else {
      throw new Error('Couldn\'t find organizationId for logged in user');
    }
  };
}

/**
 * Fetches all channels to which the logged in user is subscribed.
 */
export function loadSelfSubscriptions() {
  return async (dispatch: Dispatch, getState: GetState) => {
    const userId = fromAuth.selectTraenaId(getState());
    if (!userId) {
      throw new Error('Couldn\'t find logged in user.  Log in again.');
    }
    const subscriptions = await channelsApi.getSelfSubscribedChannels();
    dispatch(loadUserSubscriptionsSuccess(userId, subscriptions.index));
  };
}

export function deleteChannel(channelId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    await channelsApi.deleteChannel(channelId, organizationId);
    dispatch(deleteChannelSuccess(channelId));
  };
}
