// @flow
import {
  USER_SUBSCRIPTIONS_LOAD_SUCCESS,
  ORG_CHANNELS_LOAD_SUCCESS,
  CHANNEL_LOAD_SUCCESS,
  CHANNEL_DELETE_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Channel } from '../../../types/channels';

export type ReduxStateChannels = {
  subscriptionsByUserId: {
    [userId: number]: number[],
  },
  channelsByOrganizationId: {
    [organizationId: number]: number[]
  },
  channels: {
    [channelId: number]: Channel,
  },
};

const initialState = {
  subscriptionsByUserId: {},
  channelsByOrganizationId: {},
  channels: {},
};

function channelsReducer(
  state: ReduxStateChannels = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case USER_SUBSCRIPTIONS_LOAD_SUCCESS: {
      const { subscriptions, userId } = payload;
      return {
        ...state,
        channels: {
          ...state.channels,
          ...subscriptions,
        },
        subscriptionsByUserId: {
          ...state.subscriptionsByUserId,
          [userId]: Object.keys(subscriptions),
        },
      };
    }

    case ORG_CHANNELS_LOAD_SUCCESS: {
      const { channels, organizationId } = payload;
      return {
        ...state,
        channels: {
          ...state.channels,
          ...channels,
        },
        channelsByOrganizationId: {
          ...state.channelsByOrganizationId,
          [organizationId]: Object.keys(channels),
        },
      };
    }

    case CHANNEL_LOAD_SUCCESS: {
      const channel = payload;
      return {
        ...state,
        channels: {
          ...state.channels,
          [channel.id]: channel,
        },
      };
    }

    case CHANNEL_DELETE_SUCCESS: {
      const channelId = payload;
      return {
        ...state,
        channels: {
          ...state.channels,
          [channelId]: undefined,
        },
      };
    }

    default:
      return state;
  }
}

export default channelsReducer;
