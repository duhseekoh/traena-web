// @flow
import { createSelector } from 'reselect';
import type { ReduxState } from '../../rootReducer';
import * as fromAuth from '../auth/selectors';

const selectSubscriptionsByUserId = (state: ReduxState) =>
  state.channels.subscriptionsByUserId;
const selectChannelsByOrganizationId = (state: ReduxState) =>
  state.channels.channelsByOrganizationId;
export const selectChannelsById =
  (state: ReduxState) => state.channels.channels;

export const selectChannelById = (state: ReduxState, channelId: number) =>
  selectChannelsById(state)[channelId];

export const selectSelfInternalSubscriptions = createSelector(
  [
    selectSubscriptionsByUserId,
    selectChannelsById,
    fromAuth.selectTraenaId,
    fromAuth.selectOrganizationId,
  ],
  (subscriptionsByUserId, channelsById, selfUserId, selfOrganizationId) => {
    if (!selfUserId || !subscriptionsByUserId[selfUserId]) {
      return [];
    }
    const subscribedChannelIds = subscriptionsByUserId[selfUserId];
    const subscribedChannels = subscribedChannelIds.map(
      id => channelsById[id],
    );
    return subscribedChannels.filter(
      subscription => subscription.organizationId === selfOrganizationId,
    );
  },
);

export const selectOrganizationOwnedChannels = createSelector(
  [fromAuth.selectOrganizationId,
    selectChannelsByOrganizationId,
    selectChannelsById,
  ],
  (organizationId, channelsByOrganizationId, channelsById) => {
    if (!organizationId || !channelsByOrganizationId[organizationId]) {
      return [];
    }
    const orgChannelIds = channelsByOrganizationId[organizationId];
    const orgChannels = orgChannelIds.map(
      id => channelsById[id],
    );
    return orgChannels.filter(
      channel => channel.organizationId === organizationId,
    );
  },
);

type Option = { text: string, value: number }
export const selectOrganizationOwnedChannelsAsInputOptions =
  (state: ReduxState): Option[] =>
    selectOrganizationOwnedChannels(state).map(channel => ({
      text: channel.name,
      value: channel.id,
    }));

export const selectOrganizationDefaultChannelId = createSelector(
  [selectOrganizationOwnedChannels],
  organizationOwnedChannels => {
    const defaultChannel = organizationOwnedChannels.filter(
      channel => channel.default === true,
    )[0];
    // default channel, use that
    if (defaultChannel) {
      return defaultChannel.id;
    }
    // no default, so use the first in the list
    if (organizationOwnedChannels.length > 0) {
      return organizationOwnedChannels[0].id;
    }
    // no channels!
    return null;
  },
);

export const selectSelfInternalSubscriptionsAsInputOptions =
  (state: ReduxState) =>
    selectSelfInternalSubscriptions(state).map(channel => ({
      text: channel.name,
      value: channel.id,
    }));

export const selectSelfInternalDefaultSubscriptionId = createSelector(
  [selectSelfInternalSubscriptions],
  selfInternalSubscriptions => {
    const defaultSubscription = selfInternalSubscriptions.filter(
      subscription => subscription.default === true,
    )[0];
    // default channel, use that
    if (defaultSubscription) {
      return defaultSubscription.id;
    }
    // no default, so use the first in the list
    if (selfInternalSubscriptions.length > 0) {
      return selfInternalSubscriptions[0].id;
    }
    // no channels subscribed to!
    return null;
  },
);
