// @flow
import type { ReduxState } from '../../rootReducer';

/* eslint-disable import/prefer-default-export */
export const selectModalStatus = (state: ReduxState) =>
  state.resetPassword.status;
