import * as authActions from '../auth/actions';

export const RESET_PASSWORD_MODAL_LOADING = 'RESET_PASSWORD_MODAL/LOADING';
export const RESET_PASSWORD_MODAL_SUCCESS = 'RESET_PASSWORD_MODAL/SUCCESS';
export const RESET_PASSWORD_MODAL_FAIL = 'RESET_PASSWORD_MODAL/FAIL';
export const RESET_PASSWORD_MODAL_RESET = 'RESET_PASSWORD_MODAL/RESET';

function resetPasswordModalLoading() {
  return {
    type: RESET_PASSWORD_MODAL_LOADING,
  };
}

function resetPasswordModalSuccess() {
  return {
    type: RESET_PASSWORD_MODAL_SUCCESS,
  };
}

function resetPasswordModalFail() {
  return {
    type: RESET_PASSWORD_MODAL_FAIL,
  };
}

/*
* Sets reset password redux module back to initial state
*/
export function resetPasswordModalReset() {
  return {
    type: RESET_PASSWORD_MODAL_RESET,
  };
}

export function sendResetPasswordEmail(email) {
  return async dispatch => {
    dispatch(resetPasswordModalLoading());
    try {
      await authActions.sendResetPasswordEmail(email);
      dispatch(resetPasswordModalSuccess());
    } catch (err) {
      dispatch(resetPasswordModalFail());
    }
  };
}
