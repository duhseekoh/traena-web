import { UI_PROGRESS_STATUSES, type UiProgressStatus } from '../../../constants/appConstants';

import {
  RESET_PASSWORD_MODAL_LOADING,
  RESET_PASSWORD_MODAL_SUCCESS,
  RESET_PASSWORD_MODAL_FAIL,
  RESET_PASSWORD_MODAL_RESET,
} from './actions';

export type ReduxStateResetPassword = {
  status: ?UiProgressStatus,
}

const initialState = {
  status: null,
};

// eslint-disable-next-line
function resetPassword(state = initialState, { type, payload }) {
  switch (type) {
    case RESET_PASSWORD_MODAL_LOADING: {
      return {
        ...state,
        status: UI_PROGRESS_STATUSES.IN_PROGRESS,
      };
    }

    case RESET_PASSWORD_MODAL_SUCCESS: {
      return {
        ...state,
        status: UI_PROGRESS_STATUSES.COMPLETE,
      };
    }

    case RESET_PASSWORD_MODAL_FAIL: {
      return {
        ...state,
        status: UI_PROGRESS_STATUSES.ERROR,
      };
    }

    case RESET_PASSWORD_MODAL_RESET: {
      return initialState;
    }

    default:
      return state;
  }
}

export default resetPassword;
