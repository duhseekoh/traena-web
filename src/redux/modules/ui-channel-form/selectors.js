// @flow
import { formValueSelector } from 'redux-form';
import type { ReduxState } from '../../rootReducer';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const selectDeleting = (state: ReduxState) =>
  state.uiChannelForm.deleting;
export const selectChannelNameLength = (state: ReduxState) => {
  const channelNameVal = formValueSelector('channel')(state, 'name');
  if (!channelNameVal) {
    return 0;
  }
  return channelNameVal.length;
};

export const {
  selectIsLoading,
  selectIsSaving,
  selectErrorLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_CHANNEL_FORM);
