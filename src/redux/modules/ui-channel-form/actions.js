// @flow
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as channelsActions from '../channels/actions';
import * as applicationActions from '../application/actions';
import type { Channel } from '../../../types/channels';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
  setErrorLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_CHANNEL_FORM);

export const CHANNEL_FORM_CLEAR = 'CHANNEL_FORM_CLEAR';
export const CHANNEL_FORM_DELETING = 'CHANNEL_FORM_DELETING';


function channelFormDeleting(deleting: boolean) {
  return {
    type: CHANNEL_FORM_DELETING,
    payload: deleting,
  };
}

export function initializeChannelFormData(channelId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      if (channelId) {
        await dispatch(channelsActions.loadChannel(channelId));
      }
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        applicationActions.createNotification({
          message:
            'There was an issue with trying to create or edit that channel.  Try again.',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function saveChannel(channel: Channel) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSaving(true));
      let persistedChannel;
      if (channel.id) {
        persistedChannel = await dispatch(channelsActions.editChannel(channel));
      } else {
        persistedChannel =
          await dispatch(channelsActions.createChannel(channel));
      }
      dispatch(
        applicationActions.createNotification({
          message:
            `Your channel was successfully ${channel.id ? 'edited' : 'created'}!`,
          title: 'Success!',
          onDismiss: () => {
            dispatch(push(getRouteLink(routes.SUBSCRIPTION_DETAILS, {
              channelId: persistedChannel.id,
            })));
          },
        }),
      );
      dispatch(setSaving(false));
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(
        applicationActions.createNotification({
          message:
            'There was an issue with trying to create or edit that channel.  Try again.',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function deleteChannelConfirm(channelId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(channelFormDeleting(true));
      await dispatch(channelsActions.deleteChannel(channelId));
      dispatch(channelFormDeleting(false));
      dispatch(applicationActions.createNotification({
        message: 'The channel was deleted successfully.',
        title: 'Success!',
        onDismiss: () => dispatch(push(getRouteLink(routes.CHANNELS))),
      }));
    } catch (err) {
      dispatch(channelFormDeleting(false));
      dispatch(applicationActions.createNotification({
        message: 'There was an issue deleting this channel. Make sure no content is in it before deleting.',
        title: 'Try again!',
        onDismiss: () => { },
      }));
    }
  };
}

export function deleteChannel(channelId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(applicationActions.createNotification({
      message: 'Deleting this channel cannot be undone, and people will no longer be able to view its content. Any draft posts in this channel will also be deleted.  You will still be able to see who has completed content in this channel.', // eslint-disable-line
      title: 'Are you sure?',
      onDismiss: () => {},
      onConfirm: () => dispatch(deleteChannelConfirm(channelId)),
      dismissText: 'No',
      confirmText: 'Yes',
    }));
  };
}
