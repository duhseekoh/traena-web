// @flow
import {
  CHANNEL_FORM_CLEAR,
  CHANNEL_FORM_DELETING,
  uiLoadingActionTypes,
} from './actions';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

import type { Action } from '../../rootReducer';

export type ReduxStateUiChannelForm = {
  uiLoading: ReduxStateUiLoading,
  deleting: boolean,
};

const initialState: ReduxStateUiChannelForm = {
  uiLoading: initialUiLoadingState,
  deleting: false,
};

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_CHANNEL_FORM,
);

function uiChannelForm(
  state: ReduxStateUiChannelForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CHANNEL_FORM_CLEAR: {
      return {
        ...initialState,
      };
    }

    case CHANNEL_FORM_DELETING: {
      return {
        ...state,
        deleting: payload,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
    case uiLoadingActionTypes.setErrorSaving:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiChannelForm;
