// @flow
import type { ReduxState } from '../../rootReducer';

export const selectAggregateStatsIsLoading = (state: ReduxState) =>
  state.contentItemAnalyticsStats.loading;
export const selectAggregateStatsError = (state: ReduxState) =>
  state.contentItemAnalyticsStats.error;
export const selectAggregateStats = (state: ReduxState) =>
  state.contentItemAnalyticsStats.aggregateStats;
