// @flow
import * as contentApi from '../../api/content';
import type { ContentItemAggregateStats } from '../../../types/analytics';

export const CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCHING = 'CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCHING';
export const CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_ERROR = 'CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_ERROR';
export const CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_SUCCESS = 'CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_SUCCESS';

function aggregateStatsFetchError(err: string) {
  return {
    type: CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_ERROR,
    payload: err,
  };
}

function aggregateStatsFetching() {
  return {
    type: CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCHING,
  };
}

function aggregateStatsFetchSuccess(stats: ContentItemAggregateStats) {
  return {
    type: CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_SUCCESS,
    payload: stats,
  };
}

export function getAggregateStats(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(aggregateStatsFetching());
    try {
      const resp = await contentApi.getAggregateStatsForContentItem(contentId);
      dispatch(aggregateStatsFetchSuccess(resp));
    } catch (err) {
      dispatch(aggregateStatsFetchError(err));
    }
  };
}
