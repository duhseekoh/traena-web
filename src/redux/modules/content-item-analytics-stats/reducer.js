// @flow
import {
  CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCHING,
  CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_ERROR,
  CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { ContentItemAggregateStats } from '../../../types/analytics';

export type ReduxStateContentItemAnalyticsStats = {
  loading: boolean,
  error: ?string,
  aggregateStats: ?ContentItemAggregateStats,
};

const initialState: ReduxStateContentItemAnalyticsStats = {
  loading: false,
  error: null,
  aggregateStats: null,
};

function contentItemAnalytics(
  state: ReduxStateContentItemAnalyticsStats = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCHING:
      return {
        ...state,
        loading: true,
        error: null,
        aggregateStats: null,
      };

    case CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
        aggregateStats: null,
      };

    case CONTENT_ITEM_ANALYTICS_AGGREGATE_STATS_FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        aggregateStats: payload,
      };

    default:
      return state;
  }
}

export default contentItemAnalytics;
