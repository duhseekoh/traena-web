export const FIXED_FOOTER_HEIGHT_UPDATE =
  'FIXED_FOOTER/HEIGHT_UPDATE';
export const updateFooterHeight = (
  footerHeight: number,
) => ({
  type: FIXED_FOOTER_HEIGHT_UPDATE,
  payload: {
    footerHeight,
  },
});
