// @flow
import type { ReduxState } from '../../rootReducer';

export const selectFooterHeight = (state: ReduxState) => // eslint-disable-line
  state.uiFixedFooter.footerHeight;
