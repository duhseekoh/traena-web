// @flow
import {
  FIXED_FOOTER_HEIGHT_UPDATE,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiFixedFooter = {
  footerHeight: number,
};

const initialState = {
  footerHeight: 0,
};

function uiFixedFooter(
  state: ReduxStateUiFixedFooter = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case FIXED_FOOTER_HEIGHT_UPDATE: {
      const { footerHeight } = payload;
      return {
        ...state,
        footerHeight,
      };
    }

    default:
      return state;
  }
}

export default uiFixedFooter;
