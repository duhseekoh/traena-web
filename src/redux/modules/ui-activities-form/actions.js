// @flow
import _ from 'lodash';
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as activityActions from '../activities/actions';
import * as applicationActions from '../application/actions';
import { ACTIVITY_TYPES } from '../../../constants/activities';
import type { Activity, PollActivity } from '../../../types/activities';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
  setErrorLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_ACTIVITIES_FORM);

export const ACTIVITY_FORM_CLEAR = 'ACTIVITY_FORM/CLEAR';

export function clearActivityForm() {
  return {
    type: ACTIVITY_FORM_CLEAR,
  };
}

function saveActivitySuccess(contentId) {
  return async (dispatch: Dispatch) => {
    dispatch(
      applicationActions.createNotification({
        message: 'Your activity was successfully created!',
        title: 'Success!',
        onDismiss: () => {
          dispatch(
            push(
              getRouteLink(routes.CONTENT_EDIT, {
                contentId,
              }),
            ),
          );
        },
      }),
    );
  };
}

function convertFormValsToActivity(activityFormVals: Activity) {
  const activity = _.pick(activityFormVals, [
    'body',
    'contentId',
    'id',
    'type',
  ]);
  return activity;
}

function _savePollActivity(activity: PollActivity) {
  return async (dispatch: Dispatch) => {
    const { contentId } = activity;
    if (activity.id) {
      dispatch(
        applicationActions.createNotification({
          message: 'By saving these changes to your activity, your current poll results will be lost. To view new results, select the View Results link.', // eslint-disable-line
          title: 'Saving Poll Activity',
          onDismiss: () => {},
          onConfirm: async () => {
            await dispatch(activityActions.saveActivity(
              contentId,
              convertFormValsToActivity(activity)),
            );
            dispatch(push(getRouteLink(routes.CONTENT_EDIT, { contentId })));
          },
        }),
      );
    } else {
      await dispatch(activityActions.saveActivity(
        contentId,
        convertFormValsToActivity(activity)),
      );
      dispatch(push(getRouteLink(routes.CONTENT_EDIT, { contentId })));
    }
  };
}

export function saveActivity(activityFormVals: Activity) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSaving(true));
      const { contentId } = activityFormVals;
      if (activityFormVals.type === ACTIVITY_TYPES.QUESTION_SET) {
        await dispatch(activityActions.saveActivity(
          contentId,
          convertFormValsToActivity(activityFormVals)),
        );
      } else if (activityFormVals.type === ACTIVITY_TYPES.POLL) {
        await dispatch(_savePollActivity(activityFormVals));
      }
      dispatch(saveActivitySuccess(contentId));
      dispatch(setSaving(false));
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(
        applicationActions.createNotification({
          message:
            'There was an issue with trying to create or edit that activity.  Try again.',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function initializeActivityFormData(
  contentId: number,
  activityId?: number,
) {
  return async (dispatch: Dispatch) => {
    const initializationMethods = [];
    try {
      dispatch(setLoading(true));
      if (activityId) {
        initializationMethods.push(
          dispatch(activityActions.loadActivity(contentId, activityId)),
        );
      }
      await Promise.all(initializationMethods);
      dispatch(setLoading(false));
    } catch (error) {
      dispatch(setLoading(false));
      dispatch(setErrorLoading());

      dispatch(
        applicationActions.createNotification({
          message:
            'There was an issue trying to load this activity for editing.  Try again.',
          title: 'Oops!',
          onDismiss: () => {
            dispatch(
              push(
                getRouteLink(routes.CONTENT_EDIT, {
                  contentId,
                }),
              ),
            );
          },
        }),
      );
    }
  };
}

export function removeActivityFromContentItem(
  contentId: number,
  activityId: number,
) {
  return async (dispatch: Dispatch) => {
    try {
      // if the remove button is clicked, but we have not yet saved the activity, we
      // navigate back to the edit content page.  Otherwise, we prompt the user if they are sure
      // they want to delete
      if (activityId) {
        dispatch(
          applicationActions.createNotification({
            message: 'Deleting this activity cannot be undone, and you will no longer be able to view its results. You will still be able to see who has completed the activity.', // eslint-disable-line
            title: 'Are you sure?',
            onDismiss: () => {},
            onConfirm: async () => {
              await dispatch(
                activityActions.deleteActivity(contentId, activityId),
              );
              dispatch(push(getRouteLink(routes.CONTENT_EDIT, { contentId })));
            },
            dismissText: 'No',
            confirmText: 'Yes',
          }),
        );
      } else {
        dispatch(push(getRouteLink(routes.CONTENT_EDIT, { contentId })));
      }
    } catch (error) {
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to delete this activity.',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function navigateToContentForm(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(
      applicationActions.createNotification({
        message: "You may lose progress if you haven't saved your changes.",
        title: 'Remember to save!',
        onDismiss: () => {},
        onConfirm: async () => {
          dispatch(push(getRouteLink(routes.CONTENT_EDIT, { contentId })));
        },
      }),
    );
  };
}
