// @flow
import {
  ACTIVITY_FORM_CLEAR,
  uiLoadingActionTypes,
} from './actions';
import type { Action } from '../../rootReducer';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_ACTIVITIES_FORM,
);

export type ReduxStateUiActivitiesForm = {
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiActivitiesForm = {
  uiLoading: initialUiLoadingState,
};

function uiActivitiesForm(
  state: ReduxStateUiActivitiesForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case ACTIVITY_FORM_CLEAR: {
      return {
        ...initialState,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiActivitiesForm;
