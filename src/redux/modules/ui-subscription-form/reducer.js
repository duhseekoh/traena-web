// @flow
import {
  SUBSCRIPTION_FORM_SET_EDITING,
  uiLoadingActionTypes,
} from './actions';
import type { Action } from '../../rootReducer';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_FORM,
);
export type ReduxStateUiSubscriptionForm = {
  uiLoading: ReduxStateUiLoading,
  editing: boolean,
};

const initialState: ReduxStateUiSubscriptionForm = {
  uiLoading: initialUiLoadingState,
  editing: false,
};

function uiSubscriptionForm(
  state: ReduxStateUiSubscriptionForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SUBSCRIPTION_FORM_SET_EDITING: {
      return {
        ...state,
        editing: payload,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiSubscriptionForm;
