// @flow
import { formValueSelector } from 'redux-form';
import type { ReduxState } from '../../rootReducer';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  selectIsLoading,
  selectIsSaving,
  selectErrorLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_FORM);

export const selectEditing = (state: ReduxState) =>
  state.uiSubscriptionForm.editing;
export const selectChannelId = (state: ReduxState) =>
  formValueSelector('subscription')(state, 'channelId');
export const selectSubscribedUserIds = (state: ReduxState) =>
  formValueSelector('subscription')(state, 'subscribedUserIds') || [];
