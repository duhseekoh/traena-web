// @flow
import { change, formValueSelector } from 'redux-form';
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as applicationActions from '../application/actions';
import * as subscriptionsActions from '../subscriptions/actions';
import type { Dispatch, GetState } from '../../rootReducer';
import type { Subscription } from '../../../types/subscriptions';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_FORM);

export const SUBSCRIPTION_FORM_SET_EDITING = 'SUBSCRIPTION_FORM_SET_EDITING';
export function setEditing(editing: boolean) {
  return {
    type: SUBSCRIPTION_FORM_SET_EDITING,
    payload: editing,
  };
}

export function initializeSubscriptionForm(channelId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      await dispatch(subscriptionsActions.loadSubscription(channelId));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue trying to load this channel data. Please try again.',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function saveSubscription(subscription: Subscription) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSaving(true));
      await dispatch(subscriptionsActions.saveSubscription(subscription));
      dispatch(
        applicationActions.createNotification({
          message: 'This channels subscriptions have been changed.',
          title: 'Success!',
          onDismiss: () => {
            dispatch(push(getRouteLink(
              routes.SUBSCRIPTION_DETAILS,
              { channelId: subscription.channelId },
            )));
          },
        }),
      );
      dispatch(setSaving(false));
      dispatch(setEditing(false));
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(setEditing(false));
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to save these subscriptions. Try again',
          title: 'Oops!',
          onDismiss: () => {},
        }),
      );
    }
  };
}

export function addUser(userId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const subscribedUserIds = formValueSelector('subscription')(getState(), 'subscribedUserIds') || [];
    if (subscribedUserIds.includes(userId)) {
      return;
    }
    dispatch(change('subscription', 'subscribedUserIds', [
      ...subscribedUserIds,
      userId,
    ]));
  };
}

export function removeUser(userId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const subscribedUserIds = formValueSelector('subscription')(getState(), 'subscribedUserIds')
      .filter(id => id !== userId) || [];
    dispatch(change('subscription', 'subscribedUserIds', subscribedUserIds));
  };
}
