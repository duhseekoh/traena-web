// @flow
import type { ReduxState } from '../../rootReducer';

export const selectActivities = (state: ReduxState) =>
  state.activities.activitiesById;

export const selectActivityById = (state: ReduxState, activityId: number) =>
  state.activities.activitiesById[activityId];

export const selectActivitiesByContentId = (
  state: ReduxState,
  contentId: ?number,
) => {
  if (!contentId || !state.activities.activitiesByContentId[contentId]) {
    return [];
  }
  const activityIds = state.activities.activitiesByContentId[contentId];
  const activities = activityIds.map(id => state.activities.activitiesById[id]);
  return activities;
};
