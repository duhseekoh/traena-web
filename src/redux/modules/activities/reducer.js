// @flow
import {
  ACTIVITIES_LOAD_SUCCESS,
  ACTIVITY_LOAD_SUCCESS,
  ACTIVITY_DELETE_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Activity } from '../../../types/activities';

export type ReduxStateActivities = {
  activitiesByContentId: {
    [contentId: number]: number[],
  },
  activitiesById: {
    [activityId: number]: Activity[],
  },
};

const initialState = {
  activitiesByContentId: {},
  activitiesById: {},
};

function activitiesReducer(
  state: ReduxStateActivities = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case ACTIVITIES_LOAD_SUCCESS: {
      const activities = payload;
      if (!Object.keys(activities.index).length) {
        return state;
      }
      const { contentId } = activities.index[Object.keys(activities.index)[0]];
      return {
        ...state,
        activitiesById: {
          ...state.activitiesById,
          ...activities.index,
        },
        activitiesByContentId: {
          ...state.activitiesByContentId,
          [contentId]: Object.keys(activities.index).map(key => Number(key)),
        },
      };
    }

    case ACTIVITY_LOAD_SUCCESS: {
      const activity = payload;
      return {
        ...state,
        activitiesById: {
          ...state.activitiesById,
          [activity.id]: activity,
        },
        activitiesByContentId: {
          ...state.activitiesByContentId,
          [activity.contentId]: [
            ...(state.activitiesByContentId[activity.contentId] || []),
            activity.id,
          ],
        },
      };
    }

    case ACTIVITY_DELETE_SUCCESS: {
      const { activityId, contentId } = payload;
      const newState = { ...state };
      delete newState.activitiesById[activityId];
      newState.activitiesByContentId[contentId] = state.activitiesByContentId[
        contentId
      ].filter(id => id !== activityId);
      return newState;
    }

    default:
      return state;
  }
}

export default activitiesReducer;
