// @flow
import type { Activity } from '../../../types/activities';
import * as activitiesApi from '../../api/activities';
/**
 * Adds an activity in state/attaches it to the proper content item
 */
export const ACTIVITY_LOAD_SUCCESS = 'ACTIVITY/LOAD_SUCCESS';
function loadActivitySuccess(activity: Activity) {
  return {
    type: ACTIVITY_LOAD_SUCCESS,
    payload: activity,
  };
}

/**
 * Used to load a set of activities for a contentItem.  Replaces all activities currently in state
 * for that contentItem
 */
export const ACTIVITIES_LOAD_SUCCESS = 'ACTIVITIES/LOAD_SUCCESS';
function loadActivitiesSuccess(activities: Activity[]) {
  return {
    type: ACTIVITIES_LOAD_SUCCESS,
    payload: activities,
  };
}

export const ACTIVITY_DELETE_SUCCESS = 'ACTIVITY/DELETE_SUCCESS';
function deleteActivitySuccess(deletedActivity: {
  contentId: number,
  activityId: number,
}) {
  return {
    type: ACTIVITY_DELETE_SUCCESS,
    payload: deletedActivity,
  };
}


export function saveActivity(contentId: number, activity: Activity) {
  return async (dispatch: Dispatch) => {
    let persistedActivity;
    if (activity.id) {
      persistedActivity = await activitiesApi.updateActivity(
        contentId,
        activity,
      );
    } else {
      persistedActivity = await activitiesApi.createActivity(
        contentId,
        activity,
      );
    }
    dispatch(loadActivitySuccess(persistedActivity));
  };
}

export function loadContentActivities(contentId: number) {
  return async (dispatch: Dispatch) => {
    const activities = await activitiesApi.getContentActivities(contentId);
    dispatch(loadActivitiesSuccess(activities));
  };
}

export function loadActivity(contentId: number, activityId: number) {
  return async (dispatch: Dispatch) => {
    const activity = await activitiesApi.getActivity(contentId, activityId);
    dispatch(loadActivitySuccess(activity));
  };
}

export function deleteActivity(contentId: number, activityId: number) {
  return async (dispatch: Dispatch) => {
    await activitiesApi.deleteActivity(contentId, activityId);
    dispatch(deleteActivitySuccess({ contentId, activityId }));
  };
}
