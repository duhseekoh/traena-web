// @flow
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as channelsActions from '../channels/actions';
import * as applicationActions from '../application/actions';
import * as subscriptionsActions from '../subscriptions/actions';

export function initializeSubscriptionDetails(channelId: number) { // eslint-disable-line
  return async (dispatch: Dispatch) => {
    try {
      const initMethods = [
        dispatch(channelsActions.loadChannel(channelId)),
        dispatch(subscriptionsActions.loadSubscription(channelId)),
      ];
      await Promise.all(initMethods);
    } catch (err) {
      dispatch(
        applicationActions.createNotification({
          message:
            'There was an error loading details for this channel',
          title: 'Error!',
          onDismiss: () => {
            dispatch(push(getRouteLink(routes.CHANNELS)));
          },
        }),
      );
    }
  };
}
