// @flow
import type { ReduxState } from '../../rootReducer';

export const selectAggregateStatsIsLoading = (state: ReduxState) =>
  state.seriesStats.loading;
export const selectAggregateStatsError = (state: ReduxState) =>
  state.seriesStats.error;
export const selectAggregateStats = (state: ReduxState) =>
  state.seriesStats.aggregateStats;
