// @flow
import * as seriesApi from '../../api/series';
import type { SeriesAggregateStats } from '../../../types/analytics';

export const SERIES_AGGREGATE_STATS_FETCHING = 'SERIES_AGGREGATE_STATS_FETCHING';
export const SERIES_AGGREGATE_STATS_FETCH_ERROR = 'SERIES_AGGREGATE_STATS_FETCH_ERROR';
export const SERIES_AGGREGATE_STATS_FETCH_SUCCESS = 'SERIES_AGGREGATE_STATS_FETCH_SUCCESS';

function aggregateStatsFetchError(err: string) {
  return {
    type: SERIES_AGGREGATE_STATS_FETCH_ERROR,
    payload: err,
  };
}

function aggregateStatsFetching() {
  return {
    type: SERIES_AGGREGATE_STATS_FETCHING,
  };
}

function aggregateStatsFetchSuccess(stats: SeriesAggregateStats) {
  return {
    type: SERIES_AGGREGATE_STATS_FETCH_SUCCESS,
    payload: stats,
  };
}

export function getAggregateStats(seriesId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(aggregateStatsFetching());
    try {
      const resp = await seriesApi.getAggregateStatsForSeries(seriesId);
      dispatch(aggregateStatsFetchSuccess(resp));
    } catch (err) {
      dispatch(aggregateStatsFetchError(err));
    }
  };
}
