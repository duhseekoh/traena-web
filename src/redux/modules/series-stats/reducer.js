// @flow
import {
  SERIES_AGGREGATE_STATS_FETCHING,
  SERIES_AGGREGATE_STATS_FETCH_ERROR,
  SERIES_AGGREGATE_STATS_FETCH_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { SeriesAggregateStats } from '../../../types/analytics';

export type ReduxStateSeriesStats = {
  loading: boolean,
  error: ?string,
  aggregateStats: ?SeriesAggregateStats,
};

const initialState: ReduxStateSeriesStats = {
  loading: false,
  error: null,
  aggregateStats: null,
};

function contentItemAnalytics(
  state: ReduxStateSeriesStats = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SERIES_AGGREGATE_STATS_FETCHING:
      return {
        ...state,
        loading: true,
        error: null,
        aggregateStats: null,
      };

    case SERIES_AGGREGATE_STATS_FETCH_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
        aggregateStats: null,
      };

    case SERIES_AGGREGATE_STATS_FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        aggregateStats: payload,
      };

    default:
      return state;
  }
}

export default contentItemAnalytics;
