// @flow
import { createSelector } from 'reselect';
import * as fromUiUsersTable from '../ui-users-table/selectors';
import type { ReduxState } from '../../rootReducer';

export const selectPagination = (state: ReduxState) =>
  state.uiSubscribedUsersTable.pagination;

export const selectTotalElements = (state: ReduxState) =>
  selectPagination(state).totalElements;

export const selectCurrentPage = (state: ReduxState) =>
  state.uiSubscribedUsersTable.pagination.number;

const selectCurrentItemIds = (state: ReduxState) =>
  state.uiSubscribedUsersTable.currentItemIds || [];

export const selectIsSubscribedUsersTableLoading = (state: ReduxState) =>
  state.uiSubscribedUsersTable.loading;

export const selectSubscribedUsersTableError = (state: ReduxState) =>
  state.uiSubscribedUsersTable.error;

export const selectUsers = createSelector(
  [fromUiUsersTable.selectUsersById, selectCurrentItemIds],
  (usersById, currentItemIds) =>
    currentItemIds
      .map(id => usersById[id])
      .filter(o => o !== undefined),
);
