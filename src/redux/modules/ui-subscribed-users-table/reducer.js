// @flow
import {
  SUBSCRIBED_USERS_FETCH_SUCCESS,
  SUBSCRIBED_USERS_SET_PAGE,
  SUBSCRIBED_USERS_SET_LOADING,
  SUBSCRIBED_USERS_FETCH_FAILURE,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiSubscribedUsersTable = {
  currentItemIds: number[],
  pagination: {
    number: number,
    totalElements: number,
  },
  loading: boolean,
  error: boolean,
};

const initialState: ReduxStateUiSubscribedUsersTable = {
  currentItemIds: [],
  pagination: {
    number: 0,
    totalElements: 0,
  },
  loading: false,
  error: false,
};

function uiSubscribedUsersTable(
  state: ReduxStateUiSubscribedUsersTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case SUBSCRIBED_USERS_FETCH_SUCCESS: {
      const { ids, ...pagination } = payload;
      return {
        ...state,
        pagination,
        currentItemIds: ids,
      };
    }
    case SUBSCRIBED_USERS_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case SUBSCRIBED_USERS_SET_LOADING:
      return {
        ...state,
        loading: payload,
        error: false,
      };

    case SUBSCRIBED_USERS_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default uiSubscribedUsersTable;
