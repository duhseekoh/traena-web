// @flow
import * as channelApi from '../../api/channels';
import * as fromAuth from '../auth/selectors';
import * as fromUiSubscribedUsersTable from './selectors';
import { usersFetchSuccess } from '../ui-users-table/actions';
import type { Dispatch, GetState } from '../../rootReducer';
import type { IndexedPage } from '../../../types';

export const SUBSCRIBED_USERS_FETCH_SUCCESS = 'SUBSCRIBED_USERS/FETCH_SUCCESS';
export const SUBSCRIBED_USERS_FETCH_FAILURE = 'SUBSCRIBED_USERS/FETCH_FAILURE';

export const SUBSCRIBED_USERS_SET_PAGE = 'SUBSCRIBED_USERS/SET_PAGE';

export const SUBSCRIBED_USERS_SET_LOADING = 'SUBSCRIBED_USERS/SET_LOADING';

export function subscribedUsersFetchSuccess(subscribedUsers: IndexedPage<any>) {
  return {
    type: SUBSCRIBED_USERS_FETCH_SUCCESS,
    payload: subscribedUsers,
  };
}

function subscribedUsersFetchFailure(err: string) {
  return {
    type: SUBSCRIBED_USERS_FETCH_FAILURE,
    payload: err,
  };
}

export function setSubscribedUsersPage(page: number) {
  return {
    type: SUBSCRIBED_USERS_SET_PAGE,
    payload: page,
  };
}

export function setSubscribedUsersLoading(loading: boolean) {
  return {
    type: SUBSCRIBED_USERS_SET_LOADING,
    payload: loading,
  };
}

export function fetchSubscribedUsersTable(channelId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    const page = fromUiSubscribedUsersTable.selectCurrentPage(getState());
    dispatch(setSubscribedUsersLoading(true));
    try {
      const subscribedUsers = await channelApi
        .getChannelSubscribedUsers(organizationId, channelId, page);
      dispatch(subscribedUsersFetchSuccess(subscribedUsers));
      dispatch(usersFetchSuccess(subscribedUsers));
      dispatch(setSubscribedUsersLoading(false));
    } catch (err) {
      dispatch(subscribedUsersFetchFailure(err));
    }
  };
}
