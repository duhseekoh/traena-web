// @flow
import {
  CONTENT_DETAIL_LOADING,
  INITIALIZE_CONTENT_DETAIL_FAILED,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiContentDetail = {
  errorInitializing: boolean,
  loading: boolean,
};

const initialState: ReduxStateUiContentDetail = {
  errorInitializing: false,
  loading: false,
};

function uiContentDetail(
  state: ReduxStateUiContentDetail = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_DETAIL_LOADING: {
      return {
        ...state,
        loading: payload,
      };
    }

    case INITIALIZE_CONTENT_DETAIL_FAILED: {
      return {
        ...state,
        errorInitializing: true,
      };
    }

    default:
      return state;
  }
}

export default uiContentDetail;
