// @flow
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as contentActions from '../content/actions';
import * as fromContent from '../content/selectors';
import * as applicationActions from '../application/actions';
import * as activitiesActions from '../activities/actions';
import type { Dispatch, GetState } from '../../rootReducer';

export const CONTENT_DETAIL_LOADING = 'CONTENT_DETAIL_LOADING';
export function setContentDetailLoading(loading: boolean) {
  return {
    type: CONTENT_DETAIL_LOADING,
    payload: loading,
  };
}
export const INITIALIZE_CONTENT_DETAIL_FAILED = 'INITIALIZE_CONTENT_DETAIL_FAILED';
function initializeContentDetailFailed() {
  return {
    type: INITIALIZE_CONTENT_DETAIL_FAILED,
    payload: undefined, // to satisfy flow
  };
}

export function initializeContentDetail(contentId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    try {
      dispatch(setContentDetailLoading(true));
      await dispatch(contentActions.fetchContentItem(contentId));
      const contentItem =
        fromContent.selectContentItemById(getState(), { contentId });
      if (contentItem.activityCount > 0) {
        await dispatch(activitiesActions.loadContentActivities(contentId));
      }
      dispatch(setContentDetailLoading(false));
    } catch (err) {
      dispatch(setContentDetailLoading(false));
      dispatch(initializeContentDetailFailed());
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to load this content.',
          title: 'Oops!',
          onDismiss: () => dispatch(
            push(
              getRouteLink(routes.CONTENT),
            ),
          ),
        }),
      );
    }
  };
}
