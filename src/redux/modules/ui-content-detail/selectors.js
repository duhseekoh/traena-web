// @flow
import type { ReduxState } from '../../rootReducer';

export const selectErrorInitializing = (state: ReduxState) =>
  state.uiContentDetail.errorInitializing;
export const selectLoading = (state: ReduxState) =>
  state.uiContentDetail.loading;
