// @flow
import type { ReduxState } from '../../rootReducer';

export const selectContentById = (state: ReduxState) =>
  state.content.contentById;

export const selectContentItemById = (state: ReduxState, props: any) =>
  state.content.contentById[props.contentId];

export const selectContentOrganizationByContentId = (
  state: ReduxState,
  props: any,
) => {
  const contentItem = selectContentItemById(state, props);
  if (!contentItem) {
    return null;
  }
  return contentItem.author.organization;
};
