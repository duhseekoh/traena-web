// @flow
import * as contentApi from '../../api/content';
import type { Dispatch } from '../../rootReducer';

export const CONTENT_ITEM_FETCH_SUCCESS = 'CONTENT_ITEM/FETCH_SUCCESS';
export const CONTENT_ITEM_FETCH_FAILURE = 'CONTENT_ITEM/FETCH_FAILURE';
export const CONTENT_ITEM_DELETE_SUCCESS = 'CONTENT_ITEM/DELETE_SUCCESS';
export const CONTENT_ITEM_DELETE_FAILURE = 'CONTENT_ITEM/DELETE_FAILURE';

function deleteContentItemSuccess(contentId: number) {
  return {
    type: CONTENT_ITEM_DELETE_SUCCESS,
    payload: contentId,
  };
}

function deleteContentItemFailure(err: string) {
  return {
    type: CONTENT_ITEM_DELETE_FAILURE,
    payload: err,
  };
}

function contentItemFetchSuccess(contentItem) {
  return {
    type: CONTENT_ITEM_FETCH_SUCCESS,
    payload: contentItem,
  };
}

function contentItemFetchFailure(err) {
  return {
    type: CONTENT_ITEM_FETCH_FAILURE,
    payload: err,
  };
}

export function fetchContentItem(contentId: number) {
  return async (dispatch: Dispatch) => {
    try {
      const contentItem = await contentApi.getContentItem(contentId);
      dispatch(contentItemFetchSuccess(contentItem));
      return contentItem;
    } catch (err) {
      dispatch(contentItemFetchFailure(err));
      throw new Error('Content item failed to fetch');
    }
  };
}

export function deleteContentItem(contentId: number) {
  return (dispatch: Dispatch) =>
    contentApi
      .deleteContentItem(contentId)
      .then(() => dispatch(deleteContentItemSuccess(contentId)))
      .catch(err => dispatch(deleteContentItemFailure(err)));
}
