import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import {
  deleteContentItem,
  CONTENT_ITEM_DELETE_SUCCESS,
  CONTENT_ITEM_DELETE_FAILURE,
} from './actions';
import { deleteContentItem as deleteContentItemApi } from '../../api/content';

jest.mock('../../api/content');

describe('content actions', () => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch CONTENT_ITEM_DELETE_FAILURE', () => {
    deleteContentItemApi.mockImplementationOnce(() => Promise.reject());

    store.dispatch(deleteContentItem(1)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual([{ type: CONTENT_ITEM_DELETE_FAILURE }]);
    });
  });

  it('should dispatch CONTENT_ITEM_DELETE_SUCCESS', () => {
    deleteContentItemApi.mockImplementationOnce(() => Promise.resolve());

    store.dispatch(deleteContentItem(1)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual([
        { type: CONTENT_ITEM_DELETE_SUCCESS, payload: 1 },
      ]);
    });
  });
});
