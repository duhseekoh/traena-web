// @flow
import _ from 'lodash';
import {
  CONTENTS_FETCH_SUCCESS,
  CONTENTS_FETCH_FAILURE,
} from '../ui-content-table/actions';
import { CONTENT_ITEM_SAVED } from '../ui-content-form/actions';
import {
  CONTENT_ITEM_DELETE_SUCCESS,
  CONTENT_ITEM_FETCH_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateContent = {
  contentFetchError: ?string,
  contentById: {
    [contentId: number]: Object, // TODO content flow type
  },
};

const initialState: ReduxStateContent = {
  contentFetchError: null,
  contentById: {},
};

function content(
  state: ReduxStateContent = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case CONTENTS_FETCH_SUCCESS:
      return Object.assign({}, state, {
        contentById: Object.assign({}, state.contentById, payload.index),
        contentFetchError: null,
      });

    case CONTENTS_FETCH_FAILURE:
      return Object.assign({}, state, {
        contentFetchError: payload,
      });

    case CONTENT_ITEM_SAVED:
      return {
        ...state,
        contentById: {
          ...state.contentById,
          [payload.id]: payload,
        },
      };

    case CONTENT_ITEM_FETCH_SUCCESS:
      return {
        ...state,
        contentById: {
          ...state.contentById,
          [payload.id]: payload,
        },
      };

    case CONTENT_ITEM_DELETE_SUCCESS:
      if (typeof payload !== 'number') {
        // satisfy flow
        return state;
      }
      return {
        ...state,
        contentById: _.omit(state.contentById, payload),
      };

    default:
      return state;
  }
}

export default content;
