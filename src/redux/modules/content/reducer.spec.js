import reducer from './reducer';
import {
  CONTENTS_FETCH_SUCCESS,
  CONTENTS_FETCH_FAILURE,
} from '../ui-content-table/actions';
import { CONTENT_ITEM_SAVED } from '../ui-content-form/actions';
import {
  CONTENT_ITEM_DELETE_SUCCESS,
  CONTENT_ITEM_DELETE_FAILURE,
} from './actions';

describe('auth reducer', () => {
  const payload = {
    number: 0,
    size: 2,
    sort: [
      {
        direction: 'desc',
        property: 'createdTimestamp',
      },
    ],
    totalElements: 15,
    totalPages: 2,
    first: true,
    last: false,
    ids: [134, 133],
    index: {
      133: {
        id: 133,
        body: {
          text:
            'Frenz United or in full Frenz United Football Club (FUFC) is a Malaysian football club located in Malacca City.',
          type: 'DailyAction',
          title: 'Frenz',
        },
      },
      134: {
        id: 134,
        body: {
          text:
            'Sable Chief was a Newfoundland dog that served as the mascot of the Royal Newfoundland Regiment during World War I.',
          type: 'DailyAction',
          title: 'Sable Chief',
        },
      },
    },
    numberOfElements: 2,
  };

  const contentById = {
    133: {
      id: 133,
      body: {
        text:
          'Frenz United or in full Frenz United Football Club (FUFC) is a Malaysian football club located in Malacca City.',
        type: 'DailyAction',
        title: 'Frenz',
      },
    },
    134: {
      id: 134,
      body: {
        text:
          'Sable Chief was a Newfoundland dog that served as the mascot of the Royal Newfoundland Regiment during World War I.',
        type: 'DailyAction',
        title: 'Sable Chief',
      },
    },
  };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      contentFetchError: null,
      contentById: {},
    });
  });

  it('should handle CONTENTS_FETCH_SUCCESS', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENTS_FETCH_SUCCESS,
          payload,
        },
      ),
    ).toEqual({ contentById, contentFetchError: null });
  });

  it('should handle CONTENTS_FETCH_FAILURE', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENTS_FETCH_FAILURE,
          payload: 'im an error',
        },
      ),
    ).toEqual({
      contentFetchError: 'im an error',
    });
  });

  it('should handle CONTENT_ITEM_SAVED', () => {
    const initialState = {
      id: 152,
      body: { text: 'initialtxt', type: 'DailyAction', title: 'Title' },
    };
    const itemPayload = {
      id: 155,
      body: { text: 'text', type: 'DailyAction', title: 'Title' },
    };

    expect(
      reducer(
        { contentById: { 152: initialState } },
        {
          type: CONTENT_ITEM_SAVED,
          payload: itemPayload,
        },
      ),
    ).toEqual({ contentById: { 155: itemPayload, 152: initialState } });
  });

  describe('CONTENT_ITEM_DELETE_SUCCESS', () => {
    const contentItem = {
      id: 133,
      body: {
        text:
          'Frenz United or in full Frenz United Football Club (FUFC) is a Malaysian football club located in Malacca City.',
        type: 'DailyAction',
        title: 'Frenz',
      },
    };

    it('should handle CONTENT_ITEM_DELETE_SUCCESS for valid contentId', () => {
      expect(
        reducer(
          { contentById, contentFetchError: null },
          {
            type: CONTENT_ITEM_DELETE_SUCCESS,
            payload: 134,
          },
        ),
      ).toEqual({ contentById: { 133: contentItem }, contentFetchError: null });
    });

    it('should handle CONTENT_ITEM_DELETE_FAILURE for invalid contentId', () => {
      expect(
        reducer(
          { contentById, contentFetchError: null },
          {
            type: CONTENT_ITEM_DELETE_FAILURE,
            payload: 'err',
          },
        ),
      ).toEqual({ contentById, contentFetchError: null });
    });
  });
});
