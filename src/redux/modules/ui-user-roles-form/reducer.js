// @flow
import {
  USER_ROLES_FORM_LOADING,
  USER_ROLES_FORM_SAVING,
  USER_ROLES_FORM_INITIALIZE_ERROR,
  AVAILABLE_ROLES_FETCH_COMPLETE,
} from './actions';

import type { Action } from '../../rootReducer';
import type { Role } from '../../../types/users';

export type ReduxStateUiUserRolesForm = {
  isSaving: boolean,
  isLoading: boolean,
  errorInitializing: boolean,
  availableRoles: Role[],
};

const initialState = {
  isLoading: false,
  isSaving: false,
  errorInitializing: false,
  availableRoles: [],
};

function uiUserRolesForm(
  state: ReduxStateUiUserRolesForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case USER_ROLES_FORM_LOADING: {
      return {
        ...state,
        isLoading: payload,
      };
    }

    case USER_ROLES_FORM_SAVING: {
      return {
        ...state,
        isSaving: payload,
      };
    }

    case USER_ROLES_FORM_INITIALIZE_ERROR: {
      return {
        ...state,
        errorInitializing: true,
      };
    }

    case AVAILABLE_ROLES_FETCH_COMPLETE: {
      return {
        ...state,
        availableRoles: payload,
      };
    }

    default:
      return state;
  }
}

export default uiUserRolesForm;
