import { change } from 'redux-form';

import * as userApi from '../../api/user';
import * as rolesApi from '../../api/roles';
import type { Dispatch } from '../../rootReducer';

export const USER_ROLES_FORM_LOADING = 'USER_ROLES_FORM/LOADING';
export const USER_ROLES_FORM_SAVING = 'USER_ROLES_FORM/SAVING';
export const USER_ROLES_FORM_INITIALIZE_ERROR =
  'USER_ROLES_FORM/INITIALIZE_ERROR';
export const AVAILABLE_ROLES_FETCH_COMPLETE = 'AVAILABLE_ROLES/FETCH_COMPLETE';
export const ROLES_ERROR = 'ROLES/ERROR';

function userRolesFormInitializeError(err: Object) {
  return {
    type: USER_ROLES_FORM_INITIALIZE_ERROR,
    payload: err,
  };
}

function userRolesFormLoading(isLoading: boolean) {
  return {
    type: USER_ROLES_FORM_LOADING,
    payload: isLoading,
  };
}

function userRolesFormSaving(isSaving: boolean) {
  return {
    type: USER_ROLES_FORM_SAVING,
    payload: isSaving,
  };
}
/**
 * Sets the available roles for a user into redux state, so
 * each available role can be rendered as Toggle input on the user roles form.
 * @param availableRoles List of all roles available (retrieved via GET /roles)
 */
function availableRolesFetchComplete(availableRoles: Array) {
  return {
    type: AVAILABLE_ROLES_FETCH_COMPLETE,
    payload: availableRoles,
  };
}

/**
 * This method sets the initial state of the userRoles reduxForm.
 * We use this method after the user's roles are fetched from the API, to load each role
 * into reduxForm's state using reduxForm's change actionCreator.
 * @param userRoles List of all roles assigned to the user (retrieved via GET /user/:userId/roles)
 */
function userRolesFetchComplete(userRoles: Array) {
  return (dispatch: Dispatch) => {
    userRoles.forEach(role => dispatch(change('userRoles', role.name, true)));
  };
}

/**
 * We use this method on mounting of the UserRolesForm to fetch the available
 * and assigned roles for a given userId
 * @param userId id of the user for which we want to load role information
 */
export function userRolesFormInitialize(userId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(userRolesFormLoading(true));
    Promise.all([
      await rolesApi.getAvailableRoles(),
      await userApi.getUserRoles(userId),
    ])
      .then(resultArray => {
        const availableRoles = resultArray[0];
        const userRoles = resultArray[1];
        dispatch(availableRolesFetchComplete(availableRoles));
        dispatch(userRolesFetchComplete(userRoles));
        dispatch(userRolesFormLoading(false));
      })
      .catch(err => {
        dispatch(userRolesFormInitializeError(err));
        dispatch(userRolesFormLoading(false));
      });
  };
}

function rolesError(role: Object, err: Object) {
  return {
    type: ROLES_ERROR,
    payload: err,
  };
}

/**
 * Called when a user role is toggled on in the UI.  We try to set the role
 * to the user via the API, and if our request fails we undo the change to our form
 * by dispatching a change action to reduxForm
 * @param userId id of user for which we tried to add a role
 * @param role role object (as defined by api) @todo improve this definition
 */
export function addUserRole(userId: number, role: Object) {
  return async (dispatch: Dispatch) => {
    dispatch(userRolesFormSaving(true));
    try {
      await userApi.addRoleToUser(userId, role);
      dispatch(userRolesFormSaving(false));
    } catch (err) {
      dispatch(rolesError(role, err));
      dispatch(change('userRoles', role.name, false));
      dispatch(userRolesFormSaving(false));
    }
  };
}

export function removeUserRole(userId: number, role: Object) {
  return async (dispatch: Dispatch) => {
    dispatch(userRolesFormSaving(true));
    try {
      await userApi.removeRoleFromUser(userId, role);
      dispatch(userRolesFormSaving(false));
    } catch (err) {
      dispatch(rolesError(role, err));
      dispatch(change('userRoles', role.name, true));
      dispatch(userRolesFormSaving(false));
    }
  };
}
