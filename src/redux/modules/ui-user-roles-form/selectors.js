export const selectIsSaving = state => state.uiUserRolesForm.isSaving;
export const selectIsLoading = state => state.uiUserRolesForm.isLoading;

export const selectAvailableRoles = state =>
  state.uiUserRolesForm.availableRoles;
