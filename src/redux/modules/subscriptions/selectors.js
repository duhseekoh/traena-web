// @flow
import { createSelector } from 'reselect';
import _ from 'lodash';
import type { ReduxState } from '../../rootReducer';
import { selectChannelsById } from '../channels/selectors';

const selectSubscriptionsById =
  (state: ReduxState) => state.subscriptions.subscriptions;

export const selectSubscription = (state: ReduxState, channelId: number) =>
  selectSubscriptionsById(state)[channelId];

export const selectSubscriptionOrganization = (
  state: ReduxState,
  channelId: number,
) => {
  const subscription = selectSubscription(state, channelId);
  if (!subscription || !subscription.channel) {
    return null;
  }
  return subscription.channel.organization;
};


export const selectSubscriptionOrgName = (
  state: ReduxState,
  channelId: number,
) => {
  const org = selectSubscriptionOrganization(state, channelId);
  if (!org) {
    return null;
  }
  return org.name;
};

export const selectSubscriptionsWithChannels = createSelector(
  [
    selectChannelsById,
    selectSubscriptionsById,
  ], (channels, subscriptions) =>
    _.mapValues(subscriptions, s => ({ ...s, channel: channels[s.channelId] })),
);
