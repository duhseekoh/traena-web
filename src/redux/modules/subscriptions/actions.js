import * as subscriptionsApi from '../../api/subscriptions';
import * as fromAuth from '../auth/selectors';

export const SUBSCRIPTION_LOAD_SUCCESS = 'SUBSCRIPTION_LOAD_SUCCESS';
export const SUBSCRIPTIONS_LOAD_SUCCESS = 'SUBSCRIPTIONS_LOAD_SUCCESS';
export const ORG_SUBSCRIPTIONS_LOAD_SUCCESS =
  'ORG_SUBSCRIPTIONS/LOAD_SUCCESS';

export function loadOrgSubscriptionsSuccess(
  subscriptions: Subscription[],
) {
  return {
    type: ORG_SUBSCRIPTIONS_LOAD_SUCCESS,
    payload: { subscriptions },
  };
}

function loadSubscriptionSuccess(subscription: Subscription) {
  return {
    type: SUBSCRIPTION_LOAD_SUCCESS,
    payload: subscription,
  };
}

export function loadSubscription(channelId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (!organizationId) {
      throw new Error('Couldn\'t find your organization. Try logging in again.');
    }
    const subscription = await subscriptionsApi.getSubscription(
      organizationId,
      channelId,
    );
    dispatch(loadSubscriptionSuccess(subscription));
  };
}

export function saveSubscription(subscription: Subscription) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (!organizationId) {
      throw new Error('Couldn\'t find your organization. Try logging in again.');
    }
    const updatedSubscription = await subscriptionsApi.updateSubscription(
      organizationId,
      subscription,
    );
    dispatch(loadSubscriptionSuccess(updatedSubscription));
  };
}
