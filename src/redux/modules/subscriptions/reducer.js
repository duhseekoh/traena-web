// @flow
import { CHANNEL_DELETE_SUCCESS } from '../channels/actions';
import {
  SUBSCRIPTION_LOAD_SUCCESS,
  ORG_SUBSCRIPTIONS_LOAD_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Subscription } from '../../../types/subscriptions';

export type ReduxStateSubscriptions = {
  subscriptions: {
    [channelId: number]: Subscription,
  },
};

const initialState = {
  subscriptions: {},
};

function subscriptionsReducer(
  state: ReduxStateSubscriptions = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case ORG_SUBSCRIPTIONS_LOAD_SUCCESS: {
      const { subscriptions } = payload;
      return {
        ...state,
        subscriptions: {
          ...state.subscriptions,
          ...subscriptions,
        },
      };
    }

    case SUBSCRIPTION_LOAD_SUCCESS: {
      return {
        ...state,
        subscriptions: {
          ...state.subscriptions,
          [payload.channelId]: {
            ...payload,
          },
        },
      };
    }

    case CHANNEL_DELETE_SUCCESS: {
      const channelId = payload;
      const newSubscriptions = { ...state.subscriptions };
      delete newSubscriptions[channelId];

      return {
        ...state,
        subscriptions: {
          ...newSubscriptions,
        },
      };
    }

    default:
      return state;
  }
}

export default subscriptionsReducer;
