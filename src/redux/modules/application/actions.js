// @flow
export const NOTIFICATIONS_CLEAR = 'NOTIFICATIONS_CLEAR';
export const NOTIFICATIONS_CREATE = 'NOTIFICATIONS_CREATE';

export function clearNotifications() {
  return {
    type: NOTIFICATIONS_CLEAR,
  };
}

export function createNotification(notification: Object) {
  return {
    type: NOTIFICATIONS_CREATE,
    payload: notification,
  };
}
