import type { ReduxState } from '../../rootReducer';

const selectNotification = (state: ReduxState) =>
  state.application.notification || {};

export const selectNotificationTitle = (state: ReduxState) =>
  selectNotification(state).title;

export const selectNotificationMessage = (state: ReduxState) =>
  selectNotification(state).message;

export const selectNotificationDismissText = (state: ReduxState) =>
  selectNotification(state).dismissText;

export const selectNotificationConfirmText = (state: ReduxState) =>
  selectNotification(state).confirmText;

export const selectNotificationOnConfirm = (state: ReduxState) =>
  selectNotification(state).onConfirm;

export const selectNotificationOnDismiss = (state: ReduxState) =>
  selectNotification(state).onDismiss;
