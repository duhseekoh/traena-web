import configureStore from 'redux-mock-store';
import {
  clearNotifications,
  createNotification,
  NOTIFICATIONS_CLEAR,
  NOTIFICATIONS_CREATE,
} from '../application/actions';

describe('application actions', () => {
  const middlewares = [];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch NOTIFICATIONS_CLEAR', () => {
    store.dispatch(clearNotifications());
    const actions = store.getActions();
    expect(actions).toEqual([{ type: NOTIFICATIONS_CLEAR }]);
  });

  it('should dispatch NOTIFICATIONS_CREATE with payload', () => {
    const payload = {
      message: 'You just published contentItem',
      title: 'Success!',
      onDismiss: () => {},
    };
    store.dispatch(createNotification(payload));
    const actions = store.getActions();
    expect(actions).toEqual([{ type: NOTIFICATIONS_CREATE, payload }]);
  });
});
