// @flow
import {
  NOTIFICATIONS_CLEAR,
  NOTIFICATIONS_CREATE,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Notification } from '../../../types/index';

export type ReduxStateApplication = {
  notification: ?Notification,
};

const initialState: ReduxStateApplication = {
  notification: null,
};

function application(
  state: ReduxStateApplication = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case NOTIFICATIONS_CLEAR: {
      return {
        ...state,
        notification: null,
      };
    }
    case NOTIFICATIONS_CREATE:
      return {
        ...state,
        notification: payload,
      };
    default:
      return state;
  }
}

export default application;
