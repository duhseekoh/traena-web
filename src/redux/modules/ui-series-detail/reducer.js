// @flow
import { uiLoadingActionTypes } from './actions';
import type { Action } from '../../rootReducer';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export type ReduxStateUiSeriesDetail = {
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiSeriesDetail = {
  uiLoading: initialUiLoadingState,
};

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_SERIES_DETAIL,
);

function uiSeriesDetail(
  state: ReduxStateUiSeriesDetail = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case uiLoadingActionTypes.setLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiSeriesDetail;
