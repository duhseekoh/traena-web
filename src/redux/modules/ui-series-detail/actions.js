// @flow
import { push } from 'connected-react-router';
import * as applicationActions from '../application/actions';
import * as seriesActions from '../series/actions';
import { routes, getRouteLink } from '../../../constants/routes';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';
import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  uiLoadingActionTypes,
  setLoading,
  setErrorLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_SERIES_DETAIL);

export const initialize = (seriesId: number) =>
  async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      await dispatch(seriesActions.loadSeries(seriesId));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(setErrorLoading());
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to load this series.',
          title: 'Oops!',
          onDismiss: () => dispatch(
            push(
              getRouteLink(routes.SERIES),
            ),
          ),
        }),
      );
    }
  };
