// @flow
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

import {
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const {
  selectIsLoading,
  selectErrorLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_SERIES_DETAIL);
