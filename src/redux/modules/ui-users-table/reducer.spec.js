import reducer from './reducer';
import {
  USERS_FETCH_SUCCESS,
  USERS_SET_PAGE,
  USERS_SET_LOADING,
} from './actions';

describe('ui-users-table reducer', () => {
  const payload = {
    number: 0,
    size: 10,
    sort: [
      {
        direction: 'desc',
        property: 'createdTimestamp',
      },
    ],
    totalElements: 1,
    totalPages: 1,
    first: true,
    last: false,
    ids: [82],
    index: {
      82: {
        id: 82,
        email: 'ed@traena.io',
        organizationId: 1,
        firstName: 'Ed',
        lastName: 'Siok',
        profileImageURI: null,
        isDisabled: true,
        position: 'Traena Admin',
        tags: ['t'],
        createdTimestamp: '2017-08-14T18:50:06.258Z',
        modifiedTimestamp: '2017-08-15T16:22:03.267Z',
      },
    },
    numberOfElements: 1,
  };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      currentItemIds: [],
      filters: {},
      pagination: {
        number: 0,
      },
      loading: false,
      sort: {
        orders: [{
          direction: 'asc',
          property: 'firstName',
        }],
      },
    });
  });

  it('should handle USERS_FETCH_SUCCESS', () => {
    const { ids, ...pagination } = payload;
    expect(
      reducer(
        {},
        {
          type: USERS_FETCH_SUCCESS,
          payload,
        },
      ),
    ).toEqual({
      currentItemIds: ids,
      pagination,
    });
  });

  it('should handle USERS_SET_PAGE', () => {
    expect(
      reducer(
        {},
        {
          type: USERS_SET_PAGE,
          payload: 0,
        },
      ),
    ).toEqual({ pagination: { number: 0 } });
  });

  it('should handle USERS_SET_LOADING', () => {
    expect(
      reducer(
        {},
        {
          type: USERS_SET_LOADING,
          payload: true,
        },
      ),
    ).toEqual({ loading: true });
  });
});
