// @flow
import { createSelector } from 'reselect';
import type { ReduxState } from '../../rootReducer';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';

export const selectIsUserTableLoading = (state: ReduxState) =>
  state.uiUsersTable.loading;
export const selectUsersById = (state: ReduxState) => state.users.usersById;
export const selectCurrentPage = (state: ReduxState) =>
  state.uiUsersTable.pagination.number;
const selectCurrentUserIds = (state: ReduxState) =>
  state.uiUsersTable.currentItemIds || [];

export const selectUsersForCurrentPage = createSelector(
  [selectUsersById, selectCurrentUserIds],
  (usersById, currentUserIds) => currentUserIds.map(id => usersById[id]),
);

export const {
  getSortDirection,
  getSortDirectionLongName,
  selectSortQueryString,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.UI_USERS_TABLE);
