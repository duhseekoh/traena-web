// @flow
import * as userApi from '../../api/user';
import type { UserPage } from '../../api/user';
import * as fromAuth from '../auth/selectors';
import type { Dispatch, GetState } from '../../rootReducer';
import { createSortActions } from '../../../utils/redux-sort/actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import * as fromUiUsersTable from './selectors';

export const USERS_FETCH_SUCCESS = 'USERS/FETCH_SUCCESS';
export const USERS_FETCH_FAILURE = 'USERS/FETCH_FAILURE';
export const USERS_SET_LOADING = 'USERS/SET_LOADING';
export const USERS_SET_PAGE = 'USERS/SET_PAGE';

export function usersFetchSuccess(userPage: UserPage) {
  return {
    type: USERS_FETCH_SUCCESS,
    payload: userPage,
  };
}

function usersFetchFailure(err: string) {
  return {
    type: USERS_FETCH_FAILURE,
    payload: err,
  };
}

export function setUsersLoading(loading: boolean) {
  return {
    type: USERS_SET_LOADING,
    payload: loading,
  };
}

export function setUsersPage(page: number) {
  return {
    type: USERS_SET_PAGE,
    payload: page,
  };
}

export const {
  setSort,
  sortActionTypes,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.UI_USERS_TABLE, {
  sortableProperties: [
    'firstName',
    'email',
    'position',
    'createdTimestamp',
  ],
});

export function setUsersTableSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setUsersPage(0));
    dispatch(setSort(property));
  };
}

export function fetchUsersTable() {
  return (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    const page = fromUiUsersTable.selectCurrentPage(getState());
    dispatch(setUsersLoading(true));
    if (organizationId) {
      // should never be null when requesting this, but satisfy flow
      const sort = fromUiUsersTable.selectSortQueryString(getState());
      userApi
        .getUsersForOrganization(organizationId, { page, sort })
        .then((userPage: UserPage) => {
          dispatch(usersFetchSuccess(userPage));
          dispatch(setUsersLoading(false));
        })
        .catch(err => {
          dispatch(setUsersLoading(false));
          dispatch(usersFetchFailure(err));
        });
    }
  };
}
