import thunkMiddleware from 'redux-thunk';
import configureStore from 'redux-mock-store';
import {
  setUsersLoading,
  USERS_SET_LOADING,
  setUsersPage,
  USERS_SET_PAGE,
} from './actions';

jest.mock('../../api/user');
jest.mock('../auth/selectors');
jest.mock('../ui-users-table/selectors');

describe('content actions', () => {
  const middlewares = [thunkMiddleware];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch USERS_SET_LOADING', () => {
    const payload = true;
    store.dispatch(setUsersLoading(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: USERS_SET_LOADING, payload }]);
  });

  it('should dispatch USERS_SET_PAGE', () => {
    const payload = 1;
    store.dispatch(setUsersPage(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: USERS_SET_PAGE, payload }]);
  });
});
