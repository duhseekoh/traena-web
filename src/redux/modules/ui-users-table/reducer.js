// @flow
import {
  USERS_FETCH_SUCCESS,
  USERS_SET_LOADING,
  USERS_SET_PAGE,
  sortActionTypes,
} from './actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import type { Action } from '../../rootReducer';
import { createSortReducer } from '../../../utils/redux-sort/reducer';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';
import type { Sort } from '../../../utils/redux-sort/types';

export type ReduxStateUiUsersTable = {
  currentItemIds: number[],
  filters: {},
  pagination: {
    number: number,
  },
  loading: boolean,
  sort: Sort,
};

const initialState: ReduxStateUiUsersTable = {
  currentItemIds: [],
  filters: {},
  pagination: {
    number: 0,
  },
  loading: false,
  sort: {
    orders: [{
      direction: SORT_DIRECTIONS.ASCENDING,
      property: 'firstName',
    }],
  },
};

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.UI_USERS_TABLE,
);

function uiUsersTable(
  state: ReduxStateUiUsersTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case USERS_FETCH_SUCCESS: {
      const { ids, ...pagination } = payload;
      return {
        ...state,
        pagination,
        currentItemIds: ids,
      };
    }

    case USERS_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case USERS_SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiUsersTable;
