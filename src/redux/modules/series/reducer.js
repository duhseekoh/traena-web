// @flow
import {
  SERIES_ITEM_LOAD_SUCCESS,
  SERIES_LOAD_SUCCESS,
  SERIES_DELETE_SUCCESS,
  SERIES_LOAD_CONTENT_IDS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Series } from '../../../types/series';

export type ReduxStateSeries = {
  seriesById: {
    [seriesId: number]: Series,
  }
};

const initialState = {
  seriesById: {},
};

function seriesReducer(
  state: ReduxStateSeries = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SERIES_ITEM_LOAD_SUCCESS: {
      return {
        ...state,
        seriesById: {
          ...state.seriesById,
          [payload.id]: {
            ...state.seriesById[payload.id],
            ...payload,
          },
        },
      };
    }

    case SERIES_LOAD_SUCCESS: {
      const series = payload;
      return {
        ...state,
        seriesById: {
          ...state.seriesById,
          ...series.index,
        },
      };
    }

    case SERIES_DELETE_SUCCESS: {
      const { seriesId } = payload;
      return {
        ...state,
        seriesById: {
          ...state.seriesById,
          [seriesId]: undefined,
        },
      };
    }

    case SERIES_LOAD_CONTENT_IDS: {
      const { seriesId, contentIds } = payload;
      return {
        ...state,
        seriesById: {
          ...state.seriesById,
          [seriesId]: {
            ...state.seriesById[seriesId],
            contentIds,
          },
        },
      };
    }

    default:
      return state;
  }
}

export default seriesReducer;
