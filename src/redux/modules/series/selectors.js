// @flow
import { formValueSelector } from 'redux-form';
import * as fromContent from '../content/selectors';
import type { ReduxState } from '../../rootReducer';

export const selectSeriesById = (state: ReduxState) => state.series.seriesById;

export const selectSeries = (state: ReduxState, seriesId: number) =>
  selectSeriesById(state)[seriesId];

export const selectSeriesSelectedContentFromReduxForm = (state: ReduxState) => {
  const contentIds = formValueSelector('series')(state, 'contentIds') || [];
  return contentIds.map(id => fromContent.selectContentById(state)[id]);
};
