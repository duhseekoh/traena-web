// @flow
import type { Series } from '../../../types/series';
import * as seriesApi from '../../api/series';
import * as uiContentTableActions from '../ui-content-table/actions';
import type { IndexedPage } from '../../../types';

/**
* Adds multiple series to state
*/
export const SERIES_LOAD_SUCCESS = 'SERIES/LOAD_SUCCESS';
function loadSeriesSuccess(series: IndexedPage<Series>) {
  return {
    type: SERIES_LOAD_SUCCESS,
    payload: series,
  };
}

/**
* Adds one series to state
*/
export const SERIES_ITEM_LOAD_SUCCESS = 'SERIES_ITEM/LOAD_SUCCESS';
function loadSeriesItemSuccess(series: Series) {
  return {
    type: SERIES_ITEM_LOAD_SUCCESS,
    payload: series,
  };
}

export const SERIES_DELETE_SUCCESS = 'SERIES/DELETE_SUCCESS';
function deleteSeriesSuccess(seriesId: number) {
  return {
    type: SERIES_DELETE_SUCCESS,
    payload: seriesId,
  };
}

export function saveSeries(series: Series) {
  return async (dispatch: Dispatch) => {
    let persistedSeries;
    if (series.id) {
      persistedSeries = await seriesApi.updateSeries(series);
    } else {
      persistedSeries = await seriesApi.createSeries(series);
    }
    dispatch(loadSeriesItemSuccess(persistedSeries));
  };
}

export const SERIES_LOAD_CONTENT_IDS = 'SERIES/LOAD_CONTENT_IDS';
export function loadSeriesContentIds(seriesId: number, contentIds: number[]) {
  return {
    type: SERIES_LOAD_CONTENT_IDS,
    payload: { seriesId, contentIds },
  };
}

export function loadContentForSeries(seriesId: number) {
  return async (dispatch: Dispatch) => {
    const content = await seriesApi.getContentForSeries(seriesId);
    dispatch(uiContentTableActions.contentsFetchSuccess(content));
    dispatch(loadSeriesContentIds(seriesId, content.ids));
    return content;
  };
}

export function loadSeriesForOrganization(
  organizationId: number,
  options: Object,
) {
  return async (dispatch: Dispatch) => {
    const series = await seriesApi
      .getSeriesForOrganization(organizationId, options);
    dispatch(loadSeriesSuccess(series));
    return series;
  };
}

export function loadSeries(seriesId: number) {
  return async (dispatch: Dispatch) => {
    const series = await seriesApi.getSeries(seriesId);
    dispatch(loadSeriesItemSuccess(series));
    return series;
  };
}

export function deleteSeries(seriesId: number) {
  return async (dispatch: Dispatch) => {
    await seriesApi.deleteSeries(seriesId);
    dispatch(deleteSeriesSuccess(seriesId));
  };
}
