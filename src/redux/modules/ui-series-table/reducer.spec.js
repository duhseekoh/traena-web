import reducer from './reducer';
import { initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';

describe('ui-series-table reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      currentSeriesIds: [],
      pagination: {
        totalElements: 0,
        size: 0,
        number: 0,
        totalPages: 0,
        numberOfElements: 0,
        first: false,
        last: false,
      },
      uiLoading: initialUiLoadingState,
      filters: {
        source: 'internal',
      },
      sort: {
        orders: [{
          direction: 'asc',
          property: 'title',
        }],
      },
    });
  });
});
