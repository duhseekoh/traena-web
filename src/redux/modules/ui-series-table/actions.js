// @flow
import * as seriesActions from '../series/actions';
import * as fromAuth from '../auth/selectors';
import * as fromUiSeriesTable from '../ui-series-table/selectors';
import type { Series } from '../../../types/series';
import type { Dispatch, GetState } from '../../rootReducer';
import { createSortActions } from '../../../utils/redux-sort/actions';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
  type SubscriptionSources,
} from '../../../constants/appConstants';

export const {
  uiLoadingActionTypes,
  setLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_SERIES_TABLE);

export const SERIES_TABLE_LOAD_SUCCESS = 'SERIES_TABLE/LOAD_SUCCESS';
function loadSeriesTableSuccess(series: Series[]) {
  return {
    type: SERIES_TABLE_LOAD_SUCCESS,
    payload: series,
  };
}

export const SERIES_TABLE_SET_PAGE = 'SERIES_TABLE/SET_PAGE';
export function setSeriesTablePage(page: number) {
  return {
    type: SERIES_TABLE_SET_PAGE,
    payload: page,
  };
}

export const SERIES_TABLE_LOAD_FAILURE = 'SERIES_TABLE/LOAD_FAILURE';
function seriesTableFetchFailure(err: string) {
  return {
    type: SERIES_TABLE_LOAD_FAILURE,
    payload: err,
  };
}

export const SERIES_TABLE_SET_SOURCE_FILTER = 'SERIES_TABLE/SET_SOURCE_FILTER';
export function setSeriesTableSourceFilter(include: SubscriptionSources) {
  return {
    type: SERIES_TABLE_SET_SOURCE_FILTER,
    payload: include,
  };
}

export const {
  setSort,
  sortActionTypes,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.UI_SERIES_TABLE, {
  sortableProperties: [
    'title',
    'channel.name',
    'author.firstName',
    'contentCount',
    'createdTimestamp',
  ],
});

export function setSeriesTableSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setSeriesTablePage(0));
    dispatch(setSort(property));
  };
}

export function fetchSeriesTable() {
  return async (dispatch: Dispatch, getState: GetState) => {
    try {
      dispatch(setLoading(true));
      const organizationId = fromAuth.selectOrganizationId(getState());
      if (!organizationId) {
        dispatch(setLoading(false));
        dispatch(seriesTableFetchFailure('Couldn\'t find your organization. Try logging in again.'));
        return;
      }
      const page = fromUiSeriesTable.selectCurrentPage(getState());

      const filters =
        fromUiSeriesTable.selectCurrentSeriesSourceFilters(getState());
      const {
        source: subscriptionSource,
      } = filters;

      const sort = fromUiSeriesTable.selectSortQueryString(getState());
      const options = { page, subscriptionSource, sort };

      const series = await dispatch(
        seriesActions.loadSeriesForOrganization(organizationId, options),
      );
      dispatch(loadSeriesTableSuccess(series));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(seriesTableFetchFailure('There was an error fetching series data. Try again.'));
    }
  };
}
