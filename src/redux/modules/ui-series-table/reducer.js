// @flow
import {
  SERIES_TABLE_LOAD_SUCCESS,
  SERIES_TABLE_LOAD_FAILURE,
  SERIES_TABLE_SET_PAGE,
  SERIES_TABLE_SET_SOURCE_FILTER,
  sortActionTypes,
  uiLoadingActionTypes,
} from './actions';
import type { Action } from '../../rootReducer';
import type { Pagination } from '../../../types';
import { createSortReducer } from '../../../utils/redux-sort/reducer';
import type { Sort } from '../../../utils/redux-sort/types';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';

import {
  SUBSCRIPTION_SOURCES,
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
  type SubscriptionSources,
} from '../../../constants/appConstants';

import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_SERIES_TABLE,
);

export type ReduxStateUiSeriesTable = {
  currentSeriesIds: number[],
  pagination: Pagination,
  filters: {
    source: SubscriptionSources,
  },
  sort: Sort,
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiSeriesTable = {
  currentSeriesIds: [],
  pagination: {
    totalElements: 0,
    size: 0,
    number: 0,
    totalPages: 0,
    numberOfElements: 0,
    first: false,
    last: false,
  },
  filters: {
    source: SUBSCRIPTION_SOURCES.INTERNAL,
  },
  sort: {
    orders: [{
      direction: SORT_DIRECTIONS.ASCENDING,
      property: 'title',
    }],
  },
  uiLoading: initialUiLoadingState,
};

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.UI_SERIES_TABLE,
);

function uiSeriesTable(
  state: ReduxStateUiSeriesTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case SERIES_TABLE_LOAD_SUCCESS: {
      const { ids, ...pagination } = payload;
      return {
        ...state,
        pagination,
        currentSeriesIds: ids,
      };
    }

    case SERIES_TABLE_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case SERIES_TABLE_LOAD_FAILURE:
      return {
        ...state,
        error: payload,
      };

    case SERIES_TABLE_SET_SOURCE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          source: payload,
        },
      };

    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    case uiLoadingActionTypes.setLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}


export default uiSeriesTable;
