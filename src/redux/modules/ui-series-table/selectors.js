// @flow
import { createSelector } from 'reselect';
import * as fromSeries from '../series/selectors';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';
import type { ReduxState } from '../../rootReducer';
import {
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';


export const selectPagination = (state: ReduxState) =>
  state.uiSeriesTable.pagination;
export const selectCurrentPage = (state: ReduxState) =>
  state.uiSeriesTable.pagination.number;
const selectCurrentSeriesIds = (state: ReduxState) =>
  state.uiSeriesTable.currentSeriesIds || [];

export const selectSeries = createSelector(
  [fromSeries.selectSeriesById, selectCurrentSeriesIds],
  (seriesById, currentSeriesIds) => currentSeriesIds.map(id => seriesById[id]),
);

export const selectCurrentSeriesSourceFilters = (state: ReduxState) =>
  state.uiSeriesTable.filters;

export const {
  getSortDirection,
  selectSortQueryString,
  getSortDirectionLongName,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.UI_SERIES_TABLE);

export const {
  selectIsLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_SERIES_TABLE);
