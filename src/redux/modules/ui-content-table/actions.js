// @flow
import {
  CONTENT_INCLUDE_SOURCES,
  CONTENT_STATUSES,
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';
import * as contentApi from '../../api/content';
import * as fromAuth from '../auth/selectors';
import * as fromUiContentTable from './selectors';
import type { Dispatch, GetState } from '../../rootReducer';
import type { IndexedPage } from '../../../types';
import { createSortActions } from '../../../utils/redux-sort/actions';

import type {
  ContentStatus,
  ContentIncludeSource,
} from '../../../constants/appConstants';

export const CONTENTS_FETCH_SUCCESS = 'CONTENTS/FETCH_SUCCESS';
export const CONTENTS_FETCH_FAILURE = 'CONTENTS/FETCH_FAILURE';
export const CONTENT_TABLE_FETCH_SUCCESS = 'CONTENT_TABLE/FETCH_SUCCESS';

export const CONTENTS_SET_PAGE = 'CONTENTS/SET_PAGE';
export const CONTENTS_SET_SEARCH_TERM = 'CONTENTS/SET_SEARCH_TERM';
export const CONTENTS_SET_INCLUDE_FILTER = 'CONTENTS/SET_INCLUDE_FILTER';
export const CONTENTS_SET_STATUS_FILTER = 'CONTENTS/SET_STATUS_FILTER';
export const CONTENTS_SET_LOADING = 'CONTENTS/SET_LOADING';

// @TODO: To fit the patterning of the rest of the app,
// this should probably exist in the content actions
export function contentsFetchSuccess(contents: IndexedPage<any>) {
  return {
    type: CONTENTS_FETCH_SUCCESS,
    payload: contents,
  };
}

function contentsFetchFailure(err: string) {
  return {
    type: CONTENTS_FETCH_FAILURE,
    payload: err,
  };
}

export function contentTableFetchSuccess(contents: IndexedPage<any>) {
  return {
    type: CONTENT_TABLE_FETCH_SUCCESS,
    payload: contents,
  };
}

export function setContentsPage(page: number) {
  return {
    type: CONTENTS_SET_PAGE,
    payload: page,
  };
}

export function setContentsIncludeFilter(include: ContentIncludeSource) {
  return {
    type: CONTENTS_SET_INCLUDE_FILTER,
    payload: include,
  };
}

export function setContentsStatusFilter(status: ContentStatus) {
  return {
    type: CONTENTS_SET_STATUS_FILTER,
    payload: status,
  };
}

export function setContentsLoading(loading: boolean) {
  return {
    type: CONTENTS_SET_LOADING,
    payload: loading,
  };
}

export const {
  setSort,
  sortActionTypes,
  clearSort,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.UI_CONTENT_TABLE, {
  sortableProperties: [
    'modifiedTimestamp',
    'publishedTimestamp',
    'channel.name',
    'author.firstName',
    'body.type',
    'body.title',
    'likeCount',
    'commentCount',
    'completedCount',
  ],
});

export function setContentTableSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setContentsPage(0));
    dispatch(setSort(property));
  };
}

export function setSearchText(searchText: string) {
  return (dispatch: Dispatch) => {
    dispatch(clearSort());
    dispatch({
      type: CONTENTS_SET_SEARCH_TERM,
      payload: searchText,
    });
  };
}

export function fetchContentsTable() {
  return (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    const page = fromUiContentTable.selectCurrentPage(getState());
    const searchText = fromUiContentTable.selectSearchText(getState());
    const filters = fromUiContentTable.selectCurrentContentFilters(getState());
    const { include } = filters;
    dispatch(setContentsLoading(true));
    const includeStatus = filters.status;
    const sort = fromUiContentTable.selectSortQueryString(getState());

    const options = include === CONTENT_INCLUDE_SOURCES.SELF ?
      { page, include, includeStatus, sort, searchText } :
      {
        page,
        include,
        includeStatus: CONTENT_STATUSES.PUBLISHED,
        sort,
        searchText,
      };
    contentApi
      .getContentForOrganization(organizationId, options)
      .then(contents => {
        dispatch(contentsFetchSuccess(contents));
        dispatch(contentTableFetchSuccess(contents));
        dispatch(setContentsLoading(false));
      })
      .catch(err => dispatch(contentsFetchFailure(err)));
  };
}
