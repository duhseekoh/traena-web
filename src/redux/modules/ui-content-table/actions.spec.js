import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import {
  setContentsPage,
  CONTENTS_SET_PAGE,
  setContentsIncludeFilter,
  CONTENTS_SET_INCLUDE_FILTER,
  setContentsStatusFilter,
  CONTENTS_SET_STATUS_FILTER,
  setContentsLoading,
  CONTENTS_SET_LOADING,
} from './actions';

jest.mock('../auth/selectors');
jest.mock('../ui-content-table/selectors');
jest.mock('../../api/content');

describe('content actions', () => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch CONTENTS_SET_PAGE', () => {
    const payload = 1;
    store.dispatch(setContentsPage(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENTS_SET_PAGE, payload }]);
  });

  it('should dispatch CONTENTS_SET_INCLUDE_FILTER', () => {
    const payload = 'all';
    store.dispatch(setContentsIncludeFilter(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENTS_SET_INCLUDE_FILTER, payload }]);
  });

  it('should dispatch CONTENTS_SET_STATUS_FILTER', () => {
    const payload = ['DRAFTS', 'PUBLISHED'];
    store.dispatch(setContentsStatusFilter(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENTS_SET_STATUS_FILTER, payload }]);
  });

  it('should dispatch CONTENTS_SET_LOADING', () => {
    const payload = true;
    store.dispatch(setContentsLoading(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: CONTENTS_SET_LOADING, payload }]);
  });
});
