export const selectCurrentPage = jest.fn(() => Promise.resolve(1));
export const selectCurrentContentFilters = jest.fn(() => Promise.resolve({}));
