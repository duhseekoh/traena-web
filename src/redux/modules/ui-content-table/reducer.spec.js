import reducer from './reducer';
import {
  CONTENT_TABLE_FETCH_SUCCESS,
  CONTENTS_SET_PAGE,
  CONTENTS_SET_INCLUDE_FILTER,
  CONTENTS_SET_LOADING,
} from './actions';
import { CONTENT_ITEM_DELETE_SUCCESS } from '../content/actions';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';


describe('ui-content-table reducer', () => {
  const payload = {
    number: 0,
    size: 2,
    sort: [
      {
        direction: 'desc',
        property: 'createdTimestamp',
      },
    ],
    totalElements: 15,
    totalPages: 2,
    first: true,
    last: false,
    ids: [134, 133],
    index: {
      133: {
        id: 133,
        body: {
          text:
            'Frenz United or in full Frenz United Football Club (FUFC) is a Malaysian football club located in Malacca City.',
          type: 'DailyAction',
          title: 'Frenz',
        },
      },
      134: {
        id: 134,
        body: {
          text:
            'Sable Chief was a Newfoundland dog that served as the mascot of the Royal Newfoundland Regiment during World War I.',
          type: 'DailyAction',
          title: 'Sable Chief',
        },
      },
    },
    numberOfElements: 2,
  };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      currentItemIds: [],
      filters: {
        include: 'self',
        status: 'PUBLISHED',
      },
      pagination: {
        number: 0,
      },
      loading: false,
      searchText: '',
      sort: {
        orders: [{
          direction: SORT_DIRECTIONS.DESCENDING,
          property: 'publishedTimestamp',
        }],
      },
    });
  });

  it('should handle CONTENT_TABLE_FETCH_SUCCESS', () => {
    const { ids, ...pagination } = payload;
    expect(
      reducer([], {
        type: CONTENT_TABLE_FETCH_SUCCESS,
        payload,
      }),
    ).toEqual({
      currentItemIds: ids,
      pagination,
    });
  });

  it('should handle CONTENTS_SET_PAGE', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENTS_SET_PAGE,
          payload: 0,
        },
      ),
    ).toEqual({ pagination: { number: 0 } });
  });

  it('should handle CONTENTS_SET_INCLUDE_FILTER', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENTS_SET_INCLUDE_FILTER,
          payload: 'internal',
        },
      ),
    ).toEqual({ filters: { include: 'internal' } });
  });

  it('should handle CONTENTS_SET_LOADING', () => {
    expect(
      reducer(
        {},
        {
          type: CONTENTS_SET_LOADING,
          payload: true,
        },
      ),
    ).toEqual({ loading: true });
  });

  it('should handle CONTENT_ITEM_DELETE_SUCCESS', () => {
    expect(
      reducer(
        { currentItemIds: [133, 134] },
        {
          type: CONTENT_ITEM_DELETE_SUCCESS,
          payload: 133,
        },
      ),
    ).toEqual({ currentItemIds: [134] });
  });
});
