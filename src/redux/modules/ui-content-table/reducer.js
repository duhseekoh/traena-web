// @flow
import {
  CONTENT_TABLE_FETCH_SUCCESS,
  CONTENTS_SET_PAGE,
  CONTENTS_SET_INCLUDE_FILTER,
  CONTENTS_SET_STATUS_FILTER,
  CONTENTS_SET_LOADING,
  CONTENTS_SET_SEARCH_TERM,
  sortActionTypes,
} from './actions';
import { CONTENT_ITEM_DELETE_SUCCESS } from '../content/actions';
import {
  CONTENT_INCLUDE_SOURCES,
  CONTENT_STATUSES,
  SORTABLE_REDUX_MODULE_NAMES,
  type ContentStatus,
  type ContentIncludeSource,
} from '../../../constants/appConstants';

import type { Action } from '../../rootReducer';
import type { Sort } from '../../../utils/redux-sort/types';
import { createSortReducer } from '../../../utils/redux-sort/reducer';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';


export type ReduxStateUiContentTable = {
  currentItemIds: number[],
  filters: {
    include: ContentIncludeSource,
    status: ContentStatus,
  },
  pagination: {
    number: number,
  },
  loading: boolean,
  sort: Sort,
  searchText: string,
};

const initialSortState = {
  orders: [{
    direction: SORT_DIRECTIONS.DESCENDING,
    property: 'publishedTimestamp',
  }],
};

const initialState: ReduxStateUiContentTable = {
  currentItemIds: [],
  filters: {
    include: CONTENT_INCLUDE_SOURCES.SELF,
    status: CONTENT_STATUSES.PUBLISHED,
  },
  pagination: {
    number: 0,
  },
  loading: false,
  sort: initialSortState,
  searchText: '',
};

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.UI_CONTENT_TABLE,
);

function uiContentTable(
  state: ReduxStateUiContentTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case CONTENT_TABLE_FETCH_SUCCESS: {
      const { ids, ...pagination } = payload;
      return {
        ...state,
        pagination,
        currentItemIds: ids,
      };
    }
    case CONTENTS_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case CONTENTS_SET_INCLUDE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          include: payload,
        },
      };

    case CONTENTS_SET_STATUS_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          status: payload,
        },
      };

    case CONTENTS_SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case CONTENT_ITEM_DELETE_SUCCESS:
      return {
        ...state,
        currentItemIds: state.currentItemIds.filter(id => id !== payload),
      };

    case CONTENTS_SET_SEARCH_TERM:
      let { sort } = state;
      // Reset the sorting when we clear the search field
      if (!payload.length) {
        sort = initialSortState;
      }
      return {
        ...state,
        sort,
        searchText: payload,
      };

    case sortActionTypes.clearSort:
    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiContentTable;
