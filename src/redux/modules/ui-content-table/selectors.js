// @flow
import { createSelector } from 'reselect';
import * as fromContent from '../content/selectors';
import type { ReduxState } from '../../rootReducer';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const selectCurrentContentFilters = (state: ReduxState) =>
  state.uiContentTable.filters;
export const selectCurrentPage = (state: ReduxState) =>
  state.uiContentTable.pagination.number;
const selectCurrentItemIds = (state: ReduxState) =>
  state.uiContentTable.currentItemIds || [];

export const selectIsContentTableLoading = (state: ReduxState) =>
  state.uiContentTable.loading;

export const selectContents = createSelector(
  [fromContent.selectContentById, selectCurrentItemIds],
  (contentById, currentItemIds) => currentItemIds.map(id => contentById[id]),
);

export const selectSearchText = (state: ReduxState) =>
  state.uiContentTable.searchText;

export const {
  getSortDirection,
  getSortDirectionLongName,
  selectSortQueryString,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.UI_CONTENT_TABLE);
