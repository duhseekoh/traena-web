// @flow
import * as localstore from '../../../utils/localstore';
import { AUTH_CONFIG } from '../../../constants/appConstants';
import Auth0Client, { type AuthResult } from '../../api/clients/Auth0Client';
import type { Dispatch } from '../../rootReducer';

// Store Auth0 client so we don't need to keep recreating it
let _auth0Client;
function _getAuth0Client() {
  if (!_auth0Client) {
    _auth0Client = new Auth0Client(AUTH_CONFIG);
  }
  return _auth0Client;
}

function persistAuthTokens({ idToken, accessToken }: AuthResult) {
  if (idToken && accessToken) {
    localstore.set('idToken', idToken);
    localstore.set('accessToken', accessToken);
  } else {
    localstore.remove('idToken');
    localstore.remove('accessToken');
  }
}

function clearPersistedAuthTokens() {
  persistAuthTokens({ idToken: null, accessToken: null });
}

export const AUTH_LOGGED_IN_HIT = 'AUTH_LOGGED_IN_HIT';
export function authLoggedIn(authResult: AuthResult) {
  persistAuthTokens(authResult);
  return {
    type: AUTH_LOGGED_IN_HIT,
    payload: authResult,
  };
}

export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export function logout() {
  clearPersistedAuthTokens();
  return {
    type: AUTH_LOGOUT,
    payload: undefined,
  };
}

export function showLogin() {
  return () => {
    const auth0Client = _getAuth0Client();
    auth0Client.showLogin();
  };
}

/**
 * Called on return from auth0. Attempts to get the tokens supplied by auth0.
 * If successful, sets up logged in state, otherwise sets up logged out state.
 * Redirecting the user is left up to the component that dispatches this.
 */
export function handleAuthentication() {
  return async (dispatch: Dispatch) => {
    const auth0Client = _getAuth0Client();
    try {
      // eslint-disable-next-line
      const authResult: AuthResult = await auth0Client.getAuthenticationResult();
      dispatch(authLoggedIn(authResult));
    } catch (err) {
      dispatch(logout());
      throw err;
    }
  };
}

export function sendResetPasswordEmail(email: string) {
  const auth0Client = _getAuth0Client();
  return auth0Client.sendResetPasswordEmail(email);
}

export function storeCallbackRedirect(redirect: string) {
  localstore.set('callbackRedirect', redirect);
}

export function removeCallbackRedirect() {
  localstore.remove('callbackRedirect');
}

export const AUTH_SET_CALLBACK_REDIRECT = 'AUTH_SET_CALLBACK_REDIRECT';
export function setCallbackRedirect(redirect: string) {
  storeCallbackRedirect(redirect);
  return {
    type: AUTH_SET_CALLBACK_REDIRECT,
    payload: redirect,
  };
}

export const AUTH_REMOVE_CALLBACK_REDIRECT = 'AUTH_REMOVE_CALLBACK_REDIRECT';
export function clearCallbackRedirect() {
  removeCallbackRedirect();
  return {
    type: AUTH_REMOVE_CALLBACK_REDIRECT,
  };
}
