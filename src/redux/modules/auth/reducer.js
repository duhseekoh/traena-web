// @flow
import { AUTH_LOGGED_IN_HIT, AUTH_LOGOUT } from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateAuth = {
  accessToken: ?string,
  idToken: ?string,
};

const initialState: ReduxStateAuth = {
  accessToken: null,
  idToken: null,
};

function auth(
  state: ReduxStateAuth = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case AUTH_LOGGED_IN_HIT:
      const { accessToken, idToken } = payload;
      return {
        ...state,
        accessToken,
        idToken,
      };
    case AUTH_LOGOUT:
      return {
        ...initialState,
      };
    default:
      return {
        ...state,
      };
  }
}

export default auth;
