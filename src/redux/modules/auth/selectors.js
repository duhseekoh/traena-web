// @flow
import { createSelector } from 'reselect';
import jwtDecode from 'jwt-decode';
import moment from 'moment';
import { AUTH_CONFIG } from '../../../constants/appConstants';
import type { ReduxState } from '../../rootReducer';

const selectIdToken = (state: ReduxState) => state.auth.idToken;
const selectDecodedIdToken = createSelector(
  [selectIdToken],
  idToken => (idToken ? jwtDecode(idToken) : null),
);

export const selectUser = createSelector(
  [selectDecodedIdToken],
  idTokenData =>
    idTokenData ? idTokenData[AUTH_CONFIG.userClaimNamespace] : null,
);

/**
 * Token is considered expired if it doesn't exist or it expired before now.
 * Do not use createSelector because this should never be memoized.
 */
export const selectIsTokenExpired = (state: ReduxState) => {
  const idTokenData: ?Object = selectDecodedIdToken(state);
  return idTokenData ? moment().unix() >= idTokenData.exp : true;
};

// We only need to know if the user is authenticated or not and the way to
// determine that is if the token they do or don't have is expired or not.
export const selectIsUserAuthenticated = createSelector(
  [selectIsTokenExpired],
  (isExpired: boolean): boolean => !isExpired,
);

export const selectOrganizationId = createSelector(
  [selectUser],
  user => (user && user.details ? user.details.organizationId : null),
);

export const selectTraenaId = createSelector(
  [selectUser],
  user => (user && user.details ? user.details.traenaId : null),
);

export const selectUserPermissions = createSelector(
  [selectUser],
  user => (user ? user.permissions : []),
);
