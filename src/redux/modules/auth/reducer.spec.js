import reducer from './reducer';
import { AUTH_LOGGED_IN_HIT, AUTH_LOGOUT } from './actions';

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      accessToken: null,
      idToken: null,
    });
  });

  it('should handle AUTH_LOGGED_IN_HIT', () => {
    expect(
      reducer(
        {},
        {
          type: AUTH_LOGGED_IN_HIT,
          payload: { accessToken: 'accessToken', idToken: 'idtoken' },
        },
      ),
    ).toEqual({
      accessToken: 'accessToken',
      idToken: 'idtoken',
    });
  });

  it('should handle AUTH_LOGOUT', () => {
    expect(
      reducer(
        {},
        {
          type: AUTH_LOGOUT,
        },
      ),
    ).toEqual({
      accessToken: null,
      idToken: null,
    });
  });
});
