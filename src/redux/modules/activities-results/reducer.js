// @flow
import {
  ACTIVITY_RESULTS_LOADING,
  INITIALIZE_ACTIVITY_RESULTS_FAILED,
} from '../ui-activities-results/actions';
import {
  ACTIVITY_RESULTS_LOAD_SUCCESS,
} from './actions';
import type { Action } from '../../rootReducer';
import type { ActivityResults } from '../../../types/activities';

export type ReduxStateActivitiesResults = {
  errorInitializing: boolean,
  loading: boolean,
  activityResults: ?ActivityResults,
};

const initialState: ReduxStateActivitiesResults = {
  errorInitializing: false,
  loading: false,
  activityResults: null,
};

function activitiesResults(
  state: ReduxStateActivitiesResults = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case ACTIVITY_RESULTS_LOADING: {
      return {
        ...state,
        loading: payload,
      };
    }

    case INITIALIZE_ACTIVITY_RESULTS_FAILED: {
      return {
        ...state,
        errorInitializing: true,
      };
    }

    case ACTIVITY_RESULTS_LOAD_SUCCESS: {
      return {
        ...state,
        activityResults: payload,
      };
    }

    default:
      return state;
  }
}

export default activitiesResults;
