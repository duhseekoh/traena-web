// @flow
import type { ActivityResults } from '../../../types/activities';
import * as activitiesApi from '../../api/activities';

export const ACTIVITY_RESULTS_LOAD_SUCCESS = 'ACTIVITY_RESULTS/LOAD_SUCCESS';
function loadActivityResultsSuccess(activityResults: ActivityResults) {
  return {
    type: ACTIVITY_RESULTS_LOAD_SUCCESS,
    payload: activityResults,
  };
}

export function loadActivityResults(
  activityId: number,
  contentId: number,
) {
  return async (dispatch: Dispatch) => {
    const activityResults =
      await activitiesApi.getActivityResults(activityId, contentId);
    dispatch(loadActivityResultsSuccess(activityResults));
  };
}
