// @flow
import type { ReduxState } from '../../rootReducer';

export const selectLoading = (state: ReduxState) =>
  state.activitiesResults.loading;
export const selectErrorInitializing = (state: ReduxState) =>
  state.activitiesResults.errorInitializing;

export const selectActivityResults = (state: ReduxState) =>
  state.activitiesResults.activityResults;
