// @flow
import * as fromContent from '../content/selectors';
import type { ReduxState } from '../../rootReducer';

export const selectError = (state: ReduxState) =>
  state.uiSeriesContentTable.error;
export const selectContentItems = (state: ReduxState) => {
  const { contentIds } = state.uiSeriesContentTable;
  return contentIds.map(id =>
    fromContent.selectContentById(state)[id]);
};
export const selectLoading = (state: ReduxState) =>
  state.uiSeriesContentTable.loading;
