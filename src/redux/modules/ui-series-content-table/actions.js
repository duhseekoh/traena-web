// @flow
import * as seriesApi from '../../api/series';
import * as uiContentFormActions from '../ui-content-table/actions';
import type { IndexedPage } from '../../../types';
import type { ContentItem } from '../../../types/content';
import type { Dispatch } from '../../rootReducer';

export const SERIES_CONTENT_LOAD_SUCCESS = 'SERIES_CONTENT/LOAD_SUCCESS';
export function loadSeriesContentTableSuccess(
  seriesId: number,
  content: IndexedPage<ContentItem>,
) {
  return {
    type: SERIES_CONTENT_LOAD_SUCCESS,
    payload: { seriesId, content },
  };
}

export const SERIES_CONTENT_SET_LOADING = 'SERIES_CONTENT_SET_LOADING';
export function setSeriesContentLoading(loading: boolean) {
  return {
    type: SERIES_CONTENT_SET_LOADING,
    payload: loading,
  };
}

export const SERIES_CONTENT_LOAD_FAILURE = 'SERIES_CONTENT/LOAD_FAILURE';
function loadSeriesContentTableFailure(err: string) {
  return {
    type: SERIES_CONTENT_LOAD_FAILURE,
    payload: err,
  };
}

export function initializeSeriesContentTable(seriesId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSeriesContentLoading(true));
      const seriesContent =
        await seriesApi.getContentForSeries(seriesId);

      dispatch(uiContentFormActions.contentsFetchSuccess(seriesContent));
      dispatch(loadSeriesContentTableSuccess(seriesId, seriesContent));
      dispatch(setSeriesContentLoading(false));
    } catch (err) {
      dispatch(setSeriesContentLoading(false));
      dispatch(loadSeriesContentTableFailure('There was a problem loading content'));
    }
  };
}
