// @flow
import {
  SERIES_CONTENT_LOAD_SUCCESS,
  SERIES_CONTENT_SET_LOADING,
  SERIES_CONTENT_LOAD_FAILURE,
} from './actions';

import type { Action } from '../../rootReducer';

export type ReduxStateUiSeriesContentTable = {
  contentIds: number[],
  seriesId: ?number,
  loading: boolean,
  error: ?string,
};

const initialState: ReduxStateUiSeriesContentTable = {
  contentIds: [],
  seriesId: null,
  loading: false,
  error: null,
};

function uiSeriesContentTable(
  state: ReduxStateUiSeriesContentTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case SERIES_CONTENT_LOAD_SUCCESS: {
      const { content, seriesId } = payload;
      const { ids, index, ...pagination } = content;
      return {
        ...state,
        pagination,
        contentIds: ids,
        seriesId,
      };
    }

    case SERIES_CONTENT_SET_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case SERIES_CONTENT_LOAD_FAILURE:
      return {
        ...state,
        error: payload,
      };

    default:
      return state;
  }
}

export default uiSeriesContentTable;
