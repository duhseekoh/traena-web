import reducer from './reducer';

describe('ui-subscriptions-table reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      currentIds: [],
      filters: {
        source: 'internal',
        visibility: 'INTERNAL, LIMITED, MARKET',
      },
      pagination: {
        number: 0,
      },
      sort: {
        orders: [{
          direction: 'asc',
          property: 'channel.name',
        }],
      },
      uiLoading: {
        loading: false,
        saving: false,
        errorLoading: false,
        errorSaving: false,
      },
    });
  });
});
