// @flow
import {
  SUBSCRIPTIONS_TABLE_FETCH_SUCCESS,
  SUBSCRIPTIONS_TABLE_SET_PAGE,
  SUBSCRIPTIONS_TABLE_SET_SOURCE_FILTER,
  SUBSCRIPTIONS_TABLE_SET_VISIBILTY_FILTER,
  sortActionTypes,
  uiLoadingActionTypes,
} from './actions';
import {
  SUBSCRIPTION_SOURCES,
  CHANNEL_VISIBILITY_FILTERS,
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
  type SubscriptionSources,
  type ChannelVisibility,
} from '../../../constants/appConstants';
import type { Action } from '../../rootReducer';

import { createSortReducer } from '../../../utils/redux-sort/reducer';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';

import type { Sort } from '../../../utils/redux-sort/types';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE,
);

const uiLoadingReducer = createUiLoadingReducer(
  UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE,
);

export type ReduxStateUiSubscriptionTable = {
  currentIds: number[],
  filters: {
    source: SubscriptionSources,
    visibility: ChannelVisibility,
  },
  pagination: {
    number: number,
  },
  sort: Sort,
  uiLoading: ReduxStateUiLoading,
};

const initialState: ReduxStateUiSubscriptionTable = {
  currentIds: [],
  filters: {
    source: SUBSCRIPTION_SOURCES.INTERNAL,
    visibility: CHANNEL_VISIBILITY_FILTERS.ALL,
  },
  pagination: {
    number: 0,
  },
  sort: {
    orders: [{
      direction: SORT_DIRECTIONS.ASCENDING,
      property: 'channel.name',
    }],
  },
  uiLoading: initialUiLoadingState,
};

function uiSubscriptionTable(
  state: ReduxStateUiSubscriptionTable = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case SUBSCRIPTIONS_TABLE_FETCH_SUCCESS: {
      const { ids, pagination } = payload;
      return {
        ...state,
        pagination,
        currentIds: ids,
      };
    }
    case SUBSCRIPTIONS_TABLE_SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          number: payload,
        },
      };

    case SUBSCRIPTIONS_TABLE_SET_SOURCE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          source: payload,
        },
      };

    case SUBSCRIPTIONS_TABLE_SET_VISIBILTY_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          visibility: payload,
        },
      };

    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    case uiLoadingActionTypes.setLoading:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiSubscriptionTable;
