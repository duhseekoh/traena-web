// @flow
import * as subscriptionApi from '../../api/subscriptions';
import * as fromAuth from '../auth/selectors';
import * as fromUiSubscriptionTable from './selectors';
import type { Dispatch, GetState } from '../../rootReducer';
import type { IndexedPage } from '../../../types';
import { loadOrganizationChannelsSuccess } from '../channels/actions';
import { loadOrgSubscriptionsSuccess } from '../subscriptions/actions';
import { createSortActions } from '../../../utils/redux-sort/actions';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';

import type {
  SubscriptionSources,
  ChannelVisibility,
} from '../../../constants/appConstants';
import {
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
  SUBSCRIPTION_SOURCES,
} from '../../../constants/appConstants';

export const SUBSCRIPTIONS_TABLE_FETCH_SUCCESS = 'SUBSCRIPTIONS_TABLE/FETCH_SUCCESS';
export const SUBSCRIPTIONS_TABLE_FETCH_FAILURE = 'SUBSCRIPTIONS_TABLE/FETCH_FAILURE';

export const SUBSCRIPTIONS_TABLE_SET_PAGE = 'SUBSCRIPTIONS_TABLE/SET_PAGE';
export const SUBSCRIPTIONS_TABLE_SET_SOURCE_FILTER = 'SUBSCRIPTIONS_TABLE/SET_SOURCE_FILTER';
export const SUBSCRIPTIONS_TABLE_SET_VISIBILTY_FILTER = 'SUBSCRIPTIONS_TABLE/SET_VISIBILTY_FILTER';
export const SUBSCRIPTIONS_TABLE_SET_LOADING = 'SUBSCRIPTIONS_TABLE/SET_LOADING';

export function subscriptionTableFetchSuccess(subscriptions: IndexedPage<any>) {
  return {
    type: SUBSCRIPTIONS_TABLE_FETCH_SUCCESS,
    payload: subscriptions,
  };
}

export function subscriptionTableFetchFailure(err: string) {
  return {
    type: SUBSCRIPTIONS_TABLE_FETCH_FAILURE,
    payload: err,
  };
}

export function setSubscriptionTablePage(page: number) {
  return {
    type: SUBSCRIPTIONS_TABLE_SET_PAGE,
    payload: page,
  };
}

export function setSubscriptionTableSourceFilter(include: SubscriptionSources) {
  return {
    type: SUBSCRIPTIONS_TABLE_SET_SOURCE_FILTER,
    payload: include,
  };
}

export function setSubscriptionTableVisibilityFilter(
  visibility: ChannelVisibility,
) {
  return {
    type: SUBSCRIPTIONS_TABLE_SET_VISIBILTY_FILTER,
    payload: visibility,
  };
}

export const {
  setSort,
  sortActionTypes,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE, {
  sortableProperties: [
    'channel.name',
    'channel.createdTimestamp',
    'visibility',
  ],
});

export const {
  uiLoadingActionTypes,
  setLoading,
} = createUiLoadingActions(UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE);

export function setSubscriptionTableSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setSubscriptionTablePage(0));
    dispatch(setSort(property));
  };
}

export function fetchSubscriptionTable() {
  return async (dispatch: Dispatch, getState: GetState) => {
    const organizationId = fromAuth.selectOrganizationId(getState());
    if (!organizationId) {
      throw new Error('Couldn\'t find organizationId for logged in user');
    }
    try {
      const page = fromUiSubscriptionTable.selectCurrentPage(getState());
      const filters =
        fromUiSubscriptionTable.selectCurrentSubscriptionFilters(getState());
      const {
        source: subscriptionSource,
        visibility: channelVisibility,
      } = filters;
      dispatch(setLoading(true));
      const sort = fromUiSubscriptionTable.selectSortQueryString(getState());
      const options = subscriptionSource === SUBSCRIPTION_SOURCES.EXTERNAL ?
        { page, subscriptionSource, sort } :
        { page, subscriptionSource, channelVisibility, sort };
      const results = await subscriptionApi
        .getChannelSubscriptionsForOrg(
          organizationId,
          options,
        );
      dispatch(loadOrgSubscriptionsSuccess(results.subscriptions));
      dispatch(loadOrganizationChannelsSuccess(
        organizationId,
        results.channels,
      ));
      dispatch(subscriptionTableFetchSuccess(results));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(subscriptionTableFetchFailure(err));
      dispatch(setLoading(false));
    }
  };
}
