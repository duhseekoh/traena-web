// @flow
import { createSelector } from 'reselect';
import * as fromChannels from '../channels/selectors';
import * as fromSubscriptions from '../subscriptions/selectors';
import type { ReduxState } from '../../rootReducer';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

import {
  SORTABLE_REDUX_MODULE_NAMES,
  UI_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const selectCurrentSubscriptionFilters = (state: ReduxState) =>
  state.uiSubscriptionTable.filters;
export const selectCurrentPage = (state: ReduxState) =>
  state.uiSubscriptionTable.pagination.number;
const selectIds = (state: ReduxState) =>
  state.uiSubscriptionTable.currentIds || [];

export const selectChannels = createSelector([
  fromChannels.selectChannelsById,
  selectIds],
(channelsById, currentChannelIds) => currentChannelIds
  .map(id => channelsById[id])
  .filter(c => c !== undefined));

export const selectSubscriptions = createSelector([
  fromSubscriptions.selectSubscriptionsWithChannels,
  selectIds],
(subscriptionsById, currentIds) => currentIds
  .map(id => subscriptionsById[id])
  .filter(s => s !== undefined));

export const {
  getSortDirection,
  getSortDirectionLongName,
  selectSortQueryString,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE);

export const {
  selectIsLoading,
  selectErrorLoading,
} = createUiLoadingSelectors(UI_REDUX_MODULE_NAMES.UI_SUBSCRIPTION_TABLE);
