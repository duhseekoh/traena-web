// @flow
import {
  CONTENT_ITEM_EVENT_FEED_INITIALIZED,
  CONTENT_ITEM_EVENT_FEED_FETCHING,
  CONTENT_ITEM_EVENT_FEED_FETCH_ERROR,
  CONTENT_ITEM_EVENT_FEED_FETCH_SUCCESS,
} from './actions';
import type { Pagination } from '../../../types';
import type { Event } from '../../../types/events';
import type { Action } from '../../rootReducer';

export type ReduxStateContentItemAnalyticsFeed = {
  contentId: ?number,
  paginationResponse: ?Pagination,
  events: Event[],
  error: ?string,
  loading: boolean,
};

const initialState: ReduxStateContentItemAnalyticsFeed = {
  contentId: null,
  error: null,
  events: [],
  loading: false,
  paginationResponse: null,
};

function contentItemAnalyticsFeed(
  state: ReduxStateContentItemAnalyticsFeed = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_EVENT_FEED_INITIALIZED:
      return {
        ...state,
        contentId: payload,
      };

    case CONTENT_ITEM_EVENT_FEED_FETCH_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };

    case CONTENT_ITEM_EVENT_FEED_FETCHING:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case CONTENT_ITEM_EVENT_FEED_FETCH_SUCCESS:
      // pull content out, everything else is stored as the latest pagination response
      const { content, ...paginationResponse } = payload;
      return {
        ...state,
        loading: false,
        error: null,
        events: [...state.events, ...content],
        paginationResponse,
      };

    default:
      return state;
  }
}

export default contentItemAnalyticsFeed;
