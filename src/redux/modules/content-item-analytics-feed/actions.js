// @flow
import type { Dispatch, GetState } from '../../rootReducer';
import * as contentApi from '../../api/content';
import * as fromContentItemAnalyticsFeed from './selectors';

export const CONTENT_ITEM_EVENT_FEED_INITIALIZED = 'CONTENT_ITEM_EVENT_FEED_INITIALIZED';
export const CONTENT_ITEM_EVENT_FEED_FETCHING = 'CONTENT_ITEM_EVENT_FEED_FETCHING';
export const CONTENT_ITEM_EVENT_FEED_FETCH_ERROR = 'CONTENT_ITEM_EVENT_FEED_FETCH_ERROR';
export const CONTENT_ITEM_EVENT_FEED_FETCH_SUCCESS = 'CONTENT_ITEM_EVENT_FEED_FETCH_SUCCESS';

function initialized(contentId: number) {
  return {
    type: CONTENT_ITEM_EVENT_FEED_INITIALIZED,
    payload: contentId,
  };
}

function eventsFetchError(err: string) {
  return {
    type: CONTENT_ITEM_EVENT_FEED_FETCH_ERROR,
    payload: err,
  };
}

function eventsFetching() {
  return {
    type: CONTENT_ITEM_EVENT_FEED_FETCHING,
    payload: undefined, // because our flow types currently require a payload
  };
}

function eventsPageRetrieved(eventsPage: Object) {
  return {
    type: CONTENT_ITEM_EVENT_FEED_FETCH_SUCCESS,
    payload: eventsPage,
  };
}

export function getMoreEvents() {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(eventsFetching());
    try {
      const nextPage =
        fromContentItemAnalyticsFeed.selectNextPageNumber(getState());
      const contentId =
        fromContentItemAnalyticsFeed.selectContentId(getState());
      const resp =
        await contentApi.getEventsPageForContentItem(contentId, nextPage);
      dispatch(eventsPageRetrieved(resp));
    } catch (err) {
      dispatch(eventsFetchError(err));
    }
  };
}

export function initialize(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(initialized(contentId));
    return dispatch(getMoreEvents());
  };
}
