// @flow
import type { ReduxState } from '../../rootReducer';

export const selectContentId = (state: ReduxState) =>
  state.contentItemAnalyticsFeed.contentId;
export const selectIsLoading = (state: ReduxState) =>
  state.contentItemAnalyticsFeed.loading;
export const selectError = (state: ReduxState) =>
  state.contentItemAnalyticsFeed.error;
export const selectEvents = (state: ReduxState) =>
  state.contentItemAnalyticsFeed.events;
// When we haven't received a response yet (e.g. before getting first page)
// then the next page is 0, otherwise it goes off the latest pagination response.
export const selectNextPageNumber = (state: ReduxState) => {
  const { paginationResponse } = state.contentItemAnalyticsFeed;
  if (paginationResponse) {
    return paginationResponse.number + 1;
  }
  return 0;
};

export const selectHasMore = (state: ReduxState) => {
  const { paginationResponse } = state.contentItemAnalyticsFeed;
  if (!paginationResponse) {
    return true;
  }

  return !paginationResponse.last;
};
