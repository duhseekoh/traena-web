// @flow
import {
  CONTENT_ITEM_ANALYTICS_USERS_INITIALIZED,
  CONTENT_ITEM_ANALYTICS_USERS_FETCH_STARTED,
  CONTENT_ITEM_ANALYTICS_USERS_FETCH_SUCCESS,
  CONTENT_ITEM_ANALYTICS_USERS_FETCH_FAILURE,
} from './actions';
import type { Pagination } from '../../../types';
import type { UserWithContentStats } from '../../../types/analytics';
import type { Action } from '../../rootReducer';

export type ReduxStateContentItemAnalyticsUsers = {
  contentId: ?number,
  currentUsersWithStats: UserWithContentStats[],
  error: ?Object,
  loading: boolean,
  paginationResponse: ?Pagination,
};

const initialState: ReduxStateContentItemAnalyticsUsers = {
  contentId: null,
  currentUsersWithStats: [],
  error: null,
  loading: false,
  paginationResponse: null,
};

function contentItemAnalyticsUsers(
  state: ReduxStateContentItemAnalyticsUsers = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_ANALYTICS_USERS_INITIALIZED:
      return {
        ...state,
        contentId: payload,
      };

    case CONTENT_ITEM_ANALYTICS_USERS_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload,
      };

    case CONTENT_ITEM_ANALYTICS_USERS_FETCH_STARTED:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case CONTENT_ITEM_ANALYTICS_USERS_FETCH_SUCCESS:
      // pull content out, everything else is stored as the latest pagination response
      const { content, ...paginationResponse } = payload;
      return {
        ...state,
        loading: false,
        error: null,
        currentUsersWithStats: content,
        paginationResponse,
      };

    default:
      return state;
  }
}

export default contentItemAnalyticsUsers;
