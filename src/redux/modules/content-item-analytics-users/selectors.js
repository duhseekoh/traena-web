// @flow
import type { ReduxState } from '../../rootReducer';

export const selectContentId = (state: ReduxState) =>
  state.contentItemAnalyticsUsers.contentId;
export const selectIsLoading = (state: ReduxState) =>
  state.contentItemAnalyticsUsers.loading;
export const selectError = (state: ReduxState) =>
  state.contentItemAnalyticsUsers.error;
export const selectPagination = (state: ReduxState) =>
  state.contentItemAnalyticsUsers.paginationResponse;
export const selectUsersWithStats = (state: ReduxState) =>
  state.contentItemAnalyticsUsers.currentUsersWithStats;
