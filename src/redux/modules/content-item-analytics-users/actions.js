// @flow
import * as contentApi from '../../api/content';
import type { Dispatch, GetState } from '../../rootReducer';
import * as fromContentItemAnalyticsUsers from './selectors';
import type { ArrayPage } from '../../../types/index';
import type { UserWithContentStats } from '../../../types/analytics';

export const CONTENT_ITEM_ANALYTICS_USERS_INITIALIZED = 'CONTENT_ITEM_ANALYTICS_USERS_INITIALIZED';
export const CONTENT_ITEM_ANALYTICS_USERS_FETCH_STARTED = 'CONTENT_ITEM_ANALYTICS_USERS_FETCH_STARTED';
export const CONTENT_ITEM_ANALYTICS_USERS_FETCH_SUCCESS = 'CONTENT_ITEM_ANALYTICS_USERS_FETCH_SUCCESS';
export const CONTENT_ITEM_ANALYTICS_USERS_FETCH_FAILURE = 'CONTENT_ITEM_ANALYTICS_USERS_FETCH_FAILURE';

function userStatsPageFetchSuccess(
  pageResponse: ArrayPage<UserWithContentStats>,
) {
  return {
    type: CONTENT_ITEM_ANALYTICS_USERS_FETCH_SUCCESS,
    payload: pageResponse,
  };
}

function userStatsPageFetchFailure(err) {
  return {
    type: CONTENT_ITEM_ANALYTICS_USERS_FETCH_FAILURE,
    payload: err,
  };
}

export function userStatsPageFetching(page: number) {
  return {
    type: CONTENT_ITEM_ANALYTICS_USERS_FETCH_STARTED,
    payload: page,
  };
}

export function initialized(contentId: number) {
  return {
    type: CONTENT_ITEM_ANALYTICS_USERS_INITIALIZED,
    payload: contentId,
  };
}

export function getPage(page: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(userStatsPageFetching(page));
    try {
      const contentId =
        fromContentItemAnalyticsUsers.selectContentId(getState());
      const resp = await contentApi.getUserStatsForContentItem(contentId, page);
      dispatch(userStatsPageFetchSuccess(resp));
    } catch (err) {
      dispatch(userStatsPageFetchFailure(err));
    }
  };
}

export function initialize(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(initialized(contentId));
    return dispatch(getPage(0));
  };
}
