import reducer from './reducer';
import {
  USERS_FETCH_SUCCESS,
  USERS_FETCH_FAILURE,
} from '../ui-users-table/actions';

describe('users reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      usersFetchError: null,
      usersById: {},
    });
  });

  it('should handle USERS_FETCH_SUCCESS', () => {
    const usersById = {
      82: {
        id: 82,
        email: 'ed@traena.io',
        organizationId: 1,
        firstName: 'Ed',
        lastName: 'Siok',
        profileImageURI: null,
        isDisabled: true,
        position: 'Traena Admin',
        tags: ['t'],
        createdTimestamp: '2017-08-14T18:50:06.258Z',
        modifiedTimestamp: '2017-08-15T16:22:03.267Z',
      },
    };

    const payload = {
      number: 0,
      size: 10,
      sort: [
        {
          direction: 'desc',
          property: 'createdTimestamp',
        },
      ],
      totalElements: 1,
      totalPages: 1,
      first: true,
      last: false,
      ids: [82],
      index: usersById,
      numberOfElements: 1,
    };

    expect(
      reducer(
        {},
        {
          type: USERS_FETCH_SUCCESS,
          payload,
        },
      ),
    ).toEqual({
      usersById,
      usersFetchError: null,
    });
  });

  it('should handle USERS_FETCH_FAILURE', () => {
    expect(
      reducer(
        {},
        {
          type: USERS_FETCH_FAILURE,
          payload: 'im an error',
        },
      ),
    ).toEqual({
      userFetchError: 'im an error',
    });
  });
});
