// @flow
import {
  USERS_FETCH_SUCCESS,
  USERS_FETCH_FAILURE,
} from '../ui-users-table/actions';
import { USER_SAVED, USER_FETCH_SUCCESS } from '../ui-user-form/actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUsers = {
  usersFetchError: ?string,
  usersById: {
    [userId: number]: Object, // TODO user flow type
  },
};

const initialState: ReduxStateUsers = {
  usersFetchError: null,
  usersById: {},
};

function users(
  state: ReduxStateUsers = initialState,
  { type, payload = {} }: Action,
) {
  switch (type) {
    case USERS_FETCH_SUCCESS:
      return {
        ...state,
        usersById: {
          ...state.usersById,
          ...payload.index,
        },

        usersFetchError: null,
      };

    case USER_FETCH_SUCCESS:
      return {
        ...state,
        usersById: {
          ...state.usersById,
          [payload.id]: payload,
        },
      };

    case USERS_FETCH_FAILURE:
      return {
        ...state,
        userFetchError: payload,
      };

    case USER_SAVED:
      return {
        ...state,
        usersById: {
          ...state.usersById,
          [payload.id]: payload,
        },
      };

    default:
      return state;
  }
}

export default users;
