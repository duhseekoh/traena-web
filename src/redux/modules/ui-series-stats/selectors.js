// @flow
import type { ReduxState } from '../../rootReducer';

export const selectInitializing = (state: ReduxState) =>
  state.uiSeriesStats.initializing;
export const selectInitializeError = (state: ReduxState) =>
  state.uiSeriesStats.initializeError;
