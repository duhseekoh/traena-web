// @flow
import { loadSeries } from '../series/actions';

export const SERIES_STATS_INITIALIZING = 'SERIES_STATS_INITIALIZING';
export const SERIES_STATS_INITIALIZE_ERROR = 'SERIES_STATS_INITIALIZE_ERROR';
export const SERIES_STATS_INITIALIZED = 'SERIES_STATS_INITIALIZED';

function initializeError(err: string) {
  return {
    type: SERIES_STATS_INITIALIZE_ERROR,
    payload: err,
  };
}

function initializing() {
  return {
    type: SERIES_STATS_INITIALIZING,
  };
}

function initialized() {
  return {
    type: SERIES_STATS_INITIALIZED,
  };
}

export function initialize(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(initializing());
    try {
      await dispatch(loadSeries(contentId));
      dispatch(initialized());
    } catch (err) {
      dispatch(initializeError(err));
    }
  };
}
