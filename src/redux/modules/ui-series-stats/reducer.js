// @flow
import {
  SERIES_STATS_INITIALIZING,
  SERIES_STATS_INITIALIZE_ERROR,
  SERIES_STATS_INITIALIZED,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiSeriesStats = {
  initializing: boolean,
  initializeError: ?string,
};

const initialState: ReduxStateUiSeriesStats = {
  initializing: false,
  initializeError: null,
};

function uiSeriesStats(
  state: ReduxStateUiSeriesStats = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SERIES_STATS_INITIALIZING:
      return {
        ...state,
        initializing: true,
        initializeError: null,
      };

    case SERIES_STATS_INITIALIZE_ERROR:
      return {
        ...state,
        initializing: false,
        initializeError: payload,
      };

    case SERIES_STATS_INITIALIZED:
      return {
        ...state,
        initializing: false,
        initializeError: null,
      };

    default:
      return state;
  }
}

export default uiSeriesStats;
