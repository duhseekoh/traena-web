// @flow
import { fetchContentItem } from '../content/actions';

export const CONTENT_ITEM_ANALYTICS_INITIALIZING = 'CONTENT_ITEM_ANALYTICS_INITIALIZING';
export const CONTENT_ITEM_ANALYTICS_INITIALIZE_ERROR = 'CONTENT_ITEM_ANALYTICS_INITIALIZE_ERROR';
export const CONTENT_ITEM_ANALYTICS_INITIALIZED = 'CONTENT_ITEM_ANALYTICS_INITIALIZED';

function initializeError(err: string) {
  return {
    type: CONTENT_ITEM_ANALYTICS_INITIALIZE_ERROR,
    payload: err,
  };
}

function initializing() {
  return {
    type: CONTENT_ITEM_ANALYTICS_INITIALIZING,
  };
}

function initialized() {
  return {
    type: CONTENT_ITEM_ANALYTICS_INITIALIZED,
  };
}

export function initialize(contentId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(initializing());
    try {
      await dispatch(fetchContentItem(contentId));
      dispatch(initialized());
    } catch (err) {
      dispatch(initializeError(err));
    }
  };
}
