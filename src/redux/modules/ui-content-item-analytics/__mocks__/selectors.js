export const selectInitializing = state =>
  state.uiContentItemAnalytics.initializing;
export const selectInitializeError = state =>
  state.uiContentItemAnalytics.selectInitializeError;
