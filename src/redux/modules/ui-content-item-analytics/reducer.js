// @flow
import {
  CONTENT_ITEM_ANALYTICS_INITIALIZING,
  CONTENT_ITEM_ANALYTICS_INITIALIZE_ERROR,
  CONTENT_ITEM_ANALYTICS_INITIALIZED,
} from './actions';
import type { Action } from '../../rootReducer';

export type ReduxStateUiContentItemAnalytics = {
  initializing: boolean,
  initializeError: ?string,
};

const initialState: ReduxStateUiContentItemAnalytics = {
  initializing: false,
  initializeError: null,
};

function contentItemAnalytics(
  state: ReduxStateUiContentItemAnalytics = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case CONTENT_ITEM_ANALYTICS_INITIALIZING:
      return {
        ...state,
        initializing: true,
        initializeError: null,
      };

    case CONTENT_ITEM_ANALYTICS_INITIALIZE_ERROR:
      return {
        ...state,
        initializing: false,
        initializeError: payload,
      };

    case CONTENT_ITEM_ANALYTICS_INITIALIZED:
      return {
        ...state,
        initializing: false,
        initializeError: null,
      };

    default:
      return state;
  }
}

export default contentItemAnalytics;
