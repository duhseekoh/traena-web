// @flow
import type { ReduxState } from '../../rootReducer';

export const selectInitializing = (state: ReduxState) =>
  state.uiContentItemAnalytics.initializing;
export const selectInitializeError = (state: ReduxState) =>
  state.uiContentItemAnalytics.initializeError;
