// @flow
import * as seriesApi from '../../api/series';
import type { Dispatch, GetState } from '../../rootReducer';
import * as fromSeriesStatsUsers from './selectors';
import type { ArrayPage } from '../../../types/index';
import type { UserWithSeriesStats } from '../../../types/analytics';
import { createSortActions } from '../../../utils/redux-sort/actions';
import {
  SORTABLE_REDUX_MODULE_NAMES,
} from '../../../constants/appConstants';

export const SERIES_STATS_USERS_INITIALIZED = 'SERIES_STATS_USERS_INITIALIZED';
export const SERIES_STATS_USERS_FETCH_STARTED = 'SERIES_STATS_USERS_FETCH_STARTED';
export const SERIES_STATS_USERS_FETCH_SUCCESS = 'SERIES_STATS_USERS_FETCH_SUCCESS';
export const SERIES_STATS_USERS_FETCH_FAILURE = 'SERIES_STATS_USERS_FETCH_FAILURE';

function userStatsPageFetchSuccess(
  pageResponse: ArrayPage<UserWithSeriesStats>,
) {
  return {
    type: SERIES_STATS_USERS_FETCH_SUCCESS,
    payload: pageResponse,
  };
}

function userStatsPageFetchFailure(err) {
  return {
    type: SERIES_STATS_USERS_FETCH_FAILURE,
    payload: err,
  };
}

export function userStatsPageFetching(page: number) {
  return {
    type: SERIES_STATS_USERS_FETCH_STARTED,
    payload: page,
  };
}

export function initialized(seriesId: number) {
  return {
    type: SERIES_STATS_USERS_INITIALIZED,
    payload: seriesId,
  };
}

export const SORTABLE_PROPERTIES = {
  FIRST_NAME: 'firstName',
  POSITION: 'position',
  COMPLETED_CONTENT_COUNT: 'completedContentCount',
  LAST_VIEWED_CONTENT_TIMESTAMP: 'latestViewedContentTimestamp',
};

export const {
  setSort,
  sortActionTypes,
} = createSortActions(SORTABLE_REDUX_MODULE_NAMES.SERIES_STATS_USERS, {
  sortableProperties: Object.values(SORTABLE_PROPERTIES),
});

export function getPage(page: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(userStatsPageFetching(page));
    try {
      const seriesId =
        fromSeriesStatsUsers.selectSeriesId(getState());
      if (!seriesId) {
        dispatch(userStatsPageFetchFailure({}));
        return;
      }

      const sort = fromSeriesStatsUsers.selectSortQueryString(getState());
      const options = { page, sort };

      const resp = await seriesApi.getUserStatsForSeries(seriesId, options);
      dispatch(userStatsPageFetchSuccess(resp));
    } catch (err) {
      dispatch(userStatsPageFetchFailure(err));
    }
  };
}

/**
 * Updates redux-sort orders and fetches the first page.
 */
export function setStatsSort(property: string) {
  return async (dispatch: Dispatch) => {
    dispatch(setSort(property));
    return dispatch(getPage(0));
  };
}

export function initialize(seriesId: number) {
  return async (dispatch: Dispatch) => {
    dispatch(initialized(seriesId));
    return dispatch(getPage(0));
  };
}
