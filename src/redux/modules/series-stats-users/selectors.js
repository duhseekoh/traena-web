// @flow
import type { ReduxState } from '../../rootReducer';
import { createSortSelectors } from '../../../utils/redux-sort/selectors';
import { SORTABLE_REDUX_MODULE_NAMES } from '../../../constants/appConstants';

export const selectSeriesId = (state: ReduxState) =>
  state.seriesStatsUsers.seriesId;
export const selectIsLoading = (state: ReduxState) =>
  state.seriesStatsUsers.loading;
export const selectError = (state: ReduxState) => state.seriesStatsUsers.error;
export const selectPagination = (state: ReduxState) =>
  state.seriesStatsUsers.paginationResponse;
export const selectUsersWithStats = (state: ReduxState) =>
  state.seriesStatsUsers.currentUsersWithStats;

export const {
  getSortDirection,
  selectSortQueryString,
  getSortDirectionLongName,
} = createSortSelectors(SORTABLE_REDUX_MODULE_NAMES.SERIES_STATS_USERS);
