// @flow
import { SORTABLE_REDUX_MODULE_NAMES } from '../../../constants/appConstants';
import {
  SERIES_STATS_USERS_INITIALIZED,
  SERIES_STATS_USERS_FETCH_STARTED,
  SERIES_STATS_USERS_FETCH_SUCCESS,
  SERIES_STATS_USERS_FETCH_FAILURE,
  sortActionTypes,
  SORTABLE_PROPERTIES,
} from './actions';
import type { Pagination } from '../../../types';
import type { UserWithSeriesStats } from '../../../types/analytics';
import type { Action } from '../../rootReducer';
import { SORT_DIRECTIONS } from '../../../utils/redux-sort/constants';
import type { Sort } from '../../../utils/redux-sort/types';
import { createSortReducer } from '../../../utils/redux-sort/reducer';

export type ReduxStateSeriesStatsUsers = {
  seriesId: ?number,
  currentUsersWithStats: UserWithSeriesStats[],
  error: ?Object,
  loading: boolean,
  paginationResponse: ?Pagination,
  sort: Sort,
};

const initialState: ReduxStateSeriesStatsUsers = {
  seriesId: null,
  currentUsersWithStats: [],
  error: null,
  loading: false,
  paginationResponse: null,
  sort: {
    orders: [
      {
        property: SORTABLE_PROPERTIES.FIRST_NAME,
        direction: SORT_DIRECTIONS.ASCENDING,
      },
    ],
  },
};

const sortReducer = createSortReducer(
  SORTABLE_REDUX_MODULE_NAMES.SERIES_STATS_USERS,
);

function seriesStatsUsers(
  state: ReduxStateSeriesStatsUsers = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case SERIES_STATS_USERS_INITIALIZED:
      return {
        ...state,
        seriesId: payload,
      };

    case SERIES_STATS_USERS_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload,
      };

    case SERIES_STATS_USERS_FETCH_STARTED:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case SERIES_STATS_USERS_FETCH_SUCCESS:
      // pull series out, everything else is stored as the latest pagination response
      const { content, ...paginationResponse } = payload;
      return {
        ...state,
        loading: false,
        error: null,
        currentUsersWithStats: content,
        paginationResponse,
      };

    case sortActionTypes.setSort:
      return {
        ...state,
        sort: sortReducer(state.sort, { type, payload }),
      };

    default:
      return state;
  }
}

export default seriesStatsUsers;
