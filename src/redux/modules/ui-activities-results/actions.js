// @flow
import { push } from 'connected-react-router';
import { getRouteLink, routes } from '../../../constants/routes';
import * as activityResultsActions from '../activities-results/actions';
import * as applicationActions from '../application/actions';

export const ACTIVITY_RESULTS_LOADING = 'ACTIVITY_RESULTS/LOADING';
function activityResultsLoading(loading: boolean) {
  return {
    type: ACTIVITY_RESULTS_LOADING,
    payload: loading,
  };
}

export const INITIALIZE_ACTIVITY_RESULTS_FAILED =
  'INITIALIZE_ACTIVITY_RESULTS/FAILED';
function initializeActivityResultsFailed() {
  return {
    type: INITIALIZE_ACTIVITY_RESULTS_FAILED,
    payload: undefined, // because flow types expect action payloads
  };
}

export function initializeActivityResults(
  activityId: number,
  contentId: number,
) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(activityResultsLoading(true));
      await dispatch(
        activityResultsActions.loadActivityResults(activityId, contentId),
      );
      dispatch(activityResultsLoading(false));
    } catch (err) {
      dispatch(activityResultsLoading(false));
      dispatch(initializeActivityResultsFailed());
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to load this activity\'s results',
          title: 'Oops!',
          onDismiss: () => dispatch(
            push(
              getRouteLink(routes.CONTENT_EDIT, {
                contentId,
              }),
            ),
          ),
        }),
      );
    }
  };
}
