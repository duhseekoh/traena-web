import reducer from './reducer';
import { initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import { USER_FORM_CLEAR } from './actions';

describe('ui-user-form reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      uiLoading: initialUiLoadingState,
    });
  });

  it('should handle USER_FORM_CLEAR', () => {
    expect(
      reducer(
        {},
        {
          type: USER_FORM_CLEAR,
        },
      ),
    ).toEqual({
      uiLoading: initialUiLoadingState,
    });
  });
});
