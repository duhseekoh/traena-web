// @flow
import { USER_FORM_CLEAR, uiLoadingActionTypes } from './actions';
import type { Action } from '../../rootReducer';
import { createUiLoadingReducer, initialUiLoadingState } from '../../../utils/redux-ui-loading/reducer';
import type { ReduxStateUiLoading } from '../../../utils/redux-ui-loading/types';

const uiLoadingReducer = createUiLoadingReducer('uiUserForm');

export type ReduxStateUiUserForm = {
  uiLoading: ReduxStateUiLoading,
};

const initialState = {
  uiLoading: initialUiLoadingState,
};

function uiUserForm(
  state: ReduxStateUiUserForm = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case USER_FORM_CLEAR: {
      return {
        ...initialState,
      };
    }

    case uiLoadingActionTypes.setLoading:
    case uiLoadingActionTypes.setSaving:
    case uiLoadingActionTypes.setErrorLoading:
    case uiLoadingActionTypes.setErrorSaving:
      return {
        ...state,
        uiLoading: uiLoadingReducer(state.uiLoading, { type, payload }),
      };

    default:
      return state;
  }
}

export default uiUserForm;
