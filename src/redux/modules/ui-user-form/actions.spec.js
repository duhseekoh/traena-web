import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import {
  userSaved,
  USER_SAVED,
  clearUserForm,
  USER_FORM_CLEAR,
} from './actions';

jest.mock('../../api/content');

describe('content actions', () => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore({});

  afterEach(() => {
    store.clearActions();
  });

  it('should dispatch USER_SAVED', () => {
    const payload = { user_id: 1, name: 'name' };
    store.dispatch(userSaved(payload));

    const actions = store.getActions();
    expect(actions).toEqual([{ type: USER_SAVED, payload }]);
  });

  it('should dispatch USER_FORM_CLEAR', () => {
    store.dispatch(clearUserForm());

    const actions = store.getActions();
    expect(actions).toEqual([{ type: USER_FORM_CLEAR }]);
  });
});
