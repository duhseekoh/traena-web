import { createSelector } from 'reselect';
import { createUiLoadingSelectors } from '../../../utils/redux-ui-loading/selectors';

// export const selectUserById = (state, props) => state.users.usersById[props.userId] || null;

export const selectUserById = id =>
  createSelector(
    [state => state.users.usersById],
    usersById => usersById[id] || null,
  );

export const selectUserFullNameById = id =>
  createSelector(
    [selectUserById(id)],
    user => (user ? `${user.firstName} ${user.lastName}` : null),
  );

// export const selectIsSaving = state => state.uiUserForm.isSaving;


export const {
  selectIsLoading,
  selectIsSaving,
  selectErrorLoading,
} = createUiLoadingSelectors('uiUserForm');
