// @flow
import _ from 'lodash';
import { push } from 'connected-react-router';
import { SubmissionError, change } from 'redux-form';
import { getRouteLink, routes } from '../../../constants/routes';
import { SERVER_ERROR_TYPES } from '../../../constants/appConstants';
import * as applicationActions from '../application/actions';
import { createUiLoadingActions } from '../../../utils/redux-ui-loading/actions';
import * as userApi from '../../api/user';
import type { Dispatch, GetState } from '../../rootReducer';
import * as fromUiUserForm from './selectors';

export const USER_SAVED = 'USER/SAVED';
export const USER_FORM_CLEAR = 'USER/FORM_CLEAR';
export const USER_FETCH_SUCCESS = 'USER/FETCH_SUCCESS';

export function userSaved(user: Object) {
  return {
    type: USER_SAVED,
    payload: user,
  };
}

export function clearUserForm() {
  return {
    type: USER_FORM_CLEAR,
    payload: undefined, // to satisfy flow
  };
}

export function userFetchSuccess(user: Object) {
  return {
    type: USER_FETCH_SUCCESS,
    payload: user,
  };
}

export const {
  uiLoadingActionTypes,
  setLoading,
  setSaving,
} = createUiLoadingActions('uiUserForm');

/**
 * Given the user object created by redux form, spit out a user object that can be sent
 * via api.
 * TODO - come up with something more robost. potentially use classes to do transformation.
 * TODO - use flow to add types for the form vals and user object
 * @param userFormVals redux form object
 */
function convertFormValsToUser(userFormVals, organizationId): Object {
  const user = {
    ..._.pick(userFormVals, [
      'email',
      'firstName',
      'lastName',
      'position',
      'tags',
      'password',
      'id',
    ]),
    organizationId,
  };
  return user;
}

/**
 * Reset the form, notify the user its published, and go back to the routes page
 * @param message Message describing the action taken upon the user
 * @param user User object returned from API
 */
function submitUserFormSuccess(message: string, user: Object) {
  return dispatch => {
    dispatch(userSaved(user));
    dispatch(setSaving(false));
    dispatch(clearUserForm());
    dispatch(
      applicationActions.createNotification({
        message: `${message} '${user.firstName} ${user.lastName}'`,
        title: 'Success!',
        onDismiss: () => {
          dispatch(push(getRouteLink(routes.USERS)));
        },
      }),
    );
  };
}

/**
 * Used to notify user in the UI that their attempt to create or save a user has failed.
 * @param message Message describing the action that was taken which failed
 * @param err Error returned from API
 */
function submitUserFormFailure(message, err) {
  return (dispatch: Dispatch) => {
    dispatch(setSaving(false));
    // For known errors we display the message from the server on the UI.
    const serverErrorMessage =
      err.body.type === SERVER_ERROR_TYPES.ConflictError
        ? err.body.message
        : '';
    throw new SubmissionError({
      _error: `${message} ${serverErrorMessage}`,
    });
  };
}

export function fetchUser(userId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setLoading(true));
      const user = await userApi.getUser(userId);
      dispatch(userFetchSuccess(user));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        applicationActions.createNotification({
          message: 'There was an issue with trying to edit that user.',
          title: 'Oops!',
          onDismiss: () => {
            dispatch(push(getRouteLink(routes.USERS)));
          },
        }),
      );
    }
  };
}

/**
 * @param userFormVals redux form object
 * @return {Promise<undefined>}
 */
export function createUserFromForm(
  userFormVals: Object,
  organizationId: number,
) {
  return async (dispatch: Dispatch) => {
    const user = convertFormValsToUser(userFormVals, organizationId);
    dispatch(setSaving(true));
    try {
      const createdUser = await userApi.createUser(user);
      dispatch(
        submitUserFormSuccess('You just created an account for', createdUser),
      );
      return createdUser;
    } catch (err) {
      dispatch(
        submitUserFormFailure(
          'There was an issue creating the user. Please try again.',
          err,
        ),
      );
      return null;
    }
  };
}

export function editUserFromForm(userFormVals: Object) {
  return async (dispatch: Dispatch) => {
    const user = convertFormValsToUser(userFormVals);
    dispatch(setSaving(true));
    try {
      const editedUser = await userApi.editUser(user);
      dispatch(
        submitUserFormSuccess('You just edited the account for', editedUser),
      );
      return editedUser;
    } catch (err) {
      dispatch(
        submitUserFormFailure(
          'There was an issue editing the user. Please try again.',
          err,
        ),
      );
      return null;
    }
  };
}

export function enableUser(userId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    try {
      const user = fromUiUserForm.selectUserById(userId)(getState()) || {};
      const enabledUser = {
        ...user,
        isDisabled: false,
      };
      dispatch(setSaving(true));
      const result = await userApi.editUser(enabledUser);
      dispatch(fetchUser(userId));
      dispatch(setSaving(false));
      return result;
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(
        submitUserFormFailure(
          'There was an issue enabling this user. Please try again.',
          err,
        ),
      );
      dispatch(change('user', 'isDisabled', false, false, true));
      return null;
    }
  };
}

export function disableUser(userId: number) {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(setSaving(true));
      const disabledUser = await userApi.disableUser(userId);
      dispatch(fetchUser(userId));
      dispatch(setSaving(false));
      return disabledUser;
    } catch (err) {
      dispatch(setSaving(false));
      dispatch(
        submitUserFormFailure(
          'There was an issue disabling this user. Please try again.',
          err,
        ),
      );
      dispatch(change('user', 'isDisabled', true, false, true));
      return null;
    }
  };
}
