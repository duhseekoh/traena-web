// @flow
import type { Organization } from '../../../types/organizations';

export const ORGANIZATIONS_FETCH_SUCCESS =
  'ORGANIZATIONS/LOAD_SUCCESS';

export function organizationsFetchSuccess(
  organizations: { [organizationId: number]: Organization },
) {
  return {
    type: ORGANIZATIONS_FETCH_SUCCESS,
    payload: organizations,
  };
}
