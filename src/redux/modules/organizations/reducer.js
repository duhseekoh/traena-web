// @flow
import { ORGANIZATIONS_FETCH_SUCCESS } from './actions';
import type { Action } from '../../rootReducer';
import type { Organization } from '../../../types/organizations';

export type ReduxStateOrganizations = {
  organizationsById: {
    [organizationId: number]: Organization,
  },
};

const initialState = {
  organizationsById: {},
};

function organizationsReducer(
  state: ReduxStateOrganizations = initialState,
  { type, payload }: Action,
) {
  switch (type) {
    case ORGANIZATIONS_FETCH_SUCCESS: {
      return {
        ...state,
        organizationsById: {
          ...state.organizationsById,
          ...payload,
        },
      };
    }

    default:
      return state;
  }
}

export default organizationsReducer;
