// @flow
/* eslint-disable import/prefer-default-export */

import type { ReduxState } from '../../rootReducer';

export const selectOrganizationsById =
  (state: ReduxState) => state.organizations.organizationsById;
