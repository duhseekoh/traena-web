/* eslint-disable import/prefer-default-export */
// @flow

export function getAvailableRoles(): Promise<Object> {
  const endpoint = '/roles';
  return fetch(endpoint);
}
