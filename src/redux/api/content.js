// TODO add typing

import * as httpUtils from '../../utils/http';

export function getContents() {
  const endpoint = '/users/self/feed';
  return fetch(endpoint);
}

export function getContentForOrganization(organizationId, options) {
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');
  const endpoint = `/organizations/${organizationId}/content?${urlParameters}&indexed=true`;
  return fetch(endpoint);
}

export function getContentItem(contentId) {
  const endpoint = `/contents/${contentId}`;
  return fetch(endpoint);
}

export function createContentItem(content) {
  const endpoint = '/contents';
  return fetch(endpoint, {
    method: 'POST',
    body: content,
  });
}

export function editContentItem(content) {
  const endpoint = `/contents/${content.id}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: content,
  });
}

export function deleteContentItem(contentId) {
  const endpoint = `/contents/${contentId}`;
  return fetch(endpoint, {
    method: 'DELETE',
  });
}

export function getSignedS3VideoUploadUrl(fileName) {
  const endpoint = `/media/upload-url/?fileName=${fileName}&mediaType=video`;
  return fetch(endpoint, {
    method: 'POST',
  });
}

// promise resolves to { uploadUrl, fileName, id }
export function getSignedS3ImageUploadUrl(fileName) {
  const endpoint = `/media/upload-url?fileName=${fileName}&mediaType=image`;
  return fetch(endpoint, {
    method: 'POST',
  });
}

/**
 * @param s3Url Presigned S3 URL; we don't need the aws sdk on the client to authorize
 * @param file File object from a file uploader
 * @param contentType Content type header for the fetch call
 * @param onProgress Callback fires after each xhr progress update
 */
export function uploadFileToS3(s3Url, file, contentType, onProgress) {
  return httpUtils.fetchWithProgress(
    s3Url,
    {
      method: 'PUT',
      body: file,
      headers: {
        // Content-Type must match the ContentType set when creating the signed url (from api)
        'Content-Type': contentType,
      },
    },
    onProgress,
  );
}

// TODO add return value typing
export function transcodeUploadedFile(fileName) {
  const endpoint = `/media/transcoder-jobs/?fileName=${fileName}`;
  return fetch(endpoint, {
    method: 'POST',
  });
}

// TODO add typing
export function checkTranscodingProgress(transcodeId: number) {
  const endpoint = `/media/transcoder-jobs/${transcodeId}`;
  return fetch(endpoint);
}

/**
 * Kicks off image processing, waits for it, and returns the processed image info
 * @param fileName corresponds to the temporary image that was uploaded
 */
export function processImage(fileName) {
  const endpoint = `/media/image-processor-job?fileName=${fileName}`;
  return fetch(endpoint, {
    method: 'POST',
  });
}

export function getAggregateStatsForContentItem(contentId) {
  const endpoint = `/contents/${contentId}/analytics`;
  return fetch(endpoint);
}

export function getEventsPageForContentItem(contentId, page = 0) {
  const endpoint = `/contents/${contentId}/analytics/feed?page=${page}`;
  return fetch(endpoint);
}

export function getUserStatsForContentItem(contentId, page = 0) {
  const endpoint = `/contents/${contentId}/analytics/users?page=${page}`;
  return fetch(endpoint);
}
