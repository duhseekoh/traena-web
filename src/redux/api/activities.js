import type { Activity, ActivityResults } from '../../types/activities';

/**
 * Create an activity for a given contentItem.
 * @param contentId ID of the contentItem for which we want to fetch activities
 * @param activity JSON body of the activity to create
 */
export function createActivity(
  contentId: number,
  activity: Activity,
): Promise<Activity> {
  const endpoint = `/contents/${contentId}/activities`;
  return fetch(endpoint, {
    method: 'POST',
    body: activity,
  });
}

/**
 * Get an activity for a given contentItem.
 * @param contentId ID of the contentItem for which we want to fetch activities
 * @param activityId JSON body of the activity to create
 */
export function getActivity(
  contentId: number,
  activityId: number,
): Promise<Activity> {
  const endpoint = `/contents/${contentId}/activities/${activityId}`;
  return fetch(endpoint);
}

/**
 * Update an activity for a given contentItem.
 * @param contentId ID of the contentItem for which we want to fetch activities
 * @param activity JSON body of the activity to create
 */
export function updateActivity(
  contentId: number,
  activity: Activity,
): Promise<Activity> {
  const endpoint = `/contents/${contentId}/activities/${activity.id}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: activity,
  });
}

/**
 * Retrieve a list of activities for a given contentItem.
 * @param contentId ID of the contentItem for which we want to fetch activities
 */
export function getContentActivities(contentId: number): Promise<Object> {
  const endpoint = `/contents/${contentId}/activities?indexed=true`;
  return fetch(endpoint);
}

/**
 * Delete an activity by activityId for a given contentItem.
 * @param contentId ID of the contentItem for which we want to delete an activity
 * @param activityId ID of the activity we want to delete
 */
export function deleteActivity(
  contentId: number,
  activityId: number,
): Promise<void> {
  const endpoint = `/contents/${contentId}/activities/${activityId}`;
  return fetch(endpoint, {
    method: 'DELETE',
  });
}

/**
 * Get results for an activity
 * @param contentId ID of the contentItem the activity belongs to
 * @param activityId ID of the activity for which we want results
 */
export function getActivityResults(
  activityId: number,
  contentId: number,
): Promise<ActivityResults> {
  const endpoint = `/contents/${contentId}/activities/${activityId}/results`;
  return fetch(endpoint);
}
