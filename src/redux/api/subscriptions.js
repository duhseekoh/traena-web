/*
* TODO: Fix flow in this file.  Flow wont accept `Subscription` type as
* the `body` parameter for `fetch()`.
*/
import { normalize } from 'normalizr';
import { subscriptionEntity } from './schemas/schemas';
import type { Subscription } from '../../types/subscriptions';
import type { ArrayPage } from '../../types';

export async function getSubscription(
  organizationId: number,
  channelId: number,
): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/subscriptions/${channelId}`;
  return fetch(endpoint);
}

export async function updateSubscription(
  organizationId: number,
  subscription: Subscription,
): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/subscriptions/${subscription.channelId}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: subscription,
  });
}

export async function getChannelSubscriptionsForOrg(
  organizationId: number,
  options: Object,
): Promise<Object> {
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');

  const endpoint = `/organizations/${organizationId}/subscriptions?${urlParameters}`;
  const results = ((await fetch(endpoint): any): ArrayPage<Subscription>);
  const normalizedResults = normalize(results.content, [subscriptionEntity]);
  return {
    channels: {},
    subscriptions: {},
    ...normalizedResults.entities,
    pagination: results,
    ids: normalizedResults.result,
  };
}
