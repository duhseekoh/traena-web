// @flow
import type { ContentItem } from '../../types/content';
import type { Series } from '../../types/series';
import type { SeriesAggregateStats } from '../../types/analytics';
import type { IndexedPage, ArrayPage } from '../../types';

/**
 * Create a series
 * @param {Series} series JSON body of the activity to create
 */
export function createSeries(series: Series): Promise<Series> {
  const endpoint = '/series';
  return fetch(endpoint, {
    method: 'POST',
    body: series,
  });
}

/**
 * Retrieve a series
 * @param {number} seriesId ID of the series we want to fetch
 */
export function getSeries(seriesId: number): Promise<Series> {
  const endpoint = `/series/${seriesId}`;
  return fetch(endpoint);
}

/**
 * Update a series
 * @param {Series} series JSON body of the series to update
 */
export function updateSeries(series: Series): Promise<Series> {
  const endpoint = `/series/${series.id}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: series,
  });
}

/**
 * Retrieve a list of series for a given channel.
 * @param {number} channelId ID of the channel for which we want to fetch series
 */
export function getSeriesForChannel(channelId: number): Promise<Object> {
  const endpoint = `/channels/${channelId}/series`;
  return fetch(endpoint);
}

/**
 * Retrieve a list of content for a given series.
 * @param {number} seriesId ID of the series for which we want to fetch content
 */
export async function getContentForSeries(
  seriesId: number,
): Promise<IndexedPage<ContentItem>> {
  const endpoint = `/series/${seriesId}/content?indexed=true&size=100`;
  const page = ((await fetch(endpoint): any): IndexedPage<ContentItem>);
  return page;
}

/**
 * Retrieve a list of series for a given organization.
 * @param {number} organizationId ID of the organization for which we want to fetch series
 */
export async function getSeriesForOrganization(
  organizationId: number,
  options: Object,
): Promise<IndexedPage<Series>> {
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');
  const endpoint = `/organizations/${organizationId}/subscribedseries?indexed=true&${urlParameters}`;
  const result = ((await fetch(endpoint): any): IndexedPage<Series>);
  return result;
}

/**
 * Delete an series by seriesId
 * @param {number} seriesId ID of the series we want to delete
 */
export function deleteSeries(seriesId: number): Promise<any> {
  const endpoint = `/series/${seriesId}`;
  return fetch(endpoint, {
    method: 'DELETE',
  });
}

export async function getUserStatsForSeries(
  seriesId: number,
  options: Object,
): Promise<ArrayPage<Series>> {
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');
  const endpoint = `/series/${seriesId}/analytics/users?${urlParameters}`;
  const result = ((await fetch(endpoint): any): ArrayPage<Series>);
  return result;
}

export async function getAggregateStatsForSeries(
  seriesId: number,
): Promise<SeriesAggregateStats> {
  const endpoint = `/series/${seriesId}/analytics`;
  const result = ((await fetch(endpoint): any): SeriesAggregateStats);
  return result;
}
