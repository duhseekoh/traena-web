// @flow
import auth0, { type WebAuth } from 'auth0-js';
import { Auth0UnexpectedAuthResultError } from '../../../constants/errors';
import type { AuthConfig } from '../../../constants/appConstants';

export type AuthResult = {
  accessToken: ?string,
  idToken: ?string,
};

/**
 * Any auth0 communication will go through this client.
 */
class Auth0Client {
  _authConfig: AuthConfig;
  _webAuthClient: WebAuth;

  constructor(authConfig: AuthConfig) {
    if (!authConfig) {
      throw new Error('Cannot create Auth0Client. Must supply authConfig.');
    }
    this._authConfig = authConfig;
  }

  _getWebAuthClient() {
    if (!this._webAuthClient) {
      this._webAuthClient = new auth0.WebAuth({
        domain: this._authConfig.domain,
        clientID: this._authConfig.clientId,
        redirectUri: this._authConfig.redirectUri,
        audience: this._authConfig.audience,
        responseType: this._authConfig.responseType,
        scope: this._authConfig.scope,
      });
    }
    return this._webAuthClient;
  }

  showLogin() {
    const webAuthClient = this._getWebAuthClient();
    webAuthClient.authorize();
  }

  async getAuthenticationResult(): Promise<AuthResult> {
    const webAuthClient = this._getWebAuthClient();
    return new Promise((resolve, reject) => {
      webAuthClient.parseHash((err, authResult: AuthResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          resolve(authResult);
        } else if (err) {
          reject(err);
        } else {
          const error = new Auth0UnexpectedAuthResultError(
            'unexpected auth result',
            { authResult },
          );
          reject(error);
        }
      });
    });
  }

  async sendResetPasswordEmail(email: string) {
    const webAuthClient = this._getWebAuthClient();
    return new Promise((resolve, reject) => {
      webAuthClient.changePassword(
        {
          connection: this._authConfig.connection,
          email,
        },
        (err, response) => {
          if (err) {
            reject(err);
          } else {
            resolve(response);
          }
        },
      );
    });
  }
}

export default Auth0Client;
