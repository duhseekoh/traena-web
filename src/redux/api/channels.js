/*
* @TODO: Fix flow on this file. Flow thinks Promise<Response> is being returned by these methods
* but actually our interceptor makes them return the result of response.json().
*/
import type { IndexedPage } from '../../types';

export function createChannel(
  channel: Channel,
  organizationId: number,
): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/channels`;
  return fetch(endpoint, {
    method: 'POST',
    body: channel,
  });
}

export function getChannel(channelId: number): Promise<Object> {
  const endpoint = `/channels/${channelId}`;
  return fetch(endpoint);
}

/**
 * Retrieve a list of subscribed orgs for the channel.
 * @param channelId ID of the channel for which we want to fetch subscribed orgs
 */
export function getChannelSubscribedOrgs(
  organizationId: number,
  channelId: number,
  page: number = 0,
  size: number = 10): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/channels/${channelId}/subscriberorganizations?indexed=true&size=${size}&page=${page}`;
  return fetch(endpoint);
}

export function getChannelSubscribedUsers(
  organizationId: number,
  channelId: number,
  page: number = 0,
  size: number = 10,
): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/subscriptions/${channelId}/users?indexed=true&size=${size}&page=${page}`;
  return fetch(endpoint);
}

/**
 * Retrieve a list of subscribed channels for the user.
 * @param userId ID of the user for which we want to fetch subscriptions
 */
export function getUserSubscriptions(userId: number): Promise<Object> {
  const endpoint = `/users/${userId}/subscriptions?indexed=true&size=100`;
  return fetch(endpoint);
}

/**
 * Retrieve a list of subscribed channels for the currently logged in user.
 * Set to a size of 100 results so we don't have to make multiple calls to get
 * a users subscriptions. The subscription size should never reach that high.
 */
export function getSelfSubscribedChannels(): Promise<Object> {
  const endpoint = '/users/self/subscribedchannels?indexed=true&size=100';
  return fetch(endpoint);
}

/**
* Retrieve a list of content for a given channel
* @param {number} channelId the Id of the channel for which we want to return content
*/
export function getContentForChannel(
  channelId: number,
  options: { page: number, sort: string },
): Promise<IndexedPage> {
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');
  const endpoint = `/channels/${channelId}/content?indexed=true&size=20&${urlParameters}`;
  return fetch(endpoint);
}

/**
* Retrieve a list of channels for an organization
* @param {number} organizationId the ID of the organization for which we want channels
*/
export function getOrganizationChannels(
  organizationId: number): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/channels?indexed=true&size=100`;
  return fetch(endpoint);
}

export function editChannel(
  channel: Channel,
  organizationId: number,
): Promise<Object> {
  const endpoint = `/organizations/${organizationId}/channels/${channel.id}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: channel,
  });
}

/**
* Delete a channel by channelId
* @param {number} channelId ID of the channel we want to delete
*/
export function deleteChannel(
  channelId: number,
  organizationId: number,
): Promise<any> {
  const endpoint = `/organizations/${organizationId}/channels/${channelId}`;
  return fetch(endpoint, {
    method: 'DELETE',
  });
}
