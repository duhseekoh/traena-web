export type UserPage = {
  ids: number[],
  pagination: Object, // TODO - expand
};

export type GetUsersRequestOptions = {
  page: number,
  sort: any,
};

export function getUsersForOrganization(
  organizationId: number,
  { page, sort }: GetUsersRequestOptions,
): Promise<UserPage> {
  const options = { page, sort };
  const urlParameters = Object.keys(options)
    .map(i => `${i}=${options[i]}`)
    .join('&');
  const endpoint = `/organizations/${organizationId}/users?${urlParameters}&indexed=true`;
  // TODO unfortunately, the fetch interceptor calls response.json(), but flow does not know that.
  // flow thinks a Response is being returned here, but really it's the result of response.json()
  // which would be an object that we can duck type to anything. moving over to the new
  // redux-fetch-middleware package or take the fetch interceptor logic and moving it to a
  // different method we use in place of fetch.
  // in the meantime, this casting trick works
  return fetch(endpoint).then((fetchInterceptResp: any) => fetchInterceptResp);
}

export function createUser(user: Object): Promise<Object> {
  const endpoint = '/users';
  return fetch(endpoint, {
    method: 'POST',
    body: user,
  }).then((fetchInterceptResp: any) => fetchInterceptResp);
}

export function editUser(user: Object): Promise<Object> {
  const endpoint = `/users/${user.id}`;
  return fetch(endpoint, {
    method: 'PUT',
    body: user,
  });
}

export function getUser(userId: number): Promise<Object> {
  const endpoint = `/users/${userId}`;
  return fetch(endpoint);
}

export function getUserRoles(userId: number): Promise<Object> {
  const endpoint = `/users/${userId}/roles`;
  return fetch(endpoint);
}

export function addRoleToUser(userId: number, role: Object): Promise<Object> {
  const endpoint = `/users/${userId}/roles`;
  return fetch(endpoint, {
    method: 'PATCH',
    body: [role.id],
  });
}

export function removeRoleFromUser(
  userId: number,
  role: Object,
): Promise<Object> {
  const endpoint = `/users/${userId}/roles`;
  return fetch(endpoint, {
    method: 'DELETE',
    body: [role.id],
  });
}

export function disableUser(userId: number): Promise<Object> {
  const endpoint = `/users/${userId}`;
  return fetch(endpoint, {
    method: 'DELETE',
  });
}
