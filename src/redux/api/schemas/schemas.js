import { schema } from 'normalizr';

export const channelEntity = new schema.Entity('channels');
export const subscriptionEntity = new schema.Entity(
  'subscriptions',
  { channel: channelEntity },
  {
    idAttribute: (value) => value.channelId,
  });
