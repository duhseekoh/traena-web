// @flow
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import auth, { type ReduxStateAuth } from './modules/auth/reducer';
import application, { type ReduxStateApplication } from './modules/application/reducer';
import content, { type ReduxStateContent } from './modules/content/reducer';
import organizations, { type ReduxStateOrganizations } from './modules/organizations/reducer';
import uiSubscriptionTable, {
  type ReduxStateUiSubscriptionTable,
} from './modules/ui-subscriptions-table/reducer';
import uiContentTable, {
  type ReduxStateUiContentTable,
} from './modules/ui-content-table/reducer';
import uiContentForm, { type ReduxStateUiContentForm } from './modules/ui-content-form/reducer';
import uiContentView, { type ReduxStateUiContentView } from './modules/ui-content-view/reducer';
import uiContentDetail, { type ReduxStateUiContentDetail } from './modules/ui-content-detail/reducer';
import uiSeriesForm, { type ReduxStateUiSeriesForm } from './modules/ui-series-form/reducer';
import uiChannelForm, { type ReduxStateUiChannelForm } from './modules/ui-channel-form/reducer';
import users, { type ReduxStateUsers } from './modules/users/reducer';
import seriesReducer, { type ReduxStateSeries } from './modules/series/reducer';
import uiSeriesDetail, { type ReduxStateUiSeriesDetail } from './modules/ui-series-detail/reducer';
import uiSeriesContentTable, { type ReduxStateUiSeriesContentTable } from './modules/ui-series-content-table/reducer';
import uiSeriesTable, { type ReduxStateUiSeriesTable } from './modules/ui-series-table/reducer';
import uiUsersTable, { type ReduxStateUiUsersTable } from './modules/ui-users-table/reducer';
import uiUserForm, { type ReduxStateUiUserForm } from './modules/ui-user-form/reducer';
import uiUserRolesForm, {
  type ReduxStateUiUserRolesForm,
} from './modules/ui-user-roles-form/reducer';
import channelsReducer, { type ReduxStateChannels } from './modules/channels/reducer';
import activitiesReducer, { type ReduxStateActivities } from './modules/activities/reducer';
import uiChannelContentTable, { type ReduxStateUiChannelContentTable } from './modules/ui-channel-content-table/reducer';
import uiSubscribedOrgsTable, { type ReduxStateUiSubscribedOrgsTable } from './modules/ui-subscribed-orgs-table/reducer';
import uiSubscriptionForm, { type ReduxStateUiSubscriptionForm } from './modules/ui-subscription-form/reducer';
import subscriptions, { type ReduxStateSubscriptions } from './modules/subscriptions/reducer';
import uiSubscribedUsersTable, { type ReduxStateUiSubscribedUsersTable } from './modules/ui-subscribed-users-table/reducer';
import activitiesResults, { type ReduxStateActivitiesResults } from './modules/activities-results/reducer';
import uiActivitiesForm, {
  type ReduxStateUiActivitiesForm,
} from './modules/ui-activities-form/reducer';

import uiContentItemAnalytics, { type ReduxStateUiContentItemAnalytics } from './modules/ui-content-item-analytics/reducer';
import contentItemAnalyticsFeed, { type ReduxStateContentItemAnalyticsFeed } from './modules/content-item-analytics-feed/reducer';
import contentItemAnalyticsUsers, { type ReduxStateContentItemAnalyticsUsers } from './modules/content-item-analytics-users/reducer';
import contentItemAnalyticsStats, { type ReduxStateContentItemAnalyticsStats } from './modules/content-item-analytics-stats/reducer';
import uiFixedFooter, { type ReduxStateUiFixedFooter } from './modules/ui-fixed-footer/reducer';
import uiSeriesStats, { type ReduxStateUiSeriesStats } from './modules/ui-series-stats/reducer';
import seriesStatsUsers, { type ReduxStateSeriesStatsUsers } from './modules/series-stats-users/reducer';
import seriesStats, { type ReduxStateSeriesStats } from './modules/series-stats/reducer';

import resetPassword, { type ReduxStateResetPassword } from './modules/ui-reset-password/reducer';

export type ReduxState = {
  application: ReduxStateApplication,
  auth: ReduxStateAuth,
  users: ReduxStateUsers,
  content: ReduxStateContent,
  contentItemAnalyticsFeed: ReduxStateContentItemAnalyticsFeed,
  contentItemAnalyticsStats: ReduxStateContentItemAnalyticsStats,
  contentItemAnalyticsUsers: ReduxStateContentItemAnalyticsUsers,
  uiContentTable: ReduxStateUiContentTable,
  uiUsersTable: ReduxStateUiUsersTable,
  uiContentForm: ReduxStateUiContentForm,
  uiContentView: ReduxStateUiContentView,
  uiContentDetail: ReduxStateUiContentDetail,
  uiUserForm: ReduxStateUiUserForm,
  uiUserRolesForm: ReduxStateUiUserRolesForm,
  form: formReducer,
  channels: ReduxStateChannels,
  activities: ReduxStateActivities,
  uiActivitiesForm: ReduxStateUiActivitiesForm,
  uiSeriesForm: ReduxStateUiSeriesForm,
  uiSeriesContentTable: ReduxStateUiSeriesContentTable,
  uiSubscriptionTable: ReduxStateUiSubscriptionTable,
  uiChannelContentTable: ReduxStateUiChannelContentTable,
  uiSubscribedOrgsTable: ReduxStateUiSubscribedOrgsTable,
  uiSubscribedUsersTable: ReduxStateUiSubscribedUsersTable,
  series: ReduxStateSeries,
  uiSeriesDetail: ReduxStateUiSeriesDetail,
  subscriptions: ReduxStateSubscriptions,
  uiSeriesTable: ReduxStateUiSeriesTable,
  activitiesResults: ReduxStateActivitiesResults,
  uiContentItemAnalytics: ReduxStateUiContentItemAnalytics,
  uiFixedFooter: ReduxStateUiFixedFooter,
  uiChannelForm: ReduxStateUiChannelForm,
  organizations: ReduxStateOrganizations,
  uiSubscriptionForm: ReduxStateUiSubscriptionForm,
  uiSeriesStats: ReduxStateUiSeriesStats,
  seriesStatsUsers: ReduxStateSeriesStatsUsers,
  seriesStats: ReduxStateSeriesStats,
  resetPassword: ReduxStateResetPassword,
};

// TODO - after we move everything over to a /redux directory, this can live in there
export type Action = {
  +type: string,
  payload: any,
  meta?: ?Object,
};
export type Dispatch = (action: Action | ThunkAction | PromiseAction) => any; // eslint-disable-line
export type GetState = () => ReduxState;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;

const reducers = combineReducers({
  auth,
  application,
  activities: activitiesReducer,
  activitiesResults,
  channels: channelsReducer,
  content,
  form: formReducer,
  organizations,
  resetPassword,
  series: seriesReducer,
  seriesStats,
  seriesStatsUsers,
  subscriptions,
  users,
  contentItemAnalyticsFeed,
  contentItemAnalyticsStats,
  contentItemAnalyticsUsers,
  uiActivitiesForm,
  uiChannelContentTable,
  uiChannelForm,
  uiContentItemAnalytics,
  uiContentForm,
  uiContentTable,
  uiContentView,
  uiContentDetail,
  uiFixedFooter,
  uiUserForm,
  uiUsersTable,
  uiUserRolesForm,
  uiSubscribedOrgsTable,
  uiSubscribedUsersTable,
  uiSubscriptionTable,
  uiSeriesTable,
  uiSeriesForm,
  uiSeriesStats,
  uiSubscriptionForm,
  uiSeriesDetail,
  uiSeriesContentTable,
});

export default reducers;
