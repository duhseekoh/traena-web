import { createStore, applyMiddleware, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunkMiddleware from 'redux-thunk';
import reducers from '../redux/rootReducer';
import * as localstore from '../utils/localstore';

export default function configureStore(history, initialState = {}) {
  initialState.auth = {
    idToken: localstore.get('idToken') || null,
    accessToken: localstore.get('accessToken') || null,
  };
  /* eslint-disable no-underscore-dangle,indent */
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          // Specify here name, actionsBlacklist, actionsCreators and other options
        })
      : compose;
  /* eslint-enable */

  const enhancers = composeEnhancers(
    applyMiddleware(thunkMiddleware, routerMiddleware(history)),
  );
  const store = createStore(
    connectRouter(history)(reducers),
    initialState,
    enhancers,
  );

  return store;
}
