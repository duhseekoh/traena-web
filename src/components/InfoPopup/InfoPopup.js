// @flow
import React from 'react';
import { Popup, Icon } from 'semantic-ui-react';
import styles from './InfoPopup.scss';

type Props = {
  header?: string | React.DOM.Node,
  content: string | React.DOM.Node,
};

/**
 * Display info popups across our app in a consistent manner.
 * Wraps a semantic-ui-react Popup and is used just like it but with specific
 * styling.
 */
const InfoPopup = (props: Props) => {
  const { header, content, ...restProps } = props;
  return (
    <Popup
      className={styles.popup}
      trigger={<Icon name="info circle" className={styles.triggerIcon} />}
      content={<div className={styles.content}>{content}</div>}
      header={header && (<div className={styles.header}>{header}</div>)}
      on="hover"
      basic
      {...restProps}
    />
  );
};

InfoPopup.defaultProps = {
  header: null,
};

export default InfoPopup;
