// @flow
import React from 'react';
import classnames from 'classnames';
import { Button } from 'semantic-ui-react';
import styles from './Pagination.scss';
import type { Pagination as PaginationType } from '../../types';

type Props = {
  pagination: PaginationType,
  goToPage: (page: number) => any,
  page: number,
};

const PaginationButton = (props: Props) => (
  <Button
    className={
      props.pagination.number === props.page
        ? classnames(styles.paginationButton, styles.active)
        : styles.paginationButton
    }
    onClick={(e) => {
      e.preventDefault();
      props.goToPage(props.page);
    }}
  >
    {props.page + 1}
  </Button>
);

export default PaginationButton;
