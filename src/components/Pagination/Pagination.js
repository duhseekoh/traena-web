// @flow
import React from 'react';
import { Icon } from 'semantic-ui-react';
import styles from './Pagination.scss';
import PaginationButton from './PaginationButton';
import type { Pagination as PaginationType } from '../../types';

type Props = {
  pagination: PaginationType,
  goToPage: (page: number) => any,
};

const Pagination = (props: Props) => {
  const { number, totalPages } = props.pagination;
  if (totalPages <= 1) {
    // don't need to show pagination if there is only 1 page
    return null;
  }
  const totalButtons = totalPages;

  const inLead = number <= 2;
  const inEnd = totalButtons > 3 && number >= totalButtons - 3;
  const hasMid = totalButtons > 6;
  const hasEnd = totalButtons > 3;
  const inMid = hasMid && !inLead && !inEnd;
  const isLeftMidEdge = inMid && number === 3;
  const isRightMidEdge = inMid && number === totalButtons - 4;
  const isMidEdge = isLeftMidEdge || isRightMidEdge;
  const isOuterEdge = hasMid && (number === 2 || number === totalButtons - 3);

  let leadButtons = 1;
  if (inLead || isLeftMidEdge) {
    leadButtons = Math.min(totalButtons, 3);
  } else if (inEnd && totalButtons === 5) {
    leadButtons = 2;
  }

  let endButtons = 1;
  if (inEnd || isRightMidEdge || (!hasMid && number === 2)) {
    endButtons = Math.min(totalButtons - leadButtons, 3);
  } else if (inEnd && totalButtons === 5) {
    endButtons = 2;
  }

  let midButtons = 0;
  if (inMid) {
    midButtons = Math.min(totalButtons - endButtons - leadButtons, 3);
  } else if (isOuterEdge) {
    midButtons = 1;
  }

  return (
    <div className={styles.tablePagination}>
      {number > 0 && (
        <button
          className={styles.pageNextButton}
          onClick={() => {
            props.goToPage(number - 1);
          }}
        >
          <Icon name="chevron left" /> Back
        </button>
      )}
      {leadButtons > 1 ? (
        [...Array(leadButtons).keys()].map(key => (
          <PaginationButton {...props} page={key} key={key} />
        ))
      ) : (
        <PaginationButton {...props} page={0} key={0} />
      )}
      {leadButtons === 1 &&
        (hasMid || totalButtons > 5) && (
          <Icon name="ellipsis horizontal" className={styles.ellipsis} />
        )}

      {isOuterEdge &&
        leadButtons > 1 && (
          <PaginationButton {...props} page={number + 1} key={number + 1} />
        )}

      {isOuterEdge &&
        endButtons > 1 && (
          <PaginationButton {...props} page={number - 1} key={number - 1} />
        )}

      {isLeftMidEdge &&
        [...Array(midButtons).keys()].map(key => (
          <PaginationButton {...props} page={number + key} key={number + key} />
        ))}

      {isRightMidEdge &&
        !isLeftMidEdge &&
        [...Array(midButtons).keys()]
          .reverse()
          .map(key => (
            <PaginationButton
              {...props}
              page={number - key}
              key={number - key}
            />
          ))}

      {inMid &&
        !isMidEdge &&
        [...Array(midButtons).keys()].map(key => (
          <PaginationButton
            {...props}
            page={number + (key - 1)}
            key={number + (key - 1)}
          />
        ))}

      {endButtons === 1 &&
        totalButtons > 5 && (
          <Icon name="ellipsis horizontal" className={styles.ellipsis} />
        )}
      {hasEnd &&
        [...Array(endButtons).keys()]
          .reverse()
          .map(key => (
            <PaginationButton
              {...props}
              page={totalPages - 1 - key}
              key={totalPages - 1 - key}
            />
          ))}

      {number < totalPages - 1 && (
        <button
          className={styles.pageNextButton}
          onClick={() => {
            props.goToPage(number + 1);
          }}
        >
          Next <Icon name="chevron right" />
        </button>
      )}
    </div>
  );
};

export default Pagination;
