// @flow
import React from 'react';
import styles from './LinkButton.scss';

type Props = {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => any,
  children: React.DOM.Node,
};

/**
 * A button that looks like a link!
 * Would use semantic if it had this concept, but it doesn't, yet.
 */
const LinkButton = ({ onClick, children, ...rest }: Props) => (
  <button className={styles.linkButton} onClick={onClick} type="button" {...rest}>
    {children}
  </button>
);

export default LinkButton;
