// @flow
import React from 'react';
import { connect } from 'react-redux';
import * as fromUiFixedFooter from '../../redux/modules/ui-fixed-footer/selectors';

/**
 * By wrapping a component in this HOC, the wrapped component will have bottom padding
 * to compensate for a fixed footer element wrapped in fixedFooter()
 *
 */

type Props = {
  children: React.DOM.Node[],
};

type ConnectedProps = {
  footerHeight: number,
};

const FixedFooterViewport = (props: Props & ConnectedProps) => (
  <div
    style={{
      paddingBottom: props.footerHeight,
    }}
  >
    {props.children}
  </div>
);

const mapStateToProps = state => ({
  footerHeight: fromUiFixedFooter.selectFooterHeight(state),
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FixedFooterViewport);
