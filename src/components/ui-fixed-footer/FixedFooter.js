// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as uiFixedFooterActions from '../../redux/modules/ui-fixed-footer/actions';
import styles from './fixedFooter.scss';

/**
 * By wrapping a component in this HOC, the wrapped component will become a fixed footer
 * and keep updated a `ui-fixed-footer` redux state, with measurements of the viewport height
 * and the current height of the footer.
 *
 */

type Props = {
  updateFooterHeight: (number) => any,
  children: React.DOM.Node[]
}


class FixedFooter extends Component<Props> {
  constructor(props) {
    super(props);
    this.throttledUpdateWindowDimensions =
      _.throttle(this.updateWindowDimensions, 300);
  }

  componentDidMount() {
    setTimeout(() => {
      this.updateWindowDimensions();
    }, 0);
    window.addEventListener('resize', this.throttledUpdateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.throttledUpdateWindowDimensions);
    this.props.updateFooterHeight(0);
  }

  throttledUpdateWindowDimensions;
  divElement;

  updateWindowDimensions = () => {
    if (this.divElement) {
      const footerHeight = this.divElement.clientHeight;
      this.props.updateFooterHeight(footerHeight);
    }
  }

  render() {
    return (
      <div
        className={styles.fixedFooter}
        ref={(divElement) => this.divElement = divElement} // eslint-disable-line
      >
        {this.props.children}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  updateFooterHeight: (footerHeight) =>
    dispatch(uiFixedFooterActions.updateFooterHeight(
      footerHeight,
    )),
});

export default connect(null, mapDispatchToProps)(FixedFooter);
