import React from 'react';
import classnames from 'classnames';
import { Table } from 'semantic-ui-react';
import styles from './TableRow.scss';

type Props = {
  className: string,
  children: React.DOM.Node,
  onClick: any => any
}

const TableRow = (props: Props) => (
  <Table.Row
    className={
      props.onClick ?
      classnames(styles.clickableRow, props.className) :
      props.className
    }
    {...props}
  >
    {props.children}
  </Table.Row>
);

export default TableRow;
