// @flow
import React from 'react';
import classnames from 'classnames';
import { Table } from 'semantic-ui-react';
import type { SortDirectionsLong } from '../../utils/redux-sort/types';
import styles from './TableHeaderCell.scss';

type Props = {
  children?: React.DOM.Node,
  className?: ?string,
  getSortDirection?: ?(property: string) => ?SortDirectionsLong,
  // Property passed around when interacting with sort feature.
  // When not provided, cell will not appear sortable.
  property?: string,
  sortTable?: ?(property: string) => any,
};

/**
 * Our encapsulation of a semantic Table.HeaderCell.
 *
 * Provides additional styling to keep our table headers consistent across the app
 * and creates some conveniences for sorting ability.
 */
const TableHeaderCell = (props: Props) => {
  const { property, getSortDirection, sortTable, children, className } = props;
  const direction =
    getSortDirection && property ? getSortDirection(property) : null;

  const classes = classnames(className, {
    [styles.unsortable]: !property,
  });

  const handleClick = sortTable && property ? () => sortTable(property) : null;

  return (
    <Table.HeaderCell
      {...props}
      onClick={handleClick}
      sorted={direction}
      className={classes}
    >
      {children}
    </Table.HeaderCell>
  );
};

TableHeaderCell.defaultProps = {
  children: null,
  sortTable: null,
  getSortDirection: null,
  property: '',
  className: null,
};

export default TableHeaderCell;
