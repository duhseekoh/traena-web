import React from 'react';
import styles from './TableItemCount.scss';

type Props = any;

/**
 * A count of the total number of items in a table. Display next to the small
 * header describing the table.
 * Introduced in the series stats designs.
 */
const TableItemCount = (props: Props) => (
  <span className={styles.text} {...props}>{props.children}</span>
);

export default TableItemCount;
