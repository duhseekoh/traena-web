// @flow
import React from 'react';
import { Icon } from 'semantic-ui-react';
import styles from './UploadSuccessIndicator.scss';

type Props = {
  message: string,
};

const UploadSuccessIndicator = (props: Props) => (
  <div className={styles.uploadSuccessIndicator}>
    <Icon name="check" /> <span>{props.message}</span>
  </div>
);

export default UploadSuccessIndicator;
