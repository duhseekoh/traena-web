// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import { Input, Label } from 'semantic-ui-react';
import { fieldPropTypes } from 'redux-form';
import styles from './TagsInput.scss';
import TagsList from '../TagsList/TagsList';

type Props = {
  meta: fieldPropTypes.meta,
  className: string,
  input: fieldPropTypes.input,
};

type State = {
  inputValue: string,
};

class TagsInput extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      inputValue: this.props.input.value.join(' '),
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.input.value !== nextProps.input.value) {
      this.setState({
        inputValue: nextProps.input.value.join(' '),
      });
    }
  }

  handleInputChange = (e: { target: { value: string } }) => {
    const withoutSpecialChars = e.target.value.replace(/[^a-zA-Z0-9\s]/gi, '');
    this.setState(
      {
        inputValue: withoutSpecialChars,
      },
      this.buildTagArrayFromInputValue,
    );
  };

  buildTagArrayFromInputValue = () => {
    const newValues = _.uniq(_.compact(this.state.inputValue.split(' ')));
    this.props.input.onChange(newValues);
  };

  render() {
    const { meta, className, input } = this.props;
    return (
      <div className={styles.tagsInput}>
        <Input
          className={className}
          onChange={this.handleInputChange}
          value={this.state.inputValue}
          placeholder="Add a list of tags (i.e. salesmeeting)..."
        />
        {meta.error && (
          <Label pointing basic color="red">
            {meta.error}
          </Label>
        )}
        {input.value.length > 0 && <TagsList tags={input.value} />}
      </div>
    );
  }
}

export default TagsInput;
