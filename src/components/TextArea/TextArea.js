// @flow
import React from 'react';
import { fieldPropTypes } from 'redux-form';

type Props = {
  input: fieldPropTypes.input,
};

const TextArea = (props: Props) => {
  const { input, ...restProps } = props; // input is not a valid prop on textarea
  return (
    <textarea
      {...restProps}
      value={props.input.value}
      onChange={props.input.onChange}
    />
  );
};

export default TextArea;
