/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';
import styles from './AppBarAdmin.scss';

type Props = {|
  to: string,
  children: React.DOM.Node,
  className?: string,
|};

const AppBarLink = (props: Props) => (
  <NavLink
    to={props.to}
    className={classnames(styles.appBarLink, props.className)}
    activeClassName={styles.appBarLinkActive}
  >
    {props.children}
  </NavLink>
);

AppBarLink.defaultProps = {
  className: '',
};

export default AppBarLink;
