/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import styles from './AppBar.scss';
import Logo from '../../images/traena-logos-green-traena-t.svg';
import * as fromAuth from '../../redux/modules/auth/selectors';

type OwnProps = {};

type ConnectedProps = {
  isAuthenticated: boolean,
};

const AppBar = (props: OwnProps & ConnectedProps) => (
  <div>
    <div className={styles.appBar}>
      <img
        src={Logo}
        className={styles.appBarLogo}
        width={80}
        alt="Traena logo"
      />
      <span>
        {props.isAuthenticated ? (
          <Link
            as={Button}
            to={getRouteLink(routes.LOGOUT)}
            className={styles.appBarLink}
          >
            LOGOUT
          </Link>
        ) : (
          <Link
            as="a"
            to={getRouteLink(routes.LOGIN)}
            className={styles.appBarLink}
          >
            LOGIN
          </Link>
        )}
      </span>
    </div>
  </div>
);

const mapStateToProps = (state): ConnectedProps => ({
  isAuthenticated: fromAuth.selectIsUserAuthenticated(state),
});

export default withRouter(connect(mapStateToProps)(AppBar));
