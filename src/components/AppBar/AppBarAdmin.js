/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow
import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Dropdown, Button, Icon, Grid } from 'semantic-ui-react';
import { routes, getRouteLink } from '../../constants/routes';
import styles from './AppBarAdmin.scss';
import AppBarLink from './AppBarLink';
import Logo from '../../images/traena-t-white.svg';
import * as fromAuth from '../../redux/modules/auth/selectors';
import { PERMISSIONS } from '../../constants/permissions';
import AccessControl from '../../components/AccessControl/AccessControl';
import type { AuthUser } from '../../types/users';

type OwnProps = {
  // If needed, react-router Route passes in a few props (https://reacttraining.com/react-router/web/api/Route/route-props)
};

type ConnectedProps = {
  isAuthenticated: boolean,
  authUser: ?AuthUser,
};

const AppBarAdmin = ({
  authUser,
  isAuthenticated,
}: OwnProps & ConnectedProps) => (
  <div
    className={classnames(styles.appBar, {
      [styles.isHidden]: !isAuthenticated,
    })}
  >
    <Grid stretched>
      <div className={styles.appBarContent}>
        <div className={styles.appBarLogoHolder}>
          <img
            src={Logo}
            className={styles.appBarLogo}
            width={32}
            alt="Traena logo"
          />
        </div>
        {isAuthenticated &&
          authUser && (
            <div className={styles.appBarButtons}>
              <AccessControl
                allowedPermissions={[PERMISSIONS.CONTENT.MODIFY.SELF]}
                noAccessView={<span />}
              >
                <AppBarLink to={getRouteLink(routes.CONTENT)}>
                  POSTS
                </AppBarLink>
              </AccessControl>
              <AccessControl
                allowedPermissions={[PERMISSIONS.ORGANIZATIONS.MODIFY.ORG]}
                noAccessView={<span />}
              >
                <AppBarLink to={getRouteLink(routes.SERIES)}>
                  SERIES
                </AppBarLink>
              </AccessControl>
              <AccessControl
                allowedPermissions={[PERMISSIONS.USERS.MODIFY.ORG]}
                noAccessView={<span />}
              >
                <AppBarLink to={getRouteLink(routes.USERS)}>
                  USERS
                </AppBarLink>
              </AccessControl>
              <AccessControl
                allowedPermissions={[PERMISSIONS.ORGANIZATIONS.MODIFY.ORG]}
                noAccessView={<span />}
              >
                <AppBarLink
                  to={getRouteLink(routes.CHANNELS)}
                >
                  CHANNELS
                </AppBarLink>
              </AccessControl>

              <Dropdown
                icon={null}
                trigger={
                  <div
                    className={classnames(
                      styles.appBarButton,
                      styles.appBarButtonProfile,
                    )}
                  >
                    <span className={styles.nameWrapper}>
                      {`${authUser.details.firstName} ${
                        authUser.details.lastName
                      }`}
                    </span>
                    <Icon name="angle down" size="large" />
                  </div>
                }
                className={styles.profileDropdown}
              >
                <Dropdown.Menu
                  className={classnames('left', styles.profileDropdownMenu)}
                >
                  <div className={styles.organizationName}>
                    {authUser.details.organizationName}
                  </div>
                  <div className={styles.feedbackText}>
                    <div>Questions or comments?</div>
                    <a
                      href={getRouteLink(routes.EXTERNAL_FEEDBACK_URI, {
                        userEmail: authUser.email,
                        firstName: authUser.details.firstName,
                        lastName: authUser.details.lastName,
                        organizationName: authUser.details.organizationName,
                      })}
                    >
                      Contact Us
                    </a>
                  </div>
                  <Link
                    to={getRouteLink(routes.LOGOUT)}
                    href={getRouteLink(routes.LOGOUT)}
                  >
                    <Button
                      primary
                      size="medium"
                      fluid
                      className={styles.logoutButton}
                    >
                      Logout
                    </Button>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          )}
      </div>
    </Grid>
  </div>
);

function mapStateToProps(state): ConnectedProps {
  return {
    isAuthenticated: fromAuth.selectIsUserAuthenticated(state),
    authUser: fromAuth.selectUser(state),
  };
}

export default withRouter(connect(mapStateToProps)(AppBarAdmin));
