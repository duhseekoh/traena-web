// @flow
import React from 'react';
import classnames from 'classnames';

import styles from './TabButtonGroup.scss';

type Tab = {
  name: string,
  value: string,
  handleClick: () => any,
};

type Props = {
  tabs: Tab[],
  selected?: string,
};

const TabButtonGroup = ({ tabs, selected }: Props) => (
  <div className={styles.tabButtonGroup}>
    {tabs.map(tab => (
      <button
        key={tab.name}
        className={
          selected === tab.value
            ? classnames(styles.tabButton, styles.active)
            : styles.tabButton
        }
        onClick={tab.handleClick}
      >
        {tab.name}
      </button>
    ))}
  </div>
);

TabButtonGroup.defaultProps = {
  selected: '',
};

export default TabButtonGroup;
