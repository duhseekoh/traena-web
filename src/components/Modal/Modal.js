import React from 'react';
import styles from './Modal.scss';

type Props = {
  children: React.DOM.Node,
  opacity: ?number,
  backgroundColor: ?string,
};

const Modal = ({ children, opacity, backgroundColor }: Props) => (
  <div>
    <div
      className={styles.fullScreenModalBackground}
      style={{ opacity, backgroundColor }}
    />
    <div className={styles.fullScreenModal}>
      {children}
    </div>
  </div>
);

Modal.defaultProps = {
  opacity: 1,
  backgroundColor: 'white',
};

export default Modal;
