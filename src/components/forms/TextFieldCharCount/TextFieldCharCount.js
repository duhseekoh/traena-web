import React, { Component } from 'react';
import { TextAreaField } from '@panderalabs/redux-form-semantic';
import styles from './TextFieldCharCount.scss';

type Props = {
  maxLength: number,
  initialValue: string,
};

type State = {
  chars: number,
};

export default class TextFieldCharCount extends Component<Props, State> {
  static defaultProps = {
    initialValue: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      chars: this.props.initialValue.length,
    };
  }

  render() {
    return (
      <div className={styles.textFieldCharCountWrapper}>
        <TextAreaField
          {...this.props}
          maxLength={this.props.maxLength}
          onChange={vals => {
            const valsLength = Object.keys(vals).length - 1;
            this.setState({
              chars: valsLength,
            });
          }}
        />
        <span className={styles.charCount}>
          {this.props.maxLength - this.state.chars}
        </span>
      </div>
    );
  }
}
