// @flow
import React from 'react';
import { Field } from 'redux-form/immutable';
import { Checkbox as CheckboxUI } from 'semantic-ui-react';
import type { FieldProps } from 'redux-form';

// TODO - rename to CheckboxField

const Checkbox = ({
  input: { value, onChange, ...input },
  meta: { touched, error },
  ...rest
}: FieldProps) => (
  <span>
    <CheckboxUI
      {...input}
      {...rest}
      defaultChecked={!!value}
      onBlur={e => {
        e.preventDefault(); /* https://github.com/erikras/redux-form/issues/2768#issuecomment-311108309 */
      }}
      onChange={(e, data) => onChange(data.checked)}
      type="checkbox"
    />
    {touched && error && <span>{error}</span>}
  </span>
);

Checkbox.defaultProps = {
  input: null,
  meta: null,
};

export default (props: *) => <Field {...props} component={Checkbox} />;
