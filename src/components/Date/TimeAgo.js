// @flow
import React from 'react';

import moment from 'moment';

type Props = {
  date: moment | string | Date,
};

const TimeAgo = ({ date }: Props) => (
  <span>{moment(date).fromNow()}</span>
);

export default TimeAgo;
