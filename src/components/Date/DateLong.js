// @flow
import React from 'react';

import moment from 'moment';

type Props = {
  date: moment | string | Date,
};

const DateLong = ({ date }: Props) => (
  <span>{moment(date).format('MMM DD, YYYY')}</span>
);

export default DateLong;
