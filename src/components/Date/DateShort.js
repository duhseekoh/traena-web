// @flow
import React from 'react';
import moment from 'moment';
import styles from './DateShort.scss';

type Props = {
  date: moment | string | Date,
};

const DateShort = ({ date }: Props) => (
  <span className={styles.text}>{moment(date).format('MM-DD-YYYY')}</span>
);

export default DateShort;
