// @flow
import React from 'react';
import moment from 'moment';

type Props = {
  date: moment | string | Date,
};

/**
 * Given a date display only the hours and minutes.
 * e.g. '06:23 pm'
 */
const TimeShort = ({ date }: Props) => (
  <span>{moment(date).format('hh:mm a')}</span>
);

export default TimeShort;
