// @flow
import React from 'react';
import styles from './ProgressBar.scss';

type Props = {
  percent: number,
};

/**
 * 0-100 progress bar that displays at the top of the browser window.
 * If there is some progress, it is displayed, otherwise, its not there.
 */
const ProgressBar = (props: Props) => {
  if (!props.percent || props.percent === 0) {
    return null;
  }

  return (
    <div className={styles.progressBar}>
      <div
        className={styles.progressInnerBar}
        style={{ width: `${props.percent}%` }}
      />
    </div>
  );
};

export default ProgressBar;
