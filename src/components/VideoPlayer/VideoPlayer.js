// @flow
import React, { Component } from 'react';
import { Player, BigPlayButton } from 'video-react';
import 'video-react/dist/video-react.css';
import Hls from 'hls.js';
import styles from './VideoPlayer.scss';
import './VideoPlayerOverrides.css';
import type { Video } from '../../types/content';

type HLSProps = {
  src: string,
  video?: HTMLVideoElement,
  autoplay: boolean,
};

class HLSSource extends Component<HLSProps> {
  hls: Hls;

  constructor(props: HLSProps) {
    super(props);
    this.hls = new Hls({
      // Instead of choosing the first available stream quality level (lowest),
      // have the player decide beforehand based on bandwidth!
      // https://video-dev.github.io/hls.js/docs/html/class/src/hls.js~Hls.html#instance-set-startLevel
      startLevel: -1,
    });
  }

  componentDidMount() {
    const { src, video, autoplay } = this.props;
    // load hls video source base on hls.js
    if (Hls.isSupported()) {
      this.hls.loadSource(src);
      this.hls.attachMedia(video);
      this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
        if (video && autoplay) {
          video.play();
        }
      });
    }
  }

  render() {
    return <source src={this.props.src} type="application/x-mpegURL" />;
  }
}

type VideoPlayerProps = {
  video: Video,
  autoplay: boolean,
};

// eslint-disable-next-line
class VideoPlayer extends Component<VideoPlayerProps> {
  props: VideoPlayerProps;

  static defaultProps = {
    autoplay: false,
  };

  render() {
    const { videoURI } = this.props.video;
    return (
      <div className={styles.videoPlayer}>
        <Player fluid={false}>
          <HLSSource
            isVideoChild
            src={videoURI}
            autoplay={this.props.autoplay}
          />
          <BigPlayButton position="center" />
        </Player>
      </div>
    );
  }
}

export default VideoPlayer;
