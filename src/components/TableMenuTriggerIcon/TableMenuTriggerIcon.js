import React from 'react';
import { Icon } from 'semantic-ui-react';
import styles from './TableMenuTriggerIcon.scss';

type Props = any; // semantic doesn't supply any types

/**
 * We have several tables with an ellipsis on the right most column. This provides
 * a uniform implementation of that icon.
 */
const TableMenuTriggerIcon = (props: Props) => (
  <Icon className={styles.icon} name="ellipsis vertical" size="large" link {...props} />
);

export default TableMenuTriggerIcon;
