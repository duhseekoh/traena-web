// @flow
import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as fromApplication from '../../redux/modules/application/selectors';
import { clearNotifications } from '../../redux/modules/application/actions';
import styles from './Notifications.scss';

type Props = {
  title: string,
  message: string,
  onConfirm: () => any,
  onDismiss: () => any,
  dismissText: string,
  confirmText: string,
  clearNotifications: () => Promise<any>,
};

const Notifications = (props: Props) => {
  const {
    title,
    message,
    onConfirm,
    onDismiss,
    dismissText,
    confirmText,
  } = props;

  return (
    <Modal
      open={!!message}
      basic
      size="small"
      dimmer="inverted"
    >
      <Modal.Header className={styles.notificationHeader}>
        {title}
      </Modal.Header>
      <Modal.Content image className={styles.notificationContent}>
        {message}
      </Modal.Content>
      <Modal.Actions className={styles.notificationActions}>
        <Button
          primary={!onConfirm}
          size="big"
          onClick={() => {
            props.clearNotifications();
            onDismiss();
          }}
        >
          {dismissText}
        </Button>
        {onConfirm && (
          <Button
            primary
            size="big"
            onClick={() => {
              props.clearNotifications();
              onConfirm();
            }}
          >
            {confirmText}
          </Button>
        )}
      </Modal.Actions>
    </Modal>
  );
};

Notifications.defaultProps = {
  onDismiss: () => {},
  dismissText: 'Dismiss',
  confirmText: 'Confirm',
};

function mapStateToProps(state) {
  return {
    title: fromApplication.selectNotificationTitle(state),
    message: fromApplication.selectNotificationMessage(state),
    dismissText: fromApplication.selectNotificationDismissText(state),
    confirmText: fromApplication.selectNotificationConfirmText(state),
    onConfirm: fromApplication.selectNotificationOnConfirm(state),
    onDismiss: fromApplication.selectNotificationOnDismiss(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    clearNotifications: () => dispatch(clearNotifications()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
