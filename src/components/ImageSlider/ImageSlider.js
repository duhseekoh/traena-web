// @flow
import React, { Component } from 'react';
import Slider from 'react-slick';
import styles from './ImageSlider.scss';
import type { ImageEntry } from '../../types/content';

type ImageSliderProps = {
  images: ImageEntry[],
};

// eslint-disable-next-line
class ImageSlider extends Component<ImageSliderProps> {
  props: ImageSliderProps;

  static defaultProps = {
    images: [],
  };

  render() {
    const { images } = this.props;
    const hasMultiImage = images.length > 1;
    const settings = {
      dots: hasMultiImage,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      arrows: hasMultiImage,
    };
    return (
      <Slider {...settings}>
        {images.map((image) => (
          <div
            className={styles.contentImage}
            key={image.baseCDNPath}
            style={{
              backgroundImage: `url(${image.baseCDNPath}${
                image.variations.original.filename
              })`,
            }}
          />
        ))}
      </Slider>
    );
  }
}

export default ImageSlider;
