// @flow
import React from 'react';
import { connect } from 'react-redux';
import * as fromAuth from '../../redux/modules/auth/selectors';
import styles from './AccessControl.scss';
import type { AuthUser } from '../../types/users';

type OwnProps = {
  customAccessData?: any,
  allowedPermissions: string[],
  noAccessView: React.DOM.Node,
  children?: React.DOM.Node,
  accessCheck?: (customAccessData: any, user: AuthUser) => boolean,
};

type ConnectedProps = {
  permissions: string[],
  authUser: ?AuthUser,
};

type Props = OwnProps & ConnectedProps;

const AccessControl = ({
  permissions,
  allowedPermissions,
  children,
  noAccessView,
  accessCheck,
  authUser,
  customAccessData,
}: Props) => {
  let permitted = false;
  if (accessCheck && authUser) {
    permitted = accessCheck(customAccessData, authUser);
  } else {
    permitted = permissions.some(permission =>
      allowedPermissions.includes(permission),
    );
  }

  if (permitted && children) {
    if (children.length > 1) {
      return <span>{children}</span>;
    }
    return React.Children.only(children);
  }
  return noAccessView;
};

AccessControl.defaultProps = {
  noAccessView: (
    <div className={styles.pageMessage}>You cannot view this content</div>
  ),
  allowedPermissions: [],
};

const mapStateToProps = (state): ConnectedProps => ({
  permissions: fromAuth.selectUserPermissions(state),
  authUser: fromAuth.selectUser(state),
});

// getting a strange flow error if i dont include an empty mapDispatchToProps
export default connect(mapStateToProps, {})(AccessControl);
