// @flow
import React from 'react';
import classnames from 'classnames';
import { Icon } from 'semantic-ui-react';
import styles from './UpDownButtons.scss';

type Props = {
  move: (from: number, to: number) => void,
  index: number,
  totalItems: number,
};

const UpDownButtons = ({ move, index, totalItems }: Props) => (
  <div className={styles.upDownButtons}>
    <button
      className={
        index !== 0
          ? styles.upDownButton
          : classnames(styles.upDownButton, styles.disabledUpDownButton)
      }
      onClick={e => {
        e.preventDefault();
        e.stopPropagation();
        move(index, index - 1);
      }}
    >
      <Icon name="caret up" size="large" />
    </button>
    <button
      className={
        index !== totalItems - 1
          ? styles.upDownButton
          : classnames(styles.upDownButton, styles.disabledUpDownButton)
      }
      onClick={e => {
        e.preventDefault();
        e.stopPropagation();
        move(index, index + 1);
      }}
    >
      <Icon name="caret down" size="large" />
    </button>
  </div>
);

export default UpDownButtons;
