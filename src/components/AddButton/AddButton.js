// @flow
import React from 'react';
import { Icon } from 'semantic-ui-react';
import styles from './AddButton.scss';

type Props = {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => any,
  children: React.DOM.Node,
};

const AddButton = ({ onClick, children }: Props) => (
  <button className={styles.addButton} onClick={onClick}>
    <Icon className={styles.addButtonIcon} name="plus circle" size="large" />{' '}
    {children}
  </button>
);

export default AddButton;
