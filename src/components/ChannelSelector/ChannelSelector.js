// @flow
import React from 'react';
import { SelectField } from '@panderalabs/redux-form-semantic';
import type { Option } from '../../types';

type Props = {
  options: Option[],
  disabled?: boolean,
};

const ChannelSelector = (props: Props) => (
  <SelectField
    fluid
    options={props.options}
    name="channelId"
    placeholder="Select a channel"
    disabled={props.disabled}
  />
);

ChannelSelector.defaultProps = {
  disabled: false,
};

export default ChannelSelector;
