// @flow
import React from 'react';
import classnames from 'classnames';
import { Icon } from 'semantic-ui-react';
import styles from './TagsList.scss';

type Props = {
  limit?: number,
  light?: boolean,
  tags?: string[],
};

const TagsList = ({ tags, limit, light }: Props) => (
  <div
    className={light ? classnames(styles.tags, styles.tagsLight) : styles.tags}
  >
    {tags &&
      tags
        .map(value => (
          <span key={value} className={styles.tag}>
            #{value}
          </span>
        ))
        .slice(0, limit)}
    {limit !== 1000 && (
      <span className={styles.tag}>
        <Icon name="ellipsis horizontal" />
      </span>
    )}
  </div>
);

TagsList.defaultProps = {
  limit: 1000,
  tags: [],
  light: false,
};

export default TagsList;
