// @flow
import React from 'react';
import { CONTENT_STATUSES, type ContentStatus } from '../../constants/appConstants';

type Props = {
  status: ?ContentStatus,
};

const ContentStatusText = ({ status }: Props) => {
  if (status === CONTENT_STATUSES.PUBLISHED) {
    return <span>Published</span>;
  }

  if (status === CONTENT_STATUSES.DRAFT) {
    return <span>Draft</span>;
  }

  return <span>Unknown</span>;
};

export default ContentStatusText;
