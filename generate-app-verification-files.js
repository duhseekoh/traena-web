// This should be run at build time to generate the
// apple-app-site-association json file and the assetlinks.json.
// these files allows us to specify the paths that are supported
// as universal links into our app.

const fs = require('fs');
require('dotenv').config();

const aassContent = {
  applinks: {
    apps: [],
    details: [
      {
        appID: process.env.TARGET_APP_APPLE,
        paths: ['/content/*/view'],
      },
    ],
  },
};

const assetLinksContent = [{
  relation: ['delegate_permission/common.handle_all_urls'],
  target: {
    namespace: 'android_app',
    package_name: process.env.TARGET_APP_ANDROID,
    sha256_cert_fingerprints:
    [process.env.FINGERPRINT_ANDROID],
  },
}];

try {
  fs.mkdir('build/.well-known', (err) => {
    if (err) throw err;
    // Apple vverification file
    fs.writeFileSync('build/.well-known/apple-app-site-association', JSON.stringify(aassContent, null, 2));

    // Android verification file
    fs.writeFileSync('build/.well-known/assetlinks.json', JSON.stringify(assetLinksContent, null, 2));
  });
} catch (e) {
  throw (e);
  console.log('Cannot write file ', e);
}
