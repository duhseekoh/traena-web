// flow-typed signature: 5401864f775e74ef5b1fab89be99ff14
// flow-typed version: <<STUB>>/@panderalabs/redux-form-semantic_v^0.1.1/flow_v0.64.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@panderalabs/redux-form-semantic'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@panderalabs/redux-form-semantic' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@panderalabs/redux-form-semantic/dist/constants/index' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Checkbox' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/FormField' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/index' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Radio' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Select' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Text' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/TextArea' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/index' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/ReduxFormFieldWrapper' {
  declare module.exports: any;
}

declare module '@panderalabs/redux-form-semantic/dist/types/index' {
  declare module.exports: any;
}

// Filename aliases
declare module '@panderalabs/redux-form-semantic/dist/constants/index.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/constants/index'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Checkbox.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/Checkbox'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/FormField.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/FormField'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/index.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/index'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Radio.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/Radio'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Select.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/Select'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/Text.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/Text'>;
}
declare module '@panderalabs/redux-form-semantic/dist/customSemanticInputs/TextArea.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/customSemanticInputs/TextArea'>;
}
declare module '@panderalabs/redux-form-semantic/dist/index.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/index'>;
}
declare module '@panderalabs/redux-form-semantic/dist/ReduxFormFieldWrapper.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/ReduxFormFieldWrapper'>;
}
declare module '@panderalabs/redux-form-semantic/dist/types/index.js' {
  declare module.exports: $Exports<'@panderalabs/redux-form-semantic/dist/types/index'>;
}
